import React, { useEffect, useState, useRef } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import axios from 'axios'
import moment from 'moment'
import addDays from 'date-fns/addDays'
import { yupResolver } from '@hookform/resolvers/yup'
import { toast } from 'react-toastify'
import { useForm, Controller } from 'react-hook-form'
import { schema } from '../Componets/schema/Schema'
import mainContext from './main'
import { useCallback } from 'react'

export default function StateContext(props) {
  const [auth, setAuth] = useState({})
  const [toggleBtn, setToggle] = useState(false)
  const [step, setstep] = useState(1)
  const [feeTemplate, setFeeTemplate] = useState([])
  const [TakeEntryTest, setIsTakeEntryTest] = useState(false)
  const [startDates, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(null)
  const [dateofbirth, setNewDate] = useState(null)
  const [testdate, setTestdate] = useState(null)
  const [reglst, setReglst] = useState(null)
  const enddate = addDays(startDates, 14)

  // // console.log(dateofbirth, 'update Dated of Birth')

  // const [edit_check_date, set_edit_check_date] = useState(new Date())

  const startDate = moment(startDates).format()
  const endValue = moment(endDate).format()
  const dobValue = moment(dateofbirth).format()
  const testValue = moment(testdate).format()

  // const istestdate = moment(testdate).format()
  // const NewDate = moment(dateofbirth).format()
  // const SelectedStart = moment(startDate).format()
  // const SelectedEnd = moment(endDate).format()

  const [photoSrc, setPhotoSrc] = useState(null)
  const [croppedImage, setCroppedImage] = useState(photoSrc)

  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    getValues,
    trigger,
    reset,
    handleChange,

    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'blur',
  })

  function getUnique(array, key) {
    if (typeof key !== 'function') {
      const property = key
      key = function (item) {
        return item[property]
      }
    }
    return Array.from(
      array
        .reduce(function (map, item) {
          const k = key(item)
          if (!map.has(k)) map.set(k, item)
          return map
        }, new Map())
        .values()
    )
  }
  const feetable = getUnique(feeTemplate, 'feeId')

  /////////////////////////////////////////
  const nextStep = () => {
    setstep(step + 1)
  }

  const prevStep = () => {
    setstep(step - 1)
  }

  const [admissionData, setAdmissionData] = useState({
    regStd: {
      brId: 0,
      regNo: '',
      registrationParentId: 0,
      studentFirstName: '',
      studentLastName: '',
      classId: 0,
      ClassPlan: '',
      Subjects: {
        FirstSemester: [],
        MidSemester: [],
        SecondSemester: [],
        FianlSemester: [],
      },

      regDate: '',
      expiryDate: '',
      studentEmail: '',
      studentNicBform: '',
      studentDob: '',
      entryTestTypeId: 0,
      entryTestDate: '',
      sesionId: 0,
      isTakeEntryTest: false,
      gender: '',
      remarks: '',
      studentPicture: '',
      religionId: 0,
      motherLanguageId: 0,
      createdBy: 0,
      createdDate: '',
      updatedBy: 0,
      updatedDate: '',
      studentNicBform_file: '',
      CNICFront_file: '',
      CNICBack_file: '',
      BirthCertificate_file: '',
      Leaving_certificate_file: '',
    },
    regParent: {
      id: 0,
      name: '',
      email: '',
      cnicNo: '',
      mobileNo: '',
      occupationId: 0,
      relationshipId: 0,
      cityId: 0,
      areaId: 0,
      address: '',
    },
    lstFees: [],
  })

  const [formData, setFormData] = useState({
    regStd: {
      brId: 0,
      regNo: '',
      registrationParentId: 0,
      studentFirstName: '',
      studentLastName: '',
      classId: 0,
      regDate: '',
      expiryDate: '',
      studentEmail: '',
      studentNicBform: '',
      studentDob: '',
      entryTestTypeId: 0,
      entryTestDate: '',
      sesionId: 0,
      isTakeEntryTest: false,
      gender: '',
      remarks: '',
      studentPicture: '',
      religionId: 0,
      motherLanguageId: 0,
      createdBy: 0,
      createdDate: '',
      updatedBy: 0,
      updatedDate: '',
      studentNicBform_file: '',
      CNICFront_file: '',
      CNICBack_file: '',
      BirthCertificate_file: '',
      Leaving_certificate_file: '',
    },
    regParent: {
      id: 0,
      name: '',
      email: '',
      cnicNo: '',
      mobileNo: '',
      occupationId: 0,
      relationshipId: 0,
      cityId: 0,
      areaId: 0,
      address: '',
    },
    lstFees: [],
  })
  ////////////////////////////////////////

  const [ngs, setData] = useState()
  const [regkey, setKey] = useState(0)

  const RegistraionGetApi = async (regkey) => {
    const loginId = 2
    const id = regkey

    try {
      await axios({
        method: 'get',
        url: `${process.env.REACT_APP_NGSBASEURL}Registration/NewRegistration?LoginId=${loginId}&Id=${id}`,
        // url: 'https://mocki.io/v1/7f51aff1-7007-481a-bd47-518a030a1bc8',
      }).then(function (response) {
        setData(response?.data)
        // // console.log(response?.data, 'successfully respond to server')
      })
    } catch (error) {
      if (error === 400) {
        toast.error('Server not Respond Error code 400')
      } else if (error === 500) {
        toast.error('Server not Respond Error code 500')
      } else {
        toast.error('Server not Found')
      }
    }
  }

  useEffect(() => {
    RegistraionGetApi(regkey)
  }, [regkey])
  /////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////

  const regnumber = ngs?.registration?.regStd?.regNo
  const lstCitys = ngs?.registration?.lstCity
  const lstAreas = ngs?.registration?.lstArea
  const lstTestTypes = ngs?.registration?.lstTestType
  const lstSessions = ngs?.registration?.lstSession
  const lstClassPlans = ngs?.registration?.lstClassPlan
  const lstOccupations = ngs?.registration?.lstOccupation
  const lstRelationships = ngs?.registration?.lstRelationship
  const lstMotherLanguages = ngs?.registration?.lstMotherLanguages
  const lstReligions = ngs?.registration?.lstReligions
  const Gender = ngs?.registration?.lstGender
  const lstfeeHeads = ngs?.registration?.lstFeeHeads
  const lstFeeTemplatess = ngs?.registration?.lstFeeTemplates

  // const regnumber = ngs?.regStd?.regNo
  // const lstCitys = ngs?.lstCity
  // const lstAreas = ngs?.lstArea
  // const lstTestTypes = ngs?.lstTestType
  // const lstSessions = ngs?.lstSession
  // const lstClassPlans = ngs?.lstClassPlan
  // const lstOccupations = ngs?.lstOccupation
  // const lstRelationships = ngs?.lstRelationship
  // const lstMotherLanguages = ngs?.lstMotherLanguages
  // const lstReligions = ngs?.lstReligions
  // const Gender = ngs?.lstGender
  // const lstfeeHeads = ngs?.lstFeeHeads
  // const lstFeeTemplatess = ngs?.lstFeeTemplates

  ///////////////////////////////////////////////////////////////////////////////////////////
  const [lstCity, setlstCity] = useState([])
  const old_objVal = lstCitys
  const changeKeyObjects = (old_objVal) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objVal.length; i++) {
      let obj = {
        value: old_objVal[i].id,
        label: old_objVal[i].name,
      }
      res.push(obj)
    }
    setlstCity(res)
  }
  useEffect(() => {
    if (old_objVal) {
      changeKeyObjects(old_objVal)
    }
  }, [old_objVal])

  ////////////////////////////////////////////////////

  const [lstArea, setlstArea] = useState([])
  const old_objValArea = lstAreas
  const changeKeyObjArea = (old_objValArea) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objValArea.length; i++) {
      let obj = {
        value: old_objValArea[i].id,
        label: old_objValArea[i].name,
      }
      res.push(obj)
    }
    setlstArea(res)
  }
  useEffect(() => {
    if (old_objValArea) {
      changeKeyObjArea(old_objValArea)
    }
  }, [old_objValArea])

  ////////////////////////////////////////////////////

  const [lstTestType, setlstTestType] = useState(null)
  const old_objValTest = lstTestTypes
  const changeKeyObjTest = (old_objValTest) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objValTest.length; i++) {
      let obj = {
        value: old_objValTest[i].id,
        label: old_objValTest[i].name,
      }
      res.push(obj)
    }
    setlstTestType(res)
  }
  useEffect(() => {
    if (old_objValTest) {
      changeKeyObjTest(old_objValTest)
    }
  }, [old_objValTest])
  ////////////////////////////////////////////////////

  const [lstClassPlan, setlstClassplan] = useState(null)
  const old_objValClassplan = lstClassPlans
  const changeKeyObjClassplan = (old_objValClassplan) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objValClassplan.length; i++) {
      let obj = {
        value: old_objValClassplan[i].id,
        label: old_objValClassplan[i].name,
      }
      res.push(obj)
    }
    setlstClassplan(res)
  }
  useEffect(() => {
    if (old_objValClassplan) {
      changeKeyObjClassplan(old_objValClassplan)
    }
  }, [old_objValClassplan])
  ////////////////////////////////////////////////////

  const [lstSession, setlstSession] = useState(null)
  const old_objValSession = lstSessions
  const changeKeyObjSession = (old_objValSession) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objValSession.length; i++) {
      let obj = {
        value: old_objValSession[i].id,
        label: old_objValSession[i].name,
      }
      res.push(obj)
    }
    setlstSession(res)
  }
  useEffect(() => {
    if (old_objValSession) {
      changeKeyObjSession(old_objValSession)
    }
  }, [old_objValSession])

  ////////////////////////////////////////////////////

  const [lstOccupation, setlstOccupation] = useState(null)
  const old_objValOccupation = lstOccupations
  const changeKeyObjOccupation = (old_objValOccupation) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objValOccupation.length; i++) {
      let obj = {
        value: old_objValOccupation[i].id,
        label: old_objValOccupation[i].name,
      }
      res.push(obj)
    }
    setlstOccupation(res)
  }
  useEffect(() => {
    if (old_objValOccupation) {
      changeKeyObjOccupation(old_objValOccupation)
    }
  }, [old_objValOccupation])

  ////////////////////////////////////////////////////

  const [lstRelationship, setlstRelationship] = useState(null)
  const old_objValRalationship = lstRelationships
  const changeKeyObjRelation = (old_objValRalationship) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objValRalationship.length; i++) {
      let obj = {
        value: old_objValRalationship[i].id,
        label: old_objValRalationship[i].name,
      }
      res.push(obj)
    }
    setlstRelationship(res)
  }
  useEffect(() => {
    if (old_objValRalationship) {
      changeKeyObjRelation(old_objValRalationship)
    }
  }, [old_objValRalationship])

  ////////////////////////////////////////////////////

  const [lstMotherLanguage, setlstMotherLanguages] = useState(null)
  const old_objVallanguage = lstMotherLanguages
  const changeKeyObjLanguage = (old_objVallanguage) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objVallanguage.length; i++) {
      let obj = {
        value: old_objVallanguage[i].id,
        label: old_objVallanguage[i].name,
      }
      res.push(obj)
    }
    setlstMotherLanguages(res)
  }
  useEffect(() => {
    if (old_objVallanguage) {
      changeKeyObjLanguage(old_objVallanguage)
    }
  }, [old_objVallanguage])

  // useEffect(() => {
  //   const uniqueArr = getUnique(feeTemplate, 'feeId')

  //   for (let itm of uniqueArr) {
  //     lstfeeHead = lstfeeHead.filter((hed) => hed.value != itm.feeId)
  //   }
  //   setlstFeeHeads([...lstfeeHead])
  // }, [feeTemplate])

  ////////////////////////////////////////////////////

  const [lstReligion, setlstReligions] = useState(null)
  const old_objValReligion = lstReligions
  const changeKeyObjReligion = (old_objValReligion) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objValReligion.length; i++) {
      let obj = {
        value: old_objValReligion[i].id,
        label: old_objValReligion[i].name,
      }
      res.push(obj)
    }
    setlstReligions(res)
  }
  useEffect(() => {
    if (old_objValReligion) {
      changeKeyObjReligion(old_objValReligion)
    }
  }, [old_objValReligion])

  ////////////////////////////////////////////////////

  const [lstGender, setChooseGender] = useState([])
  const old_objGender = Gender
  const changeKeyObjGender = (old_objGender) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objGender.length; i++) {
      let obj = {
        value: old_objGender[i].id,
        label: old_objGender[i].name,
      }
      res.push(obj)
    }
    setChooseGender(res)
  }
  useEffect(() => {
    if (old_objGender) {
      changeKeyObjGender(old_objGender)
    }
  }, [old_objGender])

  ////////////////////////////////////////////////////

  const [lstFeeTemplates, setlstFeeTemplates] = useState(null)
  const old_objfeetemplate = lstFeeTemplatess
  const changeKeyObjfeetemplate = (old_objfeetemplate) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objfeetemplate.length; i++) {
      let obj = {
        value: old_objfeetemplate[i].id,
        label: old_objfeetemplate[i].name,
      }
      res.push(obj)
    }
    setlstFeeTemplates(res)
  }
  useEffect(() => {
    if (old_objfeetemplate) {
      changeKeyObjfeetemplate(old_objfeetemplate)
    }
  }, [old_objfeetemplate])

  ////////////////////////////////////////////////////

  const [lstfeeHead, setlstFeeHeads] = useState(null)
  const old_objfeehead = lstfeeHeads
  const changeKeyObjfeehead = (old_objfeehead) => {
    // debugger
    let res = []
    for (let i = 0; i < old_objfeehead.length; i++) {
      let obj = {
        value: old_objfeehead[i].id,
        label: old_objfeehead[i].name,
      }
      res.push(obj)
    }
    setlstFeeHeads(res)
  }
  useEffect(() => {
    if (old_objfeehead) {
      changeKeyObjfeehead(old_objfeehead)
    }
  }, [old_objfeehead])

  ////////////////////////////////////////////////////////////////////////////////////////////

  const totalsum = feetable.reduce((currentSum, nextObject) => {
    return currentSum + +nextObject.amount
  }, null)

  const discountfess = feetable.reduce((currentSum, nextObject) => {
    return currentSum + +nextObject.discount
  }, null)
  // const total = feeTemplate.reduce((currentSum, nextObject) => {
  //   return currentSum + +nextObject.discountfess
  // }, null)
  const total = totalsum - discountfess

  const Statevalues = getValues()
  const Student = ngs?.registration?.regStd
  const Parent = ngs?.registration?.regParent

  const gender = lstGender.filter(
    (o) => o?.label === Student?.gender || o?.label === Student?.gender
  )

  const sesionId = lstSession?.filter(
    (o) => o.value === Student?.sesionId || o.value === Student?.sesionId
  )
  const classId = lstClassPlan?.find((o) => o.value === Student?.classId)
  const entryTestTypeId = lstTestType?.find(
    (o) => o.value === Student?.entryTestTypeId
  )

  const motherLanguageId = lstMotherLanguage?.find(
    (o) => o.value === Student?.motherLanguageId
  )
  const religionId = lstReligion?.find((o) => o.value === Student?.religionId)
  const relationshipId = lstRelationship?.find(
    (o) => o.value === Parent?.relationshipId
  )
  const occupationId = lstOccupation?.find(
    (o) => o.value === Parent?.occupationId
  )
  const cityId = lstCity?.find((o) => o.value === Parent?.cityId)
  const areaId = lstArea?.find((o) => o.value === Parent?.areaId)

  useEffect(() => {
    setValue('regNo', regnumber)
    setValue('regDate', startDates)
    setValue('expiryDate', endDate)
    setValue('isTakeEntryTest', TakeEntryTest)
    setValue('studentDob', dateofbirth)

    // if (Statevalues) {
    //   setValue('gender', Statevalues?.gender?.label)
    // }
    // if (Statevalues) {
    //   setValue('classId', Statevalues?.classId?.value)
    // }
    // if (Statevalues) {
    //   setValue('sesionId', Statevalues?.sesionId?.value)
    // }

    // Object.keys(Student[0].expiryDate).map((key) =>
    //   setValue(key, Student[0].expiryDate[key])
    // )

    if (Student?.id > 0) {
      setValue('studentFirstName', Student?.studentFirstName)
      setValue('studentLastName', Student?.studentLastName)
      setValue('classId', classId)
      setValue('regDate', Student?.regDate)
      setValue('expiryDate', Student?.expiryDate)
      setValue('studentEmail', Student?.studentEmail)
      setValue('studentNicBform', Student?.studentNicBform)
      setValue('studentDob', Student?.studentDob)
      setValue('entryTestTypeId', entryTestTypeId)
      setValue('entryTestDate', Student?.entryTestDate)
      setValue('sesionId', sesionId)
      setValue('isTakeEntryTest', Student?.isTakeEntryTest)
      setValue('gender', gender)
      setValue('remarks', Student?.remarks)
      setValue('studentPicture', Student?.studentPicture)
      setValue('religionId', religionId)
      setValue('motherLanguageId', motherLanguageId)
      setValue('createdBy', Student?.createdBy)
      setValue('createdDate', Student?.createdDate)
      setValue('updatedBy', Student?.updatedBy)
      setValue('updatedDate', new Date())
      setValue('studentNicBform_file', Student?.studentNicBform_file)
      setValue('CNICFront_file', Student?.CNICFront_file)
      setValue('CNICBack_file', Student?.CNICBack_file)
      setValue('BirthCertificate_file', Student?.BirthCertificate_file)
      setValue('Leaving_certificate_file', Student?.Leaving_certificate_file)
      setValue('name', Parent?.name)
      setValue('email', Parent?.email)
      setValue('cnicNo', Parent?.cnicNo)
      setValue('mobileNo', Parent?.mobileNo)
      setValue('occupationId', occupationId)
      setValue('relationshipId', relationshipId)
      setValue('cityId', cityId)
      setValue('areaId', areaId)
      setValue('address', Parent?.address)
    }
  }, [
    ngs,
    regnumber,
    TakeEntryTest,
    testdate,

    startDate,
    endDate,
    Student,
    Parent,
    setValue,

    gender,
    sesionId,
    classId,
    entryTestTypeId,
    occupationId,
    relationshipId,
    cityId,
    areaId,
    motherLanguageId,
    religionId,
  ])

  // useEffect(() => {
  //   if (Student?.id > 0) {
  //     setFormData({
  //       ...formData,
  //       regStd: {
  //         ...formData.regStd,
  //         regDate: startDate,
  //         expiryDate: endValue,
  //         studentDob: dobValue,
  //         entryTestDate: testValue,
  //       },
  //     })
  //   }
  // }, [])

  // useEffect(() => {
  //   if (Student) {
  //     const regDateSet = Student?.regDate
  //     const exDateSet = Student?.expiryDate
  //     const dobDateSet = Student?.studentDob
  //     const testDateSet = Student?.entryTestDate

  //     // setStartDate(regDateSet)
  //     // setEndDate(exDateSet)
  //     // setNewDate(dobDateSet)
  //     // setTestdate(testDateSet)
  //   }
  // }, [Student])

  // // console.log(Student, Parent, 'student details')
  //////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////
  const [heading, setHeading] = useState('')

  const [Profile, setProfile] = useState(null)

  const TriggerButton = useCallback(() => {
    let image = new Image(croppedImage)
    image.croppedImage = croppedImage
    const NGSBLOB = new File(
      [image],
      `student_image_${Math.floor(Math.random() * 100000)}.jpg`,
      {
        type: 'image/jpeg',
        lastModified: new Date().getTime(),
      }
    )
    var formImage = new FormData()
    formImage.append('attachment', NGSBLOB)
    axios
      .post(
        `${process.env.REACT_APP_NGSBASEURL}FileUpload/SaveFile`,
        formImage,
        {
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
        }
      )
      .then((response) => {
        setFormData({
          ...formData,
          regStd: {
            ...formData.regStd,
            studentPicture: response.data.filePath,
          },
        })

        setValue('studentPicture', response.data.filePath)
        if (response.data.request === 200) {
          toast.success(`Server send Responce 200 successfully`)
        }

        // if (response.status === 200) {
        //   setCaptureImage(false)
        // }
      })
      .catch((error) => {
        if (error.request === 404) {
          toast.error(`Server not Found 404`)
        }
      })
  }, [croppedImage, formData, setValue])
  function Submitform(e) {
    e.preventDefault()
    TriggerButton()
    if (Statevalues.studentPicture !== '') {
      formData.lstFees = feetable
      // toast.success(`Server successfully add the image`)
      axios({
        url: `${process.env.REACT_APP_NGSBASEURL}Registration/SaveRegistration`,
        method: 'post',
        data: formData,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf8',
        },
      })
        .then(function (response) {
          if (response.status === 200) {
            toast.success(`Your Registration Form has been submitted`)
            nextStep()
          }
        })
        .catch(function (error) {
          // SetEror(error)
          if (error.response.status === 400) {
            toast.error(
              `Server not Respond Error code ${error.response.status}`
            )
          } else if (error.response.status === 500) {
            toast.error(
              `Server not Respond Error code ${error.response.status}`
            )
          } else {
            toast.error(`Server not Found ${error.response.status}`)
          }
        })
    } else {
      toast.error(`Profile image not add Server Error`)
    }
  }

  // const customStyles = {
  //   menuPortal: (base) => ({
  //     ...base,

  //     zIndex: 99999,
  //     '&:hover': {
  //       backgroundColor: 'red',
  //       color: 'white',
  //     },
  //   }),
  //   menu: (provided) => ({
  //     ...provided,
  //     zIndex: '9999 !important',
  //     backgroundColor: '#fff',
  //     color: '#333',
  //     fontSize: '14px',
  //     zIndex: 9999,
  //   }),

  //   control: (provided) => ({
  //     ...provided,
  //     margin: 0,
  //     border: '0px solid black',
  //     backgroundColor: 'white',
  //     outline: 'none',
  //     minHeight: '36px',
  //     fontSize: '14px',
  //   }),
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////////

  const [files, setFile] = useState({
    First: [],
    two: [],
    three: [],
    four: [],
    five: [],
  })
  const [show, setShow] = useState({
    Onefile: {
      file: '',
      document: '',
    },
    Twofile: {
      file: '',
      document: '',
    },
    Threefile: {
      file: '',
      document: '',
    },
    Forfile: {
      file: '',
      document: '',
    },
    Fivefile: {
      file: '',
      document: '',
    },
  })
  const [fileResult, setResult] = useState({
    resultone: '',
    resulttwo: '',
    resultthree: '',
    resultfor: '',
    resultfive: '',
  })
  const [onSucces, setSuccess] = useState({
    onSuccess1: '',
    onSuccess2: '',
    onSuccess3: '',
    onSuccess4: '',
    onSuccess5: '',
  })

  ///////////////////////////////////////////////////////////////////////////////////////////////////

  const refReactSelect = useRef()

  useEffect(() => {
    setValue(
      'areaId',
      refReactSelect.current?.props?.inputRef?.current?.props?.value
    )
  }, [refReactSelect])
  //////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////////

  const Ref = useRef(null)

  const navigate = useNavigate()
  const location = useLocation()
  const from = location.state?.from?.pathname || '/'

  const [user, setUser] = useState('')
  const [pwd, setPwd] = useState('')
  const [errMsg, setErrMsg] = useState('')
  const [accessToken, setaccessToken] = useState('')
  // The state for our timer
  const [timer, setTimer] = useState('00:00:20')
  const [ClockWorning, setClockWorning] = useState(false)

  const getTimeRemaining = useCallback((e) => {
    const total = Date.parse(e) - Date.parse(new Date())
    const seconds = Math.floor((total / 1000) % 60)
    const minutes = Math.floor((total / 1000 / 60) % 60)
    const hours = Math.floor((total / 1000 / 60 / 60) % 24)
    return {
      total,
      hours,
      minutes,
      seconds,
    }
  }, [])

  const startTimer = useCallback((e) => {
    let { total, hours, minutes, seconds } = getTimeRemaining(e)
    if (total >= 0) {
      setTimer(
        (hours > 9 ? hours : '0' + hours) +
          ':' +
          (minutes > 9 ? minutes : '0' + minutes) +
          ':' +
          (seconds > 9 ? seconds : '0' + seconds)
      )
    }
  }, [])

  const clearTimer = useCallback((e) => {
    setTimer('00:00:20')
    if (Ref.current) clearInterval(Ref.current)
    const id = setInterval(() => {
      startTimer(e)
    }, 1000)
    Ref.current = id
  }, [])

  const getDeadTime = useCallback(() => {
    let deadline = new Date()
    // // console.log(deadline, 'current')
    // deadline.setHours(deadline.getHours() + 1)
    // deadline.setMinutes(deadline.getMinutes() + 1)
    deadline.setSeconds(deadline.getSeconds() + 20)
    return deadline
  }, [])

  useEffect(() => {
    clearTimer(getDeadTime())
  }, [])
  const onClickReset = useCallback(() => {
    clearTimer(getDeadTime())
  }, [])
  let timeout = 0
  useEffect(() => {
    const displayOpa = (e) => {
      onClickReset()
      timeout = setTimeout(() => {
        if (!e) {
          onClickReset()
        }
      }, 1000)
    }
    document.body.addEventListener('mousemove', displayOpa)
  }, [timeout])

  const chcke = useCallback(() => {
    if (timer === '00:00:00') {
      navigate('/lockscreen', { replace: true })
    } else if (timer <= '00:00:10') {
      setClockWorning(true)
    } else {
      setClockWorning(false)
    }
  }, [timer])

  useEffect(() => {
    chcke()
  }, [timer])
  ////////////////////////////////////////////////
  ///FeeCollection//
  const customStyles = {
    menuPortal: (base) => ({
      ...base,

      zIndex: 9999,
      '&:hover': {
        backgroundColor: 'red',
        color: 'white',
      },
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: '9999 !important',
      backgroundColor: '#fff',
      color: '#333',
      fontSize: '14px',
    }),

    control: (provided) => ({
      ...provided,
      margin: 0,
      border: '0px solid black',
      backgroundColor: 'white',
      outline: 'none',
      minHeight: '36px',
      fontSize: '14px',
    }),
  }
  const [options, setOptions] = useState({
    value: { label: 'Select', value: 0 },
  })

  const listofoptions = [
    { label: 'Cash', value: 1 },
    { label: 'Bank', value: 2 },
    { label: 'Online Transfer', value: 3 },
  ]

  function First(value) {
    setOptions({ value })
  }

  // // console.log(options.value, 'update velues')

  const [inputList, setInputList] = useState([
    {
      Amout: '',
      lateFee: '',
      paymentMethod: '',
      PaymentDate: '',
      ReceviedBy: '',
      Remarks: '',
      CurrentDate: '',
      BankName: '',
      ChequeNo: '',
      ChequeDate: '',
      ChequeAmount: '',
      chequeStatus: '',
      RecevingDate: '',
      Bank: '',
      TransitionID: '',
    },
  ])

  const FelidsInput = (e, index) => {
    const { name, value } = e.target
    const list = [...inputList]
    list[index][name] = value
    setInputList(list)
  }

  const handleRoomChange = (option, index, name) => {
    const value = option.value
    const list = [...inputList]
    list[index][name] = value
    setInputList(list)
  }

  const DateChange = (date, index, name) => {
    const value = date
    const list = [...inputList]
    list[index][name] = value
    setInputList(list)
  }

  const HandleRemove = (index) => {
    const list = [...inputList]
    list.splice(index, 1)
    setInputList(list)
  }
  const AddMore = () => {
    setInputList([
      ...inputList,
      {
        Amout: '',
        lateFee: '',
        PaymentDate: '',
        ReceviedBy: '',
        paymentMethod: '',
        Remarks: '',
        CurrentDate: '',
        BankName: '',
        ChequeNo: '',
        ChequeDate: '',
        ChequeAmount: '',
        InQue: '',
        Cleared: '',
        Discounted: '',
        RecevingDate: '',
        Bank: '',
        TransitionID: '',
      },
    ])
  }
  const Chequestatus = [
    {
      label: 'InQue',
      value: 'InQue',
    },
    {
      label: 'Cleared',
      value: 'Cleared',
    },
    {
      label: 'Disowned',
      value: 'Disowned',
    },
  ]

  ///////////////////////////////////////////////
  return (
    <mainContext.Provider
      value={{
        timer,
        ClockWorning,
        ///////////////////////
        from,
        user,
        navigate,
        setUser,
        pwd,
        setPwd,
        errMsg,
        accessToken,
        setaccessToken,
        //////////////////////
        step,
        setstep,
        register,
        handleSubmit,
        watch,
        control,
        setValue,
        Statevalues,
        trigger,
        errors,
        Controller,
        ngs,
        formData,
        setFormData,
        feeTemplate,
        setFeeTemplate,
        feetable,

        Submitform,
        reset,
        nextStep,
        prevStep,
        ///////////////
        lstCity,
        lstOccupation,
        lstRelationship,
        lstSession,
        lstTestType,
        regnumber,
        lstMotherLanguage,
        lstReligion,
        TakeEntryTest,
        lstFeeTemplates,
        lstClassPlan,
        lstArea,
        // SelectedStart,
        // SelectedEnd,
        dateofbirth,
        setNewDate,
        total,
        lstGender,
        setIsTakeEntryTest,
        startDates,
        setStartDate,
        endDate,
        setEndDate,
        enddate,
        lstfeeHead,
        discountfess,
        totalsum,
        photoSrc,
        croppedImage,
        setCroppedImage,
        setPhotoSrc,
        setTestdate,
        testdate,

        regkey,
        setKey,
        /////////////////
        files,
        setFile,
        show,
        setShow,
        fileResult,
        setResult,
        onSucces,
        setSuccess,
        handleChange,
        refReactSelect,
        heading,
        setHeading,
        reglst,
        setReglst,
        toggleBtn,
        setToggle,
        setlstFeeHeads,
        TriggerButton,

        Profile,
        setProfile,

        gender,
        sesionId,
        classId,
        entryTestTypeId,
        occupationId,
        relationshipId,
        cityId,
        areaId,
        motherLanguageId,
        religionId,
        admissionData,
        setAdmissionData,
        customStyles,

        ////////////////////////

        inputList,
        listofoptions,
        HandleRemove,
        FelidsInput,
        customStyles,
        handleRoomChange,
        AddMore,
        errors,
        Chequestatus,
        DateChange,
        /////
        auth,
        setAuth,
      }}
    >
      {props.children}
    </mainContext.Provider>
  )
}
