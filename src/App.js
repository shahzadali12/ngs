import React, { useState } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

import Home from './Pages/Home'
import NoMatch from './Componets/Tabs_content/nomatch'
import CombineElement from './layouts/CombineElement'
import Statistics from './Componets/widgets/Statistics'
import Filemaneger from './Componets/widgets/Filemaneger'
import RegistraionList from './Pages/Registrationlist'
import AdmissionList from './Pages/AdmissionList'
import Modal from './Componets/widgets/model'
// import StudentsList from './Pages/StudentsList'
import Charts from './Componets/widgets/charts'
import NewRegistraion from './Pages/NewRegistration/registration'
import NewAdmission from './Pages/NewAddmission/Admission'
import Testing from './Pages/Testing'
import Login from './Pages/Login/Login'
import Attendace from './Pages/Attendace'
import Leaves from './Pages/leaves'
import Profile from './Pages/Profile'
import ParentInfo from './Pages/ParentInfo'
import SystemMonitoring from './Pages/SystemMonitoring'
import Layout from './layouts/Layout'
import RequireAuth from './Auth/RequireAuth'
import Create from './Pages/CreateUsers/Create'
import '/node_modules/fontawesome-4.7/css/font-awesome.min.css'
import './styles/App.css'
import './styles/colors.css'
import LockScreen from './Pages/Login/lockScreen'
import { Departments } from './Pages/BaseSettings/Departments'
import { Designations } from './Pages/BaseSettings/Designations'
import { ClassPlan } from './Pages/ClassPlan/ClassPlan'
import { Terms } from './Pages/BaseSettings/Terms'
import { Class } from './Pages/BaseSettings/Class'
import { Subjects } from './Pages/BaseSettings/Subject'
import { Section } from './Pages/BaseSettings/Section'
import { Shift } from './Pages/BaseSettings/Shift'
import { Expence } from './Pages/BaseSettings/Expence'
import { Degree } from './Pages/BaseSettings/Degree'
import { Conduct } from './Pages/BaseSettings/Conduct'
import { AssessmentTerm } from './Pages/BaseSettings/AssessmentTerm'
import { Cities } from './Pages/BaseSettings/Basic/City'
import { Areas } from './Pages/BaseSettings/Basic/Area'
import { Countrys } from './Pages/BaseSettings/Basic/Country'
import Custodain from './Pages/BaseSettings/Basic/Custodain'
import EmployeeType from './Pages/BaseSettings/Basic/EmployeeType'
import Language from './Pages/BaseSettings/Basic/Language'
import Relationship from './Pages/BaseSettings/Basic/Relationships'
import Religion from './Pages/BaseSettings/Basic/Religion'
import Transport from './Pages/BaseSettings/Basic/Transport'
import Grade from './Pages/BaseSettings/Exam/Grade'
import Fee from './Pages/BaseSettings/Fee/Fee'
import Document from './Pages/BaseSettings/Basic/DocumentName'
import FeeGeneration from './Pages/FeeManagement/FeeGeneration'
import DirectorHome from './Pages/DirectorHome'
import TeacherHome from './Pages/TeacherHome'
import ParentHome from './Pages/ParentHome'
import BirthdayIndex from './Pages/Birthday'
import FeeCollection from './Pages/FeeManagement/FeeCollection'
import StudentIDGen from './Pages/FeeManagement/StudentID'
import Cards from './Pages/FeeManagement/Cards'
import Voucher from './Pages/NewAddmission/StepLast'
import FeeVoucher from './Pages/FeeManagement/FeeVoucher'
function App() {
  const [loading, setLoading] = useState(true)
  const Loader = document.getElementById('loader')
  if (Loader) {
    setTimeout(() => {
      Loader.style.display = 'none'
      setLoading(false)
    }, 2000)
  }
  const ROLES = {
    Admin: 5150,
    Director: 1001,
    Teacher: 1002,
    Parent: 2022,
  }

  return (
    !loading && (
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/lockscreen" element={<LockScreen />} />
          <Route path="login" element={<Login />} />
          <Route element={<RequireAuth allowedRoles={[ROLES.Admin]} />}>
            <Route
              path="/"
              element={
                <CombineElement>
                  <Home />
                </CombineElement>
              }
            />
          </Route>
          <Route element={<RequireAuth allowedRoles={[ROLES.Director]} />}>
            <Route
              path="/Home"
              element={
                <CombineElement>
                  <DirectorHome />
                </CombineElement>
              }
            />
            <Route
              path="/testing"
              element={
                <CombineElement>
                  <Testing />
                </CombineElement>
              }
            />
            <Route
              path="/birthday"
              element={
                <CombineElement>
                  <BirthdayIndex />
                </CombineElement>
              }
            />
          </Route>

          <Route element={<RequireAuth allowedRoles={[ROLES.Teacher]} />}>
            <Route
              path="/THome"
              element={
                <CombineElement>
                  <TeacherHome />
                </CombineElement>
              }
            />
          </Route>
          <Route element={<RequireAuth allowedRoles={[ROLES.Parent]} />}>
            <Route
              path="/PHome"
              element={
                <CombineElement>
                  <ParentHome />
                </CombineElement>
              }
            />
          </Route>
          <Route element={<RequireAuth allowedRoles={[ROLES.Director]} />}>
            <Route
              path="/Registrationlist"
              element={
                <CombineElement>
                  <RegistraionList />
                </CombineElement>
              }
            />
            {/* https://codesandbox.io/s/wizardly-brown-0ltp6?file=/src/main.js */}
            <Route
              path="/FeeVoucher"
              element={
                <CombineElement>
                  <FeeVoucher />
                </CombineElement>
              }
            />
            <Route
              path="/Voucher"
              element={
                <CombineElement>
                  <Voucher />
                </CombineElement>
              }
            />
            <Route
              path="/FeeCollection"
              element={
                <CombineElement>
                  <FeeCollection />
                </CombineElement>
              }
            />
            <Route
              path="/StudentIDGen"
              element={
                <CombineElement>
                  <StudentIDGen />
                </CombineElement>
              }
            />
            <Route
              path="/cards"
              element={
                <CombineElement>
                  <Cards />
                </CombineElement>
              }
            />
            <Route
              path="/resource"
              element={
                <CombineElement>
                  <Filemaneger />
                </CombineElement>
              }
            />
            <Route
              path="/Attendance/students"
              element={
                <CombineElement>
                  <Attendace
                    title={'Student'}
                    name={'Asif Ali'}
                    image={
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZcD8kdS6_MMAQh81G8xascgBlhBv2rrBJytXqK1-8KsfRrMS_2urdb3RIj9Cy9Y283Kg&usqp=CAU'
                    }
                  />
                </CombineElement>
              }
            />
            <Route
              path="/Attendance/staff"
              element={
                <CombineElement>
                  <Attendace
                    title={'Staff'}
                    name={'bill gates'}
                    image={
                      'https://static.wikia.nocookie.net/fact-and-fiction/images/3/39/Bill_Gates.jpeg'
                    }
                  />
                </CombineElement>
              }
            />
            <Route
              path="/Payroll-Management/leaves"
              element={
                <CombineElement>
                  <Leaves />
                </CombineElement>
              }
            />
            <Route
              path="/newAdmission"
              element={
                <CombineElement>
                  <NewAdmission />
                </CombineElement>
              }
            />
            <Route
              path="/profile"
              element={
                <CombineElement>
                  <Profile />
                </CombineElement>
              }
            />
            <Route
              path="/system-monitoring"
              element={
                <CombineElement>
                  <SystemMonitoring />
                </CombineElement>
              }
            />
            <Route
              path="/ParentInfo"
              element={
                <CombineElement>
                  <ParentInfo />
                </CombineElement>
              }
            />
            <Route
              path="/AdmissionList"
              element={
                <CombineElement>
                  <AdmissionList />
                </CombineElement>
              }
            />

            {/* <Route
              path="/charts"
              element={
                <CombineElement>
                  <Charts />
                </CombineElement>
              }
            /> */}
            {/* <Route path="/model" element={<Modal />} /> */}

            <Route
              path="/NewRegistration"
              element={
                <CombineElement>
                  <NewRegistraion />
                </CombineElement>
              }
            />

            {/* <Route
            path="/student"
            element={
              <CombineElement>
                <StudentsList />
              </CombineElement>
            }
          /> */}
            <Route
              path="/create"
              element={
                <CombineElement>
                  <Create />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/department"
              element={
                <CombineElement>
                  <Departments />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/designation"
              element={
                <CombineElement>
                  <Designations />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Classplan"
              element={
                <CombineElement>
                  <ClassPlan />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Term"
              element={
                <CombineElement>
                  <Terms />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Class"
              element={
                <CombineElement>
                  <Class />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Subjects"
              element={
                <CombineElement>
                  <Subjects />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Section"
              element={
                <CombineElement>
                  <Section />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Shift"
              element={
                <CombineElement>
                  <Shift />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Expence"
              element={
                <CombineElement>
                  <Expence />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Degree"
              element={
                <CombineElement>
                  <Degree />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Conduct"
              element={
                <CombineElement>
                  <Conduct />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/AssessmentTerm"
              element={
                <CombineElement>
                  <AssessmentTerm />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/City"
              element={
                <CombineElement>
                  <Cities />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Area"
              element={
                <CombineElement>
                  <Areas />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Country"
              element={
                <CombineElement>
                  <Countrys />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Custodain"
              element={
                <CombineElement>
                  <Custodain />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Documents"
              element={
                <CombineElement>
                  <Document />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/EmployeeType"
              element={
                <CombineElement>
                  <EmployeeType />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Language"
              element={
                <CombineElement>
                  <Language />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Relationship"
              element={
                <CombineElement>
                  <Relationship />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Religion"
              element={
                <CombineElement>
                  <Religion />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Transport"
              element={
                <CombineElement>
                  <Transport />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Definitions/Grade"
              element={
                <CombineElement>
                  <Grade />
                </CombineElement>
              }
            />
            <Route
              path="/Settings/Fee/Fee"
              element={
                <CombineElement>
                  <Fee />
                </CombineElement>
              }
            />
            <Route
              path="/FeeGeneration"
              element={
                <CombineElement>
                  <FeeGeneration />
                </CombineElement>
              }
            />
          </Route>

          <Route path="*" element={<NoMatch />} />
        </Route>
      </Routes>
    )
  )
}

export default App

/* eslint-disable react/jsx-no-undef */
// import React, {
//   Fragment,
//   Component,
//   Suspense,
//   useEffect,
//   useState,
// } from "react";
// import { BrowserRouter, HashRouter, Link, MemoryRouter, NavLink, Navigate, NavigationType, Outlet, Route, Router, Routes, UNSAFE_LocationContext, UNSAFE_NavigationContext, UNSAFE_RouteContext, createPath, createRoutesFromChildren, createSearchParams, generatePath, matchPath, matchRoutes, parsePath, renderMatches, resolvePath, unstable_HistoryRouter, useHref, useInRouterContext, useLinkClickHandler, useLocation, useMatch, useNavigate, useNavigationType, useOutlet, useOutletContext, useParams, useResolvedPath, useRoutes, useSearchParams} from "react-router-dom";
// import Routesr from "./routes";
// import { useDispatch } from "react-redux";
// import NextApp from "./NextApp";
// import "/node_modules/fontawesome-4.7/css/font-awesome.min.css";
// import 'bootstrap/dist/css/bootstrap.min.css';
// import './App.css';

// function App() {

//   return (
//     <div>
//     <Route>
//       <Switch>
//         {Routesr.map((route, index) => {
//           return (
//             <route.route
//               key={index}
//               path='/'
//               exact={route.exact}
//               component={(props) => {
//                 const Layout = route.layout;
//                 const key =
//                   `${props.match.params.slug}${props.match.params.subSlug}${props.match.params.subSubSlug}${props.match.params.varSlug}${props.match.params.keyword}` ||
//                   route.name;
//                 return (
//                   <NextApp {...props} />
//                 );
//               }}
//             />
//           );
//         })}
//       </Switch>
//     </Route>
//     </div>

//   );
// }

// export default App;
