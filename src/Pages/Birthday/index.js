import React from 'react'

function BirthdayIndex() {
  return (
    <>
      <div className="birthday_grid">
        <div className="BirthdayCard">
          <span className="topTag">Today</span>
          <figure>
            <img src={process.env.PUBLIC_URL + '/images/child2.png'} alt="" />
          </figure>
          <img
            className="mb5"
            src={process.env.PUBLIC_URL + '/extra-images/bg.png'}
            alt=""
          />
          <h5>Anaya khalid</h5>
          <h6>Class: 5A</h6>
        </div>
        <div className="BirthdayCard">
          <span className="topTag">Today</span>
          <figure>
            <img src={process.env.PUBLIC_URL + '/images/child2.png'} alt="" />
          </figure>
          <img
            className="mb5"
            src={process.env.PUBLIC_URL + '/extra-images/bg.png'}
            alt=""
          />
          <h5>Anaya khalid</h5>
          <h6>Class: 5A</h6>
        </div>
        <div className="BirthdayCard ">
          <span className="topTag">Today</span>
          <figure>
            <img src={process.env.PUBLIC_URL + '/images/child2.png'} alt="" />
          </figure>
          <img
            className="mb5"
            src={process.env.PUBLIC_URL + '/extra-images/bg.png'}
            alt=""
          />
          <h5>Anaya khalid</h5>
          <h6>Class: 5A</h6>
        </div>
        <div className="BirthdayCard ">
          <span className="topTag">Today</span>
          <figure>
            <img src={process.env.PUBLIC_URL + '/images/child2.png'} alt="" />
          </figure>
          <img
            className="mb5"
            src={process.env.PUBLIC_URL + '/extra-images/bg.png'}
            alt=""
          />
          <h5>Anaya khalid</h5>
          <h6>Class: 5A</h6>
        </div>
        <div className="BirthdayCard ">
          <span className="topTag">Today</span>
          <figure>
            <img src={process.env.PUBLIC_URL + '/images/child2.png'} alt="" />
          </figure>
          <img
            className="mb5"
            src={process.env.PUBLIC_URL + '/extra-images/bg.png'}
            alt=""
          />
          <h5>Anaya khalid</h5>
          <h6>Class: 5A</h6>
        </div>
        <div className="BirthdayCard ">
          <span className="topTag">Today</span>
          <figure>
            <img src={process.env.PUBLIC_URL + '/images/child2.png'} alt="" />
          </figure>
          <img
            className="mb5"
            src={process.env.PUBLIC_URL + '/extra-images/bg.png'}
            alt=""
          />
          <h5>Anaya khalid</h5>
          <h6>Class: 5A</h6>
        </div>
        <div className="BirthdayCard ">
          <span className="topTag">Today</span>
          <figure>
            <img src={process.env.PUBLIC_URL + '/images/child2.png'} alt="" />
          </figure>
          <img
            className="mb5"
            src={process.env.PUBLIC_URL + '/extra-images/bg.png'}
            alt=""
          />
          <h5>Anaya khalid</h5>
          <h6>Class: 5A</h6>
        </div>
      </div>
    </>
  )
}

export default BirthdayIndex
