import React, { useMemo, useState, useContext, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import mainContext from '../ContextApi/main'
import { Link } from 'react-router-dom'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import Voucher from './NewRegistration/StepLast'
// import { useStateMachine } from 'little-state-machine'
// import updateAction from '../ContextApi/pyload'
import axios from 'axios'
import moment from 'moment'
import { date } from 'yup'
const RegistraionList = ({ heading }) => {
  const regcontxt = useContext(mainContext)

  const {
    feeTemplate,
    formData,
    setFormData,
    setValue,
    reset,
    setstep,

    regkey,
    setKey,
    watch,
    ngs,
    setHeading,
    Statevalues,
    setPhotoSrc,
    setCroppedImage,
  } = regcontxt

  const [PrintVoucher, setPrintVoucher] = useState(false)

  const PrintVoucherToggle = () => {
    setPrintVoucher(!PrintVoucher)
  }
  if (PrintVoucher) {
    document.body.classList.add('active-modal')
  } else {
    document.body.classList.remove('active-modal')
  }
  const ResetAllFelids = () => {
    reset()
    setstep(1)
    setPhotoSrc('')
    // setFormData({ formData })
    setCroppedImage('')
  }

  const gridStyle = useMemo(() => ({ height: '100%', width: '100%' }), [])
  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      resizable: false,
      filter: true,
      floatingFilter: false,
      minWidth: 120,
    }
  }, [])
  const columnDefs = [
    {
      field: 'expiryDate',
      headerName: 'Status',
      suppressMenu: false,
      // width: auto,
      minWidth: 80,
      suppressMenu: false,
      filter: false,

      cellRenderer: ({ data }) => {
        return (
          <>
            <ul className="status_lights">
              {data && data.expiryDate < moment(new Date()).format() ? (
                <li className="sick-days-Red">
                  <span></span>
                </li>
              ) : (
                <li className="sick-days-blue">
                  <span></span>
                </li>
              )}
              {data && data.expiryDate === moment(new Date()).format() ? (
                <li className="sick-days-Orange">
                  <span></span>
                </li>
              ) : (
                ''
              )}
              {/* {data && data.gender === '1' ? (
                <li className="sick-days-Green">
                  <span></span>
                </li>
              ) : (
                ''
              )} */}
            </ul>
          </>
        )
      },
    },
    {
      field: 'id',
      headerName: 'SR#',
      minWidth: 60,
      width: 60,
      suppressMenu: false,
      filter: false,
      // suppressSizeToFit: true,
      // pinned: 'left',
    },
    {
      field: 'studentPicture',
      headerName: 'Image',
      suppressMenu: true,
      filter: false,

      cellRenderer: ({ params }) => {
        // // console.log(params, 'params params')
        return (
          <img
            style={{ height: '45px', width: '45px' }}
            // src={`http://39.61.53.93:8001/api${params}`}
            src={`https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0FIiZZbXHs4JkgRVWKTciwdWW_67I0p5hYg&usqp=CAU`}
            alt={params}
          />
        )
      },
      minWidth: 80,
      width: 80,
    },
    {
      field: 'regDate',
      headerName: 'Reg#',
      suppressMenu: false,
      // width: auto,
      minWidth: 120,
      suppressSizeToFit: true,
      cellRenderer: (data) => {
        return moment(data.regDate).format('DD/MM/YYYY')
      },
    },

    {
      field: 'studentFirstName',
      headerName: 'Name',
      // minWidth: 100,
      // suppressSizeToFit: true,
      // pinned: 'left',
      suppressMenu: false,
    },
    {
      field: 'className',
      headerName: 'Class',
      suppressMenu: false,
      width: 70,
      suppressSizeToFit: true,
    },
    {
      field: 'gender',
      headerName: 'Gender',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'bform',
      headerName: 'B-Form',
      suppressMenu: false,
      editable: true,
      // width: auto,
      minWidth: 120,
      suppressSizeToFit: true,
      // cellRenderer: (data) => {
      //   return moment(data.expiryDate).format('DD/MM/YYYY')
      // }
    },
    {
      field: 'parentName',
      headerName: 'Parent',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },

    {
      field: 'cnic',
      headerName: 'CNIC',
      suppressMenu: false,
      suppressSizeToFit: true,
    },

    {
      field: 'cell',
      headerName: 'Cell',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'expiryDate',
      headerName: 'Expiry',
      suppressMenu: false,
      minWidth: 120,
      suppressSizeToFit: true,
      cellRenderer: (data) => {
        return moment(data.value).format('DD/MM/YYYY')
        // // console.log(moment(data.value).format('DD/MM/YYYY'), 'check the Dates')
      },
    },
    {
      field: 'Action',
      // cellRendererParams: { condition: myCondition },
      cellRenderer: ({ data }) => {
        return (
          <div className="actionButtons">
            <i
              onClick={() => handleEdit(data.id)}
              className="fa fa-pencil-square-o"
              aria-hidden="true"
            ></i>

            <i
              onClick={() => handlePrint(data.id)}
              className="fa fa-print"
              aria-hidden="true"
            ></i>

            {data && data.expiryDate < moment(new Date()).format() ? null : (
              <i
                onClick={() => BlockRow(data.id)}
                className="fa fa-ban"
                aria-hidden="true"
              ></i>
            )}
          </div>
        )
      },
      suppressMenu: true,
      filter: false,
      pinned: 'right',
      width: 100,
      minWidth: 100,
      suppressSizeToFit: true,
    },
  ]

  useEffect(() => {
    setHeading(heading)
  }, [])

  // const rowClassRules = useMemo(() => {
  //   return {
  //     'sick-days-Red': (params) => {
  //       return (
  //         !!params.data && params.data.expiryDate < moment(new Date()).format()
  //       )
  //     },
  //     'sick-days-Orange': (params) => {
  //       return (
  //         !!params.data &&
  //         params.data.expiryDate === moment(new Date()).format()
  //       )
  //     },
  //     'sick-days-Green': (params) => {
  //       return !!params.data && params.data.gender === '1'
  //     },
  //   }
  // }, [])

  const isRowSelectable = useMemo(() => {
    return (params) => {
      return !!params.data && params.data.Action === ''
    }
  }, [])
  // const isRowSelectable = useMemo(() => {
  //   return (params) => {
  //     return !!params.data && params.data.expiryDate < moment().format()
  //   }
  // }, [])

  const BlockRow = (field) => {
    alert('Current Id', field)
  }
  const navigate = useNavigate()

  const handleEdit = (field, regdata) => {
    setKey(field)
    navigate('/NewRegistration')
  }

  const handlePrint = (field) => {
    // PrintVoucherToggle()
    setKey(field)
    // console.log(field, 'here is data..................')
    navigate('/NewRegistration')
    setstep(4)
  }

  const onGridReady = (params) => {
    const val = 1
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=${val}`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        // // console.log(resp)
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }

  return (
    <>
      <Link
        to="/NewRegistration"
        onClick={ResetAllFelids}
        className="btn-normal mb20"
      >
        New Registraion
      </Link>
      {/* {JSON.stringify({ regdata })} */}
      <div style={gridStyle} className="ag-theme-alpine">
        <AgGridReact
          className="ag-theme-alpine h100"
          columnDefs={columnDefs}
          defaultColDef={defaultColDef}
          alwaysShowHorizontalScroll={true}
          alwaysShowVerticalScroll={true}
          onGridReady={onGridReady}
          pagination={true}
          // rowClassRules={rowClassRules}
          isRowSelectable={isRowSelectable}
          // autoGroupColumnDef={autoGroupColumnDef}
        />
      </div>
      {PrintVoucher && (
        <div className="popup_model PrintVoucher">
          <div onClick={PrintVoucherToggle} className="overlay"></div>
          <div className="modal-content">
            {/* <Voucher formData={formData} feeTemplate={feeTemplate} /> */}
            <button className="close-modal" onClick={PrintVoucherToggle}>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      )}
    </>
  )
}
export default RegistraionList
