import Charts from '../Componets/widgets/charts'
import { Link, useNavigate } from 'react-router-dom'
import Statistics from '../Componets/widgets/Statistics'
import EventCalender from '../Componets/widgets/EventCalender'
import Noticsboard from '../Componets/widgets/noticsboard'
import DayBook from '../Componets/widgets/DayBook'
import PieChart from '../Componets/widgets/PieChart'

const Home = () => {
  const notics_board = [
    {
      id: 1,
      dateTime: 'Thu Sep 02 2022 12:40:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Shahzad ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 2,
      dateTime: 'Thu Feb 02 2022 9:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Asif ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 3,
      dateTime: 'Thu Sep 01 2022 02:50:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Mujahid ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 4,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Tanveer ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 5,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Asif ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 6,
      dateTime: 'Thu jun 01 2022 08:50:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Mujahid ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 7,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Tanveer ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
  ]
  // https://www.youtube.com/watch?v=oUZjO00NkhY
  //https://www.youtube.com/watch?v=fWXk2YC8gXI
  // https://codesandbox.io/s/ol6z72kjy9?file=/src/landing.page.js

  return (
    <>
      <div className="row">
        <div className="col-md-12">
          <div className="deshboard_status">
            <Statistics
              cls={'status_cards bg-green'}
              title={'Total Students'}
              count={'150'}
              iconName={'fa fa-graduation-cap'}
              trendingclassName={'mdi mdi-trending-up'}
              percentage={'8.5%'}
              series={[
                {
                  name: 'Registration',
                  data: [58],
                },
                {
                  name: 'Admissions',
                  data: [58],
                },
                {
                  name: 'Withdrawals',
                  data: [63],
                },
              ]}
            />
            <Statistics
              cls={'status_cards bg-yellow'}
              title={'Financial'}
              count={'150'}
              iconName={'fa fa-calculator'}
              bigicon={''}
              trendingclassName={'mdi mdi-trending-up'}
              percentage={'8.5%'}
              series={[
                {
                  name: 'Receiveable',
                  data: [50000],
                },
                {
                  name: 'Recevied',
                  data: [98000],
                },
                {
                  // name: 'Partially Recevied',
                  name: 'P.Recevied',
                  data: [57000],
                },
                {
                  name: 'Remaning',
                  data: [30000],
                },
              ]}
            />
            <PieChart
              cls="status_cards bg-perple"
              title="Student Attendance"
              count="70%"
              link="/Attendance/students"
              iconName="fa fa-calendar-check-o"
              trendingclassName="mdi mdi-trending-up"
              percentage="8.5%"
              series={[70, 8, 10, 12]}
            />
            <PieChart
              cls="status_cards bg-skyblue"
              title="Staff Attendance"
              count="50%"
              link="/Attendance/staff"
              iconName="fa fa-calendar"
              trendingclassName="mdi mdi-trending-up"
              percentage="8.5%"
              series={[70, 8, 10, 12]}
            />
          </div>
        </div>

        <div className="col-md-12">
          <DayBook />
        </div>
        <div className="col-md-12">
          <Charts />
        </div>
        <div className="col-md-6">
          <div className="Calender_date">
            <h5>Acadamic Calender</h5>
            <EventCalender />
          </div>
        </div>
        <div className="col-md-6">
          <div className="Calender_date">
            <h5>Time Table Calender</h5>
            <EventCalender />
          </div>
        </div>
        <div className="col-md-12">
          <div className="row">
            <div className="col-md-6">
              <Noticsboard
                cls="notics_board"
                heading={'Notice Board'}
                data={notics_board}
                imghere={
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWTwOqU8GB0oSnckNJOcr1Kvon2tcs-G9IHQ&usqp=CAU'
                }
              />
            </div>
            <div className="col-md-6">
              <Noticsboard
                heading={'Recent Activities'}
                activities={notics_board}
                cls="notics_board extra_style"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default Home
