/* eslint-disable array-callback-return */

import React, { useMemo, useContext } from 'react'
import { Link } from 'react-router-dom'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import mainContext from '../ContextApi/main'
const AdmisstionList = () => {
  const regcontxt = useContext(mainContext)

  const {
    reset,
    setstep,

    setPhotoSrc,
    setCroppedImage,
    admissionData,
    setAdmissionData,
  } = regcontxt
  const columnDefs = [
    {
      field: 'id',
      filter: 'agNumberColumnFilter',
      suppressMenu: true,
      minWidth: 70,
      suppressSizeToFit: true,
    },
    {
      field: 'Student Picture',
      filter: 'agTextColumnFilter',
      suppressMenu: true,
      cellRenderer: 'agGroupCellRenderer',
      minWidth: 200,
      suppressSizeToFit: true,
    },
    {
      field: 'Student Name',
      filter: 'agTextColumnFilter',
      suppressMenu: true,
      width: 150,
      suppressSizeToFit: true,
    },
    {
      field: 'Student B-Form/CNIC',
      filter: 'agTextColumnFilter',
      suppressMenu: true,
      width: 150,
      suppressSizeToFit: true,
    },
    {
      field: 'age',
      filter: 'agTextColumnFilter',
      suppressMenu: true,
      width: 70,
      suppressSizeToFit: true,
    },
    {
      field: 'Class Plan',
      filter: 'agTextColumnFilter',
      suppressMenu: false,
      width: 150,
      suppressSizeToFit: true,
    },
    {
      field: 'Guardian name',
      filter: 'agTextColumnFilter',
      suppressMenu: false,
      width: 150,
      suppressSizeToFit: true,
    },
    {
      field: 'Guardian Mobile',
      filter: 'agTextColumnFilter',
      suppressMenu: false,
      width: 200,
      suppressSizeToFit: true,
    },
    {
      field: 'Guardian Email',
      filter: 'agTextColumnFilter',
      suppressMenu: false,
      width: 150,
      suppressSizeToFit: true,
    },
    {
      field: 'Guardian Address',
      filter: 'agTextColumnFilter',
      suppressMenu: false,
      width: 200,
      suppressSizeToFit: true,
    },
    {
      field: 'Action',
      filter: 'agTextColumnFilter',
      suppressMenu: false,
      width: 150,
      suppressSizeToFit: true,
    },
  ]
  // const defaultColDef = {
  //   sortable: true,
  //   editable: true,
  //   flex: 1,
  //   filter: false,
  //   floatingFilter: true,

  //   // rowSelection: 'multiple',
  //   // groupSelectsChildren: true,
  //   // suppressRowClickSelection: true,
  //   // suppressAggFuncInHeader: true,
  // }
  const defaultColDef = useMemo(() => {
    return {
      editable: true,
      sortable: true,
      resizable: false,
      filter: true,
      flex: 0,
      width: 100,
      minWidth: 100,
    }
  }, [])

  const onGridReady = (params) => {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then((resp) => resp.json())
      .then((resp) => {
        // // console.log(resp)
        params.api.applyTransaction({ add: resp })
      })
  }
  const ResetAllFelids = () => {
    reset()
    setstep(1)
    setPhotoSrc('')

    // setFormData({ formData })
    setCroppedImage('')
  }
  // // console.log(admissionData, 'update finee')
  return (
    <>
      <Link
        to="/newAdmission"
        onClick={ResetAllFelids}
        className="btn-normal mb20"
      >
        New Admission
      </Link>
      <AgGridReact
        className="ag-theme-alpine h100"
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        alwaysShowHorizontalScroll={true}
        alwaysShowVerticalScroll={true}
        onGridReady={onGridReady}
      />
    </>
  )
}
export default AdmisstionList
