import { useState, useEffect, useCallback, useContext } from 'react'
import { Link, useNavigate, useLocation } from 'react-router-dom'
import Carousel from 'react-bootstrap/Carousel'
import useAuth from '../../ContextApi/useAuth'
// import axios from '../../Axios/axios'
import { toast } from 'react-toastify'
import mainContext from '../../ContextApi/main'

// const LOGIN_URL = '/auth'
export default function LockScreen() {
  const regcontxt = useContext(mainContext)

  const {
    from,
    user,
    setUser,
    pwd,
    setPwd,
    errMsg,
    navigate,
    accessToken,
    setaccessToken,
  } = regcontxt

  const [index, setIndex] = useState(0)
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex)
  }

  const { setAuth, auth } = useAuth()

  const roles = [5150]
  useEffect(() => {
    setaccessToken('93u45joifjasiodjf20384eojaidjasjd345345l')
  })
  // console.log(auth, 'check auth')
  const handleSubmit = async (e) => {
    e.preventDefault()

    // try {
    // const response = await axios.post(LOGIN_URL,
    //     JSON.stringify({ user, pwd }),
    //     {
    //         headers: { 'Content-Type': 'application/json' },
    //         withCredentials: true
    //     }
    // );
    // // console.log(JSON.stringify(response?.data));
    //// console.log(JSON.stringify(response));

    // const accessToken = response?.data?.accessToken;
    // const roles = response?.data?.roles;

    // if (pwd === 'admin') {
    //   setAuth({ user, pwd, accessToken, roles })
    //   navigate(from, { replace: true })
    // } else {
    //   toast.error('username and password do not match.')
    //   navigate('/lockScreen')
    // }
    // setaccessToken('')
    // setPwd('')

    const roles = [5150]
    const director = [1001]
    const teacher = [1002]
    const parent = [2022]

    if (user === 'admin' && pwd === 'admin') {
      setAuth({ user, pwd, roles })
      if (auth.user === 'admin') {
        setUser('admin')
      }
      navigate('/', { replace: false })
    } else if (auth.user === 'director' && pwd === 'director') {
      setUser('director')
      setAuth({ user, pwd, director })
      navigate('/Home', { replace: false })
    } else if (user === 'teacher' && pwd === 'teacher') {
      setAuth({ user, pwd, teacher, accessToken })
      if (auth.user === 'teacher') {
        setUser('teacher')
      }
      navigate('/THome', { replace: false })
    } else if (user === 'parent' && pwd === 'parent') {
      setAuth({ user, pwd, parent, accessToken })
      if (auth.user === 'parent') {
        setUser('parent')
      }
      navigate('/PHome', { replace: false })
    } else {
      toast.error('username and password do not match.')
      navigate('/lockScreen', { replace: false })
    }

    // setUser('')
    setPwd('')

    // navigate(from, { replace: true })
    // }
    // catch (err) {
    //     if (!err?.response) {
    //         setErrMsg('No Server Response');
    //     } else if (err.response?.status === 400) {
    //         setErrMsg('Missing Username or Password');
    //     } else if (err.response?.status === 401) {
    //         setErrMsg('Unauthorized');
    //     } else {
    //         setErrMsg('Login Failed');
    //     }
    //     errRef.current.focus();
    // }
  }

  return (
    <>
      <div className="OverflowHide">
        <div className="login">
          <div className="sideMenu">
            <div className="animatlogo">
              <h1>
                <img
                  src={process.env.PUBLIC_URL + '/logoB.png'}
                  alt="logo here"
                />
              </h1>
            </div>
            <div className="heading_of_login">
              <h5>Lock Screen</h5>
              <p>Enter your password to unlock the screen!</p>
            </div>
            <div className="login_form">
              <form className="mb-50" onSubmit={handleSubmit}>
                <div className="login_id">
                  <figure>
                    <img
                      src="http://shahzadali.eu5.org/images/profile-thum.jpg"
                      alt="img here"
                    />
                    <figcaption>
                      {/* <h5>Shahzad Ali ({auth.user})</h5> */}
                      <h5>{auth.user}</h5>
                    </figcaption>
                  </figure>
                </div>
                <div className="felids">
                  <input
                    type="password"
                    id="password"
                    onChange={(e) => setPwd(e.target.value)}
                    value={pwd}
                    placeholder="password"
                    // required
                  />
                  <i className="icon-padlock fa-x2">
                    <span className="path1"></span>
                    <span className="path2"></span>
                    <span className="path3"></span>
                    <span className="path4"></span>
                    <span className="path5"></span>
                    <span className="path6"></span>
                    <span className="path7"></span>
                  </i>
                </div>

                <div className="signin_Btn mb20">
                  <button type="submit" className="btn-normal">
                    Unlock
                  </button>
                </div>
                <p>
                  Not you ? return to{' '}
                  <Link to="/login" className="helptologin">
                    Logging in
                  </Link>
                </p>
              </form>
            </div>
          </div>
        </div>
        <div className="Right_section">
          <ul className="bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
          <Carousel
            className="Slider_bg"
            activeIndex={index}
            onSelect={handleSelect}
            fade={true}
            controls={false}
          >
            <Carousel.Item>
              <img
                src={process.env.PUBLIC_URL + '/Slider/slider4.png'}
                alt="slide"
              />
              {/* <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption> */}
            </Carousel.Item>
            <Carousel.Item>
              <img
                src={process.env.PUBLIC_URL + '/Slider/slider5.png'}
                alt="slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                src={process.env.PUBLIC_URL + '/Slider/slider3.png'}
                alt="slide"
              />
            </Carousel.Item>
          </Carousel>
        </div>
      </div>
    </>
  )
}
