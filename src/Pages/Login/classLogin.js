import React, { useEffect, useState, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import mainContext from '../../ContextApi/main'
const token = localStorage.getItem('token')
export default function ClassLogin() {
  const { setLoginstate, Loginstate, logged } = useContext(mainContext)

  let history = useNavigate()

  const onChange = (e) => {
    const value = e.target.value
    const name = e.target.name
    setLoginstate((Loginstate) => ({
      ...Loginstate,
      [name]: value,
    }))
  }
  function submitForm(e) {
    e.preventDefault()
    const { username, password } = Loginstate
    if (username === 'admin' && password === 'admin') {
      localStorage.setItem('token', 'zasdasdasdasdasd')
      setLoginstate({
        loggedIn: true,
      })
    }
  }

  useEffect(() => {
    if (token === null) {
      setLoginstate({ loggedIn: false })
      history('/')
    }
  }, [logged])
  useEffect(() => {
    if (Loginstate.loggedIn) {
      history('/home')
    }
  }, [logged])

  // console.log(logged, 'login page')

  return (
    <div>
      <form onSubmit={submitForm}>
        <input
          type="text"
          placeholder="username"
          name="username"
          value={Loginstate.username}
          onChange={onChange}
        />
        <input
          type="password"
          placeholder="password"
          name="password"
          value={Loginstate.password}
          onChange={onChange}
        />
        <button type="submit" className="btn-normal">
          submit
        </button>
      </form>
    </div>
  )
}
