import { useRef, useState, useEffect, useContext } from 'react'
import { Link, useNavigate, useLocation } from 'react-router-dom'
import Carousel from 'react-bootstrap/Carousel'
import useAuth from '../../ContextApi/useAuth'
// import axios from '../../Axios/axios'
import mainContext from '../../ContextApi/main'
import { toast } from 'react-toastify'
// const LOGIN_URL = '/auth'
export default function Login() {
  const regcontxt = useContext(mainContext)
  const {
    from,
    user,
    setUser,
    pwd,
    setPwd,
    errMsg,
    navigate,
    accessToken,
    setaccessToken,
  } = regcontxt
  const [error, setError] = useState(false)
  const [index, setIndex] = useState(0)
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex)
  }

  const { setAuth, auth } = useAuth()

  const userRef = useRef()
  const errRef = useRef()

  useEffect(() => {
    userRef.current.focus()
  }, [])

  useEffect(() => {
    setInterval(() => {
      setaccessToken('93u45joifjasiodjf20384eojaidjasjd345345l')
    }, 1000)
  }, [])

  const handleSubmit = async (e) => {
    e.preventDefault()

    // try {
    // const response = await axios.post(LOGIN_URL,
    //     JSON.stringify({ user, pwd }),
    //     {
    //         headers: { 'Content-Type': 'application/json' },
    //         withCredentials: true
    //     }
    // );
    // // console.log(JSON.stringify(response?.data));
    //// console.log(JSON.stringify(response));

    // const accessToken = response?.data?.accessToken;
    // const roles = response?.data?.roles;

    // const accessToken = '93u45joifjasiodjf20384eojaidjasjd345345l'

    const roles = [5150]
    const director = [1001]
    const teacher = [1002]
    const parent = [2022]
    if (user === 'admin' && pwd === 'admin') {
      setAuth({ user, pwd, roles })
      navigate('/', { replace: true })
    } else if (user === 'director' && pwd === 'director') {
      setAuth({ user, pwd, director })
      navigate('/Home', { replace: true })
    } else if (user === 'teacher' && pwd === 'teacher') {
      setAuth({ user, pwd, teacher })
      navigate('/THome', { replace: true })
    } else if (user === 'parent' && pwd === 'parent') {
      setAuth({ user, pwd, parent })
      navigate('/PHome', { replace: true })
    } else {
      toast.error('username and password do not match.')
      navigate('/login', { replace: true })
    }

    // setUser('')
    setPwd('')

    // }
    // catch (err) {
    //     if (!err?.response) {
    //         setErrMsg('No Server Response');
    //     } else if (err.response?.status === 400) {
    //         setErrMsg('Missing Username or Password');
    //     } else if (err.response?.status === 401) {
    //         setErrMsg('Unauthorized');
    //     } else {
    //         setErrMsg('Login Failed');
    //     }
    //     errRef.current.focus();
    // }
  }
  return (
    <>
      <div className="OverflowHide">
        <div className="login">
          <div className="sideMenu">
            <div className="animatlogo">
              <h1>
                <img
                  src={process.env.PUBLIC_URL + '/logoB.png'}
                  alt="logo here"
                />
              </h1>
            </div>
            <div className="heading_of_login">
              <h5>Welcome to NGS Portal</h5>
              <p>Sign in or Join now to manage your personal account</p>
            </div>
            <div className="login_form">
              {/* <p
                ref={errRef}
                className={errMsg ? 'errmsg' : 'offscreen'}
                aria-live="assertive"
              >
                {errMsg}
              </p> */}
              <form className="mb-50" onSubmit={handleSubmit}>
                <div className="felids">
                  <input
                    type="text"
                    id="username"
                    ref={userRef}
                    autoComplete="off"
                    onChange={(e) => setUser(e.target.value)}
                    value={user}
                    placeholder="username"
                    required
                  />
                  <i className="icon-user"></i>
                </div>
                <div className="felids">
                  <input
                    type="password"
                    id="password"
                    onChange={(e) => setPwd(e.target.value)}
                    value={pwd}
                    placeholder="password"
                    required
                  />
                  <i className="icon-padlock fa-x2">
                    <span className="path1"></span>
                    <span className="path2"></span>
                    <span className="path3"></span>
                    <span className="path4"></span>
                    <span className="path5"></span>
                    <span className="path6"></span>
                    <span className="path7"></span>
                  </i>
                </div>
                <Link to="/login" className="helptologin">
                  Having Trouble Logging in?
                </Link>
                <div className="signin_Btn">
                  <button type="submit" className="btn-normal">
                    Sign in
                  </button>
                </div>
              </form>
              <div className="floating">
                <ul className="social_media">
                  <li>
                    <Link to="">
                      <i className="fa fa-facebook"></i>
                    </Link>
                  </li>
                  <li>
                    <Link to="">
                      <i className="fa fa-instagram" aria-hidden="true"></i>
                    </Link>
                  </li>
                  <li>
                    <Link to="">
                      <i className="fa fa-twitter" aria-hidden="true"></i>
                    </Link>
                  </li>
                  <li>
                    <Link to="">
                      <i className="fa fa-youtube" aria-hidden="true"></i>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="Right_section">
          <Carousel
            className="Slider_bg"
            activeIndex={index}
            onSelect={handleSelect}
            fade={true}
            controls={false}
          >
            <Carousel.Item>
              <img
                src={process.env.PUBLIC_URL + '/Slider/slider4.png'}
                alt="slide"
              />
              {/* <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption> */}
            </Carousel.Item>
            <Carousel.Item>
              <img
                src={process.env.PUBLIC_URL + '/Slider/slider5.png'}
                alt="slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                src={process.env.PUBLIC_URL + '/Slider/slider3.png'}
                alt="slide"
              />
            </Carousel.Item>
          </Carousel>
        </div>
        <div className="bottom_row">
          <ul className="bottom_bar">
            <li>
              <Link to="">
                <i className="icon-admission">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                </i>
                Reg./ Admission Module
              </Link>
            </li>
            <li>
              <Link to="">
                <i className="icon-calendar-3">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                </i>
                Attendance Module
              </Link>
            </li>
            <li>
              <Link to="">
                <i className="icon-licensing">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                  <span className="path8"></span>
                  <span className="path9"></span>
                  <span className="path10"></span>
                  <span className="path11"></span>
                  <span className="path12"></span>
                  <span className="path13"></span>
                  <span className="path14"></span>
                  <span className="path15"></span>
                  <span className="path16"></span>
                  <span className="path17"></span>
                  <span className="path18"></span>
                </i>
                Fee Module
              </Link>
            </li>
            <li>
              <Link to="">
                <i className="icon-exam">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                  <span className="path8"></span>
                  <span className="path9"></span>
                  <span className="path10"></span>
                  <span className="path11"></span>
                  <span className="path12"></span>
                  <span className="path13"></span>
                  <span className="path14"></span>
                  <span className="path15"></span>
                  <span className="path16"></span>
                  <span className="path17"></span>
                  <span className="path18"></span>
                </i>
                Examination Module
              </Link>
            </li>
            <li>
              <Link to="">
                <i className="icon-salary-1">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                </i>
                Payroll Module
              </Link>
            </li>
            <li>
              <Link to="">
                <i className="icon-accountant">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                  <span className="path8"></span>
                  <span className="path9"></span>
                  <span className="path10"></span>
                </i>
                Accounts
              </Link>
            </li>
            <li>
              <Link to="">
                <i className="icon-monitor">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                  <span className="path8"></span>
                  <span className="path9"></span>
                </i>
                Reports
              </Link>
            </li>
          </ul>
          {/* <div className="copyrights">
            <p>© 2022 Copyright NGS Limited. All Rights Reserved.</p>
            <ul>
              <li>
                <Link to="/">www.ngs.edu.pk</Link>
              </li>
              <li>
                <Link to="/">Privacy Policy</Link>
              </li>
              <li>
                <Link to="/">Contact Us</Link>
              </li>
            </ul>
          </div> */}
        </div>
      </div>
    </>
  )
}
