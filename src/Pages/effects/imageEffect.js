import React from 'react'

export default function ImageEffect(props) {
    
    
    return (
        <div className="imageEffeacts">
            <label htmlFor={props.type}>{props.type}</label>
            <input type="range" id={props.type} min={props.min ? props.min : 0} max={props.max} step="0.01" onChange={props.onChange} value={props.imgEffect} />
        </div>
    )
}
