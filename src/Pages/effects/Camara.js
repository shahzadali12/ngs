import React, { useState, useCallback, useEffect, useContext } from 'react'
import ReactDOM from 'react-dom'
import getCroppedImg from '../../Pages/WebImageSettings/imagepriview'
import mainContext from '../../ContextApi/main'
import axios from 'axios'
import Camra from '../../Componets/widgets/camra'
import RangeSlider from 'react-bootstrap-range-slider'
import ImageEffeact from '../../Componets/ImageColers/Image'
import Cropper from 'react-easy-crop'

export default function Camara() {
  const [Snapcapture, setSnapcapture] = useState('Take Picture')
  const {
    setPhotoSrc,
    photoSrc,
    croppedImage,
    setCroppedImage,
    errors,
    setFormData,
    formData,
    Statevalues,
    setValue,
    FileCrop,
    setFileCrop,
    Profile,
    admissionData,
    setProfile,
  } = useContext(mainContext)
  //////////////////////////////////////////////////////////////////////

  const DEFAULT_OPTIONS = [
    {
      name: 'Brightness',
      property: 'brightness',
      value: 100,
      range: {
        min: 0,
        max: 200,
      },
      unit: '%',
    },
    {
      name: 'Contrast',
      property: 'contrast',
      value: 100,
      range: {
        min: 0,
        max: 200,
      },
      unit: '%',
    },
    {
      name: 'Saturation',
      property: 'saturate',
      value: 100,
      range: {
        min: 0,
        max: 200,
      },
      unit: '%',
    },
    {
      name: 'Grayscale',
      property: 'grayscale',
      value: 0,
      range: {
        min: 0,
        max: 100,
      },
      unit: '%',
    },
    {
      name: 'Sepia',
      property: 'sepia',
      value: 0,
      range: {
        min: 0,
        max: 100,
      },
      unit: '%',
    },
    {
      name: 'Hue Rotate',
      property: 'hue-rotate',
      value: 0,
      range: {
        min: 0,
        max: 360,
      },
      unit: 'deg',
    },
    {
      name: 'Blur',
      property: 'blur',
      value: 0,
      range: {
        min: 0,
        max: 20,
      },
      unit: 'px',
    },
  ]

  const [selectedOptionIndex, setSelectedOptionIndex] = useState(0)
  const [options, setOptions] = useState(DEFAULT_OPTIONS)
  const selectedOption = options[selectedOptionIndex]
  function handleSliderChange({ target }) {
    setOptions((prevOptions) => {
      return prevOptions.map((option, index) => {
        if (index !== selectedOptionIndex) return option
        return { ...option, value: target.value }
      })
    })
  }
  function getImageStyle() {
    const filters = options.map((option) => {
      return `${option.property}(${option.value}${option.unit})`
    })
    return { filter: filters.join('') }
  }

  ///////////////////////////////////////////////////////////////////////////

  //// Model

  const [CaptureModel, setCaptureImage] = useState(false)

  const CaptureImage = () => {
    setCaptureImage(!CaptureModel)
  }
  if (CaptureModel) {
    document.body.classList.add('active-modal')
  } else {
    document.body.classList.remove('active-modal')
  }
  useEffect(() => {
    setCroppedImage(
      { croppedImage: [formData.regStd.studentPicture] } || {
        croppedImage: [admissionData.regStd.studentPicture],
      }
    )
  }, [setCroppedImage, formData, admissionData])
  ////////////////////////////////////////////
  /////////// Zoom Model /////////////////////
  ////////////////////////////////////////////

  const [crop, setCrop] = useState({ y: 0, x: 0 })
  const [rotation, setRotation] = useState(0)
  const [zoom, setZoom] = useState(1)
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)
  const [isReadyEffectedImage, setIsReadyEffectedImage] = useState(false)

  const [ratio, setRatio] = useState(1 / 1)

  const [effectedImage, setEffectedImage] = useState(null)

  const onCropComplete = (croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels)
  }
  const showCroppedImage = async () => {
    try {
      const croppedImage = await getCroppedImg(
        photoSrc,
        croppedAreaPixels,
        rotation
      )
      setCroppedImage(croppedImage)

      // setFileCrop(NGSBLOB)
    } catch (e) {
      console.error(e)
    }
  }

  // const onClose = useCallback(() => {
  //     setCroppedImage(null)
  // }, [])

  ////////////////////////////////////////////
  /////////// Rotate Model /////////////////////
  ////////////////////////////////////////////

  const UpdateEffects = useCallback(
    (img) => {
      setIsReadyEffectedImage(true)
      setEffectedImage(img)
      photoSrc(img)
    },
    [photoSrc]
  )

  /////////////////////////////////////////////////////////

  const imageMimeType = /image\/(png|jpg|jpeg)/i
  const [file, setFile] = useState(null)
  const [fileDataURL, setFileDataURL] = useState(null)

  const changeHandler = (e) => {
    const file = e.target.files[0]
    if (!file.type.match(imageMimeType)) {
      alert('Image mime type is not valid')
      return
    }
    setFile(file)
  }

  useEffect(() => {
    let fileReader,
      isCancel = false
    if (file) {
      fileReader = new FileReader()
      fileReader.onload = (e) => {
        const { result } = e.target
        if (result && !isCancel) {
          setFileDataURL(result)
          setPhotoSrc(result)
        }
      }
      fileReader.readAsDataURL(file)
    }

    return () => {
      isCancel = true
      if (fileReader && fileReader.readyState === 1) {
        fileReader.abort()
      }
    }
  }, [setPhotoSrc, file])

  const ChangeProfilePic = () => {
    setProfile(croppedImage)
    setCaptureImage(false)
  }
  return (
    <>
      <div
        className={
          errors.studentPicture ? `upload_image error` : 'upload_image'
        }
        onClick={CaptureImage}
      >
        {(
          <img
            src={
              formData?.regStd?.studentPicture !== ['']
                ? Profile !== ['']
                  ? Profile ||
                    formData?.regStd?.studentPicture ||
                    process.env.PUBLIC_URL + 'images/photobg.jpg'
                  : process.env.PUBLIC_URL + 'images/photobg.jpg'
                : process.env.PUBLIC_URL + 'images/photobg.jpg'
            }
            alt={formData.regStd.studentPicture}
          />
        ) || (
          <img
            src={
              admissionData?.regStd?.studentPicture !== ['']
                ? Profile !== ['']
                  ? Profile ||
                    admissionData?.regStd?.studentPicture ||
                    process.env.PUBLIC_URL + 'images/photobg.jpg'
                  : process.env.PUBLIC_URL + 'images/photobg.jpg'
                : process.env.PUBLIC_URL + 'images/photobg.jpg'
            }
            alt={admissionData.regStd.studentPicture}
          />
        )}
      </div>

      {CaptureModel && (
        <div className="popup_model camra_model">
          <div onClick={CaptureImage} className="overlay"></div>
          <div className="modal-content">
            <div className="">
              <label htmlFor="image" className="btn-normal iconss">
                <i className="fa fa-upload"></i> Upload Profile Picture
              </label>
              <input
                type="file"
                id="image"
                accept=".png, .jpg, .jpeg"
                onChange={changeHandler}
              />
            </div>

            <div className="flex ">
              <div className="columns">
                <Camra
                  setPhotoSrc={setPhotoSrc}
                  photoSrc={photoSrc}
                  setSnapcapture={setSnapcapture}
                  Snapcapture={Snapcapture}
                />

                <h5>Zoom Image</h5>
                <RangeSlider
                  value={zoom}
                  min={1}
                  max={8}
                  step={0.1}
                  aria-labelledby="Zoom"
                  onChange={(e) => setZoom(e.target.value)}
                />

                <h5>Rotate Image</h5>
                <RangeSlider
                  value={rotation}
                  min={0}
                  max={360}
                  step={1}
                  aria-labelledby="Rotation"
                  onChange={(e, rotation) => setRotation(rotation)}
                />
                <h5>Brightness Image</h5>

                <ImageEffeact
                  options={options}
                  setOptions={setOptions}
                  selectedOption={selectedOption}
                  handleSliderChange={handleSliderChange}
                  getImageStyle={getImageStyle}
                  setSelectedOptionIndex={setSelectedOptionIndex}
                />
                <h5>Image Frames</h5>
                <select onChange={({ target }) => setRatio(target.value)}>
                  <option value={16 / 18}>16/18</option>
                  <option value={16 / 12}>16/12</option>
                  <option value={16 / 9}>16/9</option>
                  <option value={2 / 1}>2/1</option>
                  <option value={1 / 1}>1/1</option>
                </select>
              </div>
              <div className="Final_result extra_width">
                <div className="crop-container">
                  {photoSrc && (
                    <>
                      <span onClick={showCroppedImage} className="btn-normal">
                        Crop Image
                      </span>
                      <Cropper
                        image={photoSrc}
                        crop={crop}
                        rotation={rotation}
                        zoom={zoom}
                        aspect={ratio}
                        onCropChange={setCrop}
                        onRotationChange={setRotation}
                        onCropComplete={onCropComplete}
                        onZoomChange={setZoom}
                      />
                    </>
                  )}
                </div>
              </div>
              <div className="Final_result">
                <div className="col_vartical">
                  {croppedImage.length > 0 && (
                    <img
                      className="p300"
                      src={croppedImage}
                      style={getImageStyle()}
                      alt="ok"
                    />
                  )}
                </div>
              </div>
            </div>
            <div className="picture_editing_tools">
              {/* <div className="filters">
                <button
                  className="btn-normal"
                  onClick={() => setIsReadyEffectedImage(true)}
                >
                  Add Effects
                </button>
              </div> */}
              <button
                type="button"
                className="btn-normal"
                onClick={CaptureImage}
              >
                Cancel
              </button>
              <button
                type="button"
                className="btn-normal"
                onClick={ChangeProfilePic}
              >
                Save Picture
              </button>
            </div>
            <button
              type="button"
              className="close-modal"
              onClick={CaptureImage}
            >
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      )}
    </>
  )
}
