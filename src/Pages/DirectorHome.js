import { useState, useContext, useEffect } from 'react'
import Charts from '../Componets/widgets/charts'
import { Link, useNavigate } from 'react-router-dom'
import Statistics from '../Componets/widgets/Statistics'
import EventCalender from '../Componets/widgets/EventCalender'
import Noticsboard from '../Componets/widgets/noticsboard'
import DayBook from '../Componets/widgets/DayBook'
import PieChart from '../Componets/widgets/PieChart'
import ReactSelect from 'react-select'
import mainContext from '../ContextApi/main'

import BirthdayIndex from './Birthday'

const DirectorHome = () => {
  const regcontxt = useContext(mainContext)

  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    Statevalues,
    trigger,
    errors,
    gender,
    lstGender,
    customStyles,
    Controller,
  } = regcontxt
  const notics_board = [
    {
      id: 1,
      dateTime: 'Thu Sep 02 2022 12:40:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Shahzad ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 2,
      dateTime: 'Thu Feb 02 2022 9:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Asif ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 3,
      dateTime: 'Thu Sep 01 2022 02:50:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Mujahid ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 4,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Tanveer ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 5,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Asif ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 6,
      dateTime: 'Thu jun 01 2022 08:50:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Mujahid ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 7,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Tanveer ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
  ]

  return (
    <>
      <div className="row">
        <div className="col-md-12">
          <div className="flex">
            <div className="graph_box">
              <div className="lft">
                <figure>
                  <img
                    src={process.env.PUBLIC_URL + '/images/error.gif'}
                    alt="img here"
                  />
                  <figcaption>
                    <h6>
                      Account <i className="fa fa-check"></i>
                    </h6>
                    <p>Total Profit</p>
                  </figcaption>
                </figure>
                <div className="Check_status">3 from 5 tasks completed</div>
                <div className="progress done">
                  <div className="progress-bar" style={{ width: '80%' }}></div>
                </div>
              </div>
              <div className="status_graph">
                <img src={process.env.PUBLIC_URL + '/up.png'} alt="img here" />
                <h6>Rs.3490.00</h6>
                <small>Last 1 Month</small>
              </div>
            </div>
            <div className="graph_box">
              <div className="lft">
                <figure>
                  <img
                    src={process.env.PUBLIC_URL + '/images/error.gif'}
                    alt="img here"
                  />
                  <figcaption>
                    <h6>
                      Expences <i className="fa fa-check"></i>
                    </h6>
                    <p>Total Expence</p>
                  </figcaption>
                </figure>
                <div className="Check_status">3 from 5 tasks completed</div>
                <div className="progress error">
                  <div className="progress-bar" style={{ width: '20%' }}></div>
                </div>
              </div>
              <div className="status_graph">
                <img
                  src={process.env.PUBLIC_URL + '/down.png'}
                  alt="img here"
                />
                <h6>Rs.1490.00</h6>
                <small>Last 1 Month</small>
              </div>
            </div>
            <div className="graph_box">
              <div className="lft">
                <figure>
                  <img
                    src={process.env.PUBLIC_URL + '/images/error.gif'}
                    alt="img here"
                  />
                  <figcaption>
                    <h6>
                      Current Balance <i className="fa fa-check"></i>
                    </h6>
                    <p>Current </p>
                  </figcaption>
                </figure>
                <div className="Check_status">3 from 5 tasks completed</div>
                <div className="progress ">
                  <div className="progress-bar" style={{ width: '50%' }}></div>
                </div>
              </div>
              <div className="status_graph">
                <img src={process.env.PUBLIC_URL + '/up.png'} alt="img here" />
                <h6>Rs.3490.00</h6>
              </div>
            </div>
          </div>
          <div className="expences">
            <div className="ex_column bg-blue">
              <div className="lft">
                <h6>budget</h6>
                <h5>Rs.20,345/-</h5>
                <div className="bottom_status">
                  <span>13%</span> Since last Month
                </div>
              </div>
              <div className="icon_batch">
                <i className="fa fa-facebook"></i>
              </div>
            </div>
            <div className="ex_column bg-gry">
              <div className="lft">
                <h6>budget</h6>
                <h5>Rs.20,345/-</h5>
                <div className="bottom_status">
                  <span>13%</span> Since last Month
                </div>
              </div>
              <div className="icon_batch">
                <i className="fa fa-facebook"></i>
              </div>
            </div>
            <div className="ex_column bg-red">
              <div className="lft">
                <h6>budget</h6>
                <h5>Rs.20,345/-</h5>
                <div className="bottom_status">
                  <span>13%</span> Since last Month
                </div>
              </div>
              <div className="icon_batch">
                <i className="fa fa-facebook"></i>
              </div>
            </div>
            <div className="ex_column bg-yellow">
              <div className="lft">
                <h6>budget</h6>
                <h5>Rs.20,345/-</h5>
                <div className="bottom_status">
                  <span>13%</span> Since last Month
                </div>
              </div>
              <div className="icon_batch">
                <i className="fa fa-facebook"></i>
              </div>
            </div>
          </div>
          <div className="deshboard_status">
            <Statistics
              cls={'status_cards bg-green'}
              title={'Total Students'}
              count={'150'}
              iconName={'fa fa-graduation-cap'}
              trendingclassName={'mdi mdi-trending-up'}
              percentage={'8.5%'}
              series={[
                {
                  name: 'Registration',
                  data: [58],
                },
                {
                  name: 'Admissions',
                  data: [58],
                },
                {
                  name: 'Withdrawals',
                  data: [63],
                },
              ]}
            />
            <Statistics
              cls={'status_cards bg-yellow'}
              title={'Financial'}
              count={'150'}
              iconName={'fa fa-calculator'}
              bigicon={''}
              trendingclassName={'mdi mdi-trending-up'}
              percentage={'8.5%'}
              series={[
                {
                  name: 'Receiveable',
                  data: [50000],
                },
                {
                  name: 'Recevied',
                  data: [98000],
                },
                {
                  // name: 'Partially Recevied',
                  name: 'P.Recevied',
                  data: [57000],
                },
                {
                  name: 'Remaning',
                  data: [30000],
                },
              ]}
            />
            <PieChart
              cls="status_cards bg-perple"
              title="Student Attendance"
              count="70%"
              link="/students/attendace"
              iconName="fa fa-calendar-check-o"
              trendingclassName="mdi mdi-trending-up"
              percentage="8.5%"
              series={[70, 8, 10, 12]}
            />
            <PieChart
              cls="status_cards bg-skyblue"
              title="Staff Attendance"
              count="50%"
              link="/staff/attendace"
              iconName="fa fa-calendar"
              trendingclassName="mdi mdi-trending-up"
              percentage="8.5%"
              series={[70, 8, 10, 12]}
            />
          </div>
        </div>

        <div className="col-md-12">
          <DayBook />
        </div>
        <div className="col-md-12">
          <Charts />
        </div>
        <div className="col-md-6">
          <div className="Calender_date">
            <h5>birthday wishes</h5>
            <BirthdayIndex />
            <div className="Result_table">
              <table>
                <thead>
                  <tr>
                    <th>Days left</th>
                    <th>Name</th>
                    <th>Class</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>2 days</td>
                    <td>Hadi</td>
                    <td>5A</td>
                  </tr>
                  <tr>
                    <td>3 days</td>
                    <td>Zuhaib</td>
                    <td>5A</td>
                  </tr>
                  <tr>
                    <td>4 days</td>
                    <td>Anaya</td>
                    <td>5A</td>
                  </tr>
                  <tr>
                    <td>5 days</td>
                    <td>Afaq</td>
                    <td>6A</td>
                  </tr>
                  <tr>
                    <td>6 days</td>
                    <td>sonia</td>
                    <td>7A</td>
                  </tr>
                  <tr>
                    <td>6 days</td>
                    <td>sonia</td>
                    <td>7A</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="col-md-6 ">
          <div className="Calender_date background">
            <div className="wappers_scame">
              <h5>Top 3 position</h5>
              <div className="flex form_desgin">
                <div className={`custom-field selections_box`}>
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">Class</h6>
                  </label>

                  <Controller
                    control={control}
                    name="gender"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        defaultValue={gender}
                        options={lstGender}
                        styles={customStyles}
                      />
                    )}
                  />
                </div>
                <div className={`custom-field selections_box`}>
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">Examinations</h6>
                  </label>

                  <Controller
                    control={control}
                    name="gender"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        defaultValue={gender}
                        options={lstGender}
                        styles={customStyles}
                      />
                    )}
                  />
                </div>
                <div className="Tranding">
                  <figure>
                    <img
                      src={process.env.PUBLIC_URL + '/images/child2.png'}
                      alt=""
                    />
                    <figcaption>
                      <h6>Asif</h6>
                      <h6>97%</h6>
                    </figcaption>
                    <span>1st</span>
                  </figure>
                  <figure>
                    <img
                      src={process.env.PUBLIC_URL + '/images/child2.png'}
                      alt=""
                    />
                    <figcaption>
                      <h6>Tanveer</h6>
                      <h6>95%</h6>
                    </figcaption>
                    <span>2nd</span>
                  </figure>
                  <figure>
                    <img
                      src={process.env.PUBLIC_URL + '/images/child2.png'}
                      alt=""
                    />
                    <figcaption>
                      <h6>Mujahid</h6>
                      <h6>94%</h6>
                    </figcaption>
                    <span>3rd</span>
                  </figure>
                </div>
                <div className="Result_table">
                  <table>
                    <thead>
                      <tr>
                        <th>Podition</th>
                        <th>Name</th>
                        <th>Percentage</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>4th</td>
                        <td>Hadi</td>
                        <td>92%</td>
                      </tr>
                      <tr>
                        <td>5th</td>
                        <td>Zuhaib</td>
                        <td>92%</td>
                      </tr>
                      <tr>
                        <td>6th</td>
                        <td>Anaya</td>
                        <td>92%</td>
                      </tr>
                      <tr>
                        <td>7th</td>
                        <td>Afaq</td>
                        <td>92%</td>
                      </tr>
                      <tr>
                        <td>8th</td>
                        <td>sonia</td>
                        <td>92%</td>
                      </tr>
                      <tr>
                        <td>9th</td>
                        <td>Khalid</td>
                        <td>92%</td>
                      </tr>
                      <tr>
                        <td>9th</td>
                        <td>Shahzad</td>
                        <td>92%</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              {/* <div className="poditions_rank">
                <div className="columnOne">
                  <figure>
                    <img
                      src={process.env.PUBLIC_URL + '/images/child2.png'}
                      alt=""
                    />
                    <span>2nd</span>
                  </figure>
                  <div className="">
                    <h6>Anaya Khalid</h6>
                    <span>5A</span>
                  </div>
                </div>
                <div className="columnOne">
                  <figure className="center">
                    <img
                      src={process.env.PUBLIC_URL + '/images/child2.png'}
                      alt=""
                    />
                    <span>1st</span>
                  </figure>
                </div>
                <div className="columnOne">
                  <figure>
                    <img
                      src={process.env.PUBLIC_URL + '/images/child2.png'}
                      alt=""
                    />
                    <span>3rd</span>
                  </figure>
                </div>
              </div> */}
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="Calender_date">
            <h5>Acadamic Calender</h5>
            <EventCalender />
          </div>
        </div>
        <div className="col-md-6">
          <div className="Calender_date">
            <h5>Time Table Calender</h5>
            <EventCalender />
          </div>
        </div>
        <div className="col-md-12">
          <div className="row">
            <div className="col-md-6">
              <Noticsboard
                cls="notics_board"
                heading={'Notice Board'}
                data={notics_board}
                imghere={
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWTwOqU8GB0oSnckNJOcr1Kvon2tcs-G9IHQ&usqp=CAU'
                }
              />
            </div>
            <div className="col-md-6">
              <Noticsboard
                heading={'Recent Activities'}
                activities={notics_board}
                cls="notics_board extra_style"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default DirectorHome
