import React from 'react'
import NumberFormat from 'react-number-format'
import ReactSelect from 'react-select'
import DatePicker from 'react-datepicker'
import LeaveFile from '../Componets/FilesUpload/leave_file'
export default function Leaves() {
  const options = [
    {
      label: 'Paid leave',
      value: 'Paidleave',
    },
    {
      label: 'Annual leave',
      value: 'Annualleave',
    },
    {
      label: 'Casual leave',
      value: 'Casualleave',
    },
    {
      label: 'Sick leave',
      value: 'Sickleave',
    },
    {
      label: 'Maternity leave',
      value: 'Maternityleave',
    },
    {
      label: 'Paternity leave',
      value: 'Paternityleave',
    },
    {
      label: 'other',
      value: 'otherleave',
    },
  ]
  return (
    <>
      <div className="LeaveApplication">
        <div className="heading sprator_line">
          <h5>Leave Applications</h5>
        </div>
        <div className="row">
          <div className="col-md-9">
            <div className="flex form_desgin">
              <div className="flex">
                <div className={`custom-field idGenerate mb0`}>
                  <label>
                    <i className="fa fa-id-card-o"></i>
                    <h6 className="margin0">
                      <span></span>
                      Leave Type
                    </h6>
                  </label>
                  <ReactSelect options={options} />
                </div>
                <div className={`custom-field mb0`}>
                  <label>
                    <i className="fa fa-id-card-o"></i>
                    <h6 className="margin0">
                      <span></span>
                      Start Time
                    </h6>
                  </label>
                  <DatePicker />
                </div>
                <div className={`custom-field mb0`}>
                  <label>
                    <i className="fa fa-id-card-o"></i>
                    <h6 className="margin0">
                      <span></span>
                      End Time
                    </h6>
                  </label>
                  <DatePicker />
                </div>

                <LeaveFile fileName={'File Attachment'} />

                <div className="flex mt30">
                  <div className={`custom-field fullwidth `}>
                    <textarea
                      autoComplete="off"
                      type="text"
                      name="remarks"
                      placeholder="Leave Reason"
                      // id="remarks"
                      // {...register('remarks', { required: true })}
                    />
                    <label htmlFor="remarks">
                      <i className={'fa fa-envelope-o'}></i>
                      <h6 className="margin0">Leave Reason</h6>
                    </label>
                  </div>
                </div>

                <button type="submit" className="btn-normal">
                  Submit
                </button>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <h5>Process</h5>
            <ul className="process_flow">
              <li className="done">
                <span>in process</span>
                <img
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWTwOqU8GB0oSnckNJOcr1Kvon2tcs-G9IHQ&usqp=CAU"
                  alt="img here"
                />
              </li>
              <li className="inprocess">
                <span>Approver by Aliza (c.e.o)</span>
                <img
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWTwOqU8GB0oSnckNJOcr1Kvon2tcs-G9IHQ&usqp=CAU"
                  alt="img here"
                />
              </li>
              <li className="">
                <span>Notifier by (c.e.o)</span>
                <img
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWTwOqU8GB0oSnckNJOcr1Kvon2tcs-G9IHQ&usqp=CAU"
                  alt="img here"
                />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}
