import React, { useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { schema } from '../../Componets/schema/Schema'
import { useNavigate } from 'react-router-dom'
import ReactSelect from 'react-select'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import { useCallback } from 'react'
export const ClassPlan = () => {
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    getValues,
    trigger,
    reset,
    handleChange,

    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'blur',
  })
  const customStyles = {
    menuPortal: (base) => ({
      ...base,

      zIndex: 9999,
      '&:hover': {
        backgroundColor: 'red',
        color: 'white',
      },
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: '9999 !important',
      backgroundColor: '#fff',
      color: '#333',
      fontSize: '14px',
    }),

    control: (provided) => ({
      ...provided,
      margin: 0,
      border: '0px solid black',
      backgroundColor: 'white',
      outline: 'none',
      minHeight: '36px',
      fontSize: '14px',
    }),
  }
  const lstRole = [
    {
      label: 'Administrative',
      value: '5051',
    },
    {
      label: 'Academic',
      value: '4032',
    },
    {
      label: 'Maneger',
      value: '2033',
    },
  ]
  const Status = [
    {
      label: 'Active',
      value: '5054',
    },
    {
      label: 'Deactivated',
      value: '4035',
    },
  ]

  const defaultColDef = useCallback(() => {
    let initParams
    return {
      flex: 1,
      minWidth: 70,
      resizable: false,
      filter: false,
      floatingFilter: false,
    }
  }, [])

  const [columnDefs, setColumnDefs] = useState([
    {
      field: 'id',
      headerName: 'ID',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 70,
      width: 70,
    },
    {
      field: 'id',
      headerName: 'Name',
      suppressMenu: true,
      filter: false,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Class',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Shift',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Section',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Department',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
    },
    {
      field: 'studentFirstName',
      headerName: 'Class Teacher',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
    },
    {
      field: 'studentFirstName',
      headerName: 'Min Subject',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
    },
    {
      field: 'studentFirstName',
      headerName: 'Max Subject',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
    },
    {
      field: 'studentFirstName',
      headerName: 'is Mandatory',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
    },
    {
      field: 'studentFirstName',
      headerName: 'Class Teacher Id',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
    },
    {
      field: 'studentFirstName',
      headerName: 'is Subject Attendance',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 170,
      width: 170,
    },
    {
      field: 'studentFirstName',
      headerName: 'level Id',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Session Id',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Order',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Level',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'Action',
      // cellRendererParams: { condition: myCondition },
      cellRenderer: ({ data }) => {
        return (
          <div className="actionButtons">
            <i
              onClick={() => handleEdit(data.id)}
              className="fa fa-pencil-square-o"
              aria-hidden="true"
            ></i>

            <i
              onClick={() => handleDelate(data.id)}
              className="fa fa-trash"
              aria-hidden="true"
            ></i>

            {/* {data && data.expiryDate < moment(new Date()).format() ? null : (
              <i
                onClick={() => BlockRow(data.id)}
                className="fa fa-ban"
                aria-hidden="true"
              ></i>
            )} */}
          </div>
        )
      },
      minWidth: 100,
      width: 100,
      suppressMenu: true,
      filter: false,
      pinned: 'right',

      suppressSizeToFit: true,
    },
  ])

  //   const isRowSelectable = useMemo(() => {
  //     return (params) => {
  //       return !!params.data && params.data.expiryDate < moment().format()
  //     }
  //   }, [])

  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }

  const navigate = useNavigate()
  const handleEdit = (field, regdata) => {
    // setKey(field)
    navigate('/NewRegistration')
  }

  const handleDelate = (field) => {
    // PrintVoucherToggle()
    // setKey(field)
    // console.log(field, 'here is data..................')
    navigate('/NewRegistration')
  }
  return (
    <div className=" BaseDataSettings">
      <div className="col-12">
        <form className="mb30">
          <div className="flex form_desgin">
            <div className="heading sprator_line">
              <h5>ClassPlan Basedata</h5>
            </div>
            <div className="flex-flex">
              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Select Plan</h6>
                </label>
                <Controller
                  control={control}
                  name="selectPlan"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>

              <div className={`custom-field`}>
                <input
                  id="ids1"
                  {...register('Name', { required: true })}
                  name="Name"
                  placeholder="Plan Name"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">Plan Name</h6>
                </label>
              </div>
              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Section</h6>
                </label>
                <Controller
                  control={control}
                  name="Section"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>
              <div className={`custom-field`}>
                <input
                  {...register('minElective', { required: true })}
                  name="minElective"
                  placeholder="minElective"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">Min Elective Subject</h6>
                </label>
              </div>

              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Term</h6>
                </label>
                <Controller
                  control={control}
                  name="Term"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>

              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Class</h6>
                </label>
                <Controller
                  control={control}
                  name="Class"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>

              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Level</h6>
                </label>
                <Controller
                  control={control}
                  name="Level"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>
              <div className={`custom-field`}>
                <input
                  {...register('minElective', { required: true })}
                  name="minElective"
                  placeholder="minElective"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">Max Elective Subject</h6>
                </label>
              </div>
              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">To Term</h6>
                </label>
                <Controller
                  control={control}
                  name="Term"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>
              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Session</h6>
                </label>
                <Controller
                  control={control}
                  name="Session"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>
              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Class Teacher</h6>
                </label>
                <Controller
                  control={control}
                  name="ClassTeacher"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>
              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Shift</h6>
                </label>
                <Controller
                  control={control}
                  name="Shift"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>
              <div className={`checkbox`}>
                <input id="Status" type="checkbox" name="" value="a" />
                <label htmlFor="Status">
                  <span></span>
                  <h6>Status:</h6>
                </label>
              </div>
              <div className={`checkbox`}>
                <input id="mandatory" type="checkbox" name="" value="a" />
                <label htmlFor="mandatory">
                  <span></span>
                  <h6>Is Mandatory</h6>
                </label>
              </div>
              <div className={`checkbox`}>
                <input id="Attendance" type="checkbox" name="" value="a" />
                <label htmlFor="Attendance">
                  <span></span>
                  <h6>Is Subject Attendance:</h6>
                </label>
              </div>
            </div>
            <div className="button_full_grid">
              <button type="button" className="btn-normal">
                save
              </button>
            </div>
          </div>
        </form>
      </div>
      <div className="col-12">
        <div className="heading sprator_line">
          <h5>All Department Basedata</h5>
        </div>
        <div className="Table">
          <AgGridReact
            className="ag-theme-alpine h100"
            columnDefs={columnDefs}
            defaultColDef={defaultColDef}
            alwaysShowHorizontalScroll={true}
            alwaysShowVerticalScroll={true}
            onGridReady={onGridReady}
            pagination={true}
            paginationPageSize={10}
          />
        </div>
      </div>
    </div>
  )
}
