import React from 'react'
import QRCode from 'qrcode.react'
import Statistics from '../Componets/widgets/Statistics'
import ExamTable from '../Componets/widgets/examTable'
import ExpensesTable from '../Componets/widgets/expensesTable'
import moment from 'moment'
import { useState } from 'react'
export default function ParentInfo() {
  const profile = [
    {
      id: 1,
      CardID: '221015',
      name: 'Ananya Faheem',
      image:
        'https://thumbs.dreamstime.com/b/cute-little-indian-indian-asian-school-boy-wearing-uniform-cute-little-indian-indian-asian-school-boy-wearing-uniform-150099692.jpg',
      gender: 'Female',
      fatherName: 'Faheem',
      motherName: 'Saira',
      dateOfBrith: '04/09/2022',
      religion: 'islam',
      fatherOccupation: 'Business man',
      email: 'Ananya@info.com',
      admissionDate: '05/09/2022',
      class: 2,
      section: '1A',
      JoiningDate: '05/09/2021',
      cardExpiry: '05/09/2022',
      Address: '260 upper Mall Lahore',
      phoneNo: '+92 312 4326247',
      phone: '35750855-66',
    },
    {
      id: 2,
      CardID: '221015',
      name: 'Asif',
      gender: 'Male',
      image:
        'https://1.bp.blogspot.com/-gbQQzsxDR8w/XId73eyzmRI/AAAAAAAAI10/gGhaedYf2O4vLFY4eXu_6PBW2ohCA5R9ACLcBGAs/s1600/namya-joshi.JPG',
      fatherName: 'Asif',
      motherName: 'Asif',
      dateOfBrith: '04/09/2022',
      religion: 'islam',
      fatherOccupation: 'Business man',
      email: 'Ananya@info.com',
      admissionDate: '05/09/2022',
      class: 2,
      section: '2A',
      JoiningDate: '05/09/2021',
      cardExpiry: '05/09/2022',
      Address: '260 upper Mall Lahore',
      phoneNo: '+92 312 4326247',
      phone: '35750855-66',
    },
    {
      id: 3,
      CardID: '221015',
      name: 'Tanveer',
      gender: 'Male',
      image:
        'https://content.instructables.com/ORIG/F4R/O2KR/J16P2AE6/F4RO2KRJ16P2AE6.jpg',
      fatherName: 'Tanveer',
      motherName: 'Tanveer',
      dateOfBrith: '04/09/2022',
      religion: 'islam',
      fatherOccupation: 'Business man',
      email: 'Ananya@info.com',
      admissionDate: '05/09/2022',
      class: 2,
      section: '1A',
      JoiningDate: '05/09/2021',
      cardExpiry: '05/09/2022',
      Address: '260 upper Mall Lahore',
      phoneNo: '+92 312 4326247',
      phone: '35750855-66',
    },
    {
      id: 4,
      CardID: '221015',
      name: 'Sohail',
      gender: 'Female',
      image:
        'https://www.dfa.ie/media/dfa/passport/passportphotographs/Facial-Features---Acceptable-300x449.jpg',
      fatherName: 'Ali',
      motherName: 'Ali',
      dateOfBrith: '04/09/2022',
      religion: 'islam',
      fatherOccupation: 'Business man',
      email: 'Ananya@info.com',
      admissionDate: '05/09/2022',
      class: 2,
      section: '1A',
      JoiningDate: '05/09/2021',
      cardExpiry: '05/09/2022',
      Address: '260 upper Mall Lahore',
      phoneNo: '+92 312 4326247',
      phone: '35750855-66',
    },
  ]
  const time = moment(new Date()).format('hh:mm:ss')
  const CDate = moment(new Date()).format('DD/MM/YY')

  const [actives, SetActive] = useState(false)
  return (
    <>
      <div className="Parent_infomation">
        <div className="row">
          <div className="col-md-6">
            <div className="viewslist">
              <button
                type="button"
                className={`${actives ? 'active' : ''} `}
                onClick={() => SetActive(true)}
              >
                Card View
              </button>
              <button
                type="button"
                className={`${actives === false ? 'active' : ''} `}
                onClick={() => SetActive(false)}
              >
                List View
              </button>
            </div>
            <div className="bg-center margin-rtl">
              {profile &&
                profile.map((value) => {
                  return (
                    <div
                      className={`${actives ? 'Student_card' : 'siblings'} `}
                    >
                      {/* <div className="Student_card"> */}
                      <span className="child_view">My Child {value.id}</span>
                      <figure>
                        <img
                          src={process.env.PUBLIC_URL + 'logoB.png'}
                          alt="img here"
                        />
                        <h5>National Grammar School</h5>
                      </figure>
                      <div className="student_image">
                        <img src={value.image} alt="imghere" />
                      </div>
                      <div className="Card_base">
                        <div className="sib_none">
                          <h5>{value.name}</h5>
                          <h5>{value.section}</h5>
                        </div>
                        <div className="info_crd">
                          <ul>
                            <li className="none">
                              Name :<span>{value.name}</span>
                            </li>
                            <li className="none">
                              section :<span>{value.section}</span>
                            </li>
                            <li>
                              Student ID :<span>{value.CardID}</span>
                            </li>
                            <li>
                              Joining Date :<span>{value.JoiningDate}</span>
                            </li>
                            <li className="sib_none">
                              Card Expiry :<span>{value.cardExpiry}</span>
                            </li>
                            <li>
                              Phone no :<span>{value.phoneNo}</span>
                            </li>
                            <li>{value.Address}</li>
                          </ul>
                          <div className="QrCodes">
                            <QRCode
                              value={[
                                `
Name: ${value.name} 
ID: ${value.CardID} 
Section: ${value.section} 
Phone No: ${value.phoneNo} 
Today Attendance Date: ${CDate} 
Today Attendance Time: ${time}
                                 `,
                              ]}
                              size={actives ? 60 : 90}
                              level={'L'}
                              bgColor={'transparent'}
                              fgColor={'#000'}
                              includeMargin={false}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })}
            </div>
            {/* <div className="bg-center margin-rtl">
              {/* {profile &&
                profile.map((value) => {
                  return (
                    <div className="Student_card">
                      <span className="child_view">My Child {value.id}</span>
                      <figure>
                        <img
                          src={process.env.PUBLIC_URL + 'logoB.png'}
                          alt="img here"
                        />
                        <h5>National Grammar School</h5>
                      </figure>
                      <div className="student_image">
                        <img
                          //   src={'https://via.placeholder.com/250x250.png'}
                          src={value.image}
                          alt="imghere"
                        />
                      </div>
                      <div className="Card_base">
                        <h5>{value.name}</h5>
                        <h5>{value.section}</h5>
                        <div className="info_crd">
                          <ul>
                            <li>
                              Student ID :<span>{value.CardID}</span>
                            </li>
                            <li>
                              Joining Date :<span>{value.JoiningDate}</span>
                            </li>
                            <li>
                              Card Expiry :<span>{value.cardExpiry}</span>
                            </li>
                            <li>
                              Phone no :<span>{value.phone}</span>
                            </li>
                            <li>{value.Address}</li>
                          </ul>
                          <div className="QrCodes">
                            <QRCode
                              value={value.CardID || value.name}
                              size={70}
                              level={'L'}
                              bgColor={'transparent'}
                              fgColor={'#000'}
                              includeMargin={false}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })} 
            </div> */}
          </div>
          <div className="col-md-6">
            <div className="flex x2">
              <Statistics
                cls={'status_cards bg-red'}
                title={'Due Fees'}
                count={'1,550'}
                iconName={'fa fa-money'}
              />

              <Statistics
                cls={'status_cards bg-green'}
                title={'Upcomming Exams'}
                count={'3'}
                iconName={'fa fa-pencil-square-o'}
              />
              <Statistics
                cls={'status_cards bg-skyblue'}
                title={'Result Published'}
                count={'08'}
                iconName={'fa fa-thumbs-o-up'}
              />
              <Statistics
                cls={'status_cards bg-perple'}
                title={'Total Expenses'}
                count={'1,0550'}
                iconName={'fa fa-handshake-o'}
              />
            </div>
            <div className="Table_of_content">
              {/* <figure>
                  <img
                    src={process.env.PUBLIC_URL + 'images/error.gif'}
                    alt="img here"
                  />
                </figure> */}
              <h4>All Expenses</h4>
              <div className="">
                <ExpensesTable />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
