// import CreatableSelect from 'react-select/creatable'
// import React, { useState, useCallback, useEffect } from 'react'
// import axios from 'axios'

// function CreateSelectBox() {
//   const [formData, setFormData] = useState({
//     regParent: {
//       cityId: '',
//       areaId: '',
//     },
//   })
//   const [data, setData] = useState()

//   ///////////////       API       ///////////////////////////////

//   useEffect(() => {
//     const Deta = async () => {
//       try {
//         const Apidata = await axios.get(
//           'https://jsonplaceholder.typicode.com/posts'
//         )
//         setData(Apidata.data)
//       } catch (error) {
//         // console.log(error)
//       }
//     }
//     Deta()
//   }, [])

//   ///////////////////////////////////////////////////////////////////
//   ///////////////////////   Change OBJECT Key //////////////////////
//   ///////////////////////////////////////////////////////////////////

//   const AreasObj = data
//   const [options, setArea] = useState([])

//   const changeKeyObjects = (AreasObj) => {
//     // debugger
//     let res = []
//     for (let i = 0; i < 10; i++) {
//       let obj = {
//         value: AreasObj[i].title,
//         label: AreasObj[i].title,
//       }

//       res.push(obj)
//     }
//     setArea(res)
//   }

//   useEffect(() => {
//     if (AreasObj) {
//       changeKeyObjects(AreasObj)
//     }
//   }, [AreasObj])

//   // console.log(options)

//   ///////////////////////////////////////////////////////////////////
//   ///////////////////////   Change OBJECT NAME  end//////////////////////
//   ///////////////////////////////////////////////////////////////////

//   ///////////////       API  END     ///////////////////////////////

//   const Arry = [
//     { value: 'jack', label: 'Jack' },
//     { value: 'john', label: 'John' },
//     { value: 'mike', label: 'Mike' },
//   ]

//   const [inputValue, setInputValue] = useState('')

//   const [selection, setSelection] = useState(Arry) ///options  <= Add api data

//   const [selectedValues, setSelectedValues] = useState({ selected: [] })
//   const [current, setCurrent] = useState([])
//   const handleInputChange = useCallback(
//     (value) => {
//       setInputValue(value)
//     },
//     [setInputValue]
//   )
//   const setNewValue = useCallback(
//     (selectedvalue) => {
//       const difference = current.filter((x) => !inputValue.includes(x))
//       const newOption = {
//         value: inputValue.toString(),
//         label: inputValue.toString(),
//       }
//       inputValue !== '' && setSelection([...selection, newOption])
//       setCurrent(current)

//       setInputValue('')
//       setSelectedValues(difference)
//       // setFormData({
//       //   ...formData,
//       //   regParent: { ...formData.regParent, areaId: selectedvalue.value },
//       // })
//       // console.log(selectedvalue.value)
//     },
//     [setSelectedValues, current, inputValue, selection]
//   )

//   return (
//     <>
//       <CreatableSelect
//         options={options}
//         onChange={setNewValue}
//         onInputChange={handleInputChange}
//         inputValue={inputValue}
//         value={selectedValues}
//         controlShouldRenderValue={true}
//       />

//       {/* <CreatableSelect
//       options={options}
//       value={value}
//       onChange={onChange}
//       getOptionLabel={getOptionLabel}
//       getOptionValue={getOptionValue}
//       placeholder="Select packing"
//       isSearchable
//       getNewOptionData={(inputValue: string): Coding => ({ code: inputValue, display: inputValue })}
//       formatCreateLabel={(inputValue: string) => `Create new... ${inputValue}`}
//       isValidNewOption={(inputValue: string) => !!inputValue}
// /> */}
//     </>
//   )
// }

// export default CreateSelectBox

// import React, { useState, useEffect } from 'react'
// import moment from 'moment'
// import DatePicker from 'react-datepicker'
// import subDays from 'date-fns/subDays'
// import addDays from 'date-fns/addDays'
// // import Select from 'react-select'
// import ReactSelect from 'react-select'
// import 'react-datepicker/dist/react-datepicker.css'
// export default function Testing() {
//   const [formData, setFormData] = useState({
//     regStd: {
//       regDate: '',
//       expiryDate: '',
//     },
//   })

//   const [startDate, setStartDate] = useState(null)
//   const [EndDate, setEndDate] = useState(new Date())

//   const [State, setState] = useState({
//     value: { label: 'cash', value: 1 },
//   })

//   const options = [
//     { label: 'cash', value: 1 },
//     { label: 'online', value: 2 },
//     { label: 'payments', value: 3 },
//   ]

//   function handleChange(value) {
//     setState({ value: value })
//   }
//   return (
//     <>
//       <div className="flex form_desgin">
//         <div className="flex">
//           <div className={`datePick custom-field idGenerate`}>
//             <label htmlFor="dateIns">
//               <i className={`fa fa-calendar`}></i>
//               <h6 className="margin0">
//                 <span></span>
//                 Reg.date
//               </h6>
//             </label>

//             {/* <DatePicker
//               selected={startDate}
//               onChange={(date) => setStartDate(date)}
//               minDate={addDays(new Date(), 0)}
//               placeholderText="Start date"
//             /> */}
//             <ReactSelect
//               options={options}
//               value={State.value}
//               onChange={(value) => handleChange(value)}
//               defaultValue={{ label: 'cash', value: 1 }}
//             />
//           </div>
//           <div className={`datePick custom-field idGenerate`}>
//             <label htmlFor="dateIn">
//               <i className={`fa fa-calendar`}></i>
//               <h6 className="margin0">
//                 <span></span>
//                 exp.date
//               </h6>
//             </label>

//             <DatePicker
//               selected={EndDate}
//               onChange={(date) => setEndDate(date)}
//               maxDate={addDays(new Date(), 14)}
//               placeholderText="End date"
//             />
//           </div>
//         </div>
//       </div>
//       <h2>
//         {/* {minDate}-----
//         {maxDate} */}
//       </h2>
//     </>
//   )
// }

// import axios from 'axios'
// import { useState, useEffect } from 'react'

// export const api = axios.create({
//   baseURL: 'https://jsonplaceholder.typicode.com',
// })

// export const getPosts = async () => {
//   const response = await api.get('/posts')
//   return response.data
// }

// function Testing() {
//   const [posts, setPosts] = useState([])
//   const [searchResults, setSearchResults] = useState([])

//   const handleSubmit = (e) => e.preventDefault()

//   const handleSearchChange = (e) => {
//     if (!e.target.value) return setSearchResults(posts)

//     const resultsArray = posts.filter(
//       (post) =>
//         post.title.includes(e.target.value) ||
//         post.body.includes(e.target.value)
//     )
//     setSearchResults(resultsArray)
//   }
//   useEffect(() => {
//     getPosts().then((json) => {
//       setPosts(json)
//       setSearchResults(json)
//     })
//   }, [])
//   return (
//     <div>
//       <table>
//         <thead>
//           <tr>
//             <th>SR#</th>
//             <th>Name</th>
//             <th>Students</th>
//           </tr>
//         </thead>
//       </table>

//       <form className="search" onSubmit={handleSubmit}>
//         <input
//           className="search__input"
//           type="text"
//           id="search"
//           onChange={handleSearchChange}
//         />
//         <button className="search__button">Submit</button>
//       </form>

//       {searchResults?.length ? (
//         searchResults.map((val) => {
//           return (
//             <>
//               <article>
//                 <h2>{val.title}</h2>
//                 <p>{val.body}</p>
//                 <p>Post ID: {val.id}</p>
//               </article>
//             </>
//           )
//         })
//       ) : (
//         <article>
//           <p>No Matching Posts</p>
//         </article>
//       )}
//     </div>
//   )
// }

// export default Testing

// export default GoogleApiWrapper({
//   apiKey: 'AIzaSyAz0GdgbD5rsr8wmE4jWcAPxzGcSE_tIZ8',
// })(MapContainer)

// import { useState, useMemo } from 'react'
// import { GoogleMap, useLoadScript, Marker } from '@react-google-maps/api'
// import usePlacesAutocomplete, {
//   getGeocode,
//   getLatLng,
// } from 'use-places-autocomplete'

// export default function Places() {
//   const { isLoaded } = useLoadScript({
//     googleMapsApiKey: 'AIzaSyAz0GdgbD5rsr8wmE4jWcAPxzGcSE_tIZ8',
//     libraries: ['places'],
//   })

//   if (!isLoaded) return <div>Loading...</div>
//   return <Map />
// }

// function Map() {
//   const center = useMemo(() => ({ lat: 43.45, lng: -80.49 }), [])
//   const [selected, setSelected] = useState(null)

//   return (
//     <>
//       <div className="places-container">
//         <PlacesAutocomplete setSelected={setSelected} />
//       </div>

//       <GoogleMap
//         zoom={10}
//         center={center}
//         mapContainerClassName="map-container"
//       >
//         {selected && <Marker position={selected} />}
//       </GoogleMap>
//     </>
//   )
// }

// const PlacesAutocomplete = ({ setSelected }) => {
//   const {
//     ready,
//     value,
//     setValue,
//     suggestions: { status, data },
//     clearSuggestions,
//   } = usePlacesAutocomplete()

//   const handleSelect = async (address) => {
//     setValue(address, false)
//     clearSuggestions()

//     const results = await getGeocode({ address })
//     const { lat, lng } = await getLatLng(results[0])
//     setSelected({ lat, lng })
//   }

//   return (
//     <>
//       <input
//         value={value}
//         onChange={(e) => setValue(e.target.value)}
//         disabled={!ready}
//         className="combobox-input"
//         placeholder="Search an address"
//       />
//       <select onSelect={handleSelect}>
//         {status === 'OK' &&
//           data.map(({ place_id, description }) => (
//             <option key={place_id} value={description} />
//           ))}
//       </select>
//     </>
//   )
// }

// import { useRef, useEffect, useState } from 'react'
// const Testing = () => {
//   const autoCompleteRef = useRef()
//   const inputRef = useRef()
//   const options = {}
//   const [Add, setAdd] = useState()
//   useEffect(() => {
//     autoCompleteRef.current = new window.google.maps.places.Autocomplete(
//       inputRef.current,
//       options
//     )
//     autoCompleteRef.current.addListener('place_changed', async function () {
//       const place = await autoCompleteRef.current.getPlace()
//       setAdd(place)
//     })
//   }, [inputRef])
//   // console.log(Add, 'update location')
//   return (
//     <div>
//       <h1>{Add?.name}</h1>
//       <input ref={inputRef} />
//       {Add?.address_components.map((val) => (
//         <p>
//           {val.long_name} Short Name :{val.short_name} Types: {val.types}
//         </p>
//       ))}
//     </div>
//   )
// }
// export default Testing

// import React, { useState } from 'react'
// import { useEffect } from 'react'

// const Testing = () => {
//   /** "selected" here is state variable which will hold the
//    * value of currently selected dropdown.
//    */
//   const [selected, setSelected] = useState('')

//   /** Function that will set different values to state variable
//    * based on which dropdown is selected
//    */
//   const changeSelectOptionHandler = (event) => {
//     setSelected(event.target.value)
//   }

//   /** Different arrays for different dropdowns */
//   const algorithm = [
//     {
//       label: 'Searching Algorithm',
//       value: '1',
//     },
//     {
//       label: 'Sorting Algorithm',
//       value: '2',
//     },
//     {
//       label: 'Languagess',
//       value: '3',
//     },
//   ]
//   const language = [
//     {
//       label: 'C++',
//       value: 'C++',
//     },
//     {
//       label: 'Java',
//       value: 'Java',
//     },
//     {
//       label: 'Python',
//       value: 'Python',
//     },
//     {
//       label: 'C#',
//       value: 'C#',
//     },
//   ]
//   const dataStructure = [
//     { label: 'Arrays', value: 'Arrays' },
//     { label: 'LinkedList', value: 'LinkedList' },
//     { label: 'Stack', value: 'Stack' },
//     { label: 'Queue', value: 'Queue' },
//   ]

//   /** Type variable to store different array for different dropdown */
//   let type = null

//   /** This will be used to create set of options that user will see */
//   let options = null
//   const filter = algorithm
//     ?.filter((item) => item.value)
//     .map((label) => label.value)

//   // // console.log(filters, 'asasas')
//   /** Setting Type variable according to dropdown */

//   if (filter) {
//     const aa = filter
//     // console.log(filter, 'update ')
//     if (selected === aa) {
//       type = dataStructure
//     } else if (selected === aa) {
//       type = language
//     } else if (selected === aa) {
//       type = dataStructure
//     }
//   }

//   /** If "Type" is null or undefined then options will be null,
//    * otherwise it will create a options iterable based on our array
//    */
//   if (type) {
//     options = type.map((el, i) => (
//       <option key={el.value} value={el.value}>
//         {el.label}
//       </option>
//     ))
//   }

//   // // console.log(selected, 'view')
//   // console.log(options, 'Type view')

//   return (
//     <>
//       <form className="flex">
//         {/** Bind changeSelectOptionHandler to onChange method of select.
//          * This method will trigger every time different
//          * option is selected.
//          */}
//         <select onChange={changeSelectOptionHandler} className="column">
//           <option>Choose...</option>
//           {/* <option>Algorithm</option>
//             <option>Language</option>
//             <option>Data Structure</option> */}
//           {algorithm.map((val, i) => (
//             <option key={val.value} value={val.value}>
//               {val.label}
//             </option>
//           ))}
//         </select>

//         <select className="column">
//           {
//             /** This is where we have used our options variable */
//             options
//           }
//         </select>
//       </form>
//     </>
//   )
// }

// export default Testing

// import React from 'react'
// import { useEffect } from 'react'
// import { useState } from 'react'

// function Testing() {
//   const [state, setState] = useState('')

//   const Arry = [
//     {
//       label: 'January',
//       value: 'January',
//     },
//     {
//       label: 'February',
//       value: 'February',
//     },
//     {
//       label: 'march',
//       value: 'march',
//     },
//     {
//       label: 'April',
//       value: 'April',
//     },
//     {
//       label: 'May',
//       value: 'May',
//     },
//     {
//       label: 'June',
//       value: 'June',
//     },
//     {
//       label: 'July',
//       value: 'July',
//     },
//     {
//       label: 'August',
//       value: 'August',
//     },
//     {
//       label: 'September',
//       value: 'September',
//     },
//     {
//       label: 'October',
//       value: 'October',
//     },
//     {
//       label: 'November',
//       value: 'November',
//     },
//     {
//       label: 'December',
//       value: 'December',
//     },
//   ]

//   const TAbleData = [
//     {
//       id: 1,
//       name: 'January',
//     },
//     {
//       id: 2,
//       name: 'February',
//     },
//     {
//       id: 3,
//       name: 'march',
//     },
//     {
//       id: 4,
//       name: 'April',
//     },
//     {
//       id: 5,
//       name: 'May',
//     },
//     {
//       id: 6,
//       name: 'June',
//     },
//     {
//       id: 7,
//       name: 'July',
//     },
//     {
//       id: 8,
//       name: 'August',
//     },
//     {
//       id: 9,
//       name: 'September',
//     },
//     {
//       id: 10,
//       name: 'October',
//     },
//     {
//       id: 11,
//       name: 'November',
//     },
//     {
//       id: 12,
//       name: 'December',
//     },
//   ]

//   const [activeState, setActiveState] = useState([])

//   const EventChnager = (e) => {
//     e.preventDefault()
//     let value = e.target.value
//     let name = e.target.name
//     const selected = TAbleData.filter((val) => val.name.includes(value)).map(
//       (val) => {
//         return setState(val)
//       }
//     )

//     // walk through all cells that have the groupId class

//     // get current cell
//     var currentCell = e.target.value
//     // get the cells groupid
//     var groupid = currentCell.text()
//     // determine if it the group id matches the selected value and hide/show
//     if (groupid >= selected) {
//       currentCell.parent().show()
//     } else {
//       currentCell.parent().hide()
//     }
//   }

//   useEffect(() => {
//     let update = TAbleData?.find((o) => o.name === state.value)
//     if (activeState) {
//       let res = []
//       for (let i = 0; i >= TAbleData; i++) {
//         res.push(i)
//       }
//       // console.log(res, 'asd')
//     }
//     // console.log(update, 'update loop')
//   }, [])

//   // function hideRows() {
//   //   // find selected value (get only the first as multi-selected is not required)
//   //   var selected = [0].value
//   //   // walk through all cells that have the groupId class

//   //   // get current cell
//   //   var currentCell = []
//   //   // get the cells groupid
//   //   var groupid = currentCell.text()
//   //   // determine if it the group id matches the selected value and hide/show
//   //   if (groupid >= selected) {
//   //     currentCell.parent().show()
//   //   } else {
//   //     currentCell.parent().hide()
//   //   }
//   // }
//   // wire-up select change event
//   // $('#groups').change(hideRows)
//   // // fire show/hide for doc load
//   // hideRows()

//   // const data = [
//   //   { title: 'One', prix: 100 },
//   //   { title: 'Two', prix: 200 },
//   //   { title: 'Three', prix: 300 },
//   // ]

//   // // console.log(data.reduce((a, v) => (a = a + v.prix), 0))
//   return (
//     <div>
//       <select
//         className="selection_box mb30"
//         onChange={EventChnager}
//         value={state}
//       >
//         {Arry.map((val) => {
//           return (
//             <option value={val.value} key={val.value}>
//               {val.label}
//             </option>
//           )
//         })}
//       </select>

//       <table>
//         <thead>
//           <tr>
//             <th>SR #</th>
//             <th>Month</th>
//             <th>multiple Selected month</th>
//             <th>count</th>
//           </tr>
//         </thead>
//         <tbody>
//           {TAbleData.map((val) => {
//             return (
//               <tr key={val.id}>
//                 <td>{val.id}</td>
//                 <td>{val.name}</td>
//                 <td>20</td>
//                 <td>30</td>
//               </tr>
//             )
//           })}
//         </tbody>
//       </table>
//     </div>
//   )
// }

// export default Testing

// import React, { useState } from 'react'
// import DatePicker from 'react-datepicker'
// import moment from 'moment'
// function Testing() {
//   const Monthly = [
//     {
//       id: 1,
//       name: 'January',
//     },
//     {
//       id: 2,
//       name: 'February',
//     },
//     {
//       id: 3,
//       name: 'march',
//     },
//     {
//       id: 4,
//       name: 'April',
//     },
//     {
//       id: 5,
//       name: 'May',
//     },
//     {
//       id: 6,
//       name: 'June',
//     },
//     {
//       id: 7,
//       name: 'July',
//     },
//     {
//       id: 8,
//       name: 'August',
//     },
//     {
//       id: 9,
//       name: 'September',
//     },
//     {
//       id: 10,
//       name: 'October',
//     },
//     {
//       id: 11,
//       name: 'November',
//     },
//     {
//       id: 12,
//       name: 'December',
//     },
//   ]
//   const planetList = [
//     { id: '01', name: 'Mercury', size: '2440', unit: 'km' },
//     { id: '02', name: 'Venus', size: '6052', unit: 'km' },
//     { id: '03', name: 'Earth', size: '6371', unit: 'km' },
//     { id: '04', name: 'Mars', size: '3390', unit: 'km' },
//     { id: '05', name: 'Jupiter', size: '69911', unit: 'km' },
//     { id: '06', name: 'Saturn', size: '58232', unit: 'km' },
//     { id: '07', name: 'Uranus', size: '25362', unit: 'km' },
//     { id: '08', name: 'Neptune', size: '24622', unit: 'km' },
//   ]
//   const [filteredList, setFilteredList] = useState(Monthly)
//   // const [searchQuery, setSearchQuery] = useState('')

//   // const handleSearch = (event) => {
//   //   const query = event.target.value
//   //   // setSearchQuery(query)

//   //   const searchList = Monthly.filter((item) => {
//   //     return item.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
//   //   })

//   //   setFilteredList(searchList)
//   // }

//   const onFilterChange = (event) => {
//     const selectedSize = Number(event.target.value)

//     const filterList = Monthly.filter((item) => {
//       return Number(item.id) > selectedSize
//     })

//     setFilteredList(filterList)
//   }

//   const [data, setData] = useState(['Eli', 'Smith', 'Jhon'])
//   const [startDate, setStartDate] = useState(new Date('2014/02/08'))
//   const [endDate, setEndDate] = useState(new Date('2014/02/10'))

//   const handleDelete = (index, e) => {
//     setFilteredList(filteredList.filter((v, i) => i !== index))
//   }
//   const rows = data.map((item, index) => {
//     return (
//       <tr key={index}>
//         <td>{item}</td>
//         <td>
//           <button onClick={(e) => handleDelete(index, e)}>Delete</button>
//         </td>
//       </tr>
//     )
//   })

//   const handleInputData = (e, index) => {
//     e.preventDefault()
//     const { name, value } = e.target
//     startDate[index][name] = value
//     setStartDate([...startDate])
//   }

//   return (
//     <div className="container">
//       <h2>Search Filter Array of Objects</h2>

//       <table>{rows}</table>
//       <div className="list-wrapper">
//         <div className="filter-container">
//           {/* <input
//               type="text"
//               name="search"
//               placeholder="Search"
//               value={searchQuery}
//               onChange={handleSearch}
//             /> */}
//           <div>
//             <select name="size" onChange={onFilterChange}>
//               <option value="">Filter by Size</option>
//               {/* <option value="2000">Greater Than 2000km</option>
//                 <option value="6000">Greater Than 6000km</option>
//                 <option value="10000">Greater Than 10000km</option>
//                 <option value="25000">Greater Than 25000km</option> */}

//               {Monthly.map((val) => {
//                 return (
//                   <option value={val.id} key={val.id}>
//                     {val.name}
//                   </option>
//                 )
//               })}
//             </select>
//           </div>
//         </div>

//         {filteredList.map((item, index) => {
//           return (
//             <div className="card" key={item.id}>
//               <DatePicker
//                 selected={startDate}
//                 onChange={(date) => handleInputData(index, date)}
//                 selectsStart
//                 // value={item.value}
//                 // startDate={startDate}
//                 // endDate={endDate}
//               />
//               <DatePicker
//                 selected={endDate}
//                 onChange={(date) => handleInputData(index, date)}
//                 selectsEnd
//                 // startDate={startDate}
//                 // endDate={endDate}
//                 // minDate={startDate}
//               />
//             </div>
//           )
//         })}
//       </div>
//     </div>
//   )
// }

// export default Testing

import React, { useEffect, useState } from 'react'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
function Testing() {
  // const [userinfo, setUserInfo] = useState({
  //   languages: [],
  //   response: [],
  // })

  // const handleChange = (e) => {
  //   const { name, value, checked } = e.target
  //   // console.log(`${value} is ${checked}`)
  //   const { languages, response } = userinfo
  //   if (checked) {
  //     setUserInfo({
  //       ...userinfo,
  //       [name]: [...userinfo[name], value],
  //     })
  //   } else {
  //     setUserInfo({
  //       ...userinfo,
  //       [name]: [...userinfo[name].filter((e) => e !== value)],
  //     })
  //   }
  // }

  // // console.log(userinfo, 'update values ')

  // for (let i = 0; i <= 11; i++) {
  //   months.push(monthName[d.getMonth()])
  //   monthsInt.push(monthIn[d.getMonth()])
  //   years.push(d.getFullYear())
  //   d.setMonth(d.getMonth() + 1)
  // }
  // months.forEach(function (v, i) {
  //   var obj = {}
  //   obj.month = v
  //   obj.monthIn = monthsInt[i]
  //   obj.years = years[i]
  //   MonthnYear.push(obj)
  // })

  // const [filteredList, setFilteredList] = useState(MonthnYear)
  // const EventChnager = (e) => {
  //   let Event = e.target.value

  //   // data.forEach((checkdate, i) => {
  //   //   list.push(checkdate + i)
  //   // })

  //   // for (let i = 0; i < data.length; i++) {
  //   //   data.push(checkdate + i)
  //   // }
  //   let Result = []
  //   const checkdate = data.filter((val) => val.monthIn == Event)
  //   data.forEach((el, i) => {
  //     Result.push(testing >= el)

  //     // // console.log(el, 'update Arry')
  //   })
  //   // // console.log(Result, 'update Arry')
  //   // // console.log(Result, 'update Arry')
  //   // console.log(Object.keys(Result))
  //   // // console.log(JSON.stringify(Result))
  //   // setData([data, testing])
  // }
  var d = new Date().getMonth() + 1
  // // console.log(d, 'update date')
  // const RestoreDefault = (e) => {
  //   let shwow = e.target.value
  // }
  // const currentMon = data?.find((o) => o.value)

  // // console.log(data, 'updated Data')

  ///////////////////////////
  // const getDatesDiff = (start_date, end_date, date_format = 'YYYY-MM-DD') => {
  //   const getDateAsArray = (date) => {
  //     return moment(new Date(date), date_format)
  //   }
  //   const diff =
  //     getDateAsArray(end_date).diff(getDateAsArray(start_date), 'month') + 1
  //   const dates = []
  //   for (let i = 0; i < diff; i++) {
  //     const nextDate = getDateAsArray(start_date).add(i, 'month')
  //     // const isWeekEndDay = nextDate.isoWeekday() > 5
  //     const Month = nextDate.month() > 30

  //     if (!Month) dates.push(nextDate.format(date_format))
  //   }
  //   return dates
  // }
  // const date_log = getDatesDiff('2021-10-17', '2022-05-25')
  // // console.log(date_log)

  function getMonthDifference(startDate, endDate) {
    return (
      endDate.getMonth() -
      startDate.getMonth() +
      12 * (endDate.getFullYear() - startDate.getFullYear())
    )
  }

  // // 👇️ 2
  // // console.log(
  //   getMonthDifference(new Date('2022-01-15'), new Date('2022-03-16'))
  // )

  // // 👇️ 5
  // // console.log(
  //   getMonthDifference(new Date('2022-01-15'), new Date('2022-06-16'))
  // )

  // // 👇️ 14
  // // console.log(
  //   getMonthDifference(new Date('2022-01-15'), new Date('2023-03-16'))
  // )

  const [startDates, setstartDates] = useState()
  const [endDates, setendDates] = useState()
  const start = new Date('2022-01-15')
  const end = new Date('2022-08-15')
  const datefilter = getMonthDifference(start, end)
  // // console.log(datefilter, 'filter Dates')

  var monthName = new Array(
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  )
  var monthIn = new Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
  // var months = []
  // var years = []
  // var monthsInt = []
  // var d = new Date()
  // var id = []
  // d.setDate(1)

  const GetElements = () => {
    // const aa = datefilter.forEach((element) => {
    //   // console.log(element)
    //   // return (monthName = element)
    // })
    let ee = []
    // for (let i = 0; i < monthIn.length; i++) {
    //   const view = datefilter[i]

    //   ee.push(view)
    // }
    for (let b = 0; b < monthIn.length; b++) {
      let subs = monthIn.slice(datefilter[b], monthName[b])

      ee.push(subs)
    }
    // // console.log(ee, 'view')
    // // console.log(aa)
  }
  useEffect(() => {
    GetElements()
  }, [])

  var month = new Array()

  month[0] = 'January'
  month[1] = 'February'
  month[2] = 'March'
  month[3] = 'April'
  month[4] = 'May'
  month[5] = 'June'
  month[6] = 'July'
  month[7] = 'August'
  month[8] = 'September'
  month[9] = 'October'
  month[10] = 'November'
  month[11] = 'December'

  const object = []

  for (
    let n = 0;
    n <= new Date().getMonth() <= 0 ? 0 : new Date().getMonth();
    n++
  ) {
    object.push([month[n % 12], n + 1])
  }
  // // console.log(object)

  let array = [1, 2, 3, 4, 5, 6]

  // while (index < array.length) {
  //   // console.log(array[index])
  //   index++
  // }
  // // console.log(index)

  // for (let index = 0; index < array.length; index++) {
  //   // console.log(array[index], 'check')
  // }
  // array.forEach((element, index, are) => {
  //   // console.log(element, index, are, 'check ele')
  // })

  return (
    <>
      {/* <div className="container-fluid top ">
        <div className="container mt-5  pb-5 pt-5">
          <h3 className="form-head-contact-h3 ">
            Your programming expertise lies in what languages?{' '}
          </h3>

          <form>
            <div className="row">
              <div className="col-md-6">
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="response"
                    value="Javascript"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    Javascript
                  </label>
                </div>
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="response"
                    value="Python"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    Python
                  </label>
                </div>
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="response"
                    value="Java"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    Java
                  </label>
                </div>
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="response"
                    value="PHP"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    PHP
                  </label>
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="languages"
                    value="C#"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    C#
                  </label>
                </div>
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="languages"
                    value="C++"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    C++
                  </label>
                </div>
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="languages"
                    value="C"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    C
                  </label>
                </div>
                <div className="form-check m-3">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="languages"
                    value="Typescript"
                    id="flexCheckDefault"
                    onChange={handleChange}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckDefault"
                  >
                    Typescript
                  </label>
                </div>
              </div>
            </div>

            <div className="form-floating mt-3 mb-3 text-center">
              <label htmlFor="exampleFormControlTextarea1">
                You're proficient in the following languages :{' '}
              </label>
              <textarea
                className="form-control text"
                name="response"
                value={userinfo.response}
                placeholder="The checkbox values will be displayed here "
                id="floatingTextarea2"
                style={{ height: '150px' }}
                onChange={handleChange}
              ></textarea>
            </div>
          </form>
        </div>
      </div> */}

      {/* <DatePicker
        dateFormat="dd/MM/yyyy"
        selected={startDates}
        onChange={getMonthDifference(startDates)}
      />
      <DatePicker
        dateFormat="dd/MM/yyyy"
        selected={endDates}
        onChange={getMonthDifference(endDates)}
      /> */}
    </>
  )
}

export default Testing
