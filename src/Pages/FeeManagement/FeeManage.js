import React, { useState, useEffect, useContext } from 'react'
import mainContext from '../../ContextApi/main'
import ReactSelect from 'react-select'
// import DatePicker from 'react-datepicker'
// import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment'
// import { Environment } from 'ag-grid-community'
function FeeManage() {
  const regcontxt = useContext(mainContext)

  const {
    control,

    Controller,
    customStyles,
  } = regcontxt

  // const arry = [
  //   {
  //     id: 1,
  //     amount: 12000,
  //     discount: 1000,
  //     formula: 1,
  //   },
  // ]
  const MonthSelect = [
    {
      id: 1,
      label: 'January',
      value: 'January',
    },
    {
      id: 2,
      label: 'February',
      value: 'February',
    },
    {
      id: 3,
      label: 'March',
      value: 'March',
    },
    {
      id: 4,
      label: 'April',
      value: 'April',
    },
    {
      id: 5,
      label: 'May',
      value: 'May',
    },
    {
      id: 6,
      label: 'June',
      value: 'June',
    },
    {
      id: 7,
      label: 'July',
      value: 'July',
    },
    {
      id: 8,
      label: 'August',
      value: 'August',
    },
    {
      id: 9,
      label: 'September',
      value: 'September',
    },
    {
      id: 10,
      label: 'October',
      value: 'October',
    },
    {
      id: 11,
      label: 'November',
      value: 'November',
    },
    {
      id: 12,
      label: 'December',
      value: 'December',
    },
  ]
  const formula = [
    {
      id: 1,
      label: 1,
      value: 1,
    },
    {
      id: 2,
      label: 1.2,
      value: 2,
    },
    {
      id: 3,
      label: 2.4,
      value: 3,
    },
  ]
  // const SelectFormula = [
  //   {
  //     id: 1,
  //     label: 'Month',
  //     value: 'Month',
  //   },
  //   {
  //     id: 2,
  //     label: 'Formula',
  //     value: 'Formula',
  //   },
  // ]
  // const SelectYear = [
  //   {
  //     id: 1,
  //     label: '2021',
  //     value: '2021',
  //   },
  //   {
  //     id: 2,
  //     label: '2022',
  //     value: '2022',
  //   },
  // ]

  const [startDates, setstartDate] = useState([])
  // const [endDates, setendDate] = useState()

  const [data, setData] = useState(MonthSelect)

  // function getMonthDifference(startDate, endDate) {
  //   return (
  //     endDate.getMonth() -
  //     startDate.getMonth() +
  //     12 * (endDate.getFullYear() - startDate.getFullYear())
  //   )
  // }
  var monthly = new Array([])
  monthly[0] = 'January'
  monthly[1] = 'February'
  monthly[2] = 'March'
  monthly[3] = 'April'
  monthly[4] = 'May'
  monthly[5] = 'June'
  monthly[6] = 'July'
  monthly[7] = 'August'
  monthly[8] = 'September'
  monthly[9] = 'October'
  monthly[10] = 'November'
  monthly[11] = 'December'
  let oblock = []
  let currentMonth = moment([2021, 6, 1]).format('MM')
  let LastD = moment(currentMonth + 11)
    .month(11)
    .format('MM')
  for (let n = currentMonth; n <= LastD; n++) {
    oblock.push(monthly[n % 12])
  }
  const onFilterChange = (event) => {
    const selected = event.target.value

    const filterList = MonthSelect.filter((item) => {
      return Number(item.id) >= selected
    })
    setData(filterList)
  }

  const monthlyDateRange = () => {
    // const startmonth = moment(new Date('11/22/2022')).format('MMMM-YY')
    const endmonth = moment(new Date('12/25/2023')).format('MMMM-YY')

    const currentMonth = moment(new Date()).format('MMMM-YY')
    const oblock = []

    for (let n = currentMonth; n < endmonth; n++) {
      oblock.push([monthly[n], n + 1])
    }

    // console.log(
    //   oblock,
    //   currentMonth,
    //   startmonth,
    //   endmonth,
    //   'check the current month'
    // )
    // // console.log(object, monthly, 'update Month')
  }

  useEffect(() => {
    monthlyDateRange()
  })

  const handleChanges = (e, index) => {
    let { name, value } = e.target
    setstartDate({
      ...startDates,
      [name]: value,
    })

    const filterList = MonthSelect.filter((item, i) => {
      return i !== index
    })
    setData(filterList)
    // console.log(startDates, 'filter date pattrent')
  }

  // const FilterByCurrent = (items, index) => {
  //   for (let i = MonthSelect.length - 1; i >= 0; i--) {
  //     MonthSelect.splice(i, 2)
  //     // console.log(items, 'asdasdasd')
  //   }
  //   // console.log(items, index, 'asdasdasd')
  // }

  // console.log(startDates, 'date view')
  return (
    <div className="Fees_block">
      <div className="flex form_desgin mb30">
        <div className={`custom-field selections_box`}>
          <label>
            <i className={`fa fa-snowflake-o`}></i>
            <h6 className="margin0">Select Month</h6>
          </label>

          <select
            className="selection_box mb30"
            onChange={onFilterChange}
            defaultValue={MonthSelect.value}
            value={MonthSelect.value}
          >
            {MonthSelect.map((val) => {
              return (
                <option value={val.id} key={val.value}>
                  {val.label}
                </option>
              )
            })}
          </select>
        </div>

        <div className={`custom-field selections_box`}>
          <label>
            <i className={`fa fa-snowflake-o`}></i>
            <h6 className="margin0">Select Formula</h6>
          </label>
          <Controller
            control={control}
            name="SelectFormula"
            render={({ field }) => (
              <ReactSelect
                {...field}
                value={formula.label}
                options={formula}
                styles={customStyles}
              />
            )}
          />
        </div>

        <button type="button" className="btn-normal">
          Reload Default
        </button>
      </div>
      <table className="grid_by_table mb30">
        <thead>
          <tr>
            <th>Month no.</th>
            <th>Start Month</th>
            <th>End Month</th>
            <th>Amount</th>
            <th>Discount</th>
            <th>Formula</th>
            <th>Years</th>
          </tr>
        </thead>
        <tbody>
          {data.map((val, index) => {
            return (
              <tr key={index}>
                <td>{val.id}</td>
                <td>{MonthSelect.value ? data.value : val.value}</td>
                <td>
                  {
                    // <DatePicker
                    //   selected={index && endDates}
                    //   onChange={(date, index) => {
                    //     setendDate(date)
                    //     getMonthDifference(startDates, endDates)
                    //   }}
                    //   name={val.id}
                    //   dateFormat="MM/yyyy"
                    //   showMonthYearPicker
                    //   showFullMonthYearPicker
                    //   showFourColumnMonthYearPicker
                    // />
                    // <DatePicker
                    //   selected={startDates}
                    //   placeholderText="Select Date"
                    //   onChange={(date) => setstartDate(date)}
                    //   maxDate={new Date(val.value)}
                    //   filterDate={testing}
                    //   showMonthYearPicker
                    //   showFullMonthYearPicker
                    //   showFourColumnMonthYearPicker
                    // />
                    // <DatePicker
                    //   dateFormat="MMM-yyyy"
                    //   selected={startDates[index]}
                    //   placeholderText="End date"
                    //   onChange={(date) => {
                    //     setstartDate(index[date])
                    //     // getMonthDifference(new Date('01-07-2023', index))
                    //   }}
                    //   minDate={new Date('01-08-2022')}
                    //   maxDate={new Date('01-07-2023')}
                    //   showMonthYearPicker
                    // />
                    <select
                      className="selection_box mb30"
                      // onChange={(e, index) =>
                      //   setstartDate({
                      //     ...startDates,
                      //     [e.target.name]: e.target.value,
                      //   })
                      // }
                      onChange={(e) => handleChanges(e, index)}
                      name={val.value}
                      value={startDates[index]}
                    >
                      {data.map((vale) => {
                        return (
                          <option value={vale.value} key={vale.value}>
                            {vale.label}
                          </option>
                        )
                      })}
                    </select>
                  }
                </td>
                <td>{1000 + val.id}</td>
                <td>
                  {/* <input type="number" value={val.id} defaultValue={val.id}  /> */}
                  {val.id}
                </td>
                <td>1</td>
                <td>{val.years}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}
export default FeeManage
