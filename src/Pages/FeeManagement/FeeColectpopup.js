import React from 'react'
import Attachment from '../../Componets/FilesUpload/Attachment'
import NumberFormat from 'react-number-format'
// import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import ReactSelect from 'react-select'
function FeeColectpopup(props) {
  return (
    <div className="Payment_collect">
      <h4 className="mb30">Full Payment / Partial payment </h4>
      <ul className="Payments_collections">
        {props.inputList.map((x, i) => {
          return (
            <li key={i}>
              <div
                className={
                  x.paymentMethod === 2 || x.paymentMethod === 3
                    ? 'flex form_desgin twocolumns'
                    : `flex form_desgin onlyone`
                }
              >
                <div className={`custom-field selections_box`}>
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">Select Mathod</h6>
                  </label>
                  <ReactSelect
                    options={props.listofoptions}
                    value={props.listofoptions?.filter(
                      (o) => o.value === x.paymentMethod
                    )}
                    styles={props.customStyles}
                    onChange={(option) =>
                      props.handleRoomChange(option, i, 'paymentMethod')
                    }
                  />
                </div>
                {x.paymentMethod === 2 || x.paymentMethod === 3 ? (
                  <Attachment className="mb5 uploadedImage" />
                ) : null}
              </div>

              <div className="addMore flex">
                {x.paymentMethod === 1 ? (
                  <div className="add_row">
                    {props.inputList.length - 1 === i && (
                      <button
                        type="button"
                        className="add_element"
                        onClick={props.AddMore}
                      >
                        <i className="fa fa-plus"></i>
                      </button>
                    )}
                    {props.inputList.length !== 1 && (
                      <button
                        type="button"
                        onClick={() => props.HandleRemove(i)}
                      >
                        <i className="fa fa-trash"></i>
                      </button>
                    )}
                  </div>
                ) : x.paymentMethod === 2 ? (
                  <div className="add_row">
                    {props.inputList.length - 1 === i && (
                      <button
                        type="button"
                        className="add_element"
                        onClick={props.AddMore}
                      >
                        <i className="fa fa-plus"></i>
                      </button>
                    )}
                    {props.inputList.length !== 1 && (
                      <button
                        type="button"
                        onClick={() => props.HandleRemove(i)}
                      >
                        <i className="fa fa-trash"></i>
                      </button>
                    )}
                  </div>
                ) : x.paymentMethod === 3 ? (
                  <div className="add_row">
                    {props.inputList.length - 1 === i && (
                      <button
                        type="button"
                        className="add_element"
                        onClick={props.AddMore}
                      >
                        <i className="fa fa-plus"></i>
                      </button>
                    )}
                    {props.inputList.length !== 1 && (
                      <button
                        type="button"
                        onClick={() => props.HandleRemove(i)}
                      >
                        <i className="fa fa-trash"></i>
                      </button>
                    )}
                  </div>
                ) : null}

                {x.paymentMethod === 1 ? (
                  <>
                    <div className="flex form_desgin">
                      <div className={`custom-field idGenerate`}>
                        <NumberFormat
                          name="Payment"
                          onChange={(e) => props.FelidsInput(e, i)}
                          format="#####"
                          mask=""
                        />

                        <label htmlFor="ids8">
                          <i className="fa fa-user-o"></i>
                          <h6 className="margin0">Payment Rs.</h6>
                        </label>
                      </div>

                      <div className={`custom-field idGenerate`}>
                        <NumberFormat
                          name="LateFee"
                          format="#####"
                          onChange={(e) => props.FelidsInput(e, i)}
                          mask=""
                        />

                        <label htmlFor="ids8">
                          <i className="fa fa-user-o"></i>
                          <h6 className="margin0">Late Charges</h6>
                        </label>
                      </div>
                      <div className={`datePick custom-field idGenerate`}>
                        <DatePicker
                          selected={x.PaymentDate}
                          monthsShown={1}
                          onChange={(date) =>
                            props.DateChange(date, i, 'PaymentDate')
                          }
                          // onChange={(date) => FelidsInput(date, i)}
                        />

                        <label>
                          <i className={`fa fa-calendar`}></i>
                          <h6 className="margin0">
                            <span></span>
                            Date Of Payment
                          </h6>
                        </label>
                      </div>

                      <div className={`custom-field fullwidth`}>
                        <textarea
                          autoComplete="off"
                          type="text"
                          name="remarks"
                          placeholder="remarks"
                          onChange={(e) => props.FelidsInput(e, i)}
                        />
                        <label htmlFor="remarks">
                          <i className={'fa fa-envelope-o'}></i>
                          <h6 className="margin0">Remarks</h6>
                        </label>
                      </div>
                    </div>
                    <div className="Crud_actions">
                      <button className="btn-normal clr2" type="button">
                        Accept
                      </button>
                    </div>
                  </>
                ) : x.paymentMethod === 2 ? (
                  <>
                    <div className="flex form_desgin">
                      <div className={`custom-field idGenerate`}>
                        <NumberFormat
                          name="Payment"
                          onChange={(e) => props.FelidsInput(e, i)}
                          format="#####"
                          mask=""
                        />

                        <label htmlFor="ids8">
                          <i className="fa fa-user-o"></i>
                          <h6 className="margin0">Payment Rs.</h6>
                        </label>
                      </div>

                      <div className={`custom-field selections_box`}>
                        <ReactSelect
                          // defaultValue={religionId}
                          name="Bank"
                          styles={props.customStyles}
                          options={[
                            {
                              label: 'MCB Bank',
                              value: 'mcb',
                            },
                            {
                              label: 'SCB Bank',
                              value: 'SCB',
                            },
                            {
                              label: 'AL Bank',
                              value: 'AL',
                            },
                            {
                              label: 'HBL Bank',
                              value: 'HBL',
                            },
                          ]}
                        />

                        <label htmlFor="ids8">
                          <h6 className="margin0">Select Bank</h6>
                        </label>
                      </div>
                      <div className={`custom-field idGenerate`}>
                        <NumberFormat
                          name="Payment"
                          onChange={(e) => props.FelidsInput(e, i)}
                          format="##########"
                          mask=""
                        />

                        <label htmlFor="ids8">
                          <i className="fa fa-user-o"></i>
                          <h6 className="margin0">ChequeNo</h6>
                        </label>
                      </div>
                      <div className={`datePick custom-field idGenerate`}>
                        <DatePicker
                          selected={x.PaymentDate}
                          monthsShown={1}
                          onChange={(date) =>
                            props.DateChange(date, i, 'PaymentDate')
                          }
                          // onChange={(date) => FelidsInput(date, i)}
                        />

                        <label>
                          <i className={`fa fa-calendar`}></i>
                          <h6 className="margin0">
                            <span></span>
                            ChequeDate
                          </h6>
                        </label>
                      </div>
                      <div className={`custom-field selections_box`}>
                        <ReactSelect
                          options={props.Chequestatus}
                          value={props.Chequestatus.value}
                          styles={props.customStyles}
                          onChange={(option) =>
                            props.handleRoomChange(option, i, 'chequeStatus')
                          }
                        />

                        <label htmlFor="ids8">
                          <h6 className="margin0">Cheque Status</h6>
                        </label>
                      </div>

                      {x.chequeStatus === 'Cleared' ||
                      x.chequeStatus === 'Disowned' ? (
                        <div className={`datePick custom-field idGenerate`}>
                          <DatePicker
                            selected={x.PaymentDate}
                            monthsShown={1}
                            onChange={(date) =>
                              props.DateChange(date, i, 'PaymentDate')
                            }
                            // onChange={(date) => FelidsInput(date, i)}
                          />

                          <label>
                            <i className={`fa fa-calendar`}></i>
                            <h6 className="margin0">
                              <span></span>
                              Status Date
                            </h6>
                          </label>
                        </div>
                      ) : null}
                      <div className={`custom-field fullwidth`}>
                        <textarea
                          autoComplete="off"
                          type="text"
                          name="remarks"
                          placeholder="remarks"
                          onChange={(e) => props.FelidsInput(e, i)}
                        />
                        <label htmlFor="remarks">
                          <i className={'fa fa-envelope-o'}></i>
                          <h6 className="margin0">remarks</h6>
                        </label>
                      </div>
                    </div>
                    <div className="Crud_actions">
                      <button className="btn-normal clr2" type="button">
                        Accept
                      </button>
                    </div>
                  </>
                ) : x.paymentMethod === 3 ? (
                  <>
                    <div className="flex form_desgin">
                      <div className={`custom-field idGenerate`}>
                        <NumberFormat
                          name="Payment"
                          onChange={(e) => props.FelidsInput(e, i)}
                          format="#####"
                          mask=""
                        />

                        <label htmlFor="ids8">
                          <i className="fa fa-user-o"></i>
                          <h6 className="margin0">Payment Rs.</h6>
                        </label>
                      </div>

                      <div className={`datePick custom-field idGenerate`}>
                        <DatePicker
                          selected={x.PaymentDate}
                          monthsShown={1}
                          onChange={(date) =>
                            props.DateChange(date, i, 'PaymentDate')
                          }
                          // onChange={(date) => FelidsInput(date, i)}
                        />

                        <label>
                          <i className={`fa fa-calendar`}></i>
                          <h6 className="margin0">
                            <span></span>
                            Date Of Payment
                          </h6>
                        </label>
                      </div>
                      <div className={`custom-field selections_box`}>
                        <ReactSelect
                          // defaultValue={religionId}
                          name="Bank"
                          styles={props.customStyles}
                          options={[
                            {
                              label: 'MCB Bank',
                              value: 'mcb',
                            },
                            {
                              label: 'SCB Bank',
                              value: 'SCB',
                            },
                            {
                              label: 'AL Bank',
                              value: 'AL',
                            },
                            {
                              label: 'HBL Bank',
                              value: 'HBL',
                            },
                          ]}
                        />

                        <label htmlFor="ids8">
                          <h6 className="margin0">Select Bank</h6>
                        </label>
                      </div>
                      <div className={`custom-field idGenerate`}>
                        <NumberFormat
                          name="TransitionID"
                          onChange={(e) => props.FelidsInput(e, i)}
                          format="#############################"
                          mask=""
                        />

                        <label htmlFor="ids8">
                          <i className="fa fa-user-o"></i>
                          <h6 className="margin0">TransitionID</h6>
                        </label>
                      </div>
                      <div className={`custom-field fullwidth `}>
                        <textarea
                          autoComplete="off"
                          type="text"
                          name="remarks"
                          placeholder="remarks"
                          onChange={(e) => props.FelidsInput(e, i)}
                        />
                        <label htmlFor="remarks">
                          <i className={'fa fa-envelope-o'}></i>
                          <h6 className="margin0">Remarks</h6>
                        </label>
                      </div>
                    </div>
                    <div className="Crud_actions">
                      <button className="btn-normal clr2" type="button">
                        Accept
                      </button>
                    </div>
                  </>
                ) : (
                  <></>
                )}
              </div>
              {/* <div className="Total_calculate">
                      <h4>{x.Amout}</h4>
                      <h4>{x.lateFee}</h4>
                      <h4>{x.paymentMethod}</h4>
                      <h4>{x.PaymentDate}</h4>
                      <h4>{x.ReceviedBy}</h4>
                      <h4>{x.Remarks}</h4>
                      <h4>{x.CurrentDate}</h4>
                      <h4>{x.BankName}</h4>
                      <h4>{x.ChequeNo}</h4>
                      <h4>{x.ChequeDate}</h4>
                      <h4>{x.ChequeAmount}</h4>
                      <h4>{x.chequeStatus}</h4>
                      <h4>{x.RecevingDate}</h4>
                      <h4>{x.Bank}</h4>
                      <h4>{x.TransitionID}</h4>
                    </div> */}
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default FeeColectpopup
