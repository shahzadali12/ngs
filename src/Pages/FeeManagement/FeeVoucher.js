import React, {
  useState,
  useContext,
  useCallback,
  useRef,
  useEffect,
} from 'react'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { schema } from '../../Componets/schema/Schema'
import { useNavigate } from 'react-router-dom'
import ReactSelect from 'react-select'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'

import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import mainContext from '../../ContextApi/main'

const FeeVoucher = () => {
  const gridRef = useRef()
  const regcontxt = useContext(mainContext)
  const [rowData, setRowData] = useState()
  const {
    feeTemplate,
    formData,
    setFormData,
    setstep,

    regkey,
    setKey,

    ngs,
    setHeading,
    Statevalues,
  } = regcontxt
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    getValues,
    trigger,
    reset,
    handleChange,

    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'blur',
  })

  const customStyles = {
    menuPortal: (base) => ({
      ...base,

      zIndex: 9999,
      '&:hover': {
        backgroundColor: 'red',
        color: 'white',
      },
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: '9999 !important',
      backgroundColor: '#fff',
      color: '#333',
      fontSize: '14px',
    }),

    control: (provided) => ({
      ...provided,
      margin: 0,
      border: '0px solid black',
      backgroundColor: 'white',
      outline: 'none',
      minHeight: '36px',
      fontSize: '14px',
    }),
  }
  const lstRole = [
    {
      id: 1,
      label: 'Administrative',
      value: '5051',
    },
    {
      id: 2,
      label: 'Academic',
      value: '4032',
    },
    {
      id: 3,
      label: 'Maneger',
      value: '2033',
    },
  ]
  const Status = [
    {
      id: 1,
      label: 'Active',
      value: '5054',
    },
    {
      id: 2,
      label: 'Deactivated',
      value: '4035',
    },
  ]

  const defaultColDef = useCallback(() => {
    return {
      flex: 1,
      minWidth: 70,
      resizable: false,
      filter: 'agSetColumnFilter',
      headerCheckboxSelection: isFirstColumn,
      checkboxSelection: isFirstColumn,
    }
  })

  const [columnDefs, setColumnDefs] = useState([
    {
      headerCheckboxSelection: isFirstColumn,
      checkboxSelection: isFirstColumn,
      suppressMenu: true,
      suppressSizeToFit: true,
      minWidth: 70,
      width: 70,
      filter: 'agSetColumnFilter',
      // rowGroup: true,
      // cellRendererParams: {
      //   checkbox: true,
      // },
      cellRenderer: ({ data }) => {
        return // console.log(data)
      },
    },
    {
      field: 'id',
      headerName: 'SR#',
      suppressMenu: true,
      suppressSizeToFit: true,
      minWidth: 70,
      width: 70,
      filter: 'agSetColumnFilter',
    },
    {
      field: 'studentPicture',
      headerName: 'Image',
      suppressMenu: true,

      cellRenderer: ({ params }) => {
        // // console.log(params, 'params params')
        return (
          <img
            className="Radius-imge"
            style={{ height: '45px', width: '45px' }}
            // src={`http://39.61.53.93:8001/api${params}`}
            src={`https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0FIiZZbXHs4JkgRVWKTciwdWW_67I0p5hYg&usqp=CAU`}
            alt={params}
          />
        )
      },
      minWidth: 80,
      width: 80,
    },

    {
      field: 'studentFirstName',
      headerName: 'Name',
      minWidth: 100,
      width: 100,
      filter: 'agSetColumnFilter',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'id',
      headerName: 'Student Id',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 120,
      width: 120,
      filter: 'agSetColumnFilter',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'studentFirstName',
      headerName: 'Class',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 120,
      width: 120,
      filter: 'agSetColumnFilter',
      suppressMenu: false,
      suppressSizeToFit: true,
    },

    {
      field: 'studentFirstName',
      headerName: 'Fee Category',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
      filter: 'agSetColumnFilter',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'studentFirstName',
      headerName: 'Fee Month',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
      filter: 'agDateColumnFilter',
      // filterParams: dateFilterParams,
    },
    {
      field: 'id',
      headerName: 'Amount',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
      filter: 'agSetColumnFilter',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'studentFirstName',
      headerName: 'Discount',
      suppressMenu: false,
      minWidth: 150,
      width: 150,
      suppressSizeToFit: true,
      filter: 'agSetColumnFilter',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'studentFirstName',
      headerName: 'Paid',
      suppressMenu: false,
      minWidth: 150,
      width: 150,
      suppressSizeToFit: true,
      filter: 'agSetColumnFilter',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'studentFirstName',
      headerName: 'Dues',
      suppressMenu: false,
      minWidth: 150,
      width: 150,
      suppressSizeToFit: true,
      filter: 'agSetColumnFilter',
      suppressMenu: false,
      suppressSizeToFit: true,
    },

    {
      field: 'Action',
      // cellRendererParams: { condition: myCondition },
      cellRenderer: ({ data }) => {
        return (
          <div className="actionButtons">
            {/* <i
                onClick={() => handleEdit(data.id)}
                className="fa fa-pencil-square-o"
                aria-hidden="true"
              ></i> */}

            <i
              onClick={() => handlePrint(data.id)}
              className="fa fa-print"
              aria-hidden="true"
            ></i>
            <i
              onClick={() => handleDelate(data.id)}
              className="fa fa-ban"
              aria-hidden="true"
            ></i>

            {/* {data && data.expiryDate < moment(new Date()).format() ? null : (
                <i
                  onClick={() => BlockRow(data.id)}
                  className="fa fa-ban"
                  aria-hidden="true"
                ></i>
              )} */}
          </div>
        )
      },
      suppressMenu: false,
      filter: false,
      pinned: 'right',
      minWidth: 100,
      width: 100,
      suppressSizeToFit: true,
    },
  ])
  function isFirstColumn(params) {
    var displayedColumns = params.columnApi.getAllDisplayedColumns()
    var thisIsFirstColumn = displayedColumns[0] === params.column

    return thisIsFirstColumn
  }

  // const onQuickFilterChanged = () => {
  //   gridApi.setQuickFilter(document.getElementById('quickFilter').value);
  // };
  //   const isRowSelectable = useMemo(() => {
  //     return (params) => {
  //       return !!params.data && params.data.expiryDate < moment().format()
  //     }
  //   }, [])
  // gridApi.refreshCells()
  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }
  // const onGridReady = useCallback((params) => {
  //   fetch('http://172.17.0.15:8001/api/Registration/RegistrationList?LoginId=1')
  //     .then((resp) => resp.json())
  //     .then((data) => {
  //       setRowData(data)
  //     })
  // }, [])
  const navigate = useNavigate()
  const handleEdit = (field, regdata) => {
    // setKey(field)
    navigate('/Voucher')
  }

  const handleDelate = (field) => {
    // PrintVoucherToggle()
    // setKey(field)
    // // console.log(field, 'here is data..................')
    navigate('/Voucher')
  }
  const handlePrint = (field) => {
    // PrintVoucherToggle()
    setKey(field)
    // // console.log(field, 'here is data..................')
    navigate('/Voucher')
    // setstep(4)
  }
  const [startDate, setStartDate] = useState(new Date())
  const [activeTab, setActiveTab] = useState('tab1')
  const activeToggle = (e) => {
    setActiveTab(e)
  }

  return (
    <div className=" Grid_view ">
      <div className="col-12">
        <form className="mb30">
          <div className="flex form_desgin">
            <div className="heading sprator_line">
              <h5>Fee Voucher Printing</h5>
            </div>
            <div className="column-100">
              <div className="flex">
                <div className={`custom-field`}>
                  <input
                    id="ids7"
                    {...register('Bystudent', { required: true })}
                    name="Bystudent"
                    placeholder="Search Student ID / Name"
                  />
                  <label htmlFor="ids7">
                    <i className="fa fa-user-o"></i>
                    <h6 className="margin0">Search Name / ID</h6>
                  </label>
                </div>

                <div className={`custom-field selections_box`}>
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">Select Month</h6>
                  </label>
                  <Controller
                    control={control}
                    name="SelectMonth"
                    render={({ field }) => (
                      <DatePicker
                        selected={startDate}
                        onChange={(date) => setStartDate(date)}
                        dateFormat="MM/yyyy"
                        showMonthYearPicker
                      />
                    )}
                  />
                </div>

                <div className={`custom-field selections_box`}>
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">Select Class</h6>
                  </label>
                  <Controller
                    control={control}
                    name="Classes"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        value={lstRole.label}
                        options={lstRole}
                        styles={customStyles}
                      />
                    )}
                  />
                </div>

                <div className="button_full_grid">
                  <button type="button" className="btn-normal">
                    {/* <i className="fa fa-search" aria-hidden="true"></i> */}
                    Search
                  </button>
                  <button type="button" className="btn-normal">
                    {/* <i className="fa fa-print" aria-hidden="true"></i> */}
                    Generate
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div className="col-12">
        <AgGridReact
          className="ag-theme-alpine h100"
          columnDefs={columnDefs}
          defaultColDef={defaultColDef}
          alwaysShowHorizontalScroll={true}
          alwaysShowVerticalScroll={true}
          onGridReady={onGridReady}
          rowData={rowData}
          pagination={true}
          suppressRowClickSelection={true}
          rowSelection={'multiple'}
          // rowClassRules={rowClassRules}
          // isRowSelectable={isRowSelectable}
          // autoGroupColumnDef={autoGroupColumnDef}
        />
      </div>
    </div>
  )
}

export default FeeVoucher
