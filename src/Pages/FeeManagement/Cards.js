// import moment from 'moment'
import React, { useRef } from 'react'
// import { Link } from 'react-router-dom'
import ReactToPrint, {
  PrintContextConsumer,
  // useReactToPrint,
} from 'react-to-print'
import QRCode from 'qrcode.react'
function Cards() {
  const ref = useRef()

  // const reactToPrintContent = useCallback(() => {
  //   return ref.current
  // }, [ref])

  // const handleAfterPrint = React.useCallback(() => {
  //   // console.log('`onAfterPrint` called') // tslint:disable-line no-console
  // }, [])

  const profile = [
    {
      id: 1,
      CardID: '221015',
      name: 'Ananya Faheem',
      image:
        'https://thumbs.dreamstime.com/b/cute-little-indian-indian-asian-school-boy-wearing-uniform-cute-little-indian-indian-asian-school-boy-wearing-uniform-150099692.jpg',
      gender: 'Female',
      fatherName: 'Faheem',
      motherName: 'Saira',
      dateOfBrith: '04/09/2022',
      religion: 'islam',
      fatherOccupation: 'Business man',
      email: 'Ananya@info.com',
      admissionDate: '05/09/2022',
      class: 2,
      section: '1A',
      JoiningDate: '05/09/2021',
      cardExpiry: '05/09/2022',
      Address: '260 upper Mall Lahore',
      phoneNo: '+92 312 4326247',
      phone: '35750855-66',
    },
    // {
    //   id: 2,
    //   CardID: '221015',
    //   name: 'Asif',
    //   gender: 'Male',
    //   image:
    //     'https://1.bp.blogspot.com/-gbQQzsxDR8w/XId73eyzmRI/AAAAAAAAI10/gGhaedYf2O4vLFY4eXu_6PBW2ohCA5R9ACLcBGAs/s1600/namya-joshi.JPG',
    //   fatherName: 'Asif',
    //   motherName: 'Asif',
    //   dateOfBrith: '04/09/2022',
    //   religion: 'islam',
    //   fatherOccupation: 'Business man',
    //   email: 'Ananya@info.com',
    //   admissionDate: '05/09/2022',
    //   class: 2,
    //   section: '2A',
    //   JoiningDate: '05/09/2021',
    //   cardExpiry: '05/09/2022',
    //   Address: '260 upper Mall Lahore',
    //   phoneNo: '+92 312 4326247',
    //   phone: '35750855-66',
    // },
    // {
    //   id: 3,
    //   CardID: '221015',
    //   name: 'Tanveer',
    //   gender: 'Male',
    //   image:
    //     'https://content.instructables.com/ORIG/F4R/O2KR/J16P2AE6/F4RO2KRJ16P2AE6.jpg',
    //   fatherName: 'Tanveer',
    //   motherName: 'Tanveer',
    //   dateOfBrith: '04/09/2022',
    //   religion: 'islam',
    //   fatherOccupation: 'Business man',
    //   email: 'Ananya@info.com',
    //   admissionDate: '05/09/2022',
    //   class: 2,
    //   section: '1A',
    //   JoiningDate: '05/09/2021',
    //   cardExpiry: '05/09/2022',
    //   Address: '260 upper Mall Lahore',
    //   phoneNo: '+92 312 4326247',
    //   phone: '35750855-66',
    // },
    // {
    //   id: 4,
    //   CardID: '221015',
    //   name: 'Sohail',
    //   gender: 'Female',
    //   image:
    //     'https://www.dfa.ie/media/dfa/passport/passportphotographs/Facial-Features---Acceptable-300x449.jpg',
    //   fatherName: 'Ali',
    //   motherName: 'Ali',
    //   dateOfBrith: '04/09/2022',
    //   religion: 'islam',
    //   fatherOccupation: 'Business man',
    //   email: 'Ananya@info.com',
    //   admissionDate: '05/09/2022',
    //   class: 2,
    //   section: '1A',
    //   JoiningDate: '05/09/2021',
    //   cardExpiry: '05/09/2022',
    //   Address: '260 upper Mall Lahore',
    //   phoneNo: '+92 312 4326247',
    //   phone: '35750855-66',
    // },
    // {
    //   id: 4,
    //   CardID: '221015',
    //   name: 'Sohail',
    //   gender: 'Female',
    //   image:
    //     'https://www.dfa.ie/media/dfa/passport/passportphotographs/Facial-Features---Acceptable-300x449.jpg',
    //   fatherName: 'Ali',
    //   motherName: 'Ali',
    //   dateOfBrith: '04/09/2022',
    //   religion: 'islam',
    //   fatherOccupation: 'Business man',
    //   email: 'Ananya@info.com',
    //   admissionDate: '05/09/2022',
    //   class: 2,
    //   section: '1A',
    //   JoiningDate: '05/09/2021',
    //   cardExpiry: '05/09/2022',
    //   Address: '260 upper Mall Lahore',
    //   phoneNo: '+92 312 4326247',
    //   phone: '35750855-66',
    // },
    // {
    //   id: 4,
    //   CardID: '221015',
    //   name: 'Sohail',
    //   gender: 'Female',
    //   image:
    //     'https://www.dfa.ie/media/dfa/passport/passportphotographs/Facial-Features---Acceptable-300x449.jpg',
    //   fatherName: 'Ali',
    //   motherName: 'Ali',
    //   dateOfBrith: '04/09/2022',
    //   religion: 'islam',
    //   fatherOccupation: 'Business man',
    //   email: 'Ananya@info.com',
    //   admissionDate: '05/09/2022',
    //   class: 2,
    //   section: '1A',
    //   JoiningDate: '05/09/2021',
    //   cardExpiry: '05/09/2022',
    //   Address: '260 upper Mall Lahore',
    //   phoneNo: '+92 312 4326247',
    //   phone: '35750855-66',
    // },
    // {
    //   id: 4,
    //   CardID: '221015',
    //   name: 'Sohail',
    //   gender: 'Female',
    //   image:
    //     'https://www.dfa.ie/media/dfa/passport/passportphotographs/Facial-Features---Acceptable-300x449.jpg',
    //   fatherName: 'Ali',
    //   motherName: 'Ali',
    //   dateOfBrith: '04/09/2022',
    //   religion: 'islam',
    //   fatherOccupation: 'Business man',
    //   email: 'Ananya@info.com',
    //   admissionDate: '05/09/2022',
    //   class: 2,
    //   section: '1A',
    //   JoiningDate: '05/09/2021',
    //   cardExpiry: '05/09/2022',
    //   Address: '260 upper Mall Lahore',
    //   phoneNo: '+92 312 4326247',
    //   phone: '35750855-66',
    // },
    // {
    //   id: 4,
    //   CardID: '221015',
    //   name: 'Sohail',
    //   gender: 'Female',
    //   image:
    //     'https://www.dfa.ie/media/dfa/passport/passportphotographs/Facial-Features---Acceptable-300x449.jpg',
    //   fatherName: 'Ali',
    //   motherName: 'Ali',
    //   dateOfBrith: '04/09/2022',
    //   religion: 'islam',
    //   fatherOccupation: 'Business man',
    //   email: 'Ananya@info.com',
    //   admissionDate: '05/09/2022',
    //   class: 2,
    //   section: '1A',
    //   JoiningDate: '05/09/2021',
    //   cardExpiry: '05/09/2022',
    //   Address: '260 upper Mall Lahore',
    //   phoneNo: '+92 312 4326247',
    //   phone: '35750855-66',
    // },
    // {
    //   id: 4,
    //   CardID: '221015',
    //   name: 'Sohail',
    //   gender: 'Female',
    //   image:
    //     'https://www.dfa.ie/media/dfa/passport/passportphotographs/Facial-Features---Acceptable-300x449.jpg',
    //   fatherName: 'Ali',
    //   motherName: 'Ali',
    //   dateOfBrith: '04/09/2022',
    //   religion: 'islam',
    //   fatherOccupation: 'Business man',
    //   email: 'Ananya@info.com',
    //   admissionDate: '05/09/2022',
    //   class: 2,
    //   section: '1A',
    //   JoiningDate: '05/09/2021',
    //   cardExpiry: '05/09/2022',
    //   Address: '260 upper Mall Lahore',
    //   phoneNo: '+92 312 4326247',
    //   phone: '35750855-66',
    // },
  ]
  // const time = moment(new Date()).format('hh:mm:ss')
  // const CDate = moment(new Date()).format('DD/MM/YY')

  //   const totalsum = Arry[0].AllHeads?.FeeHeads.reduce(
  //     (currentSum, nextObject) => {
  //       return currentSum + +nextObject.Fee
  //     },
  //     null
  //   )

  //   const Grand = +Arry[0].AllHeads?.Remaining + totalsum
  //   const AdvancePayment = Grand - Arry[0].AllHeads?.AdvanceFee
  //   const afterDueTotal = AdvancePayment + 100
  //   // console.log(AdvancePayment, 'total sum')

  const ComponentToPrint = React.forwardRef((props, ref) => {
    return (
      <>
        {/* <button className="btn-normal" onClick={handlePrint}>
          DownLoad file
        </button> */}

        <div className="Parent_infomation" ref={ref}>
          <div className="flex ">
            {profile &&
              profile.map((value, index) => {
                return (
                  <>
                    <div className="flex StudentCards" key={index}>
                      <div className="custom-field Student_card extraSize">
                        <figure>
                          <img
                            src={process.env.PUBLIC_URL + 'logoB.png'}
                            alt="img here"
                          />
                          <h5>National Grammar School</h5>
                        </figure>
                        <div className="student_image">
                          <img src={value.image} alt="imghere" />
                        </div>
                        <div className="Card_base">
                          <div className="sib_none">
                            <h5>{value.name}</h5>
                            <h5>{value.section}</h5>
                          </div>
                          <div className="info_crd">
                            <ul>
                              <li className="none">
                                Name :<span>{value.name}</span>
                              </li>
                              <li className="none">
                                section :<span>{value.section}</span>
                              </li>
                              <li>
                                Student ID :<span>{value.CardID}</span>
                              </li>
                              <li>
                                Joining Date :<span>{value.JoiningDate}</span>
                              </li>
                              <li className="sib_none">
                                Card Expiry :<span>{value.cardExpiry}</span>
                              </li>
                              <li>
                                Phone no :<span>{value.phoneNo}</span>
                              </li>
                              <li>{value.Address}</li>
                            </ul>
                            <div className="QrCodes">
                              <QRCode
                                value={[
                                  `
Name: ${value.name} 
ID: ${value.CardID} 
Section: ${value.section} 
Phone No: ${value.phoneNo} 
                           `,
                                ]}
                                size={50}
                                level={'M'}
                                bgColor={'transparent'}
                                fgColor={'#000'}
                                includeMargin={false}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="BackSide">
                        <img
                          src={process.env.PUBLIC_URL + '/l2.png'}
                          alt="logo"
                        />
                        <span>www.ngs.edu.pk</span>
                      </div>
                    </div>
                    <div className="flex StudentCards PreSchool">
                      <div className="custom-field Student_card extraSize">
                        <figure>
                          <img
                            src={process.env.PUBLIC_URL + 'logo_back.png'}
                            alt="img here"
                          />
                        </figure>
                        <div className="student_image">
                          <div className="borderAfter">
                            <img src={value.image} alt="imghere" />
                          </div>
                        </div>
                        <div className="Card_base">
                          <div className="sib_none">
                            <h5>{value.name}</h5>
                            <h5>{value.section}</h5>
                          </div>
                          <div className="info_crd">
                            <ul>
                              <li className="none">
                                Name :<span>{value.name}</span>
                              </li>
                              <li className="none">
                                section :<span>{value.section}</span>
                              </li>
                              <li>
                                Student ID :<span>{value.CardID}</span>
                              </li>
                              <li>
                                Joining Date :<span>{value.JoiningDate}</span>
                              </li>
                              <li className="sib_none">
                                Card Expiry :<span>{value.cardExpiry}</span>
                              </li>
                              <li>
                                Phone no :<span>{value.phoneNo}</span>
                              </li>
                              <li>{value.Address}</li>
                            </ul>
                            <div className="QrCodes">
                              <QRCode
                                value={[
                                  `
Name: ${value.name} 
ID: ${value.CardID} 
Section: ${value.section} 
Phone No: ${value.phoneNo} 
                           `,
                                ]}
                                size={50}
                                level={'M'}
                                bgColor={'transparent'}
                                fgColor={'#000'}
                                includeMargin={false}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="BackSide">
                        <img
                          src={process.env.PUBLIC_URL + '/plogo.png'}
                          alt="logo"
                        />
                        <span>www.ngs.edu.pk</span>
                      </div>
                    </div>
                  </>
                )
              })}
          </div>
        </div>
      </>
    )
  })
  // https://codesandbox.io/s/jspdf-download-notification-o0v3ky?file=/src/App.js
  return (
    <>
      <ReactToPrint content={() => ref.current}>
        <PrintContextConsumer>
          {({ handlePrint }) => (
            <div className="mb30">
              <button onClick={handlePrint} className="btn-normal">
                Print & Download
              </button>
              {/* <Link className="btn-normal" to="/home">
                Back to home
              </Link> */}
            </div>
          )}
        </PrintContextConsumer>
      </ReactToPrint>
      <ComponentToPrint ref={ref} />
    </>
  )
}

export default Cards
