import ProcessImage from "react-imgpro";

const AddEffect = ({ sourceImage, outputImage, brightness, contrast }) => {


    return (
        <ProcessImage
            image={sourceImage}
            resize={{ }}
            //   brightness={brightness / 10}
            //   contrast={contrast / 10 }
            brightness={brightness ? brightness : 0}
            contrast={contrast ? contrast : 0}
            quality={100}
            storage={true}
            processedImage={outputImage} />
    );

}

export default AddEffect;