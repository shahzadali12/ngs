import React from 'react'
import QRCode from 'qrcode.react'
function ChallanVoucher({
  SchoolName,
  campus,
  BankName,
  AccountNo,
  FeeMonth,
  name,
  SId,
  classes,
  phone,
  Guardian,
  FamilyCode,
  ChallanId,
  BillID,
  Address,
  heads,
  totalsum,
  Remaining,
  AdvanceFee,
  AdvancePayment,
  afterDueTotal,
}) {
  return (
    <>
      <div className="ChallanForm">
        <div className="columns">
          <div className="waterMark">
            <span>Bank Copy</span>
          </div>
          <div className="flex_challan">
            <h1>
              {SchoolName === 'National Grammar School' ? (
                <img
                  src={process.env.PUBLIC_URL + '/logoB.png'}
                  alt="logo here"
                />
              ) : (
                <img
                  src={process.env.PUBLIC_URL + '/logo.png'}
                  alt="logo here"
                />
              )}
            </h1>
            <div className="HeadTitle">
              <h5>{SchoolName}</h5>
              <small>{campus} Campus</small>
              <p>
                Payable at any branch of {BankName} Bank ACCOUNT NO.
                {AccountNo}
              </p>
              <span>{FeeMonth}</span>
            </div>
          </div>
          <div className="List_of_challan">
            <ul className="Student_info">
              <li>
                <b>Name:</b> <span>{name}</span>
              </li>
              <li>
                <b>SID.#:</b> <span>{SId}</span>
              </li>
              <li>
                <b>Class:</b> <span>{classes}</span>
              </li>
              <li>
                <b>Phone.#:</b> <span>{phone}</span>
              </li>
              <li>
                <b>Guardian:</b> <span>{Guardian}</span>
              </li>
              <li>
                <b>Family.#:</b> <span>{FamilyCode}</span>
              </li>

              <li>
                <b>Challan.#:</b> <span>{ChallanId}</span>
              </li>
              <li>
                <b>Bill.#:</b> <span>{BillID}</span>
              </li>
              <li className="address_box">
                <b>Address:</b>
                <span>{Address}</span>
              </li>
            </ul>
            <div className="table_Challan">
              <table>
                <thead>
                  <tr>
                    <th>SR#</th>
                    <th>Particulars</th>
                    <th>Current</th>
                  </tr>
                </thead>
                <tbody>
                  {heads.map((valcol) => {
                    return (
                      <tr key={valcol.id}>
                        <td>{valcol.id}</td>
                        <td>{valcol.Heads}</td>
                        <td>{valcol.Fee}</td>
                      </tr>
                    )
                  })}
                </tbody>
                <tfoot>
                  <tr>
                    <td colSpan="2">
                      <span>Gross total</span>
                    </td>
                    <td>{totalsum}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Arrears</span>
                    </td>
                    <td>{Remaining}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Advance Fee Received</span>
                    </td>
                    <td>{AdvanceFee}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Total Payable within Due Date</span>
                    </td>
                    <td>{AdvancePayment}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Total Payable after Due Date</span>
                    </td>
                    <td>{afterDueTotal}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div className="Due_dates">
              <h6>
                <span>
                  issue date: <span>Sep, 8,2022</span>
                </span>
              </h6>
              <h6>
                Due Date: <span>Sep, 16,2022</span>
              </h6>
              <h6>
                Bill Validity date: <span>Sep, 8,2022</span>
              </h6>
            </div>
            <div className="QrCodes right">
              <QRCode
                value={[
                  `
Name: shahzad Ali 
ID: 001234234
Section: 5A
Account Title: Shahzad Ali
Account Number: 6021420311714162605`,
                ]}
                size={90}
                level={'L'}
                bgColor={'transparent'}
                fgColor={'#000'}
                includeMargin={false}
              />
            </div>
          </div>
          <div className="footerbase">
            <div className="footer_table">
              <div className="waterMark">
                <span>Bank Copy</span>
                <span>Bank Copy</span>
                <span>Bank Copy</span>
              </div>
            </div>
            <div className="notes">
              <span>Notes</span>
              <ul>
                <li>Only cash & MBL Cheque will be Accepted.</li>
                <li>
                  After Due Date Student will pay PKR100/- per day as charity on
                  late deposit.
                </li>
                <li>
                  The additional amount collected after due date will be donated
                  for charitable purpose.
                </li>
                <li>Please pay before due date.</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="waterMark">
            <span>School Copy</span>
          </div>
          <div className="flex_challan">
            <h1>
              {SchoolName === 'National Grammar School' ? (
                <img
                  src={process.env.PUBLIC_URL + '/logoB.png'}
                  alt="logo here"
                />
              ) : (
                <img
                  src={process.env.PUBLIC_URL + '/logo.png'}
                  alt="logo here"
                />
              )}
            </h1>
            <div className="HeadTitle">
              <h5>{SchoolName}</h5>
              <small>{campus} Campus</small>
              <p>
                Payable at any branch of {BankName} Bank ACCOUNT NO.
                {AccountNo}
              </p>
              <span>{FeeMonth}</span>
            </div>
          </div>
          <div className="List_of_challan">
            <ul className="Student_info">
              <li>
                <b>Name:</b> <span>{name}</span>
              </li>
              <li>
                <b>SID.#:</b> <span>{SId}</span>
              </li>
              <li>
                <b>Class:</b> <span>{classes}</span>
              </li>
              <li>
                <b>Phone.#:</b> <span>{phone}</span>
              </li>
              <li>
                <b>Guardian:</b> <span>{Guardian}</span>
              </li>
              <li>
                <b>Family.#:</b> <span>{FamilyCode}</span>
              </li>

              <li>
                <b>Challan.#:</b> <span>{ChallanId}</span>
              </li>
              <li>
                <b>Bill.#:</b> <span>{BillID}</span>
              </li>
              <li className="address_box">
                <b>Address:</b>
                <span>{Address}</span>
              </li>
            </ul>
            <div className="table_Challan">
              <table>
                <thead>
                  <tr>
                    <th>SR#</th>
                    <th>Particulars</th>
                    <th>Current</th>
                  </tr>
                </thead>
                <tbody>
                  {heads.map((valcol) => {
                    return (
                      <tr key={valcol.id}>
                        <td>{valcol.id}</td>
                        <td>{valcol.Heads}</td>
                        <td>{valcol.Fee}</td>
                      </tr>
                    )
                  })}
                </tbody>
                <tfoot>
                  <tr>
                    <td colSpan="2">
                      <span>Gross total</span>
                    </td>
                    <td>{totalsum}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Arrears</span>
                    </td>
                    <td>{Remaining}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Advance Fee Received</span>
                    </td>
                    <td>{AdvanceFee}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Total Payable within Due Date</span>
                    </td>
                    <td>{AdvancePayment}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Total Payable after Due Date</span>
                    </td>
                    <td>{afterDueTotal}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div className="Due_dates">
              <h6>
                <span>
                  issue date: <span>Sep, 8,2022</span>
                </span>
              </h6>
              <h6>
                Due Date: <span>Sep, 16,2022</span>
              </h6>
              <h6>
                Bill Validity date: <span>Sep, 8,2022</span>
              </h6>
            </div>
            <div className="QrCodes right">
              <QRCode
                value={[
                  `
Name: shahzad Ali 
ID: 001234234
Section: 5A
Account Title: Shahzad Ali
Account Number: 6021420311714162605`,
                ]}
                size={90}
                level={'L'}
                bgColor={'transparent'}
                fgColor={'#000'}
                includeMargin={false}
              />
            </div>
          </div>
          <div className="footerbase">
            <div className="footer_table">
              <div className="waterMark">
                <span>School Copy</span>
                <span>School Copy</span>
                <span>School Copy</span>
              </div>
            </div>
            <div className="notes">
              <span>Notes</span>
              <ul>
                <li>Only cash & MBL Cheque will be Accepted.</li>
                <li>
                  After Due Date Student will pay PKR100/- per day as charity on
                  late deposit.
                </li>
                <li>
                  The additional amount collected after due date will be donated
                  for charitable purpose.
                </li>
                <li>Please pay before due date.</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="waterMark">
            <span>Guardian Copy</span>
          </div>
          <div className="flex_challan">
            <h1>
              {SchoolName === 'National Grammar School' ? (
                <img
                  src={process.env.PUBLIC_URL + '/logoB.png'}
                  alt="logo here"
                />
              ) : (
                <img
                  src={process.env.PUBLIC_URL + '/logo.png'}
                  alt="logo here"
                />
              )}
            </h1>
            <div className="HeadTitle">
              <h5>{SchoolName}</h5>
              <small>{campus} Campus</small>
              <p>
                Payable at any branch of {BankName} Bank ACCOUNT NO.
                {AccountNo}
              </p>
              <span>{FeeMonth}</span>
            </div>
          </div>
          <div className="List_of_challan">
            <ul className="Student_info">
              <li>
                <b>Name:</b> <span>{name}</span>
              </li>
              <li>
                <b>SID.#:</b> <span>{SId}</span>
              </li>
              <li>
                <b>Class:</b> <span>{classes}</span>
              </li>
              <li>
                <b>Phone.#:</b> <span>{phone}</span>
              </li>
              <li>
                <b>Guardian:</b> <span>{Guardian}</span>
              </li>
              <li>
                <b>Family.#:</b> <span>{FamilyCode}</span>
              </li>

              <li>
                <b>Challan.#:</b> <span>{ChallanId}</span>
              </li>
              <li>
                <b>Bill.#:</b> <span>{BillID}</span>
              </li>
              <li className="address_box">
                <b>Address:</b>
                <span>{Address}</span>
              </li>
            </ul>
            <div className="table_Challan">
              <table>
                <thead>
                  <tr>
                    <th>SR#</th>
                    <th>Particulars</th>
                    <th>Current</th>
                  </tr>
                </thead>
                <tbody>
                  {heads.map((valcol) => {
                    return (
                      <tr key={valcol.id}>
                        <td>{valcol.id}</td>
                        <td>{valcol.Heads}</td>
                        <td>{valcol.Fee}</td>
                      </tr>
                    )
                  })}
                </tbody>
                <tfoot>
                  <tr>
                    <td colSpan="2">
                      <span>Gross total</span>
                    </td>
                    <td>{totalsum}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Arrears</span>
                    </td>
                    <td>{Remaining}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Advance Fee Received</span>
                    </td>
                    <td>{AdvanceFee}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Total Payable within Due Date</span>
                    </td>
                    <td>{AdvancePayment}</td>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <span>Total Payable after Due Date</span>
                    </td>
                    <td>{afterDueTotal}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div className="Due_dates">
              <h6>
                <span>
                  issue date: <span>Sep, 8,2022</span>
                </span>
              </h6>
              <h6>
                Due Date: <span>Sep, 16,2022</span>
              </h6>
              <h6>
                Bill Validity date: <span>Sep, 8,2022</span>
              </h6>
            </div>
            <div className="QrCodes right">
              <QRCode
                value={[
                  `
Name: shahzad Ali 
ID: 001234234
Section: 5A
Account Title: Shahzad Ali
Account Number: 6021420311714162605`,
                ]}
                size={90}
                level={'L'}
                bgColor={'transparent'}
                fgColor={'#000'}
                includeMargin={false}
              />
            </div>
          </div>
          <div className="footerbase">
            <div className="footer_table">
              <div className="waterMark">
                <span>Guardian Copy</span>
                <span>Guardian Copy</span>
                <span>Guardian Copy</span>
              </div>
            </div>
            <div className="notes">
              <span>Notes</span>
              <ul>
                <li>Only cash & MBL Cheque will be Accepted.</li>
                <li>
                  After Due Date Student will pay PKR100/- per day as charity on
                  late deposit.
                </li>
                <li>
                  The additional amount collected after due date will be donated
                  for charitable purpose.
                </li>
                <li>Please pay before due date.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default ChallanVoucher
