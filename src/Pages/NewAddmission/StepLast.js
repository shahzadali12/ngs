// import moment from 'moment'
import React, { useRef } from 'react'
import { Link } from 'react-router-dom'
import ReactToPrint, {
  PrintContextConsumer,
  // useReactToPrint,
} from 'react-to-print'
// import QRCode from 'qrcode.react'
// import { useState } from 'react'
import ChallanVoucher from './Voucher'

function Voucher({
  admissionData,
  croppedImage,
  discountfess,
  // totalsum,
  givenHeadList,
  feeTemplate,
}) {
  const refs = useRef()

  // const reactToPrintContent = React.useCallback(() => {
  //   return refs.current
  // }, [refs])

  // const handleAfterPrint = React.useCallback(() => {
  //   // console.log('`onAfterPrint` called') // tslint:disable-line no-console
  // }, [])

  const Arry = [
    {
      id: 1,
      SchoolName: 'National Grammar School',
      Accountinfo: {
        id: 1,
        BankName: 'Habib Metropolitan',
        AccountTitle: 'Shahzad Ali',
        AccountNo: '602143203117112312389',
      },
      StudentInfo: {
        id: 2,
        campus: 'Khanpur',
        name: 'Shahzad Ali',
        SId: '20302323',
        class: 'preparatory',
        phone: '923124326247',
        Guardian: 'Nazar Ali',
        FamilyCode: '6230',
        ChallanId: '884423',
        BillID: '234569',
        Address:
          'House No# 37B , Street No#6 khiyah-Bany Akhtar Phase 3 Khanpur',
      },
      AllHeads: {
        id: 3,
        issueDate: 'Sep, 8, 2022',
        dueDate: 'Sep, 16, 2022',
        BilValidDate: 'Sep, 8, 2022',
        FeeMonth: 'October 2022',
        Remaining: '12000',
        AdvanceFee: '5000',
        paid: '0',
        unpaid: '12000',
        FeeHeads: [
          {
            id: 1,
            Heads: 'Tuition Fee',
            Fee: '5000',
          },
          {
            id: 2,
            Heads: 'Tuition Fee',
            Fee: '20000',
          },
          {
            id: 3,
            Heads: 'Tuition Fee',
            Fee: '7000',
          },
          {
            id: 4,
            Heads: 'Tuition Fee',
            Fee: '9000',
          },
          {
            id: 5,
            Heads: 'Tuition Fee',
            Fee: '20000',
          },
          {
            id: 6,
            Heads: 'Tuition Fee',
            Fee: '7000',
          },
          {
            id: 7,
            Heads: 'Tuition Fee',
            Fee: '9000',
          },
          {
            id: 8,
            Heads: 'Tuition Fee',
            Fee: '9000',
          },
        ],
      },
    },
  ]

  const totalsum = Arry[0].AllHeads?.FeeHeads.reduce(
    (currentSum, nextObject) => {
      return currentSum + +nextObject.Fee
    },
    null
  )

  const Grand = +Arry[0].AllHeads?.Remaining + totalsum
  const AdvancePayment = Grand - Arry[0].AllHeads?.AdvanceFee
  const afterDueTotal = AdvancePayment + 100

  const ComponentToPrint = React.forwardRef((props, ref) => {
    return (
      <>
        {/* <button className="btn-normal" onClick={handlePrint}>
          DownLoad file
        </button> */}

        <div ref={ref}>
          {Arry?.map((val, i) => {
            return (
              <ChallanVoucher
                key={i}
                SchoolName={val.SchoolName}
                campus={val.StudentInfo?.campus}
                BankName={val.Accountinfo?.BankName}
                AccountNo={val.Accountinfo?.AccountNo}
                FeeMonth={val.AllHeads?.FeeMonth}
                name={val.StudentInfo?.name}
                SId={val.StudentInfo?.SId}
                classes={val.StudentInfo?.class}
                phone={val.StudentInfo?.phone}
                Guardian={val.StudentInfo?.Guardian}
                FamilyCode={val.StudentInfo?.FamilyCode}
                ChallanId={val.StudentInfo?.ChallanId}
                BillID={val.StudentInfo?.BillID}
                Address={val.StudentInfo?.Address}
                heads={val.AllHeads?.FeeHeads}
                totalsum={totalsum}
                Remaining={val.AllHeads?.Remaining}
                AdvanceFee={val.AllHeads?.AdvanceFee}
                AdvancePayment={AdvancePayment}
                afterDueTotal={afterDueTotal}
              />
            )
          })}
        </div>
      </>
    )
  })

  return (
    <>
      <ReactToPrint content={() => refs.current}>
        <PrintContextConsumer>
          {({ handlePrint }) => (
            <div className="mb30">
              <button onClick={handlePrint} className="btn-normal">
                Print & Download
              </button>
              <Link className="btn-normal" to="/home">
                Back to home
              </Link>
            </div>
          )}
        </PrintContextConsumer>
      </ReactToPrint>
      <ComponentToPrint refs={refs} />
    </>
  )
}

export default Voucher
