import React from 'react'

import FeeManage from '../FeeManagement/FeeManage'

function FeeYearly({ nextStep, prevStep, setFormData, formData }) {
  const SubmitFee = (e) => {
    nextStep()
    e.preventDefault()
  }

  return (
    <>
      <form onSubmit={SubmitFee}>
        <div className="flex aligntop">
          <div className="column9">
            <FeeManage />
          </div>
          <div className="column3">
            <div className="topGrid">
              <img
                src={process.env.PUBLIC_URL + '/logoB.png'}
                alt="logo here"
              />
              <small>Admission Fee</small>
              <h3>Fee voucher total Amount</h3>
            </div>
            <div className="Total_amounts">
              <h5>Fee Summary</h5>
              <ul>
                <li>
                  <span>Default Fee</span> <span>Rs.12000</span>
                </li>
                <li>
                  <span>Fee Heads</span> <span>Rs.6000</span>
                </li>
                <li>
                  <span>Discount</span> <span>Rs.200</span>
                </li>
                <li>
                  <span>Sub Total</span> <span>Rs.18200</span>
                </li>
                <li>
                  <span>Total</span> <span>Rs.18200</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="columns">
          <button className="btn-normal" onClick={prevStep}>
            Previous
          </button>

          <button className="btn-normal" type="submit">
            Next Step
          </button>
        </div>
      </form>
    </>
  )
}

export default FeeYearly
