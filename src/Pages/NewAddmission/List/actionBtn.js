import React from 'react'

function ActionBtn(props) {
  const btnClickedHandler = () => {
    props.clicked(props.value)
  }
  return (
    <div>
      {' '}
      <button onClick={btnClickedHandler}>Click Me!</button>
      <button onClick={btnClickedHandler}>Click Me!</button>
      <button onClick={btnClickedHandler}>Click Me!</button>
    </div>
  )
}

export default ActionBtn
