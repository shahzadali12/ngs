/* eslint-disable array-callback-return */
import React, { useMemo, useState, useContext } from 'react'
import mainContext from '../../../ContextApi/main'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import axios from 'axios'
import moment from 'moment'
const RegList = () => {
  const {
    setAdmissionData,
    admissionData,
    setValue,
    regkey,
    setKey,
    setReglst,
  } = useContext(mainContext)
  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      minWidth: 100,
      resizable: false,
      filter: true,
      floatingFilter: false,
    }
  }, [])

  const [columnDefs, setColumnDefs] = useState([
    {
      field: 'id',
      headerName: 'SR#',
      minWidth: 100,
      suppressSizeToFit: true,
      // pinned: 'left',
      cellRenderer: 'agGroupCellRenderer',
      cellRendererParams: {
        checkbox: true,
      },
    },
    {
      field: 'studentPicture',
      headerName: 'Image',
      suppressMenu: true,
      filter: false,
      cellRenderer: ({ params }) => {
        // // console.log(params, 'params params')
        return (
          <img
            style={{ height: '45px', width: '45px' }}
            // src={`http://39.61.53.93:8001/api${params}`}
            src={`https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0FIiZZbXHs4JkgRVWKTciwdWW_67I0p5hYg&usqp=CAU`}
            alt={params}
          />
        )
      },
      minWidth: 80,
    },
    {
      field: 'regDate',
      headerName: 'Reg#',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      minWidth: 120,
      suppressSizeToFit: true,
      cellRenderer: (data) => {
        return moment(data.regDate).format('DD/MM/YYYY')
      },
    },

    {
      field: 'studentFirstName',
      headerName: 'Name',
      // minWidth: 100,
      // suppressSizeToFit: true,
      // pinned: 'left',
      suppressMenu: false,
    },
    {
      field: 'className',
      headerName: 'Class',
      suppressMenu: false,
      width: 70,
      suppressSizeToFit: true,
    },
    {
      field: 'gender',
      headerName: 'Gender',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'bform',
      headerName: 'B-Form',
      suppressMenu: false,
      // width: auto,
      minWidth: 120,
      suppressSizeToFit: true,
      // cellRenderer: (data) => {
      //   return moment(data.expiryDate).format('DD/MM/YYYY')
      // }
    },
    {
      field: 'parentName',
      headerName: 'Parent',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },

    {
      field: 'cnic',
      headerName: 'CNIC',
      suppressMenu: false,
      suppressSizeToFit: true,
    },

    {
      field: 'cell',
      headerName: 'Cell',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'expiryDate',
      headerName: 'Expiry',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      minWidth: 120,
      suppressSizeToFit: true,
      cellRenderer: (data) => {
        return moment(data.regDate).format('DD/MM/YYYY')
      },
    },
  ])

  const isRowSelectable = useMemo(() => {
    return (params) => {
      return !!params.data && params.data.expiryDate < moment().format()
    }
  }, [])

  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }

  const rowSelectionType = 'signle'
  const onSelectionChanged = (event) => {
    const selectedData = event.api.getSelectedRows()
    if (selectedData) {
      setReglst(selectedData[0].id)
    }
  }

  return (
    <>
      <h4>Students Registraion List</h4>
      <AgGridReact
        className="ag-theme-alpine h100"
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        alwaysShowHorizontalScroll={true}
        alwaysShowVerticalScroll={true}
        onGridReady={onGridReady}
        pagination={true}
        onSelectionChanged={onSelectionChanged}
        rowSelection={rowSelectionType}
        isRowSelectable={isRowSelectable}
      />
    </>
  )
}
export default RegList
