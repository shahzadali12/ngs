import { useState, useContext, useEffect } from 'react'
import { toast, ToastContainer } from 'react-toastify'
import axios from 'axios'
import mainContext from '../../ContextApi/main'
import StepOne from './StepOne'
import StepTwo from './StepTwo'
import Final from './StepLast'
import RegViewpage from './Viewpage'
import Acadamics from './Acadamics'
import FeeYearly from './FeeYearly'
import FeeColectpopup from '../FeeManagement/FeeColectpopup'
import FeeColect from './FeeColect'
// import moment from 'moment'
function NewRegistraion({ heading }) {
  //state for steps

  // function for going to next step by increasing step state by 1

  const regcontxt = useContext(mainContext)

  const {
    formData,
    setFormData,
    step,
    handleSubmit,
    setValue,
    errors,
    admissionData,
    setAdmissionData,
    feeTemplate,
    setFeeTemplate,
    Submitform,
    nextStep,
    prevStep,
    formErrors,
    lstfeeHead,
    lstFeeTemplates,
    discountfess,
    total,
    totalsum,
    photoSrc,
    croppedImage,
    setCroppedImage,
    getValues,
    feetable,
    setlstFeeHeads,
    setHeading,
    Statevalues,
    lstfeeHeadoptions,
    setlstfeeHeadoptions,

    //////////

    inputList,
    listofoptions,
    HandleRemove,
    FelidsInput,
    handleRoomChange,
    AddMore,
    Chequestatus,
    DateChange,
  } = regcontxt

  const [setvalue, setSetValue] = useState(1)
  const [sethead, setHeadValue] = useState(1)

  const [currentSelectedFeeHead, setCurrentSelectedFeeHead] = useState([])

  const onSelectTemplate = (e) => {
    setSetValue(e.target.value)
    axios
      .get(
        `${process.env.REACT_APP_NGSBASEURL}Registration/GetFeePlanTemplateDetail?Id=${e.target.value}&Type=FeeTemplate`
      )
      .then((resp) => {
        const res = resp?.data?.feeData
        setFeeTemplate(res)
      })
      .catch((resp) => console.log(resp))
  }

  const GetNewHead = (e) => {
    setHeadValue(e.target.value)
    axios
      .get(
        `${process.env.REACT_APP_NGSBASEURL}Registration/GetFeePlanTemplateDetail?Id=${e.target.value}&Type=FeeHead`
      )
      .then((resp) => {
        const res = resp?.data?.feeData
        setCurrentSelectedFeeHead(res)
      })
      .catch((resp) => console.log(resp))
  }

  const handleAddMore = () => {
    if (currentSelectedFeeHead.length > 0) {
      feetable.push(currentSelectedFeeHead[0])
      setCurrentSelectedFeeHead([])
    }
  }

  const handleDelete = (index, e) => {
    setFeeTemplate(feetable.filter((v, i) => i !== index))
  }

  //////////////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////
  const handleInputData = (e, index) => {
    e.preventDefault()
    const { name, value } = e.target
    feetable[index][name] = value
    setFeeTemplate([...feetable])
  }

  const handleInput = (e, parent) => {
    const value = e.target.value
    const name = e.target.name
    setAdmissionData((prevState) => ({
      ...prevState,
      [parent]: {
        ...prevState[parent],
        [name]: value,
      },
    }))
  }

  const customStyles = {
    menuPortal: (base) => ({
      ...base,

      zIndex: 9999,
      '&:hover': {
        backgroundColor: 'red',
        color: 'white',
      },
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: '9999 !important',
      backgroundColor: '#fff',
      color: '#333',
      fontSize: '14px',
    }),

    control: (provided) => ({
      ...provided,
      margin: 0,
      border: '0px solid black',
      backgroundColor: 'white',
      outline: 'none',
      minHeight: '36px',
      fontSize: '14px',
    }),
  }
  useEffect(() => {
    setHeading(heading)
  }, [])
  // console.log(admissionData, 'hellow')

  // console.log(
  //   admissionData.regStd.Subjects === undefined
  //     ? 'hellow'
  //     : admissionData.regStd.Subjects
  // )
  return (
    <>
      <ToastContainer />
      <ol className="c-progress-steps">
        <li
          className={
            step === 1
              ? 'c-progress-steps__step current'
              : step >= 1
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Student information</span>
        </li>
        <li
          className={
            step === 2
              ? 'c-progress-steps__step current'
              : step >= 2
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Acadamics</span>
        </li>
        <li
          className={
            step === 3
              ? 'c-progress-steps__step current'
              : step >= 3
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Default Fees</span>
        </li>
        <li
          className={
            step === 4
              ? 'c-progress-steps__step current'
              : step >= 4
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Annual Plan</span>
        </li>
        <li
          className={
            step === 5
              ? 'c-progress-steps__step current'
              : step >= 5
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Fees Collection</span>
        </li>
        <li
          className={
            step === 6
              ? 'c-progress-steps__step current'
              : step >= 6
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>view screen</span>
        </li>
        <li
          className={
            step === 7
              ? 'c-progress-steps__step current'
              : step >= 7
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Print voucher</span>
        </li>
      </ol>
      {step === 1 && (
        <StepOne handleInput={handleInput} customStyles={customStyles} />
      )}

      {step === 2 && (
        <Acadamics
          nextStep={nextStep}
          prevStep={prevStep}
          formDeta={admissionData}
          setFormDeta={setAdmissionData}
          // admissionData={admissionData}
          // setAdmissionData={setAdmissionData}
        />
      )}
      {step === 3 && (
        <StepTwo
          nextStep={nextStep}
          prevStep={prevStep}
          // admissionData={admissionData}
          // setAdmissionData={setAdmissionData}
          formData={admissionData}
          setFormData={setAdmissionData}
          feeTemplate={feetable}
          lstfeeHead={lstfeeHead}
          lstFeeTemplates={lstFeeTemplates}
          formErrors={formErrors}
          onSelectTemplate={onSelectTemplate}
          GetNewHead={GetNewHead}
          handleAddMore={handleAddMore}
          handleInputData={handleInputData}
          handleDelete={handleDelete}
          discountfess={discountfess}
          totalsum={totalsum}
          setvalue={setvalue}
          sethead={sethead}
          total={total}
          //////////
          handleSubmit={handleSubmit}
          errors={errors}
          customStyles={customStyles}
          toast={toast}
          ////
          setCurrentSelectedFeeHead={setCurrentSelectedFeeHead}
          feetable={feetable}
          setlstFeeHeads={setlstFeeHeads}
          lstfeeHeadoptions={lstfeeHeadoptions}
          setlstfeeHeadoptions={setlstfeeHeadoptions}
        />
      )}
      {step === 4 && (
        <FeeYearly
          nextStep={nextStep}
          prevStep={prevStep}
          // admissionData={admissionData}
          // setAdmissionData={setAdmissionData}
          formData={admissionData}
          setFormData={setAdmissionData}
        />
      )}
      {step === 5 && (
        <>
          <div className="BaseDataSettings ">
            <FeeColect
              inputList={inputList}
              listofoptions={listofoptions}
              HandleRemove={HandleRemove}
              FelidsInput={FelidsInput}
              handleRoomChange={handleRoomChange}
              AddMore={AddMore}
              Chequestatus={Chequestatus}
              DateChange={DateChange}
              nextStep={nextStep}
              prevStep={prevStep}
            />
          </div>
          <div className="columns">
            <button className="btn-normal" onClick={prevStep}>
              Previous
            </button>

            <button className="btn-normal" onClick={nextStep}>
              Next Step
            </button>
          </div>
        </>
      )}
      {step === 6 && (
        <RegViewpage
          admissionData={admissionData}
          // setAdmissionData={setAdmissionData}
          nextStep={nextStep}
          prevStep={prevStep}
          photoSrc={photoSrc}
          croppedImage={croppedImage}
          setCroppedImage={setCroppedImage}
          feeTemplate={feeTemplate}
          discountfess={discountfess}
          totalsum={totalsum}
          Submitform={Submitform}
          Statevalues={Statevalues}
          getValues={getValues}
        />
      )}
      {step === 7 && (
        <Final
          admissionData={admissionData}
          // setAdmissionData={setAdmissionData}
          prevStep={prevStep}
          photoSrc={photoSrc}
          croppedImage={croppedImage}
          setCroppedImage={setCroppedImage}
          feeTemplate={feeTemplate}
          discountfess={discountfess}
          totalsum={totalsum}
        />
      )}
    </>
  )
}

export default NewRegistraion
