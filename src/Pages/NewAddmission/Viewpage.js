import moment from 'moment'
const RegViewpage = ({
  admissionData,
  croppedImage,
  discountfess,
  totalsum,
  givenHeadList,
  feeTemplate,
  nextStep,
  prevStep,
  // Submitform,
}) => {
  const Submitform = (e) => {
    nextStep()
    e.preventDefault()
  }
  // console.log(admissionData)
  return (
    <>
      <div className="viewpage">
        <div className="heading-style">
          <h3>
            {admissionData.regStd && admissionData.regStd.studentFirstName}
          </h3>
          <span>
            Registration ID.{' '}
            {admissionData.regStd && admissionData.regStd.regNo}
          </span>
          <br />
          <span>
            Reg Date
            {moment(
              admissionData.regStd && admissionData.regStd.regDate
            ).format('DD-MM-YYYY')}
          </span>
          <br />
          <span>
            Expiry Date
            {moment(
              admissionData.regStd && admissionData.regStd.expiryDate
            ).format('DD-MM-YYYY')}
          </span>
          <br />
          <span>Branch ID. Gulberg Branch</span>
        </div>
        <div className="flex_contain mb30">
          <div className="row">
            <div className="col-md-4">
              <div className="profile_view">
                <figure>
                  {croppedImage === undefined ? (
                    <img
                      src={
                        admissionData.regStd &&
                        admissionData.regStd.studentPicture
                      }
                      alt={croppedImage}
                    />
                  ) : null}
                </figure>
                {admissionData.regStd && (
                  <>
                    <h3 className="bg-title">Personal Detail</h3>
                    <div className="rtl_view p0">
                      {admissionData.regStd && (
                        <h4>
                          Full Name:
                          {admissionData.regStd &&
                            admissionData.regStd.studentFirstName}
                          {admissionData.regStd &&
                            admissionData.regStd.StudentLastName}
                        </h4>
                      )}
                      {admissionData.regStd && (
                        <h4>
                          Gender:
                          {admissionData.regStd && admissionData.regStd.gender}
                        </h4>
                      )}
                      {admissionData.regStd && (
                        <h4>
                          Date of Birth:
                          {moment(admissionData.regStd.studentDob).format(
                            'DD-MM-YYYY'
                          )}
                        </h4>
                      )}
                      {admissionData.regStd && (
                        <h4>
                          B-From:
                          {admissionData.regStd &&
                            admissionData.regStd.studentNicBform}
                        </h4>
                      )}

                      {admissionData.regStd && (
                        <h4>
                          Email ID:
                          {admissionData.regStd &&
                            admissionData.regStd.studentEmail}
                        </h4>
                      )}
                      {admissionData.regParent && (
                        <p>
                          Address:
                          {admissionData.regParent &&
                            admissionData.regParent.address}
                        </p>
                      )}
                      {admissionData.regStd && (
                        <p>
                          Remarks:{' '}
                          {admissionData.regStd && admissionData.regStd.remarks}
                        </p>
                      )}
                    </div>
                  </>
                )}
              </div>
            </div>

            <div className="col-md-4">
              <div className="rtl_view">
                {admissionData.regStd && (
                  <>
                    <h3 className="bg-title">Entry Test Detail</h3>
                    <h4>
                      Test Type :{' '}
                      {admissionData.regStd &&
                        admissionData.regStd.entryTestTypeId}
                    </h4>
                    <h4>
                      Test Date :{' '}
                      {admissionData.regStd &&
                        moment(admissionData.regStd.entryTestDate).format(
                          'DD/MM/YYYYY'
                        )}
                    </h4>
                  </>
                )}
                {admissionData.regStd && (
                  <>
                    <h3 className="bg-title">Student Class Plan</h3>
                    <h4>
                      Class Plan :
                      {admissionData.regStd && admissionData.regStd.classId}
                    </h4>
                    {/* <ul className="sub_list">
                      <li className="none-bg">
                        <h4>Subjects:</h4>
                      </li>
                      {formDeta.StudentSubjects.map((val, i) => (
                        <li key={i}>{val}</li>
                      ))}
                    </ul> */}
                    {admissionData.regStd && (
                      <h4>
                        Session :{' '}
                        {admissionData.regStd && admissionData.regStd.sesionId}
                      </h4>
                    )}
                    {admissionData.regStd && (
                      <h4>
                        Subjects :
                        {admissionData.regStd &&
                          admissionData.regStd.StudentSubjects}
                      </h4>
                    )}
                  </>
                )}
                {/* {admissionData.diseasename && (
                  <>
                    <h3 className="bg-title">Medical information</h3>
                    {formDeta.diseasename && (
                      <h4>Disease Name : {formDeta.diseasename}</h4>
                    )}
                    {formDeta.medicalcondition && (
                      <h4>Medical Condition: {formDeta.medicalcondition}</h4>
                    )}
                    {formDeta.emergencynumber && (
                      <h4>Emergency Number: {formDeta.emergencynumber}</h4>
                    )}
                    {formDeta.mentalhealth && (
                      <h4>Mental Health: {formDeta.mentalhealth}</h4>
                    )}
                  </>
                )} */}
              </div>
            </div>
            <div className="col-md-4">
              <div className="rtl_view">
                {admissionData.regParent && (
                  <>
                    <h3 className="bg-title">Primary Guardian Detail</h3>
                    <h4>
                      Name :{' '}
                      {admissionData.regParent && admissionData.regParent.name}
                    </h4>
                    {admissionData.regParent && (
                      <h4>
                        Email@ :{' '}
                        {admissionData.regParent &&
                          admissionData.regParent.email}
                      </h4>
                    )}
                    <h4>
                      CNIC Number :{' '}
                      {admissionData.regParent &&
                        admissionData.regParent.cnicNo}
                    </h4>
                    <h4>
                      Phone number :
                      {admissionData.regParent &&
                        admissionData.regParent.mobileNo}
                    </h4>
                    {admissionData.regParent &&
                      admissionData.regParent.occupationId && (
                        <h4>
                          Guardian Occupation :
                          {admissionData.regParent &&
                            admissionData.regParent.occupationId}
                        </h4>
                      )}
                    {admissionData.regParent && (
                      <h4>
                        Guardian Relation :
                        {admissionData.regParent &&
                          admissionData.regParent.relationshipId}
                      </h4>
                    )}
                    <h4>
                      City :{' '}
                      {admissionData.regParent &&
                        admissionData.regParent.cityId}
                    </h4>
                    <h4>
                      Area :{' '}
                      {admissionData.regParent &&
                        admissionData.regParent.areaId}
                    </h4>
                    <p>
                      Address:{' '}
                      {admissionData.regParent &&
                        admissionData.regParent.address}
                    </p>
                    {/* {admissionData.regParent.AddmissionOfficerReviewsP && (
                      <p>Remarks: {admissionData.regParent.AddmissionOfficerReviewsP}</p>
                    )} */}
                  </>
                )}
                {/* {formDeta.SecondguardianName && (
                  <>
                    <h3 className="bg-title">Secondary Guardian Detail</h3>
                    <h4>Name : {formDeta.SecondguardianName}</h4>
                    <h4>Email@ : {formDeta.SecondguardianEmail}</h4>
                    <h4>CNIC Number : {formDeta.SecondguardianCNIC}</h4>
                    <h4>Phone number : {formDeta.SecondguardianPhone}</h4>
                    {formDeta.SecondguardianOccupation && (
                      <h4>
                        Guardian Occupation :{formDeta.SecondguardianOccupation}
                      </h4>
                    )}
                    {formDeta.SecondguardianRelation && (
                      <h4>
                        Guardian Relation : {formDeta.SecondguardianRelation}
                      </h4>
                    )}
                    <p>Address: {formDeta.SecondguardianAddress}</p>
                    {formDeta.AddmissionOfficerReviewsS && (
                      <p>Remarks: {formDeta.AddmissionOfficerReviewsS}</p>
                    )}
                  </>
                )} */}
              </div>
            </div>
            <div className="col-md-12">
              {feeTemplate.length > 0 && (
                <>
                  <table className="grid_by_table mb0">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Head Name</th>
                        <th>Amount</th>
                        <th>Discount</th>
                        <th>total</th>
                      </tr>
                    </thead>

                    <tbody>
                      {feeTemplate.map((val, index) => (
                        <tr key={index}>
                          <td>{val.feeName}</td>
                          <td>{val.feeType}</td>
                          <td>{val.amount}</td>
                          <td>{val.discount}</td>
                          <td>
                            {parseInt(val.amount) - parseInt(val.discount)}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </>
              )}
              <div className="flex align-left">
                <div className="columns">
                  <button className="btn-normal" onClick={prevStep}>
                    Previous
                  </button>

                  <button className="btn-normal style2" onClick={Submitform}>
                    Proceed and continue
                  </button>
                </div>
                {totalsum && (
                  <div className="Total_Amount">
                    <ul>
                      <li>
                        <div className="Fees_cols">
                          <h5>Default Total Fees</h5>
                          {/* <h5>{totalsum}</h5> */}
                          <h5>Rs.{totalsum}</h5>
                        </div>
                      </li>
                      <li>
                        <div className="Fees_cols">
                          <h5>Total Discount</h5>
                          {/* <h5>{discountfess}</h5> */}
                          <h5>Rs.{discountfess}</h5>
                        </div>
                      </li>

                      <li>
                        <div className="Fees_cols">
                          <h5>Total Payable Amount</h5>
                          {/* <h5>{total}</h5> */}
                          <h5>
                            Rs.{parseInt(totalsum) - parseInt(discountfess)}
                          </h5>
                        </div>
                      </li>
                    </ul>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="flex align-left"></div>
      </div>
    </>
  )
}
export default RegViewpage
