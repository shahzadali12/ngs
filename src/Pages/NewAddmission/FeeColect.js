import React from 'react'
import FeeColectpopup from '../FeeManagement/FeeColectpopup'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
function FeeColect(props) {
  return (
    <>
      <div className="flex aligntop">
        <div className="column9">
          <FeeColectpopup
            inputList={props.inputList}
            listofoptions={props.listofoptions}
            HandleRemove={props.HandleRemove}
            FelidsInput={props.FelidsInput}
            customStyles={props.customStyles}
            handleRoomChange={props.handleRoomChange}
            AddMore={props.AddMore}
            errors={props.errors}
            Chequestatus={props.Chequestatus}
          />
        </div>

        <div className="column3">
          <div className="topGrid">
            <img src={process.env.PUBLIC_URL + '/logoB.png'} alt="logo here" />
            <small>Admission Fee</small>
            <h3>Fee voucher total Amount</h3>
          </div>
          <div className="Total_amounts">
            <h5>Fee Summary</h5>
            <ul>
              <li>
                <span>Default Fee</span> <span>Rs.12000</span>
              </li>
              <li>
                <span>Fee Heads</span> <span>Rs.6000</span>
              </li>
              <li>
                <span>Discount</span> <span>Rs.200</span>
              </li>
              <li>
                <span>Sub Total</span> <span>Rs.18200</span>
              </li>
              <li>
                <span>Total</span> <span>Rs.18200</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}

export default FeeColect
