import React, { useEffect, useState } from 'react'
import ReactSelect from 'react-select'
import Selectors from '../../Componets/widgets/ValidationInputs/feeSelector'
const StepTwo = ({
  nextStep,
  prevStep,
  setFormData,
  lstfeeHead,
  formData,
  lstFeeTemplates,
  onSelectTemplate,
  GetNewHead,
  handleAddMore,
  handleInputData,

  handleDelete,

  discountfess,
  totalsum,
  feeTemplate,
  total,
  toast,
  feetable,
  setlstFeeHeads,
  watch,
  control,
  Controller,
  customStyles,
}) => {
  /////////////////////////////////////

  const submitFormData = (e, data) => {
    nextStep()
    e.preventDefault()
    setFormData({
      ...formData,
      lstFees: data,
    })
    if (feeTemplate.length > 0) {
      nextStep()
      toast.success(`Fee Table Send to next Steps!`)
    } else {
      toast.error(`Fee Table Required!`)
    }
  }

  const [column, setColumn] = useState(lstfeeHead)

  useEffect(() => {
    setColumn(lstfeeHead)
    for (let i = 0; i < feetable.length; i++) {
      let num = feetable[i].feeId
      setColumn((lstfeeHead) =>
        lstfeeHead.filter((column) => column.value !== num)
      )
    }
  }, [feetable, lstfeeHead])

  return (
    <>
      <form onSubmit={submitFormData}>
        <div className="Fees_grid mb30">
          <div className="flex form_desgin">
            <div className="column">
              <Selectors
                icon="fa-snowflake-o"
                fieldname="Fees Template"
                choices={lstFeeTemplates}
                value={formData && formData.lstFeeTemplates}
                onChange={onSelectTemplate}
                name="lstFeeTemplates"
                defaultValue={formData && formData.lstFeeTemplates}
              />
              <div className="add_model_head flex">
                <Selectors
                  icon="fa-snowflake-o"
                  fieldname="Fees Head"
                  choices={column}
                  value={formData && formData.lstfeeHead}
                  onChange={GetNewHead}
                  name="lstFeeHeads"
                  defaultValue={formData && formData.lstfeeHead}
                />
                <div
                  type="button"
                  onClick={handleAddMore}
                  className="btn-normal"
                >
                  add head
                </div>
              </div>
            </div>
          </div>

          {feetable.length > 0 && (
            <table className="grid_by_table mb0">
              <thead>
                <tr>
                  <th>Head ID</th>
                  <th>Head Name</th>
                  <th>Fees Type</th>
                  <th>Operation</th>
                  <th>Default Fees</th>
                  <th>Discount</th>
                  <th>Actions</th>
                </tr>
              </thead>

              <tbody>
                {feetable.map((val, index) => {
                  return (
                    <tr key={index}>
                      <td>{val.feeId}</td>
                      <td>{val.feeName}</td>
                      <td>{val.feeType}</td>
                      <td>{val.operation}</td>
                      <td>
                        <input
                          value={val.amount}
                          onChange={(e) => handleInputData(e, index)}
                          name="amount"
                          type="number"
                        />
                      </td>
                      <td>
                        <input
                          onChange={(e) => handleInputData(e, index)}
                          value={val.discount}
                          name="discount"
                          type="number"
                        />
                      </td>
                      <td>
                        <button
                          type="button"
                          className="remove_item style-2"
                          onClick={() => handleDelete(index)}
                        >
                          <i className="fa fa-trash-o"></i>
                        </button>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          )}
          <div className="flex align-left">
            <div></div>
            {totalsum && (
              <div className="Total_Amount">
                <ul>
                  <li>
                    <div className="Fees_cols">
                      <h5>Default Total Fees</h5>
                      <h5>{totalsum}</h5>
                    </div>
                  </li>
                  <li>
                    <div className="Fees_cols">
                      <h5>Total Discount</h5>
                      <h5>{discountfess}</h5>
                    </div>
                  </li>

                  <li>
                    <div className="Fees_cols">
                      <h5>Total Payable Amount</h5>
                      <h5>{total}</h5>
                    </div>
                  </li>
                </ul>
              </div>
            )}
          </div>
        </div>

        <div className="columns">
          <button className="btn-normal" onClick={prevStep}>
            Previous
          </button>

          <button className="btn-normal" type="submit">
            Next Step
          </button>
        </div>
      </form>
    </>
  )
}

export default StepTwo
