import React, { useState, useEffect, useCallback } from 'react'

import Input from '../../Componets/widgets/ValidationInputs/input'
function Acadamic({ setFormDeta, formDeta, nextStep, prevStep }) {
  const [checkeds, setChecked] = useState([])

  const Arry = [
    {
      name: 'First Semester',
      terms: [
        {
          // termName: 'Mid Semester',
          Semesters: 'FirstSemester',
          CompulsorySubjects: {
            name: 'Compulsory Subjects',
            subject: [
              {
                id: 10001,
                sb: 'Urdu',
              },
              {
                id: 10002,
                sb: 'English',
              },
              {
                id: 10003,
                sb: 'Math',
              },
              {
                id: 10004,
                sb: 'Science',
              },
            ],
          },
          Elective: {
            name: 'Elective Subjects',
            subject: [
              {
                id: 10005,
                sb: 'Physics',
              },
              {
                id: 10006,
                sb: 'Computer',
              },
              {
                id: 10007,
                sb: 'NetWorking',
              },
              {
                id: 10008,
                sb: 'Algol',
              },
            ],
          },
        },
        {
          Semesters: 'MidSemester',
          CompulsorySubjects: {
            name: 'Compulsory Subjects',
            subject: [
              {
                id: 10009,
                sb: 'Arabic',
              },
              {
                id: 10010,
                sb: 'Islamiyat',
              },
            ],
          },
          Elective: {
            name: 'Elective Subjects',
            subject: [
              {
                id: 10011,
                sb: 'Criminology',
              },
              {
                id: 10012,
                sb: 'Computer',
              },
              {
                id: 10013,
                sb: 'Austrology',
              },
              {
                id: 10014,
                sb: 'Algol',
              },
            ],
          },
        },
      ],
    },
    {
      name: 'Second Semester',
      terms: [
        {
          Semesters: 'SecondSemester',
          CompulsorySubjects: {
            name: 'Compulsory Subjects',
            subject: [
              {
                id: 10021,
                sb: 'Urdu',
              },
              {
                id: 10022,
                sb: 'English',
              },
              {
                id: 10023,
                sb: 'Math',
              },
              {
                id: 10024,
                sb: 'Science',
              },
            ],
          },
          Elective: {
            name: 'Elective Subjects',
            subject: [
              {
                id: 10025,
                sb: 'Physics',
              },
              {
                id: 10026,
                sb: 'Computer',
              },
              {
                id: 10027,
                sb: 'NetWorking',
              },
              {
                id: 10028,
                sb: 'Algol',
              },
            ],
          },
        },
        {
          Semesters: 'FianlSemester',
          CompulsorySubjects: {
            name: 'Compulsory Subjects',

            subject: [
              {
                id: 10029,
                sb: 'Arabic',
              },
              {
                id: 10030,
                sb: 'Islamiyat',
              },
            ],
          },
          Elective: {
            name: 'Elective Subjects',
            subject: [
              {
                id: 10031,
                sb: 'Criminology',
              },
              {
                id: 10032,
                sb: 'Computer',
              },
              {
                id: 10033,
                sb: 'Austrology',
              },
            ],
          },
        },
      ],
    },
  ]

  const handleChange = (e) => {
    const { name, value, checked } = e.target
    // console.log(`${value} is ${checked} ${name}`)
    setChecked(checked)
    if (checkeds) {
      setFormDeta({
        ...formDeta,
        regStd: {
          ...formDeta.regStd,
          Subjects: {
            ...formDeta.regStd.Subjects,
            [name]: [...formDeta.regStd.Subjects[name], value],
          },
        },
      })
    } else {
      setFormDeta({
        ...formDeta,
        regStd: {
          ...formDeta.regStd,
          Subjects: {
            ...formDeta.regStd.Subjects,
            [name]: [
              ...formDeta.regStd.Subjects[name].filter((e) => e !== value),
            ],
          },
        },
      })
    }
  }

  // var checkedItems = checked.length
  //   ? checked.reduce((total, item) => {
  //       return total + ', ' + item
  //     })
  //   : ''

  const selectedArr = []
  const Subject2ID = []
  const SubjectID = []
  const listOfsubs = formDeta.regStd.Subjects
  const res = [].concat(...Object.values(listOfsubs))
  selectedArr.push(res)
  // console.log(formDeta, 'upasdasd')

  for (let i = 0; i < Arry.length; i++) {
    for (let b = 0; b < Arry[i].terms.length; b++) {
      let subs = Arry[i].terms[b].CompulsorySubjects.subject
      let Elective = Arry[i].terms[b].Elective.subject
      var results = subs.flat().map((y) => y.id)
      var ElectiveResult = Elective.flat().map((y) => y.id)
      SubjectID.push(results, ElectiveResult)
    }
    // }
  }
  if (Subject2ID !== []) {
    const resId = [].concat(...Object.values(SubjectID))
    Subject2ID.push(resId)
  }

  const classPlan = [
    {
      CLASS_ID: 30271,
      CLASS_Name: 'Play Group (Red)',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: '5nkk4As2LUs`idkFe`',
    },
    {
      CLASS_ID: 30272,
      CLASS_Name: 'Reception (Red)',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: '1`91R4ue`QbzKYlZDQ`idkFe`',
    },
    {
      CLASS_ID: 30273,
      CLASS_Name: 'Preparatory (Red)',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: '42ZfWYM01l0`idkFe`',
    },
    {
      CLASS_ID: 30274,
      CLASS_Name: '1A',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: 'jR7wCw5A3dM`idkFe`',
    },
    {
      CLASS_ID: 30275,
      CLASS_Name: '2A',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: 'Z16ufjFqsvM`idkFe`',
    },
    {
      CLASS_ID: 30276,
      CLASS_Name: '3A',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: '56ajS5oiKVs`idkFe`',
    },
    {
      CLASS_ID: 30277,
      CLASS_Name: '4A',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: 'ltib`91R4ue`NMNqY4`idkFe`',
    },
    {
      CLASS_ID: 30278,
      CLASS_Name: '5A',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: '/Rx1ibXA6q0`idkFe`',
    },
    {
      CLASS_ID: 30285,
      CLASS_Name: '6A',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: 'sbL5Nhmz`91R4ue`IU`idkFe`',
    },
    {
      CLASS_ID: 30286,
      CLASS_Name: '7A',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: 'vFVBgIF8ufQ`idkFe`',
    },
    {
      CLASS_ID: 30287,
      CLASS_Name: '8A',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: '2Q2DnpY2SV0`idkFe`',
    },
    {
      CLASS_ID: 30288,
      CLASS_Name: 'O1R',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: 'DZd/Sg/GXlQ`idkFe`',
    },
    {
      CLASS_ID: 30289,
      CLASS_Name: 'O2R',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: 'Ec2MZzMSD9g`idkFe`',
    },
    {
      CLASS_ID: 30290,
      CLASS_Name: 'O3R',
      CLASS_IS_SUPPLEMENTARY_BILLS: false,
      EncryptId: 'c0hdBYD8R2I`idkFe`',
    },
  ]

  // const filters = Subject2ID.find((item) => item === 10002)

  // console.log(formDeta, 'check by filter')
  // console.log(newse, 'update Arry State')
  // const nestedPromise = async (items = []) => {
  //   return await Promise.all(
  //     items.map(async (item) => {
  //       if (Array.isArray(item) && item.length) {
  //         return await nestedPromise(item)
  //       }
  //       // return await call to your function
  //       return 'response-' + item
  //     })
  //   )
  // }

  // const items = [
  //   [4, 1, 2],
  //   [2, 5],
  // ]
  // nestedPromise(items).then((results) => {
  //   // console.log(results, 'result')
  // })

  // var isChecked = (item) =>
  //   checked.includes(item) ? 'checked-item' : 'not-checked-item'

  // const aaa = selectedArr.find((item) =>
  //   // console.log(item.includes('10030'), 'check thssssssssssssss')
  // )
  // console.log(formDeta, 'update')
  return (
    <>
      <div className="flex form_desgin">
        <div className="flex">
          <div className="columns">
            <div className="custom-field selections_box">
              <label>
                <i className="fa fa-briefcase"></i>
                <h6 className="margin0">Class Plan</h6>
              </label>
              <select
                onChange={(event) =>
                  setFormDeta({
                    ...formDeta,
                    regStd: {
                      ...formDeta.regStd,
                      ClassPlan: event.target.value,
                    },
                  })
                }
                value={formDeta?.regStd?.ClassPlan}
              >
                {classPlan && (
                  <>
                    {classPlan.map((val) => (
                      <option key={val.CLASS_ID} value={val.value}>
                        {val.CLASS_Name}
                      </option>
                    ))}
                  </>
                )}
              </select>
            </div>
          </div>
        </div>
      </div>

      {formDeta?.regStd?.ClassPlan === '1A' && (
        <div className="flex_grid">
          {Arry.map((val, index) => {
            return (
              <div className="level_one" key={index}>
                <h5>{val.name}</h5>
                {val?.terms.map((vel, i) => {
                  return (
                    <div key={i} className="level_one">
                      {vel?.termName && <h5>{vel.termName}</h5>}

                      <div className="flex">
                        <div className="flex columns">
                          <h6 className="sm-title">
                            {vel?.CompulsorySubjects?.name}
                          </h6>
                          {vel?.CompulsorySubjects?.subject.map((sb) => {
                            return (
                              //console.log(sub)
                              <div className="columns" key={sb.id}>
                                <Input
                                  id={sb.id}
                                  onChange={(e) => handleChange(e)}
                                  type="checkbox"
                                  cls="checkbox "
                                  defaultChecked={
                                    // Subject2ID.find((item) => item === sb.id)
                                    //   ? true
                                    //   : false

                                    selectedArr.find((item) =>
                                      item.includes(sb.id.toString())
                                    )
                                      ? true
                                      : false
                                  }
                                  fieldname={sb.sb}
                                  span={<span></span>}
                                  name={vel?.Semesters}
                                  value={sb.id}
                                />
                              </div>
                            )
                          })}
                        </div>
                        <div className="flex columns">
                          <h6 className="sm-title">{vel?.Elective?.name}</h6>
                          {vel?.Elective?.subject.map((sb) => {
                            return (
                              <div className="columns" key={sb.id}>
                                <Input
                                  id={sb.id}
                                  type="checkbox"
                                  cls="checkbox"
                                  fieldname={sb.sb}
                                  onChange={handleChange}
                                  span={<span></span>}
                                  defaultChecked={
                                    // Subject2ID.find((item) => item === sb.id)
                                    //   ? true
                                    //   : false

                                    selectedArr.find((item) =>
                                      item.includes(sb.id.toString())
                                    )
                                      ? true
                                      : false
                                  }
                                  name={vel?.Semesters}
                                  value={sb.id}
                                />
                              </div>
                            )
                          })}
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            )
          })}
        </div>
      )}
      <div className="columns">
        <button className="btn-normal" onClick={prevStep}>
          Previous
        </button>

        <button className="btn-normal" onClick={nextStep}>
          Next Step
        </button>
      </div>
    </>
  )
}

export default Acadamic
