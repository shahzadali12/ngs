/* eslint-disable array-callback-return */
import React, { useMemo, useState, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import mainContext from '../../ContextApi/main'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import axios from 'axios'
import moment from 'moment'
const Users = () => {
  const {
    setAdmissionData,
    admissionData,
    setValue,
    regkey,
    setKey,
    setReglst,
  } = useContext(mainContext)
  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      minWidth: 70,
      resizable: false,
      filter: false,
      floatingFilter: false,
    }
  }, [])

  const [columnDefs, setColumnDefs] = useState([
    {
      field: 'id',
      headerName: 'SR#',
      suppressMenu: false,
      suppressSizeToFit: true,
      width: 70,
      minWidth: 70,
    },
    {
      field: 'studentPicture',
      headerName: 'Image',
      suppressMenu: true,
      filter: false,

      cellRenderer: ({ params }) => {
        // // console.log(params, 'params params')
        return (
          <img
            style={{ height: '45px', width: '45px' }}
            // src={`http://39.61.53.93:8001/api${params}`}
            src={`https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0FIiZZbXHs4JkgRVWKTciwdWW_67I0p5hYg&usqp=CAU`}
            alt={params}
          />
        )
      },
      minWidth: 80,
      width: 80,
    },
    {
      field: 'studentFirstName',
      headerName: 'Name',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      minWidth: 100,
      suppressSizeToFit: true,
    },

    {
      field: 'studentFirstName',
      headerName: 'Email',
      minWidth: 110,
      suppressSizeToFit: true,
      //   pinned: 'left',
      suppressMenu: false,
    },
    {
      field: 'expiryDate',
      headerName: 'Password',
      suppressMenu: false,
      // width: auto,
      minWidth: 80,
      suppressMenu: false,
      filter: false,

      cellRenderer: ({ data }) => {
        // console.log(data, 'show me the data form Api')
        return (
          <>
            {/* {data && data.expiryDate < moment(new Date()).format() ? (
              <span className="status_btn red">Due</span>
            ) : (
              <span className="status_btn green">Paid</span>
            )} */}
            {data && data.gender === '1' ? (
              //   <span className="status_btn">Expiry</span>
              <span className="status_btn green">Paid</span>
            ) : (
              <span className="status_btn red">Due</span>
            )}
            {/* {data && data.gender === '1' ? (
                  <li className="sick-days-Green">
                    <span></span>
                  </li>
                ) : (
                  ''
                )} */}
          </>
        )
      },
    },
    {
      field: 'id',
      headerName: 'Designation',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'id',
      headerName: 'Assign Role',
      suppressMenu: false,
      // width: auto,
      minWidth: 120,
      suppressSizeToFit: true,
      // cellRenderer: (data) => {
      //   return moment(data.expiryDate).format('DD/MM/YYYY')
      // }
    },
    {
      field: 'Action',
      // cellRendererParams: { condition: myCondition },
      cellRenderer: ({ data }) => {
        return (
          <div className="actionButtons">
            <i
              onClick={() => handleEdit(data.id)}
              className="fa fa-pencil-square-o"
              aria-hidden="true"
            ></i>

            <i
              onClick={() => handleDelate(data.id)}
              className="fa fa-trash"
              aria-hidden="true"
            ></i>

            {/* {data && data.expiryDate < moment(new Date()).format() ? null : (
              <i
                onClick={() => BlockRow(data.id)}
                className="fa fa-ban"
                aria-hidden="true"
              ></i>
            )} */}
          </div>
        )
      },
      suppressMenu: true,
      filter: false,
      pinned: 'right',
      width: 80,
      minWidth: 80,
      suppressSizeToFit: true,
    },
  ])

  //   const isRowSelectable = useMemo(() => {
  //     return (params) => {
  //       return !!params.data && params.data.expiryDate < moment().format()
  //     }
  //   }, [])

  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }

  const rowSelectionType = 'signle'
  const onSelectionChanged = (event) => {
    const selectedData = event.api.getSelectedRows()
    if (selectedData) {
      setReglst(selectedData[0].id)
    }
  }
  const BlockRow = (field) => {
    alert('Current Id', field)
  }
  const navigate = useNavigate()
  const handleEdit = (field, regdata) => {
    setKey(field)
    navigate('/NewRegistration')
  }

  const handleDelate = (field) => {
    // PrintVoucherToggle()
    setKey(field)
    // console.log(field, 'here is data..................')
    navigate('/NewRegistration')
  }
  return (
    <>
      <AgGridReact
        className="ag-theme-alpine h100"
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        alwaysShowHorizontalScroll={true}
        alwaysShowVerticalScroll={true}
        onGridReady={onGridReady}
        pagination={false}
        // onSelectionChanged={onSelectionChanged}
        // rowSelection={rowSelectionType}
        // isRowSelectable={isRowSelectable}
      />
    </>
  )
}
export default Users
