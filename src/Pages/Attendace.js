import React, { useContext } from 'react'
import ReactSelect from 'react-select'
import mainContext from '../ContextApi/main'
import DatePicker from 'react-datepicker'

export default function Attendace({ title, image, name }) {
  const regcontxt = useContext(mainContext)
  const {
    control,

    Controller,
    customStyles,

    lstRelationship,
  } = regcontxt

  var numDaysInMonth, daysInWeek, daysIndex, index, i, l, daysArray
  daysInWeek = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat']

  function getDaysArray(year, month) {
    numDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    // daysInWeek = [
    //     'Sunday',
    //     'Monday',
    //     'Tuesday',
    //     'Wednesday',
    //     'Thursday',
    //     'Friday',
    //     'Saturday',
    //   ]
    daysIndex = { Sun: 0, Mon: 1, Tue: 2, Wed: 3, Thu: 4, Fri: 5, Sat: 6 }
    index = daysIndex[new Date(year, month - 1, 1).toString().split('')[0]]
    daysArray = []

    for (i = 0, l = numDaysInMonth[month - 1]; i < l; i++) {
      //   daysArray.push(i + 1 + '.' + daysInWeek[index++])
      daysArray.push(i + 1)
      if (index === 5) index = 0
    }

    return daysArray
  }
  const current = getDaysArray('2022', '10')
  //   // console.log(current, 'current months')

  return (
    <>
      <div className="StudentAttendace flex form_desgin">
        <div className="Filter_Scale">
          <h5>Checkout {title} Attendace</h5>
          <div className="filter_grid flex">
            <div className={`custom-field selections_box `}>
              <label>
                <i className="fa fa-compress"></i>
                <h6 className="margin0">
                  <span></span>
                  Select Class
                </h6>
              </label>

              <Controller
                control={control}
                name="SelectClass"
                render={({ field }) => (
                  <ReactSelect
                    {...field}
                    styles={customStyles}
                    //   value={SelectClass}
                    options={lstRelationship}
                  />
                )}
              />
            </div>

            <div className={`custom-field selections_box`}>
              <label>
                <i className="fa fa-compress"></i>
                <h6 className="margin0">
                  <span></span>
                  Select Section
                </h6>
              </label>

              <Controller
                control={control}
                name="SelectSection"
                render={({ field }) => (
                  <ReactSelect
                    {...field}
                    styles={customStyles}
                    //   value={SelectSection}
                    options={lstRelationship}
                  />
                )}
              />
            </div>
            <div className={`datePick custom-field idGenerate `}>
              <DatePicker
              //   name="studentDob"
              //   selected={dateofbirth}
              //   monthsShown={1}
              //   value={dateofbirth && moment(dateofbirth).format('DD/MM/YYYY')}
              //   showYearDropdown
              //   onChange={(date) => setNewDate(date)}
              />

              <label>
                <i className={`fa fa-calendar`}></i>
                <h6 className="margin0">
                  <span></span>
                  Select Month
                </h6>
              </label>
            </div>
            <div className={`datePick custom-field idGenerate `}>
              <DatePicker
              //   name="studentDob"
              //   selected={dateofbirth}
              //   monthsShown={1}
              //   value={dateofbirth && moment(dateofbirth).format('DD/MM/YYYY')}
              //   showYearDropdown
              //   onChange={(date) => setNewDate(date)}
              />

              <label>
                <i className={`fa fa-calendar`}></i>
                <h6 className="margin0">
                  <span></span>
                  Select Year
                </h6>
              </label>
            </div>
            <button type="submit" className="btn-normal">
              Search
            </button>
          </div>
        </div>
        <div className="attandace_table">
          {/* <h5>Attendace Sheet of Class One : Section A, April 2022</h5> */}
          <h5>{title} Attendace Sheet</h5>
          <table>
            <thead>
              <tr>
                <th>{title} Names</th>
                {current.map((val, index) => (
                  <th key={index}>{val}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {current.map((val, index) => (
                <tr key={index}>
                  <td>
                    <img className="Stut_img" src={image} alt="img here" />
                    <h6>{name}</h6>
                    <span>ID.00123</span>
                  </td>
                  {current.map((val) => (
                    <td>
                      <div className="attend_block">
                        {daysInWeek === 'Sun' ? (
                          <span className="not fa fa-times"></span>
                        ) : (
                          <span className="yes fa fa-check"></span>
                        )}
                      </div>
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  )
}
