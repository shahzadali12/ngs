import React, { useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import DatePicker from 'react-datepicker'
import { schema } from '../../Componets/schema/Schema'
import { useNavigate } from 'react-router-dom'
import addDays from 'date-fns/addDays'
import ReactSelect from 'react-select'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import { useCallback } from 'react'
export const Terms = () => {
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    getValues,
    trigger,
    reset,
    handleChange,

    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'blur',
  })
  const [startDates, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(null)
  const enddate = addDays(startDates, 360)
  const customStyles = {
    menuPortal: (base) => ({
      ...base,

      zIndex: 9999,
      '&:hover': {
        backgroundColor: 'red',
        color: 'white',
      },
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: '9999 !important',
      backgroundColor: '#fff',
      color: '#333',
      fontSize: '14px',
    }),

    control: (provided) => ({
      ...provided,
      margin: 0,
      border: '0px solid black',
      backgroundColor: 'white',
      outline: 'none',
      minHeight: '36px',
      fontSize: '14px',
    }),
  }

  const defaultColDef = useCallback(() => {
    return {
      flex: 1,
      minWidth: 70,
      resizable: false,
      filter: false,
      floatingFilter: false,
    }
  }, [])

  const [columnDefs, setColumnDefs] = useState([
    {
      field: 'id',
      headerName: 'SR#',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 70,
      width: 70,
    },
    {
      field: 'studentFirstName',
      headerName: 'Name',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Start Date',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'End Date',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 100,
      width: 100,
    },
    {
      field: 'studentFirstName',
      headerName: 'Value',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
    },
    {
      field: 'studentFirstName',
      headerName: 'Status',
      suppressMenu: false,
      suppressSizeToFit: true,
      minWidth: 150,
      width: 150,
    },
    {
      field: 'Action',
      // cellRendererParams: { condition: myCondition },
      cellRenderer: ({ data }) => {
        return (
          <div className="actionButtons">
            <i
              onClick={() => handleEdit(data.id)}
              className="fa fa-pencil-square-o"
              aria-hidden="true"
            ></i>

            <i
              onClick={() => handleDelate(data.id)}
              className="fa fa-trash"
              aria-hidden="true"
            ></i>

            {/* {data && data.expiryDate < moment(new Date()).format() ? null : (
              <i
                onClick={() => BlockRow(data.id)}
                className="fa fa-ban"
                aria-hidden="true"
              ></i>
            )} */}
          </div>
        )
      },
      suppressMenu: true,
      filter: false,
      //   pinned: 'right',

      suppressSizeToFit: true,
    },
  ])

  //   const isRowSelectable = useMemo(() => {
  //     return (params) => {
  //       return !!params.data && params.data.expiryDate < moment().format()
  //     }
  //   }, [])

  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }

  const navigate = useNavigate()
  const handleEdit = (field, regdata) => {
    // setKey(field)
    navigate('/NewRegistration')
  }

  const handleDelate = (field) => {
    // PrintVoucherToggle()
    // setKey(field)
    // // console.log(field, 'here is data..................')
    navigate('/NewRegistration')
  }
  return (
    <div className="flex BaseDataSettings">
      <div className="col-4">
        <form className="mb30">
          <div className="flex form_desgin">
            <div className="heading sprator_line">
              <h5>Create Terms Basedata</h5>
            </div>
            <div className="flex">
              <div className={`custom-field`}>
                <input
                  id="ids1"
                  {...register('ID', { required: true })}
                  name="ID"
                  placeholder="23423154145"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">ID</h6>
                </label>
              </div>
              <div className={`custom-field`}>
                <input
                  id="ids1"
                  {...register('name', { required: true })}
                  name="name"
                  placeholder="name"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">Name</h6>
                </label>
              </div>
              <div className="flex-flex">
                <div
                  className={`datePick custom-field idGenerate ${
                    errors.regDate ? 'error' : ''
                  }`}
                >
                  <label htmlFor="dateIns">
                    <i className={`fa fa-calendar`}></i>
                    <h6 className="margin0">
                      <span></span>
                      {errors.regDate
                        ? 'Start Date is Required!'
                        : 'Start Date'}
                    </h6>
                  </label>
                  <DatePicker
                    dateFormat="dd/MM/yyyy"
                    selected={startDates}
                    onChange={(date) => {
                      setStartDate(date)
                    }}
                    showYearDropdown
                  />
                </div>
                <div
                  className={`datePick custom-field idGenerate ${
                    errors.expiryDate ? 'error' : ''
                  }`}
                >
                  <label htmlFor="dateIn">
                    <i className={`fa fa-calendar`}></i>
                    <h6 className="margin0">
                      <span></span>
                      {errors.expiryDate ? 'End Date is Required!' : 'End Date'}
                    </h6>
                  </label>
                  {/* Statevalues.expiryDate */}
                  <DatePicker
                    name="expiryDate"
                    selected={enddate}
                    dateFormat="dd/MM/yyyy"
                    onChange={(date) => setEndDate(date)}
                    includeDateIntervals={[
                      {
                        start: startDates,
                        end: enddate,
                      },
                    ]}
                  />
                </div>
              </div>
            </div>
            <div className="button_full_grid">
              <button type="button" className="btn-normal">
                Create Term
              </button>
            </div>
          </div>
        </form>
      </div>
      <div className="col-8">
        <div className="heading sprator_line">
          <h5>All Terms Basedata</h5>
        </div>
        <div className="Table">
          <AgGridReact
            className="ag-theme-alpine h100"
            columnDefs={columnDefs}
            defaultColDef={defaultColDef}
            alwaysShowHorizontalScroll={true}
            alwaysShowVerticalScroll={true}
            onGridReady={onGridReady}
            pagination={true}
            paginationPageSize={10}
          />
        </div>
      </div>
    </div>
  )
}
