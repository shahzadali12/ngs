import React, { useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { schema } from '../../Componets/schema/Schema'
import { useNavigate } from 'react-router-dom'
import ReactSelect from 'react-select'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import { useCallback } from 'react'
export const Designations = () => {
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    getValues,
    trigger,
    reset,
    handleChange,

    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'blur',
  })
  const customStyles = {
    menuPortal: (base) => ({
      ...base,

      zIndex: 9999,
      '&:hover': {
        backgroundColor: 'red',
        color: 'white',
      },
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: '9999 !important',
      backgroundColor: '#fff',
      color: '#333',
      fontSize: '14px',
    }),

    control: (provided) => ({
      ...provided,
      margin: 0,
      border: '0px solid black',
      backgroundColor: 'white',
      outline: 'none',
      minHeight: '36px',
      fontSize: '14px',
    }),
  }
  const lstcategory = [
    {
      label: 'Staff',
      value: '5051',
    },
    {
      label: 'admin',
      value: '5051',
    },
    {
      label: 'Academic',
      value: '4030',
    },
  ]
  const lstDepartment = [
    {
      label: 'M.I.S',
      value: '5051',
    },
    {
      label: 'Examination',
      value: '5051',
    },
    {
      label: 'Teachers',
      value: '5051',
    },
    {
      label: 'accounts',
      value: '4030',
    },
  ]
  const lstRole = [
    {
      label: 'i.T Manager',
      value: '5051',
    },
    {
      label: 'Developer',
      value: '5051',
    },
  ]

  const defaultColDef = useCallback(() => {
    return {
      flex: 1,
      minWidth: 70,
      resizable: false,
      filter: false,
      floatingFilter: false,
      pinned: 'right',
    }
  }, [])

  const [columnDefs, setColumnDefs] = useState([
    {
      field: 'id',
      headerName: 'SR#',
      suppressMenu: false,
      suppressSizeToFit: true,
      width: 70,
      minWidth: 70,
      pinned: 'left',
    },
    {
      field: 'id',
      headerName: 'ID',
      suppressMenu: true,
      filter: false,
      minWidth: 70,
      width: 70,
      pinned: 'left',
    },
    {
      field: 'studentFirstName',
      headerName: 'Employee Type',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      minWidth: 70,
      pinned: 'left',
      suppressSizeToFit: true,
    },

    {
      field: 'studentFirstName',
      headerName: 'Rank',
      minWidth: 150,
      width: 150,
      pinned: 'left',
      suppressSizeToFit: true,
      //   pinned: 'left',
      suppressMenu: false,
    },
    {
      field: 'studentFirstName',
      headerName: 'Role Status',
      suppressMenu: false,

      suppressMenu: false,
      filter: false,

      cellRenderer: ({ data }) => {
        // // console.log(data, 'show me the data form Api')
        return (
          <>
            {/* {data && data.expiryDate < moment(new Date()).format() ? (
              <span className="status_btn red">Due</span>
            ) : (
              <span className="status_btn green">Paid</span>
            )} */}
            {data && data.gender === '1' ? (
              //   <span className="status_btn">Expiry</span>
              <>
                <span className="dotblink active"></span>Active
              </>
            ) : (
              <>
                <span className="dotblink deactive"></span>Deactivated
              </>
            )}
            {/* {data && data.gender === '1' ? (
                  <li className="sick-days-Green">
                    <span></span>
                  </li>
                ) : (
                  ''
                )} */}
          </>
        )
      },
      width: 200,
      minWidth: 200,
      pinned: 'left',
    },
    {
      field: 'Action',
      // cellRendererParams: { condition: myCondition },
      cellRenderer: ({ data }) => {
        return (
          <div className="actionButtons">
            <i
              onClick={() => handleEdit(data.id)}
              className="fa fa-pencil-square-o"
              aria-hidden="true"
            ></i>

            <i
              onClick={() => handleDelate(data.id)}
              className="fa fa-trash"
              aria-hidden="true"
            ></i>

            {/* {data && data.expiryDate < moment(new Date()).format() ? null : (
              <i
                onClick={() => BlockRow(data.id)}
                className="fa fa-ban"
                aria-hidden="true"
              ></i>
            )} */}
          </div>
        )
      },
      suppressMenu: true,
      filter: false,
      pinned: 'left',
      width: 100,
      minWidth: 100,
      suppressSizeToFit: true,
    },
  ])

  //   const isRowSelectable = useMemo(() => {
  //     return (params) => {
  //       return !!params.data && params.data.expiryDate < moment().format()
  //     }
  //   }, [])

  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }

  const navigate = useNavigate()
  const handleEdit = (field, regdata) => {
    // setKey(field)
    // navigate('/NewRegistration')
  }

  const handleDelate = (field) => {
    // PrintVoucherToggle()
    // setKey(field)
    // // console.log(field, 'here is data..................')
    // navigate('/NewRegistration')
  }
  return (
    <div className="flex BaseDataSettings">
      <div className="col-4">
        <form className="mb30">
          <div className="flex form_desgin">
            <div className="heading sprator_line">
              <h5>Create User</h5>
            </div>
            <div className="flex">
              <div className={`custom-field`}>
                <input
                  id="ids1"
                  {...register('ID', { required: true })}
                  name="id"
                  placeholder="23423154145"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">ID</h6>
                </label>
              </div>
              <div className={`custom-field`}>
                <input
                  id="ids1"
                  {...register('Name', { required: true })}
                  name="Name"
                  placeholder="23423154145"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">Name</h6>
                </label>
              </div>
              <div className={`custom-field`}>
                <input
                  id="ids4"
                  {...register('username', { required: true })}
                  name="username"
                  placeholder="username"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">username / CNIC / E-mail</h6>
                </label>
              </div>

              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Select Category</h6>
                </label>
                <Controller
                  control={control}
                  name="SelectRole"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstcategory.label}
                      options={lstcategory}
                      styles={customStyles}
                    />
                  )}
                />
              </div>

              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Select Department</h6>
                </label>
                <Controller
                  control={control}
                  name="RoleDepartment"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstDepartment.label}
                      options={lstDepartment}
                      styles={customStyles}
                    />
                  )}
                />
              </div>

              <div className={`custom-field selections_box`}>
                <label>
                  <i className={`fa fa-snowflake-o`}></i>
                  <h6 className="margin0">Select Role</h6>
                </label>
                <Controller
                  control={control}
                  name="Role"
                  render={({ field }) => (
                    <ReactSelect
                      {...field}
                      value={lstRole.label}
                      options={lstRole}
                      styles={customStyles}
                    />
                  )}
                />
              </div>
              <div className={`custom-field`}>
                <input
                  id="ids4"
                  {...register('Password', { required: true })}
                  name="Password"
                  type="Password"
                  placeholder="Password"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">Password</h6>
                </label>
              </div>
              <div className={`custom-field`}>
                <input
                  id="ids4"
                  {...register('confirm password', { required: true })}
                  name="confirmpassword"
                  type="Password"
                  placeholder="confirm password"
                />
                <label htmlFor="ids1">
                  <i className="fa fa-user-o"></i>
                  <h6 className="margin0">confirm password</h6>
                </label>
              </div>
            </div>
            <div className="button_full_grid">
              <button type="button" className="btn-normal">
                Create User
              </button>
            </div>
          </div>
        </form>
      </div>
      <div className="col-8">
        <div className="heading sprator_line">
          <h5>All Staff</h5>
        </div>
        <div className="Table">
          <AgGridReact
            className="ag-theme-alpine h100"
            columnDefs={columnDefs}
            defaultColDef={defaultColDef}
            alwaysShowHorizontalScroll={true}
            alwaysShowVerticalScroll={true}
            onGridReady={onGridReady}
            pagination={true}
            paginationPageSize={10}
          />
        </div>
      </div>
    </div>
  )
}
