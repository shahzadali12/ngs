import React from 'react'
import ApexCharts from 'apexcharts'
import Noticsboard from '../Componets/widgets/noticsboard'
export default function SystemMonitoring() {
  const notics_board = [
    {
      id: 1,
      dateTime: 'Thu Sep 02 2022 12:40:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Shahzad ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 2,
      dateTime: 'Thu Feb 02 2022 9:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Asif ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 3,
      dateTime: 'Thu Sep 01 2022 02:50:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Mujahid ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 4,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Tanveer ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 5,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Asif ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 6,
      dateTime: 'Thu jun 01 2022 08:50:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Mujahid ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
    {
      id: 7,
      dateTime: 'Thu Sep 02 2022 10:00:00 GMT+0500 (Pakistan Standard Time)',
      name: 'Tanveer ALi',
      description:
        "Great School Managment simply is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since",
    },
  ]

  return (
    <>
      <div className="workingLogs">
        <div className="row">
          <div className="col-md-8">
            <Noticsboard
              heading={'System Activities'}
              activities={notics_board}
              cls="notics_board extra_style"
            />
          </div>
          <div className="col-md-4">
            <Noticsboard
              heading={'All users Activities'}
              activities={notics_board}
              cls="notics_board extra_style"
            />
          </div>
          <div className="col-md-4">
            <Noticsboard
              heading={'Financial Activities'}
              activities={notics_board}
              cls="notics_board extra_style"
            />
          </div>
          <div className="col-md-4">
            <Noticsboard
              heading={'Students Activities'}
              activities={notics_board}
              cls="notics_board extra_style"
            />
          </div>
          <div className="col-md-4">
            <Noticsboard
              heading={'Teachers Activities'}
              activities={notics_board}
              cls="notics_board extra_style"
            />
          </div>
        </div>
      </div>
    </>
  )
}
