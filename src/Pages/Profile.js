import React, { useState } from 'react'
import QRCode from 'qrcode.react'
import ExamTable from '../Componets/widgets/examTable'
export default function Profile() {
  const profile = [
    {
      CardID: '221015',
      name: 'Ananya Faheem',
      gender: 'Female',
      fatherName: 'Faheem',
      motherName: 'Saira',
      dateOfBrith: '04/09/2022',
      religion: 'islam',
      fatherOccupation: 'Business man',
      email: 'Ananya@info.com',
      admissionDate: '05/09/2022',
      class: 2,
      section: '1A',
      JoiningDate: '05/09/2021',
      cardExpiry: '05/09/2022',
      Address: '260 upper Mall Lahore',
      phoneNo: '+92 312 4326247',
      phone: '35750855-66',
    },
  ]

  //   const downloadQRCode = () => {
  //     // Generate download with use canvas and stream
  //     const canvas = document.getElementById('qr-gen')
  //     const pngUrl = canvas
  //       .toDataURL('image/png')
  //       .replace('image/png', 'image/octet-stream')
  //     let downloadLink = document.createElement('a')
  //     downloadLink.href = pngUrl
  //     downloadLink.download = `${profile[0].ID}.png`
  //     document.body.appendChild(downloadLink)
  //     downloadLink.click()
  //     document.body.removeChild(downloadLink)
  //   }
  const [qrValue, setQrValue] = useState(profile)
  // console.log(qrValue[0].CardID)
  return (
    <>
      <div className="information">
        <div className="row">
          <div className="col-md-9">
            <div className="heading sprator_line">
              <h5>My information</h5>
            </div>
            <div className="profile_grid">
              <div className="profile_image">
                <div className="col">
                  <img
                    //   src={'https://via.placeholder.com/250x250.png'}
                    src={process.env.PUBLIC_URL + 'images/child2.png'}
                    alt="imghere"
                  />
                  <div className="personal_info">
                    <ul>
                      <li>
                        Name : <span>{profile[0].name}</span>
                      </li>
                      <li>
                        gender : <span>{profile[0].gender}</span>
                      </li>
                      <li>
                        Father Name : <span>{profile[0].fatherName}</span>
                      </li>
                      <li>
                        Mother Name : <span>{profile[0].motherName}</span>
                      </li>
                      <li>
                        Date Of Birth : <span>{profile[0].dateOfBrith}</span>
                      </li>
                      <li>
                        Religion : <span>{profile[0].religion}</span>
                      </li>
                      <li>
                        Father Occupation :
                        <span>{profile[0].fatherOccupation}</span>
                      </li>
                      <li>
                        Email : <span>{profile[0].email}</span>
                      </li>
                      <li>
                        Admission Date : <span>{profile[0].admissionDate}</span>
                      </li>
                      <li>
                        Class : <span>{profile[0].class}</span>
                      </li>
                      <li>
                        Section : <span>{profile[0].section}</span>
                      </li>
                      <li>
                        Student ID : <span>{profile[0].CardID}</span>
                      </li>
                      <li>
                        Address : <span>{profile[0].Address}</span>
                      </li>
                      <li>
                        phoneNo : <span>{profile[0].phoneNo}</span>
                      </li>
                    </ul>
                  </div>
                </div>

                {/* <div className="Student_card">
                  <figure>
                    <img
                      src={process.env.PUBLIC_URL + 'logoB.png'}
                      alt="img here"
                    />
                    <h5>National Grammar School</h5>
                  </figure>
                  <div className="student_image">
                    <img
                      //   src={'https://via.placeholder.com/250x250.png'}
                      src={process.env.PUBLIC_URL + 'images/child2.png'}
                      alt="imghere"
                    />
                  </div>
                  <div className="Card_base">
                    <h5>{profile[0].name}</h5>
                    <h5>{profile[0].section}</h5>
                    <div className="info_crd">
                      <ul>
                        <li>
                          Student ID :<span>{profile[0].CardID}</span>
                        </li>
                        <li>
                          Joining Date :<span>{profile[0].JoiningDate}</span>
                        </li>
                        <li>
                          Card Expiry :<span>{profile[0].cardExpiry}</span>
                        </li>
                        <li>
                          Phone no :<span>{profile[0].phone}</span>
                        </li>
                        <li>{profile[0].Address}</li>
                      </ul>
                      <div className="QrCodes">
                        <QRCode
                          value={profile[0].CardID || profile[0].name}
                          size={70}
                          level={'L'}
                          bgColor={'transparent'}
                          fgColor={'#000'}
                          includeMargin={false}
                        />
                      </div>
                    </div>
                  </div>
                </div> */}
                <ul className="Grouping_btn">
                  <li>
                    <button type="button">
                      <i className="fa fa-edit"></i>
                      <span>edit picture</span>
                    </button>
                  </li>
                  <li>
                    <button type="button">
                      <i className="fa fa-edit"></i>
                      <span>Print Card</span>
                    </button>
                  </li>
                  <li>
                    <button type="button">
                      <i className="fa fa-edit"></i>
                      <span>Share with social media</span>
                    </button>
                  </li>
                  {/* <li>
                    <button type="button">
                      <i className="fa fa-edit"></i>
                    </button>
                  </li> */}
                </ul>
              </div>
              <div className="Table_of_content">
                {/* <figure>
                  <img
                    src={process.env.PUBLIC_URL + 'images/error.gif'}
                    alt="img here"
                  />
                </figure> */}
                <h4>All Exam Result</h4>
                <div className="">
                  <ExamTable />
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="bg-center">
              <div className="Student_card">
                <figure>
                  <img
                    src={process.env.PUBLIC_URL + 'logoB.png'}
                    alt="img here"
                  />
                  <h5>National Grammar School</h5>
                </figure>
                <div className="student_image">
                  <img
                    //   src={'https://via.placeholder.com/250x250.png'}
                    src={process.env.PUBLIC_URL + 'images/child2.png'}
                    alt="imghere"
                  />
                </div>
                <div className="Card_base">
                  <h5>{profile[0].name}</h5>
                  <h5>{profile[0].section}</h5>
                  <div className="info_crd">
                    <ul>
                      <li>
                        Student ID :<span>{profile[0].CardID}</span>
                      </li>
                      <li>
                        Joining Date :<span>{profile[0].JoiningDate}</span>
                      </li>
                      <li>
                        Card Expiry :<span>{profile[0].cardExpiry}</span>
                      </li>
                      <li>
                        Phone no :<span>{profile[0].phone}</span>
                      </li>
                      <li>{profile[0].Address}</li>
                    </ul>
                    <div className="QrCodes">
                      <QRCode
                        value={profile[0].CardID || profile[0].name}
                        size={70}
                        level={'L'}
                        bgColor={'transparent'}
                        fgColor={'#000'}
                        includeMargin={false}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
