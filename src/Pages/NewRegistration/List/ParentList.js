/* eslint-disable array-callback-return */
import React, { useMemo, useContext } from 'react'
import mainContext from '../../../ContextApi/main'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import ActionBtn from './actionBtn'

const ParentList = () => {
  const { setFormData, formData, setValue, regkey, setKey } =
    useContext(mainContext)

  const columnDefs = [
    {
      field: 'id',
      headerName: 'SR#',
      // minWidth: 60,
      // pinned: 'left',
      minWidth: 100,
      suppressMenu: false,
      cellRenderer: 'agGroupCellRenderer',
      cellRendererParams: {
        checkbox: true,
      },
    },
    {
      field: 'parentName',
      headerName: 'Parent',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'expiryDate',
      headerName: 'Cell',
      suppressMenu: false,
      editable: true,
      // width: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'parentName',
      headerName: 'Occupation',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'className',
      headerName: 'RelationShip',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'className',
      headerName: 'Email',
      suppressMenu: false,
      suppressSizeToFit: true,
    },
    {
      field: 'studentFirstName',
      headerName: 'City',
      // minWidth: 100,
      // suppressSizeToFit: true,
      // pinned: 'left',
      suppressMenu: false,
    },
    {
      field: 'studentLastName',
      headerName: 'Area',
      suppressMenu: false,
      // minWidth: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'gender',
      headerName: 'Address',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },
  ]
  // const btnClickedHandler = (e) => {
  //   // console.log(e)
  // }
  // const defaultColDef = {
  //   sortable: true,
  //   editable: true,
  //   flex: 1,
  //   filter: false,
  //   floatingFilter: true,

  //   // rowSelection: 'multiple',
  //   // groupSelectsChildren: true,
  //   // suppressRowClickSelection: true,
  //   // suppressAggFuncInHeader: true,
  // }

  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      minWidth: 100,
      resizable: false,
      filter: true,
      floatingFilter: false,
    }
  }, [])

  // const getSelectedRowData = () => {
  //   let selectedData = gridApi.getSelectedRows()
  //   alert(`Selected Nodes:\n${JSON.stringify(selectedData)}`)
  //   return selectedData
  // }

  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
      })
  }
  const rowSelectionType = 'signle'
  const onSelectionChanged = (event) => {
    const selectedData = event.api.getSelectedRows()

    setKey(selectedData[0].id)

    // Object.keys(selectedData[0]).map((key) =>
    //   setValue(key, selectedData[0][key])
    // )
    // setValue('studentPicture', selectedData[0].studentPicture)
  }
  return (
    <>
      <h4>Students Parent List</h4>
      <AgGridReact
        className="ag-theme-alpine h100"
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        alwaysShowHorizontalScroll={true}
        alwaysShowVerticalScroll={true}
        onGridReady={onGridReady}
        onSelectionChanged={onSelectionChanged}
        rowSelection={rowSelectionType}
      />
    </>
  )
}
export default ParentList
