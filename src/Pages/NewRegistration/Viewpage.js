import moment from 'moment'
const RegViewpage = ({
  formData,
  croppedImage,
  discountfess,
  totalsum,
  givenHeadList,
  feeTemplate,
  nextStep,
  prevStep,
  Submitform,
  Profile,
  Statevalues,
}) => {
  // console.log(Statevalues, 'sate value..................................')
  return (
    <>
      <div className="viewpage">
        <div className="heading-style">
          <h3>{Statevalues && Statevalues?.studentFirstName}</h3>
          <span>Registration ID. {Statevalues && Statevalues?.regNo}</span>
          <br />
          <span>
            Reg Date :{' '}
            {moment(Statevalues && Statevalues?.regDate).format('DD-MM-YYYY')}
          </span>
          <br />
          <span>
            Expiry Date :{' '}
            {moment(Statevalues && Statevalues?.expiryDate).format(
              'DD-MM-YYYY'
            )}
          </span>
          <br />
          <span>Branch ID. Gulberg Branch</span>
        </div>
        <div className="flex_contain mb30">
          <div className="row">
            <div className="col-md-4">
              <div className="profile_view">
                <figure>
                  <img
                    src={
                      formData?.regStd?.studentPicture !== ['']
                        ? Profile !== ['']
                          ? Profile ||
                            formData?.regStd?.studentPicture ||
                            process.env.PUBLIC_URL + 'images/photobg.jpg'
                          : process.env.PUBLIC_URL + 'images/photobg.jpg'
                        : process.env.PUBLIC_URL + 'images/photobg.jpg'
                    }
                    alt={Statevalues?.studentPicture}
                  />
                </figure>
                {Statevalues && (
                  <>
                    <h3 className="bg-title">Personal Detail</h3>
                    <div className="rtl_view p0">
                      {Statevalues && (
                        <h4>
                          Full Name :
                          {Statevalues && Statevalues?.studentFirstName}
                          {Statevalues && Statevalues?.StudentLastName}
                        </h4>
                      )}
                      {Statevalues?.gender && (
                        <h4>Gender :{Statevalues?.gender?.label}</h4>
                      )}
                      {Statevalues && (
                        <h4>
                          Date of Birth :
                          {moment(Statevalues?.studentDob).format('DD-MM-YYYY')}
                        </h4>
                      )}
                      {Statevalues && (
                        <h4>
                          B-From :{Statevalues && Statevalues?.studentNicBform}
                        </h4>
                      )}

                      {Statevalues && (
                        <h4>
                          Email ID :{Statevalues && Statevalues?.studentEmail}
                        </h4>
                      )}
                      {formData.regParent && (
                        <p>Address :{Statevalues && Statevalues?.address}</p>
                      )}
                      {Statevalues && (
                        <p>Remarks : {Statevalues && Statevalues?.remarks}</p>
                      )}
                    </div>
                  </>
                )}
              </div>
            </div>

            <div className="col-md-4">
              <div className="rtl_view">
                {Statevalues && (
                  <>
                    <h3 className="bg-title">Entry Test Detail</h3>
                    <h4>
                      Test Type :
                      {Statevalues && Statevalues?.entryTestTypeId?.label}
                    </h4>
                    <h4>
                      Test Date :
                      {Statevalues &&
                        moment(Statevalues?.entryTestDate).format(
                          'DD/MM/YYYYY'
                        )}
                    </h4>
                  </>
                )}
                {Statevalues && (
                  <>
                    <h3 className="bg-title">Student Class Plan</h3>
                    <h4>
                      Class Plan : {Statevalues && Statevalues?.classId?.label}
                    </h4>
                    {/* <ul className="sub_list">
                      <li className="none-bg">
                        <h4>Subjects:</h4>
                      </li>
                      {formDeta.StudentSubjects.map((val, i) => (
                        <li key={i}>{val}</li>
                      ))}
                    </ul> */}
                    {Statevalues && (
                      <h4>
                        Session : {Statevalues && Statevalues?.sesionId?.label}
                      </h4>
                    )}
                  </>
                )}
                {/* {formData.diseasename && (
                  <>
                    <h3 className="bg-title">Medical information</h3>
                    {formDeta.diseasename && (
                      <h4>Disease Name : {formDeta.diseasename}</h4>
                    )}
                    {formDeta.medicalcondition && (
                      <h4>Medical Condition: {formDeta.medicalcondition}</h4>
                    )}
                    {formDeta.emergencynumber && (
                      <h4>Emergency Number: {formDeta.emergencynumber}</h4>
                    )}
                    {formDeta.mentalhealth && (
                      <h4>Mental Health: {formDeta.mentalhealth}</h4>
                    )}
                  </>
                )} */}
              </div>
            </div>
            <div className="col-md-4">
              <div className="rtl_view">
                {Statevalues && (
                  <>
                    <h3 className="bg-title">Primary Guardian Detail</h3>
                    <h4>Name : {Statevalues?.name}</h4>
                    {Statevalues?.email && (
                      <h4>Email@ : {Statevalues?.email}</h4>
                    )}
                    <h4>CNIC Number : {Statevalues?.cnicNo}</h4>
                    <h4>Phone number : {Statevalues?.mobileNo}</h4>
                    {Statevalues?.occupationId && (
                      <h4>
                        Guardian Occupation : {Statevalues?.occupationId?.label}
                      </h4>
                    )}
                    {Statevalues && (
                      <h4>
                        Guardian Relation : {Statevalues?.relationshipId?.label}
                      </h4>
                    )}
                    <h4>City : {Statevalues?.cityId?.label}</h4>
                    <h4>Area : {Statevalues?.areaId?.label}</h4>
                    <p>
                      Address:
                      {Statevalues?.address}
                    </p>
                    {/* {formData.regParent.AddmissionOfficerReviewsP && (
                      <p>Remarks: {formData.regParent.AddmissionOfficerReviewsP}</p>
                    )} */}
                  </>
                )}
                {/* {formDeta.SecondguardianName && (
                  <>
                    <h3 className="bg-title">Secondary Guardian Detail</h3>
                    <h4>Name : {formDeta.SecondguardianName}</h4>
                    <h4>Email@ : {formDeta.SecondguardianEmail}</h4>
                    <h4>CNIC Number : {formDeta.SecondguardianCNIC}</h4>
                    <h4>Phone number : {formDeta.SecondguardianPhone}</h4>
                    {formDeta.SecondguardianOccupation && (
                      <h4>
                        Guardian Occupation :{formDeta.SecondguardianOccupation}
                      </h4>
                    )}
                    {formDeta.SecondguardianRelation && (
                      <h4>
                        Guardian Relation : {formDeta.SecondguardianRelation}
                      </h4>
                    )}
                    <p>Address: {formDeta.SecondguardianAddress}</p>
                    {formDeta.AddmissionOfficerReviewsS && (
                      <p>Remarks: {formDeta.AddmissionOfficerReviewsS}</p>
                    )}
                  </>
                )} */}
              </div>
            </div>
            <div className="col-md-12">
              {feeTemplate.length > 0 && (
                <>
                  <table className="grid_by_table mb0">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Head Name</th>
                        <th>Amount</th>
                        <th>Discount</th>
                        <th>total</th>
                      </tr>
                    </thead>

                    <tbody>
                      {feeTemplate.map((val, index) => (
                        <tr key={index}>
                          <td>{val.feeName}</td>
                          <td>{val.feeType}</td>
                          <td>{val.amount}</td>
                          <td>{val.discount}</td>
                          <td>
                            {parseInt(val.amount) - parseInt(val.discount)}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </>
              )}
              <div className="flex align-left">
                <div className="columns">
                  <button className="btn-normal" onClick={prevStep}>
                    Previous
                  </button>

                  <button className="btn-normal style2" onClick={Submitform}>
                    Proceed and continue
                  </button>
                </div>
                {totalsum && (
                  <div className="Total_Amount">
                    <ul>
                      <li>
                        <div className="Fees_cols">
                          <h5>Default Total Fees</h5>
                          {/* <h5>{totalsum}</h5> */}
                          <h5>Rs.{totalsum}</h5>
                        </div>
                      </li>
                      <li>
                        <div className="Fees_cols">
                          <h5>Total Discount</h5>
                          {/* <h5>{discountfess}</h5> */}
                          <h5>Rs.{discountfess}</h5>
                        </div>
                      </li>

                      <li>
                        <div className="Fees_cols">
                          <h5>Total Payable Amount</h5>
                          {/* <h5>{total}</h5> */}
                          <h5>
                            Rs.{parseInt(totalsum) - parseInt(discountfess)}
                          </h5>
                        </div>
                      </li>
                    </ul>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="flex align-left"></div>
      </div>
    </>
  )
}
export default RegViewpage
