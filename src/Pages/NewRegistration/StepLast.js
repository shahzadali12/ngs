import moment from 'moment'
import React, { forwardRef, useRef } from 'react'
import ReactToPrint, { PrintContextConsumer } from 'react-to-print'
import { Link } from 'react-router-dom'

function Voucher({
  formData,
  discountfess,
  totalsum,
  feeTemplate,
  Statevalues,
  Profile,
  prevStep,
}) {
  const ref = useRef()
  // const reactToPrintContent = React.useCallback(() => {
  //   return ref.current
  // }, [ref.current])

  // function openPrintWindow(url, name, specs) {
  //   const printWindow = window.open(url, name, specs)

  //   const printAndClose = function () {
  //     if (printWindow.document.readyState == 'complete') {
  //       clearInterval(sched)
  //       printWindow.print()
  //       printWindow.close()
  //     }
  //     window.location.replace('/NewRegistration')
  //   }
  //   const sched = setInterval(printAndClose, 200)
  // }

  // window.addEventListener('afterprint', (event) => {
  //   // console.log('After print')
  //   window.location.replace('/NewRegistration')
  // })

  const ComponentToPrint = React.forwardRef((props, ref, handlePrint) => {
    return (
      <>
        {/* <button onClick={handlePrint} className="btn-normal">
          Print & Download
        </button> */}
        <Link className="btn-normal" to="/home">
          Back to home
        </Link>
        <div ref={ref} className="custom_margin">
          <div className="voucher">
            <div className="flex mb30">
              <div className="logo">
                <img
                  src={process.env.PUBLIC_URL + '/logoB.png'}
                  alt="logo here"
                />
              </div>
              <div className="heading_titile">
                <h4>National Grammar School</h4>
                <h5>Registraion Slip</h5>
                <h6>
                  <i className="fa fa-map-marker"></i>gulberg branch
                </h6>
              </div>

              <div className="reg_picture">
                {Profile && (
                  <img
                    src={
                      formData?.regStd?.studentPicture !== ['']
                        ? Profile !== ['']
                          ? Profile ||
                            formData?.regStd?.studentPicture ||
                            process.env.PUBLIC_URL + 'images/photobg.jpg'
                          : process.env.PUBLIC_URL + 'images/photobg.jpg'
                        : process.env.PUBLIC_URL + 'images/photobg.jpg'
                    }
                    alt={Statevalues?.studentPicture}
                  />
                )}
              </div>
            </div>
            <div className="flex align-starts ">
              <div className="column">
                <h6>Student information</h6>
                <ul>
                  {Statevalues && (
                    <li>
                      <b>Name:</b>
                      {Statevalues?.studentFirstName}
                      {Statevalues?.studentLastName}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Gender:</b> {Statevalues?.gender?.label}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>B-Form:</b>
                      {Statevalues?.studentNicBform}
                    </li>
                  )}
                </ul>
              </div>
              <div className="column center_point">
                <h6>Parent/Guardian information</h6>
                <ul>
                  {Statevalues && (
                    <li>
                      <b>Name:</b>
                      {Statevalues.name}
                    </li>
                  )}
                  {formData.regParent && (
                    <li>
                      <b>CNIC:</b>
                      {Statevalues.cnicNo}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Contact:</b>
                      {Statevalues.mobileNo}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>E-mail:</b>
                      {Statevalues.email}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Address:</b>
                      {Statevalues.address}
                    </li>
                  )}
                </ul>
              </div>
              <div className="column rtl">
                <h6>School information</h6>
                <ul>
                  {Statevalues && (
                    <li>
                      <b>Reg:</b> {Statevalues.regNo}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Reg Date: </b>
                      {moment(Statevalues.regDate).format('DD-MM-YYYY')}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Expiry Date: </b>
                      {moment(Statevalues.expiryDate).format('DD-MM-YYYY')}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Session:</b>
                      {Statevalues.sesionId?.label}
                    </li>
                  )}
                  {formData.regStd && (
                    <li>
                      <b>Class:</b> {Statevalues.classId?.label}
                    </li>
                  )}
                </ul>
              </div>
            </div>
            <table className="grid_by_table FinalScreen">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Fee Types</th>
                  <th>Amount</th>
                  <th>Discount</th>
                  <th>total</th>
                </tr>
              </thead>

              <tbody>
                {feeTemplate.map((val, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{val.feeName}</td>
                    <td>{val.amount}</td>
                    <td>{val.discount}</td>
                    <td>{parseInt(val.amount) - parseInt(val.discount)}</td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="flex align-bottom">
              <div className="signature">
                {/* <h5>Executive Manager Signature case (in words)</h5> */}
              </div>
              <div className="copy_title">
                <small>Parent / Guardian Copy</small>
              </div>
              <div className="Total_Amount FinalScreen">
                <ul>
                  <li>
                    <b>Default Total Fees</b>
                    <span>Rs.{totalsum}</span>
                  </li>
                  <li>
                    <b>Total Discount</b>
                    <span>Rs.{discountfess}</span>
                  </li>
                  <li>
                    <b>Total Payable Amount</b>
                    {totalsum && (
                      <span>
                        Rs.
                        {totalsum
                          ? parseInt(totalsum) - parseInt(discountfess)
                          : null}
                      </span>
                    )}
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="Spratorline"></div>
          <div className="voucher">
            <div className="flex mb30">
              <div className="logo">
                <img
                  src={process.env.PUBLIC_URL + '/logoB.png'}
                  alt="logo here"
                />
              </div>
              <div className="heading_titile">
                <h4>National Grammar School</h4>
                <h5>Registraion Slip</h5>
                <h6>
                  <i className="fa fa-map-marker"></i>gulberg branch
                </h6>
              </div>

              <div className="reg_picture">
                {Profile && (
                  <img
                    src={
                      formData?.regStd?.studentPicture !== ['']
                        ? Profile !== ['']
                          ? Profile ||
                            formData?.regStd?.studentPicture ||
                            process.env.PUBLIC_URL + 'images/photobg.jpg'
                          : process.env.PUBLIC_URL + 'images/photobg.jpg'
                        : process.env.PUBLIC_URL + 'images/photobg.jpg'
                    }
                    alt={Statevalues?.studentPicture}
                  />
                )}
              </div>
            </div>
            <div className="flex align-starts ">
              <div className="column">
                <h6>Student information</h6>
                <ul>
                  {Statevalues && (
                    <li>
                      <b>Name:</b>
                      {Statevalues?.studentFirstName}
                      {Statevalues?.studentLastName}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Gender:</b> {Statevalues?.gender?.label}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>B-Form:</b>
                      {Statevalues?.studentNicBform}
                    </li>
                  )}
                </ul>
              </div>
              <div className="column center_point">
                <h6>Parent/Guardian information</h6>
                <ul>
                  {Statevalues && (
                    <li>
                      <b>Name:</b>
                      {Statevalues.name}
                    </li>
                  )}
                  {formData.regParent && (
                    <li>
                      <b>CNIC:</b>
                      {Statevalues.cnicNo}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Contact:</b>
                      {Statevalues.mobileNo}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>E-mail:</b>
                      {Statevalues.email}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Address:</b>
                      {Statevalues.address}
                    </li>
                  )}
                </ul>
              </div>
              <div className="column rtl">
                <h6>School information</h6>
                <ul>
                  {Statevalues && (
                    <li>
                      <b>Reg:</b> {Statevalues.regNo}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Reg Date: </b>
                      {moment(Statevalues.regDate).format('DD-MM-YYYY')}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Expiry Date: </b>
                      {moment(Statevalues.expiryDate).format('DD-MM-YYYY')}
                    </li>
                  )}
                  {Statevalues && (
                    <li>
                      <b>Session:</b>
                      {Statevalues.sesionId?.label}
                    </li>
                  )}
                  {formData.regStd && (
                    <li>
                      <b>Class:</b> {Statevalues.classId?.label}
                    </li>
                  )}
                </ul>
              </div>
            </div>
            <table className="grid_by_table FinalScreen">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Fee Types</th>
                  <th>Amount</th>
                  <th>Discount</th>
                  <th>total</th>
                </tr>
              </thead>

              <tbody>
                {feeTemplate.map((val, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{val.feeName}</td>
                    <td>{val.amount}</td>
                    <td>{val.discount}</td>
                    <td>{parseInt(val.amount) - parseInt(val.discount)}</td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="flex align-bottom">
              <div className="signature">
                {/* <h5>Executive Manager Signature case (in words)</h5> */}
              </div>
              <div className="copy_title">
                <small>Parent / Guardian Copy</small>
              </div>
              <div className="Total_Amount FinalScreen">
                <ul>
                  <li>
                    <b>Default Total Fees</b>
                    <span>Rs.{totalsum}</span>
                  </li>
                  <li>
                    <b>Total Discount</b>
                    <span>Rs.{discountfess}</span>
                  </li>
                  <li>
                    <b>Total Payable Amount</b>
                    {totalsum && (
                      <span>
                        Rs.
                        {totalsum
                          ? parseInt(totalsum) - parseInt(discountfess)
                          : null}
                      </span>
                    )}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  })
  // console.log(formData)
  return (
    <>
      <ReactToPrint content={() => ref.current}>
        <PrintContextConsumer>
          {({ handlePrint }) => (
            <>
              <button onClick={handlePrint} className="btn-normal">
                Print & Download
              </button>
            </>
          )}
        </PrintContextConsumer>
      </ReactToPrint>
      <ComponentToPrint ref={ref} />
    </>
  )
}

export default Voucher
