import React, { useState, useContext, useRef, useEffect } from 'react'
import CreatableSelect from 'react-select/creatable'
import mainContext from '../../ContextApi/main'
import NumberFormat from 'react-number-format'

import Input from '../../Componets/widgets/ValidationInputs/input'
import Textarea from '../../Componets/widgets/ValidationInputs/Textarea'
import ReactSelect from 'react-select'
import RegList from './List/Retistrations'
import Camara from '../effects/Camara'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import UploadedFiles from '../../Componets/FilesUpload/Uploadedfiles'
// import Validate from './validate'
// import CreateSelectBox from '../../Componets/widgets/Createable'

import ParentList from './List/ParentList'
import moment from 'moment'

const StepOne = ({ handleInput, customStyles }) => {
  ////////////////////////////////////////////////
  // const RegistraionData = useContext(MainContext)
  // // console.log(RegistraionData)

  const [studentinfo, setStudentinfo] = useState(true)
  const [acadamicinfo, setAcadamicinfo] = useState(true)
  const [PrimaryGuadain, setPrimaryGuadain] = useState(true)
  /////////////////////////////////////////////////////////////

  const regcontxt = useContext(mainContext)

  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    Statevalues,
    trigger,
    errors,
    Controller,

    formData,
    setFormData,

    nextStep,

    ///////////////
    lstCity,
    lstOccupation,
    lstRelationship,
    lstSession,
    lstTestType,

    lstMotherLanguage,
    lstReligion,
    TakeEntryTest,

    lstClassPlan,
    lstArea,

    dateofbirth,
    setNewDate,

    lstGender,
    setIsTakeEntryTest,
    startDates,
    setStartDate,
    endDate,
    setEndDate,
    enddate,

    setTestdate,
    testdate,
    alloptions,
    setAlloptions,
    handleChange,
    refReactSelect,
    setKey,
    reglst,

    gender,
    sesionId,
    classId,
    entryTestTypeId,
    occupationId,
    relationshipId,
    cityId,
    areaId,
    motherLanguageId,
    religionId,
  } = regcontxt

  const onSubmit = (data) => {
    setFormData({
      ...formData,
      regStd: {
        ...formData.regStd,
        regNo: data.regNo,
        studentPicture: data.studentPicture,
        studentFirstName: data.studentFirstName,
        studentLastName: data.studentLastName,
        classId: data?.classId?.value,
        regDate: data?.regDate,
        studentPicture: data?.studentPicture,
        expiryDate: data?.expiryDate,
        studentEmail: data?.studentEmail,
        studentNicBform: data?.studentNicBform,
        studentDob: data?.studentDob,
        isTakeEntryTest: data?.isTakeEntryTest,
        entryTestTypeId: data?.entryTestTypeId?.value,
        entryTestDate: data?.entryTestDate,
        sesionId: data?.sesionId?.value,
        gender: data?.gender?.label,
        remarks: data.remarks,
        religionId: data?.religionId?.value,
        motherLanguageId: data?.motherLanguageId?.value,
        createdDate: data?.createdDate ? data?.createdDate : new Date(),
        updatedDate: data?.updatedDate ? data?.updatedDate : new Date(),
      },
      regParent: {
        ...formData.regParent,
        name: data.name,
        email: data.email,
        cnicNo: data.cnicNo,
        mobileNo: data.mobileNo,
        occupationId: data?.occupationId?.value,
        relationshipId: data?.relationshipId?.value,
        cityId: data?.cityId?.value,
        areaId: data?.areaId?.value,
        address: data.address,
      },
    })
    // JSON.stringify(data)
    // alert(JSON.stringify(data))

    nextStep()
  }

  const [modal, setModal] = useState(false)

  const toggleModal = () => {
    setModal(!modal)
  }
  if (modal) {
    document.body.classList.add('active-modal')
  } else {
    document.body.classList.remove('active-modal')
  }

  const [addParent, setAddParent] = useState(false)

  const AddParentToggle = () => {
    setAddParent(!addParent)
  }
  if (addParent) {
    document.body.classList.add('active-modal')
  } else {
    document.body.classList.remove('active-modal')
  }

  return (
    <div className="form_style">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex start_top">
          <div className="columns-w8">
            <div className="flex form_desgin">
              <div className="flex">
                {/* <div onClick={toggleModal} className=" btn-normal uploadfiles">
                  <i className="fa fa-cloud-upload"></i>
                  <span>Existing Registration</span>
                </div> */}

                <div
                  className={`custom-field idGenerate ${
                    errors.regNo ? 'error' : ''
                  }`}
                >
                  <label>
                    <i className="fa fa-id-card-o"></i>
                    <h6 className="margin0">
                      <span></span>
                      {errors.regNo ? 'RegNo is Required!' : 'Reg.No'}
                    </h6>
                  </label>
                  <Controller
                    control={control}
                    name="regNo"
                    render={({ field }) => (
                      <NumberFormat format="#############" mask="" {...field} />
                    )}
                  />
                </div>
                <div
                  className={`datePick custom-field idGenerate ${
                    errors.regDate ? 'error' : ''
                  }`}
                >
                  <label htmlFor="dateIns">
                    <i className={`fa fa-calendar`}></i>
                    <h6 className="margin0">
                      <span></span>
                      {errors.regDate ? 'RegDate is Required!' : 'Reg.date'}
                    </h6>
                  </label>
                  <DatePicker
                    dateFormat="dd/MM/yyyy"
                    selected={startDates}
                    onChange={(date) => {
                      setStartDate(date)
                    }}
                  />
                </div>
                <div
                  className={`datePick custom-field idGenerate ${
                    errors.expiryDate ? 'error' : ''
                  }`}
                >
                  <label htmlFor="dateIn">
                    <i className={`fa fa-calendar`}></i>
                    <h6 className="margin0">
                      <span></span>
                      {errors.expiryDate
                        ? 'ExpiryDate is Required!'
                        : 'exp.date'}
                    </h6>
                  </label>
                  <DatePicker
                    name="expiryDate"
                    selected={endDate}
                    dateFormat="dd/MM/yyyy"
                    onChange={(date) => setEndDate(date)}
                    includeDateIntervals={[
                      {
                        start: startDates,
                        end: enddate,
                      },
                    ]}
                  />
                </div>
              </div>
              <div className="heading sprator_line">
                <h5 onClick={() => setStudentinfo(!studentinfo)}>
                  <span className={studentinfo ? 'ti-minus' : 'ti-plus'}></span>
                  Student information
                </h5>
              </div>
              <div
                className={`flex ${studentinfo ? 'active' : 'inactive'}`}
                aria-expanded={studentinfo}
              >
                <div
                  className={`custom-field ${
                    errors.studentFirstName ? 'errors' : null
                  }`}
                >
                  <input
                    id="ids1"
                    {...register('studentFirstName', { required: true })}
                    name="studentFirstName"
                    placeholder="student First Name"
                  />
                  <label htmlFor="ids1">
                    <i className="fa fa-user-o"></i>
                    <h6 className="margin0">
                      {errors.studentFirstName
                        ? 'student First Name is Required!'
                        : 'Student First Name'}
                    </h6>
                  </label>
                </div>
                <div
                  className={`custom-field ${
                    errors.studentLastName ? 'errors' : null
                  }`}
                >
                  <input
                    id="ids20"
                    {...register('studentLastName', { required: true })}
                    name="studentLastName"
                    placeholder="student Last Name"
                    defaultValue={Statevalues.studentLastName}
                  />
                  <label htmlFor="ids20">
                    <i className="fa fa-user-o"></i>
                    <h6 className="margin0">
                      {errors.studentLastName
                        ? errors.studentLastName.message
                        : 'Student Last Name'}
                    </h6>
                  </label>
                </div>
                <div
                  className={`custom-field ${
                    errors.studentEmail ? 'errors' : null
                  }`}
                >
                  <input
                    id="ids3"
                    {...register('studentEmail', { required: true })}
                    placeholder="student Email"
                    name="studentEmail"
                    defaultValue={Statevalues.studentEmail}
                  />
                  <label htmlFor="ids3">
                    <i className="fa fa-user-o"></i>
                    <h6 className="margin0">
                      {errors.studentEmail
                        ? errors.studentEmail.message
                        : 'Student Email'}
                    </h6>
                  </label>
                </div>
                <div
                  className={`custom-field idGenerate ${
                    errors.studentNicBform ? 'errors' : null
                  }`}
                >
                  <Controller
                    control={control}
                    name="studentNicBform"
                    render={({ field }) => (
                      <NumberFormat format="#############" mask="" {...field} />
                    )}
                  />
                  <label htmlFor="ids4">
                    <i className="fa fa-user-o"></i>
                    <h6 className="margin0">
                      {errors.studentNicBform
                        ? 'Student B-Form is Required!'
                        : 'Student B-Form'}
                    </h6>
                  </label>
                </div>
                <div
                  className={`datePick custom-field idGenerate ${
                    errors.studentDob ? 'error' : ''
                  }`}
                >
                  <DatePicker
                    name="studentDob"
                    selected={dateofbirth}
                    monthsShown={1}
                    value={
                      dateofbirth && moment(dateofbirth).format('DD/MM/YYYY')
                    }
                    defaultValue={
                      dateofbirth && moment(dateofbirth).format('DD/MM/YYYY')
                    }
                    showYearDropdown
                    onChange={(date) => setNewDate(date)}
                  />

                  <label>
                    <i className={`fa fa-calendar`}></i>
                    <h6 className="margin0">
                      <span></span>
                      {errors.studentDob
                        ? 'Date of Birth Required!'
                        : 'Student Date of Birth'}
                    </h6>
                  </label>
                </div>

                <div
                  className={`custom-field selections_box ${
                    errors.gender ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">
                      {errors.gender
                        ? 'Student Gender is Required'
                        : 'Student Gender'}
                    </h6>
                  </label>

                  <Controller
                    control={control}
                    name="gender"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        defaultValue={gender}
                        options={lstGender}
                        styles={customStyles}
                      />
                    )}
                  />
                </div>
                <div
                  className={`custom-field selections_box ${
                    errors.religionId ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">
                      {errors.religionId
                        ? 'Student religion is Required'
                        : 'Student religion'}
                    </h6>
                  </label>
                  <Controller
                    control={control}
                    name="religionId"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        defaultValue={religionId}
                        styles={customStyles}
                        options={lstReligion}
                      />
                    )}
                  />
                </div>

                <div
                  className={`custom-field selections_box ${
                    errors.motherLanguageId ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">
                      {errors.motherLanguageId
                        ? 'Student Language is Required'
                        : 'Student Language'}
                    </h6>
                  </label>
                  <Controller
                    control={control}
                    name="motherLanguageId"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        styles={customStyles}
                        defaultValue={motherLanguageId}
                        options={lstMotherLanguage}
                      />
                    )}
                  />
                </div>
              </div>

              <div className="heading sprator_line">
                <h5 onClick={() => setAcadamicinfo(!acadamicinfo)}>
                  <span
                    className={acadamicinfo ? 'ti-minus' : 'ti-plus'}
                  ></span>
                  Acadamic information
                </h5>
              </div>
              <div
                className={`flex ${acadamicinfo ? 'active' : 'inactive'}`}
                aria-expanded={acadamicinfo}
              >
                <div
                  className={`custom-field selections_box ${
                    errors.sesionId ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className={`fa fa-snowflake-o`}></i>
                    <h6 className="margin0">
                      {errors.sesionId
                        ? 'Student Session is Required'
                        : 'Student Session'}
                    </h6>
                  </label>
                  <Controller
                    control={control}
                    name="sesionId"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        styles={customStyles}
                        defaultValue={sesionId}
                        options={lstSession}
                      />
                    )}
                  />
                </div>

                <div
                  className={`custom-field selections_box ${
                    errors.classId ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className={`fa fa-briefcase`}></i>
                    <h6 className="margin0">
                      {errors.classId
                        ? 'Student class is Required'
                        : 'Student class'}
                    </h6>
                  </label>
                  <Controller
                    control={control}
                    name="classId"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        styles={customStyles}
                        defaultValue={classId}
                        options={lstClassPlan}
                      />
                    )}
                  />
                </div>
                {TakeEntryTest === true && (
                  <>
                    <div
                      className={`custom-field selections_box ${
                        errors.entryTestTypeId ? 'errors' : ''
                      }`}
                    >
                      <label>
                        <i className={`fa fa-briefcase`}></i>
                        <h6 className="margin0">
                          {errors.entryTestTypeId
                            ? 'entry Test Type is Required'
                            : 'entry Test Type'}
                        </h6>
                      </label>
                      <Controller
                        control={control}
                        name="entryTestTypeId"
                        render={({ field }) => (
                          <ReactSelect
                            {...field}
                            styles={customStyles}
                            defaultValue={entryTestTypeId}
                            options={lstTestType}
                          />
                        )}
                      />
                    </div>
                    <div
                      className={`datePick custom-field idGenerate ${
                        errors.entryTestDate ? 'error' : ''
                      }`}
                    >
                      <Controller
                        control={control}
                        name="entryTestDate"
                        render={({ field }) => (
                          <DatePicker
                            {...field}
                            dateFormat="dd/MM/yyyy"
                            selected={testdate}
                            popperPlacement="bottom-end"
                            onChange={(date) => setTestdate(date)}
                            value={
                              testdate && moment(testdate).format('DD/MM/YYYY')
                            }
                          />
                        )}
                      />

                      <label>
                        <i className={`fa fa-calendar`}></i>
                        <h6 className="margin0">
                          <span></span>
                          {errors.entryTestDate
                            ? 'Entry Test Date Required!'
                            : 'Entry Test Date'}
                        </h6>
                      </label>
                    </div>
                  </>
                )}
                <div className="flex">
                  <div
                    className={`custom-field fullwidth  ${
                      errors.remarks ? 'errors' : ''
                    }`}
                  >
                    <textarea
                      autoComplete="off"
                      type="text"
                      name="remarks"
                      placeholder="remarks"
                      id="remarks"
                      {...register('remarks', { required: true })}
                    />
                    <label htmlFor="remarks">
                      <i className={'fa fa-envelope-o'}></i>
                      <h6 className="margin0">
                        {errors.remarks ? errors.remarks.message : 'remarks'}
                      </h6>
                    </label>
                  </div>
                </div>
              </div>
              <div className="heading sprator_line">
                <h5 onClick={() => setPrimaryGuadain(!PrimaryGuadain)}>
                  <span
                    className={PrimaryGuadain ? 'ti-minus' : 'ti-plus'}
                  ></span>
                  Primary Guardian information
                  <button
                    type="button"
                    className="btn-normal"
                    onClick={AddParentToggle}
                  >
                    <i className="fa fa-user-o"></i>
                    EXISTING PARENT
                  </button>
                </h5>
              </div>
              <div
                className={`flex ${PrimaryGuadain ? 'active' : 'inactive'}`}
                aria-expanded={PrimaryGuadain}
              >
                <div
                  className={`custom-field ${errors.name ? 'errors' : null}`}
                >
                  <input
                    id="ids6"
                    {...register('name', { required: true })}
                    name="name"
                    placeholder="Guardian Name"
                  />
                  <label htmlFor="ids6">
                    <i className="fa fa-user-o"></i>
                    <h6 className="margin0">
                      {errors.name
                        ? 'Guardian Name is Required!'
                        : 'Guardian Name'}
                    </h6>
                  </label>
                </div>
                <div
                  className={`custom-field ${errors.email ? 'errors' : null}`}
                >
                  <input
                    id="ids7"
                    {...register('email', { required: true })}
                    name="email"
                    placeholder="student Email"
                  />
                  <label htmlFor="ids7">
                    <i className="fa-envelope-o"></i>
                    <h6 className="margin0">
                      {errors.email ? errors.email.message : 'Email@'}
                    </h6>
                  </label>
                </div>

                <div
                  className={`custom-field idGenerate ${
                    errors.cnicNo ? 'errors' : null
                  }`}
                >
                  <Controller
                    control={control}
                    name="cnicNo"
                    render={({ field }) => (
                      <NumberFormat format="#############" mask="" {...field} />
                    )}
                  />
                  <label htmlFor="ids8">
                    <i className="fa fa-user-o"></i>
                    <h6 className="margin0">
                      {errors.cnicNo ? errors.cnicNo.message : 'CNIC NO'}
                    </h6>
                  </label>
                </div>

                <div
                  className={`custom-field idGenerate ${
                    errors.mobileNo ? 'errors' : null
                  }`}
                >
                  <Controller
                    control={control}
                    name="mobileNo"
                    render={({ field }) => (
                      <NumberFormat format="#############" mask="" {...field} />
                    )}
                  />
                  <label htmlFor="ids8">
                    <i className="fa fa-phone"></i>
                    <h6 className="margin0">
                      {errors.mobileNo ? errors.mobileNo.message : 'Mobile No.'}
                    </h6>
                  </label>
                </div>

                <div
                  className={`custom-field selections_box ${
                    errors.occupationId ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className={`fa fa-briefcase`}></i>
                    <h6 className="margin0">
                      {errors.occupationId
                        ? 'Guardian Occupation is Required'
                        : 'Guardian Occupation'}
                    </h6>
                  </label>
                  <Controller
                    control={control}
                    name="occupationId"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        styles={customStyles}
                        defaultValue={occupationId}
                        options={lstOccupation}
                      />
                    )}
                  />
                </div>

                <div
                  className={`custom-field selections_box ${
                    errors.relationshipId ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className={`fa fa-compress`}></i>
                    <h6 className="margin0">
                      {errors.relationshipId
                        ? 'Student Relation is Required'
                        : 'RelationShip'}
                    </h6>
                  </label>
                  <Controller
                    control={control}
                    name="relationshipId"
                    render={({ field }) => (
                      <ReactSelect
                        {...field}
                        styles={customStyles}
                        defaultValue={relationshipId}
                        options={lstRelationship}
                      />
                    )}
                  />
                </div>

                <div
                  className={`custom-field selections_box ${
                    errors.cityId ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className={`fa fa-compress`}></i>
                    <h6 className="margin0">
                      {errors.cityId ? 'City is Required' : 'City'}
                    </h6>
                  </label>
                  <Controller
                    name="cityId"
                    render={({ field }) => (
                      <CreatableSelect
                        {...field}
                        defaultValue={cityId}
                        styles={customStyles}
                        options={lstCity}
                      />
                    )}
                    control={control}
                    rules={{ required: true }}
                  />
                </div>

                <div
                  className={`custom-field selections_box ${
                    errors.areaId ? 'errors' : ''
                  }`}
                >
                  <label>
                    <i className="fa fa-compress"></i>
                    <h6 className="margin0">
                      <span></span>
                      {errors.areaId ? 'Area is Required!' : 'area'}
                    </h6>
                  </label>

                  <Controller
                    name="areaId"
                    control={control}
                    onFocus={() => refReactSelect.current.focus()}
                    {...register('areaId')}
                    render={({ field }) => (
                      <CreatableSelect
                        {...field}
                        // inputRef={refReactSelect}
                        styles={customStyles}
                        defaultValue={areaId}
                        // ref={refReactSelect}
                        options={lstArea}
                      />
                    )}
                  />
                </div>

                <div className="flex">
                  <div
                    className={`custom-field fullwidth ${
                      errors.address ? 'errors' : ''
                    }`}
                  >
                    <textarea
                      autoComplete="off"
                      type="text"
                      name="address"
                      placeholder="address"
                      id="address"
                      {...register('address', { required: true })}
                    />
                    <label htmlFor="address">
                      <i className={'fa fa-map-marker'}></i>
                      <h6 className="margin0">
                        {errors.address ? errors.address.message : 'address'}
                      </h6>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <button
              type="submit"
              onClick={() => {
                trigger()
              }}
              className="btn-normal"
            >
              Next Step
            </button>
          </div>
          <div className="columns lst">
            <div className="element_pr">
              <Camara />
              <div className="flex">
                <div className="selections mb20">
                  <h6>take entry test *</h6>
                  <div className="flex">
                    <div className="radio_btns">
                      <Input
                        id="yes"
                        cls="radio-box"
                        fieldname={`yes`}
                        type="radio"
                        span={<span></span>}
                        name="testtype"
                        onChange={() => setIsTakeEntryTest(true)}
                        checked={TakeEntryTest === true}
                        value="1"
                      />
                    </div>
                    <div className="radio_btns">
                      <Input
                        id="no"
                        cls="radio-box"
                        fieldname={`no`}
                        type="radio"
                        span={<span></span>}
                        name="testtype"
                        onChange={() => setIsTakeEntryTest(false)}
                        checked={TakeEntryTest === false}
                        value="2"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="full-width parentOfupload">
                <UploadedFiles />
              </div>
            </div>
          </div>
        </div>
      </form>

      {modal && (
        <div className="popup_model proceed">
          <div onClick={toggleModal} className="overlay"></div>
          <div className="modal-content">
            <RegList />
            <div className="pull-right">
              <button
                type="button"
                className="btn-normal rtl"
                onClick={toggleModal}
              >
                Cancel
              </button>
              <button
                type="button"
                className="btn-normal rtl"
                onClick={() => setKey(reglst)}
              >
                <span onClick={toggleModal}>Save data</span>
              </button>
            </div>
            <button className="close-modal" onClick={toggleModal}>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      )}
      {addParent && (
        <div className="popup_model proceed">
          <div onClick={AddParentToggle} className="overlay"></div>
          <div className="modal-content">
            <ParentList
              formData={formData}
              setFormData={setFormData}
              setValue={setValue}
            />
            <div className="pull-right">
              <button
                type="button"
                className="btn-normal"
                onClick={AddParentToggle}
              >
                Proceed
              </button>
            </div>
            <button className="close-modal" onClick={AddParentToggle}>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      )}
    </div>
  )
}

export default StepOne
