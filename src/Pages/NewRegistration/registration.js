import { useState, useContext, useEffect } from 'react'
import { toast, ToastContainer } from 'react-toastify'
import axios from 'axios'
import mainContext from '../../ContextApi/main'
import StepOne from './StepOne'
import StepTwo from './StepTwo'
import Final from './StepLast'
import RegViewpage from './Viewpage'
// import moment from 'moment'
function NewRegistraion({ heading }) {
  //state for steps

  // function for going to next step by increasing step state by 1

  const regcontxt = useContext(mainContext)

  const {
    watch,
    control,
    Controller,
    step,
    handleSubmit,
    setValue,
    errors,
    formData,
    setFormData,
    feeTemplate,
    setFeeTemplate,
    Submitform,
    nextStep,
    prevStep,
    formErrors,
    lstfeeHead,
    lstFeeTemplates,
    discountfess,
    total,
    totalsum,
    photoSrc,
    croppedImage,
    setCroppedImage,
    getValues,
    feetable,
    setlstFeeHeads,
    setHeading,
    Statevalues,
    lstfeeHeadoptions,
    setlstfeeHeadoptions,
    Profile,
    customStyles,
  } = regcontxt

  const [setvalue, setSetValue] = useState(1)
  const [sethead, setHeadValue] = useState(1)

  const [currentSelectedFeeHead, setCurrentSelectedFeeHead] = useState([])

  const onSelectTemplate = (e) => {
    setSetValue(e.target.value)
    axios
      .get(
        `${process.env.REACT_APP_NGSBASEURL}Registration/GetFeePlanTemplateDetail?Id=${e.target.value}&Type=FeeTemplate`
      )
      .then((resp) => {
        const res = resp?.data?.feeData
        setFeeTemplate(res)
      })
      .catch((resp) => console.log(resp))
  }

  const GetNewHead = (e) => {
    setHeadValue(e.target.value)
    axios
      .get(
        `${process.env.REACT_APP_NGSBASEURL}Registration/GetFeePlanTemplateDetail?Id=${e.target.value}&Type=FeeHead`
      )
      .then((resp) => {
        const res = resp?.data?.feeData
        setCurrentSelectedFeeHead(res)
      })
      .catch((resp) => console.log(resp))
  }

  const handleAddMore = () => {
    if (currentSelectedFeeHead.length > 0) {
      feetable.push(currentSelectedFeeHead[0])
      setCurrentSelectedFeeHead([])
    }
  }

  const handleDelete = (index, e) => {
    setFeeTemplate(feetable.filter((v, i) => i !== index))
  }

  //////////////////////////////////////////////////////////////////////////////
  // // console.log(
  //   feeTemplate,
  //   feetable,
  //   'New Data TAble herer ............................................'
  // )
  ///////////////////////////////////////
  const handleInputData = (e, index) => {
    e.preventDefault()
    const { name, value } = e.target
    feetable[index][name] = value
    setFeeTemplate([...feetable])
  }

  const handleInput = (e, parent) => {
    const value = e.target.value
    const name = e.target.name
    setFormData((prevState) => ({
      ...prevState,
      [parent]: {
        ...prevState[parent],
        [name]: value,
      },
    }))
  }

  useEffect(() => {
    setHeading(heading)
  }, [])
  // // console.log(lstFeeTemplates)
  return (
    <>
      <ToastContainer />
      <ol className="c-progress-steps">
        <li
          className={
            step === 1
              ? 'c-progress-steps__step current'
              : step >= 1
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Student information</span>
        </li>
        <li
          className={
            step === 2
              ? 'c-progress-steps__step current'
              : step >= 2
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Fees information</span>
        </li>
        <li
          className={
            step === 3
              ? 'c-progress-steps__step current'
              : step >= 3
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>view screen</span>
        </li>
        <li
          className={
            step === 4
              ? 'c-progress-steps__step current'
              : step >= 4
              ? 'c-progress-steps__step green'
              : 'c-progress-steps__step done'
          }
        >
          <span>Print voucher</span>
        </li>
      </ol>
      {step === 1 && (
        <StepOne handleInput={handleInput} customStyles={customStyles} />
      )}

      {step === 2 && (
        <StepTwo
          nextStep={nextStep}
          prevStep={prevStep}
          formData={formData}
          setFormData={setFormData}
          feeTemplate={feetable}
          lstfeeHead={lstfeeHead}
          lstFeeTemplates={lstFeeTemplates}
          formErrors={formErrors}
          onSelectTemplate={onSelectTemplate}
          GetNewHead={GetNewHead}
          handleAddMore={handleAddMore}
          handleInputData={handleInputData}
          handleDelete={handleDelete}
          discountfess={discountfess}
          totalsum={totalsum}
          setvalue={setvalue}
          sethead={sethead}
          total={total}
          //////////
          setValue={setValue}
          handleSubmit={handleSubmit}
          errors={errors}
          customStyles={customStyles}
          toast={toast}
          ////
          setCurrentSelectedFeeHead={setCurrentSelectedFeeHead}
          feetable={feetable}
          setlstFeeHeads={setlstFeeHeads}
          lstfeeHeadoptions={lstfeeHeadoptions}
          setlstfeeHeadoptions={setlstfeeHeadoptions}
          watch={watch}
          control={control}
          Controller={Controller}
        />
      )}
      {step === 3 && (
        <RegViewpage
          formData={formData}
          // setFormData={setFormData}
          nextStep={nextStep}
          prevStep={prevStep}
          photoSrc={photoSrc}
          croppedImage={croppedImage}
          setCroppedImage={setCroppedImage}
          feeTemplate={feetable}
          discountfess={discountfess}
          totalsum={totalsum}
          Submitform={Submitform}
          Statevalues={Statevalues}
          getValues={getValues}
          Profile={Profile}
        />
      )}
      {step === 4 && (
        <Final
          formData={formData}
          // setFormData={setFormData}
          prevStep={prevStep}
          Profile={Profile}
          photoSrc={photoSrc}
          croppedImage={croppedImage}
          setCroppedImage={setCroppedImage}
          feeTemplate={feetable}
          discountfess={discountfess}
          totalsum={totalsum}
          Statevalues={Statevalues}
        />
      )}
    </>
  )
}

export default NewRegistraion
