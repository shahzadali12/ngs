import { useLocation, Navigate, Outlet } from 'react-router-dom'
import useAuth from '../ContextApi/useAuth'

const RequireAuth = ({ allowedRoles }) => {
  const { auth } = useAuth()
  const location = useLocation()
  // // console.log(auth.director, 'view login')
  return auth?.roles?.find((role) => allowedRoles?.includes(role)) ||
    auth?.teacher?.find((role) => allowedRoles?.includes(role)) ||
    auth.director?.find((role) => allowedRoles?.includes(role)) ||
    auth?.parent?.find((role) => allowedRoles?.includes(role)) ? (
    <Outlet />
  ) : auth?.user ? (
    <Navigate to="/login" state={{ from: location }} replace />
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  )
}

export default RequireAuth
