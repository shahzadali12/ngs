import React from 'react'
import Head from '../Componets/Header'
import Sidemenu from '../Componets/SideWidget'
import ContentArea from './ContentArea'

const combineElement = ({ children }) => {
  return (
    <>
      <Head />
      <div className="wrapper">
        <div className="flex">
          <Sidemenu />

          <ContentArea children={children} />
        </div>
      </div>
    </>
  )
}
export default combineElement
