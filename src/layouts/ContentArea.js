import React, { useContext } from 'react'
import mainContext from '../ContextApi/main'

export default function ContentArea({ children }) {
  const regcontxt = useContext(mainContext)
  const { toggleBtn, ClockWorning, timer } = regcontxt

  return (
    <>
      {ClockWorning && (
        <div className="popup_model CountDownAlert">
          <div className="overlay"></div>
          <div className="modal-content">
            <h2>{timer}</h2>
          </div>
        </div>
      )}
      <div
        className={`wapper_content ${toggleBtn === true ? 'bigexpend' : ''}`}
      >
        <div className="flex">
          <img
            className="background_image"
            src={process.env.PUBLIC_URL + '/logoB.png'}
            alt="img here"
          />
        </div>
        <div className="element_group">{children}</div>
      </div>
    </>
  )
}
