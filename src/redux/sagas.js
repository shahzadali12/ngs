import { call, put, takeEvery } from 'redux-saga/effects'
import { getNgsSuccess } from './Slice'
function* workGetNgsFetch() {
  let loginId = 1
  let id = 0
  const ngs = yield call(() =>
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/NewRegistration?LoginId=${loginId}&Id=${id}`
    )
  )
  const res = yield ngs.json()
  const responce = res
  yield put(getNgsSuccess(responce))
}

function* mySaga() {
  yield takeEvery('ngs/getNgsFetch', workGetNgsFetch)
}

export default mySaga
