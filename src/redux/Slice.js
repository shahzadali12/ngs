import { createSlice } from '@reduxjs/toolkit'

export const ngsSlice = createSlice({
  name: 'ngs',
  initialState: {
    ngs: [],
    isLoading: false,
  },
  reducers: {
    getNgsFetch: (state) => {
      state.isLoading = true
    },
    getNgsSuccess: (state, action) => {
      state.ngs = action.payload
      state.isLoading = false
    },
    getNgsFailure: (state) => {
      state.isLoading = false
    },
  },
})
export const { getNgsFetch, getNgsSuccess, getNgsFailure } = ngsSlice.actions

export default ngsSlice.reducer
