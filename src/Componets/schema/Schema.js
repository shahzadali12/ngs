import moment, { invalid } from 'moment'
import * as yup from 'yup'

const re =
  /^((ftp|http|https):\/\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+((\/)[\w#]+)*(\/\w+\?[a-zA-Z0-9_]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/gm
export const schema = yup
  .object()
  .shape({
    ////////////////////////////////////////////////////////////////////////
    /////////////  Student INFO Validation ///////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    regNo: yup
      .string()
      .required()
      .matches(/^[0-9]+$/, 'Must be only digits')
      .min(9, 'Must be exactly 9 digits')
      .max(13, 'Must be exactly 13 digits'),
    regDate: yup.date().required('Date is required'),
    expiryDate: yup.date().required('Date is required'),
    studentFirstName: yup.string().required('First Name is Required!'),
    classId: yup
      .object()
      .shape({
        label: yup.string().required('class is Required!'),
        value: yup.string().required('class is Required!'),
      })
      .nullable()
      .required('class is Required!'),

    sesionId: yup
      .object()
      .shape({
        label: yup.string().required('session is Required!'),
        value: yup.string().required('session is Required!'),
      })
      .nullable()
      .required('session is Required!'),
    studentEmail: yup
      .string()
      .email('Student Email Not Valid')
      .required('Student Email is Required!'),
    studentNicBform: yup
      .string()
      .required()
      .matches(/^[0-9]+$/, 'Must be only digits')
      .min(13, 'Must be exactly 14 digits')
      .max(13, 'Must be exactly 14 digits'),
    // studentDob: yup.date().required('Date of Brith is Required!'),

    gender: yup.object().shape({
      label: yup.string().required('Gender is Required!'),
      value: yup.string().required('Gender is Required!'),
    }),

    // studentPicture: yup
    //   .string()
    //   .matches(re, 'URL is not valid')
    //   .required('Student Picture is Required!'),

    // religionId: yup.object().shape({
    //   label: yup.string().required('Religion is Required!'),
    //   value: yup.number().required('Religion is Required!'),
    // }),
    // motherLanguageId: yup.object().shape({
    //   label: yup.string().required('Language is Required!'),
    //   value: yup.number().required('Language is Required!'),
    // }),

    //////////////////////////////////////////////////////////////////////
    //////////////////  Entry Test Type //////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    isTakeEntryTest: yup.boolean().default(false),
    // isTakeEntryTest: yup.string().required('isTakeEntryTest is Required!'),
    // entryTestTypeId: yup.string().required('Entry Test type is Required!'),
    // entryTestDate: yup.string().required('Entry Test Date is Required!'),

    //////////////////////////////////////////////////////////////////////
    //////////////////  PARENT //////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    name: yup.string().required('Parent Name is Required!'),
    email: yup.string().email('Email not Valid').required('Email is Required!'),
    cnicNo: yup
      .string()
      .required()
      .matches(/^[0-9]+$/, 'Must be only digits')
      .min(13, 'CNIC Must be exactly 13 digits')
      .max(13, 'CNIC Must be exactly 13 digits'),
    mobileNo: yup.string().required('Parent Mobile No. is Required!'),

    // occupationId: yup.object().shape({
    //   label: yup.string().required('Occupation is Required!'),
    //   value: yup.string().required('Occupation is Required!'),
    // }),
    // relationshipId: yup.object().shape({
    //   label: yup.string().required('Parent Relationship is Required!'),
    //   value: yup.number().required('Parent Relationship is Required!'),
    // }),

    cityId: yup
      .object()
      .shape({
        label: yup.string().required('Parent City is Required!'),
        value: yup.string().required('Parent City is Required!'),
      })
      .required(''),
    areaId: yup
      .object()
      .shape({
        label: yup.string().required('Parent Area is Required!'),
        value: yup.string().required('Parent Area is Required!'),
      })
      .required(''),

    address: yup.string().required('Parent Address is Required!'),

    // lstFeeTemplates: yup.string().required('Fee Templates is Required!'),
  })
  .required()
