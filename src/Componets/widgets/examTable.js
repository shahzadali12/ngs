/* eslint-disable array-callback-return */
import React, { useMemo, useState, useContext } from 'react'
import mainContext from '../../ContextApi/main'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import axios from 'axios'
import moment from 'moment'
const ExamTable = () => {
  const {
    setAdmissionData,
    admissionData,
    setValue,
    regkey,
    setKey,
    setReglst,
  } = useContext(mainContext)
  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      minWidth: 100,
      resizable: false,
      filter: false,
      floatingFilter: false,
    }
  }, [])

  const [columnDefs, setColumnDefs] = useState([
    {
      field: 'regDate',
      headerName: 'Exam Name',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      minWidth: 110,
      suppressSizeToFit: true,
      cellRenderer: (data) => {
        return moment(data.regDate).format('DD/MM/YYYY')
      },
    },
    {
      field: 'regDate',
      headerName: 'Subject',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      minWidth: 100,
      suppressSizeToFit: true,
      cellRenderer: (data) => {
        return moment(data.regDate).format('DD/MM/YYYY')
      },
    },

    {
      field: 'studentFirstName',
      headerName: 'Grade Point',
      minWidth: 110,
      suppressSizeToFit: true,
      //   pinned: 'left',
      suppressMenu: false,
    },
    {
      field: 'className',
      headerName: '% Form',
      suppressMenu: false,
      width: 70,
      suppressSizeToFit: true,
    },
    {
      field: 'gender',
      headerName: '% Upto',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'bform',
      headerName: 'Date',
      suppressMenu: false,
      // width: auto,
      minWidth: 100,
      suppressSizeToFit: true,
      // cellRenderer: (data) => {
      //   return moment(data.expiryDate).format('DD/MM/YYYY')
      // }
    },
  ])

  //   const isRowSelectable = useMemo(() => {
  //     return (params) => {
  //       return !!params.data && params.data.expiryDate < moment().format()
  //     }
  //   }, [])

  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }

  const rowSelectionType = 'signle'
  const onSelectionChanged = (event) => {
    const selectedData = event.api.getSelectedRows()
    if (selectedData) {
      setReglst(selectedData[0].id)
    }
  }

  return (
    <>
      <AgGridReact
        className="ag-theme-alpine h100"
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        alwaysShowHorizontalScroll={true}
        alwaysShowVerticalScroll={true}
        onGridReady={onGridReady}
        pagination={false}
        // onSelectionChanged={onSelectionChanged}
        // rowSelection={rowSelectionType}
        // isRowSelectable={isRowSelectable}
      />
    </>
  )
}
export default ExamTable
