import React, { useState, useEffect } from 'react'
import ApexCharts from 'react-apexcharts'
function DayBook() {
  const [state, setState] = useState({
    grid: { show: true },
    series: [
      {
        name: 'Total Recevied Cash',
        data: [44, 55, 57, 56, 61, 58, 63, 60, 66],
      },
      {
        name: 'Bank transferred Amount',
        data: [76, 85, 101, 98, 87, 105, 91, 114, 94],
      },
      {
        name: 'Online transferred Amount',
        data: [35, 41, 36, 26, 45, 48, 52, 53, 41],
      },
    ],
    options: {
      chart: {
        type: 'bar',
        height: '100%',
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '100%',
          endingShape: 'rounded',
        },
      },
      dataLabels: {
        enabled: true,
      },
      stroke: {
        show: true,
        width: 100,
        colors: ['transparent'],
      },
      xaxis: {
        labels: {
          show: true,
        },
        // categories: [
        //   "Feb",
        //   'Mar',
        //   'Apr',
        //   'May',
        //   'Jun',
        //   'Jul',
        //   'Aug',
        //   'Sep',
        //   'Oct',
        // ],
        categories: [
          '1',
          '2',
          '3',
          '4',
          '5',
          '6',
          '7',
          '8',
          '9',
          '10',
          '11',
          '12',
          '13',
          '14',
          '15',
          '16',
          '17',
          '18',
          '19',
          '20',
          '21',
          '22',
          '23',
          '24',
          '25',
          '26',
          '27',
          '28',
          '29',
          '30',
        ],
      },
      //   yaxis: {
      //     title: {
      //       text: '$ (thousands)',
      //     },
      //   },
      fill: {
        opacity: 1,
      },
      title: {
        text: '30 Days Book',
        align: 'left',
        margin: 0,
        offsetX: 10,
        offsetY: 20,
        floating: false,
        style: {
          fontSize: '16px',
          fontWeight: '600',
          color: '#263238',
        },
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return 'Rs.' + val
          },
        },
      },
    },
  })

  useEffect(() => {
    setState({
      series: [
        {
          name: 'Total Received Cash',
          data: [
            44, 55, 57, 56, 61, 58, 63, 60, 66, 44, 55, 57, 56, 61, 58, 63, 60,
            66, 44, 55, 57, 56, 61, 58, 63, 60, 66, 63, 60, 66,
          ],
        },
        {
          name: 'Bank Transferred Amount',
          data: [
            44, 55, 57, 56, 61, 58, 63, 60, 66, 44, 55, 57, 56, 61, 58, 63, 60,
            66, 44, 55, 57, 56, 61, 58, 63, 60, 66, 63, 60, 66,
          ],
        },
        {
          name: 'Online Transferred Amount',
          data: [
            44, 55, 57, 56, 61, 58, 63, 60, 66, 44, 55, 57, 56, 61, 58, 63, 60,
            66, 44, 55, 57, 56, 61, 58, 63, 60, 66, 63, 60, 66,
          ],
        },
      ],
      options: {
        chart: {
          type: 'bar',
          height: '100%',
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '80%',
            endingShape: 'rounded',
          },
        },
        dataLabels: {
          enabled: false,
        },
        stroke: {
          show: true,
          width: 0,
        },
        xaxis: {
          categories: [
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11',
            '12',
            '13',
            '14',
            '15',
            '16',
            '17',
            '18',
            '19',
            '20',
            '21',
            '22',
            '23',
            '24',
            '25',
            '26',
            '27',
            '28',
            '29',
            '30',
          ],
        },
        legend: {
          position: 'top',
          fontSize: '12px',
          //   fontFamily: 'Helvetica, Arial',
          fontWeight: 600,
          itemMargin: {
            horizontal: 20,
            vertical: 20,
          },
        },
        // yaxis: {
        //   title: {
        //     text: '$ (thousands)',
        //   },
        // },
        fill: {
          opacity: 1,
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return 'Rs.' + val
            },
          },
        },
      },
    })
  }, [])
  return (
    <div className="Chart_graph ">
      <ApexCharts
        options={state.options}
        series={state.series}
        type="bar"
        height={350}
      />
    </div>
  )
}

export default DayBook
