import React, { useContext, useEffect, useState } from 'react'
import mainContext from '../../ContextApi/main'
import Cards from '../SubWidgets/card'

const Statistics = ({
  cls,
  title,
  count,
  iconName,
  trendingClass,
  percentage,
  series,
  bigicon,
}) => {
  ///////////////////////////////////////////////////////////
  ///////////////Status One/////////////
  ///////////////////////////////////////////////////////////

  const Settings = [
    {
      options: {
        chart: {
          type: 'bar',
          height: '100%',
        },

        xaxis: {
          categories: [''],
          position: 'top',
          axisBorder: {
            show: true,
          },
          axisTicks: {
            show: false,
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              },
            },
          },
          tooltip: {
            enabled: true,
          },
        },

        plotOptions: {
          bar: {
            borderRadius: 0,
            columnWidth: '100%',
            dataLabels: {
              position: 'top', // top, center, bottom
            },
          },
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val
          },
          offsetY: 0,
          style: {
            fontSize: '12px',
            colors: ['#ffffff'],
          },
        },
        stroke: {
          show: true,
          width: 10,
          colors: ['transparent'],
        },

        fill: {
          opacity: 1,
        },

        tooltip: {
          y: {
            formatter: function (val) {
              return val
            },
            theme: {
              color: ['transparent'],
              background: '#000',
            },
          },
        },
      },
    },
  ]

  ///////////////////////////////////////////////////////////
  ///////////////Status Two/////////////
  ///////////////////////////////////////////////////////////

  return (
    <>
      <Cards
        count={count}
        classCard={cls}
        title={title}
        iconName={iconName}
        trendingclassName={trendingClass}
        percentage={percentage}
        stateSettings={Settings[0].options}
        series={series}
        bigicon={bigicon}
      />
    </>
  )
}
export default Statistics
