import React from 'react';

function Checkbox(props) {
    return (
        <div className={'checkbox' && props.cls ? props.cls : "checkbox"}>
            <input defaultChecked={props.defaultChecked} autoComplete="off" required type={props.type} onChange={props.onChange} onClick={props.onClick} disabled={props.disabled} name={props.name} id={props.fieldname} value={props.value} placeholder={props.fieldname} checked={props.checked} />
            <label htmlFor={props.fieldname}><h6 className="margin0">{props.span}{props.fieldname}</h6></label>

        </div>
    );
}

export default Checkbox;