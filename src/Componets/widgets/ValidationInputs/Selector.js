const Selectors = (props) => {
  return (
    <>
      <div
        className={
          `custom-field selections_box ${props.errors ? 'errors' : ''}` &&
          props.cls
            ? props.cls
            : `custom-field selections_box ${props.errors ? 'errors' : ''}`
        }
      >
        <label>
          <i className={`fa ${props.icon}`}></i>
          <h6 className="margin0">
            {props.span}
            {props.errors ? props.errors : props.fieldname}
          </h6>
        </label>
        <select name={props.name} onChange={props.onChange} value={props.value}>
          <option value="">Select option</option>
          {props.choices &&
            props.choices.map((val) => (
              <option key={val.id} value={val.name}>
                {val.name}
              </option>
            ))}
        </select>
      </div>
    </>
  )
}
export default Selectors
