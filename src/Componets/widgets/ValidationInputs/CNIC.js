/* eslint-disable no-mixed-operators */

const CNIC = (props) => {


    return (
        // size="4" maxlength="3"
        <>

            <div className={'custom-field' && props.cls || "custom-field"}>
                <input autoComplete="off" size="4" maxlength="16" required type={props.type} onChange={props.onChange} disabled={props.disabled} name={props.name} id={props.fieldname} value={props.value} placeholder={props.fieldname} />
                <label htmlFor={props.fieldname}>{props.span}{props.fieldname}</label>
            </div>
        </>


    )

}
export default CNIC;




