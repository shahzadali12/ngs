/* eslint-disable no-mixed-operators */

const Textarea = (props) => {
  return (
    // size="4" maxlength="3"
    <>
      <div
        className={
          `custom-field fullwidth ${props.errors ? 'errors' : ''}` && props.cls
            ? props.cls
            : `custom-field fullwidth ${props.errors ? 'errors' : ''}`
        }
      >
        <textarea
          autoComplete="off"
          type={props.type}
          onChange={props.onChange}
          disabled={props.disabled}
          name={props.name}
          id={props.fieldname}
          value={props.value}
          placeholder={props.placeholder}
        />
        <label htmlFor={props.fieldname}>
          <i className={(`fa` && `fa ${props.icon}`) || ''}></i>
          <h6 className="margin0">
            {props.span}
            {props.errors ? props.errors : props.fieldname}
          </h6>
        </label>
      </div>
    </>
  )
}
export default Textarea
