import ReactDOM from 'react-dom'
const Input = (props) => {
  return (
    // size="4" maxlength="3"
    <>
      <div
        className={
          `custom-field ${props.errors ? 'errors' : ''}` && props.cls
            ? props.cls
            : `custom-field ${props.errors ? 'errors' : ''}`
        }
      >
        <input
          defaultChecked={props.defaultChecked}
          autoComplete="off"
          type={props.type}
          onChange={props.onChange}
          onClick={props.onClick}
          disabled={props.disabled}
          name={props.name}
          id={props.id}
          value={props.value}
          placeholder={props.fieldname}
          checked={props.checked}
          {...props.baseTitle}
          ref={props.ref}
        />
        <label htmlFor={props.id}>
          <i
            className={
              `fa` && `fa ${props.icon}`
                ? `fa ${props.icon}`
                : `fa ${props.icon}`
            }
          ></i>
          <h6 className="margin0">
            {props.span}
            {props.errors ? props.errors.message : props.fieldname}
          </h6>
        </label>
      </div>
    </>
  )
}
export default Input
