import React from 'react'
import Chart from 'react-apexcharts'
import { Link } from 'react-router-dom'

export default function PieChart({
  cls,
  title,
  count,
  iconName,
  trendingClass,
  percentage,
  series,
  link,
}) {
  const state = {
    options: {
      chart: {
        width: 380,
        type: 'pie',
      },

      labels: ['Presents', 'Absents', 'Late', 'Leave'],
      legend: {
        position: 'bottom',
      },

      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200,
            },
            legend: {
              position: 'bottom',
            },
          },
        },
      ],
    },
  }

  return (
    <>
      <div className={cls}>
        <div className="flex pieChars">
          <h5>
            {title}
            <Link to={link}>
              <i className="fa fa-eye" aria-hidden="true"></i>View Details
            </Link>
          </h5>

          <i className={`${iconName} designicon`}></i>
        </div>
        <Chart options={state.options} series={series} type="pie" />
      </div>
    </>
  )
}
