import { useRef, useCallback } from 'react'
import Webcam from 'react-webcam'

function Camra({ setSnapcapture, Snapcapture, setPhotoSrc }) {
  const webcamRef = useRef(null)

  const capture = () => {
    const snapSrc = webcamRef.current.getScreenshot()
    setPhotoSrc(snapSrc)
    setSnapcapture('Retake')
  }

  return (
    <>
      <div className="webCam">
        <Webcam audio={false} ref={webcamRef} screenshotFormat="image/jpeg" />
        <button className="btn_capture" onClick={capture} type="button">
          <i className="fa fa-camera" aria-hidden="true"></i>
          {Snapcapture}
        </button>
      </div>
    </>
  )
}
export default Camra
