import CreatableSelect from 'react-select/creatable'
import React, { useState, useCallback, useEffect, useContext } from 'react'
import mainContext from '../../ContextApi/main'
function CreateSelectBox({ lstAreas }) {
  ///////////////////////////////////////////////////////////////////
  ///////////////////////   Change OBJECT Key //////////////////////
  ///////////////////////////////////////////////////////////////////

  const [data, setData] = useState([])
  const [options, setArea] = useState([])
  useEffect(() => {
    setData(lstAreas)
  }, [lstAreas])

  const regcontxt = useContext(mainContext)

  const { setValue, errors, pArea, areaId } = regcontxt

  const AreasObj = data
  const changeKeyObjects = (AreasObj) => {
    // debugger
    let res = []
    for (let i = 0; i < AreasObj.length; i++) {
      let obj = {
        value: AreasObj[i].id,
        label: AreasObj[i].name,
      }
      res.push(obj)
    }
    setArea(res)
  }
  useEffect(() => {
    if (AreasObj) {
      changeKeyObjects(AreasObj)
    }
  }, [AreasObj])

  ///////////////////////////////////////////////////////////////////
  ///////////////////////   Change OBJECT NAME  end//////////////////////
  ///////////////////////////////////////////////////////////////////
  const [inputValue, setInputValue] = useState('')
  const [selection, setSelection] = useState(options) ///options  <= Add api data

  const [selectedValues, setSelectedValues] = useState({ selected: [] })
  const [current, setCurrent] = useState([])
  const handleInputChange = useCallback(
    (value) => {
      setInputValue(value)
    },
    [setInputValue]
  )
  const setNewValue = useCallback(
    (selectedvalue) => {
      const difference = current.filter((x) => !inputValue.includes(x))
      const newOption = {
        value: selectedvalue,
        label: inputValue.toString(),
      }
      inputValue !== '' && setSelection([...selection, newOption])
      setCurrent(current)

      setInputValue('')
      setSelectedValues(difference)
      // setFormData({
      //   ...formData,
      //   regParent: { ...formData.regParent, areaId: selectedvalue.value },
      // })

      if (selectedvalue.value) {
        setValue('areaId', selectedvalue.value)
      }
    },
    [setSelectedValues, current, inputValue, selection]
  )

  const customStyles = {
    option: (provided, state, isSelected) => ({
      ...provided,
      color: state.isSelected ? '#fff' : '#0656ff',
      background: state.isSelected
        ? 'linear-gradient(14deg, #0656ff 0%, rgba(6, 86, 255, 0.6))'
        : '',
      padding: 10,
      margin: 0,
      'option--is-selected': isSelected,
    }),

    singleValue: (provided, state) => {
      const opacity = state.isDisabled ? 0.5 : 1
      const transition = 'opacity 300ms'

      return { ...provided, opacity, transition }
    },
  }
  return (
    <>
      <CreatableSelect
        options={options}
        className={`selections_box ${errors.areaId ? 'errors' : ''}`}
        styles={customStyles}
        onChange={setNewValue}
        onInputChange={handleInputChange}
        inputValue={inputValue}
        value={selectedValues.selected}
        controlShouldRenderValue={true}
      />
    </>
  )
}

export default CreateSelectBox
