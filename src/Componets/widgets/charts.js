import { render } from '@testing-library/react'
import React, { useEffect, useState } from 'react'
import Chart from 'react-apexcharts'
function Charts() {
  const [state, setState] = useState({
    grid: { show: false },
    series: [
      {
        name: 'Mid Semester 1',
        type: 'column',
        data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30],
      },
      {
        name: 'End Semester 1',
        type: 'area',
        data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43],
      },
      {
        name: 'Mid Semester 2',
        type: 'line',
        data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
      },
      {
        name: 'End Semester 2 Final',
        type: 'line',
        data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
      },
    ],
    options: {
      chart: {
        height: 350,
        type: 'line',
        stacked: false,
      },
      stroke: {
        width: [0, 2, 5],
        curve: 'smooth',
      },

      plotOptions: {
        bar: {
          columnWidth: '50%',
        },
        labels: {
          render: 'image',
          textMargin: -30,
          images: [
            {
              src: 'https://i.stack.imgur.com/9EMtU.png',
              width: 20,
              height: 20,
            },
            null,
            {
              src: 'https://i.stack.imgur.com/9EMtU.png',
              width: 20,
              height: 20,
            },
          ],
        },
      },
      legend: {
        position: 'top',
        fontSize: '12px',
        //   fontFamily: 'Helvetica, Arial',
        fontWeight: 600,
        itemMargin: {
          horizontal: 20,
          vertical: 20,
        },
      },
      fill: {
        opacity: [0.85, 0.25, 1],
        gradient: {
          inverseColors: false,
          shade: 'light',
          type: 'vertical',
          opacityFrom: 0.85,
          opacityTo: 0.55,
          stops: [0, 100, 100, 100],
        },
      },

      labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
      markers: {
        size: 100,
      },
      xaxis: {
        type: 'datetime',
        min: 0,
      },
      yaxis: {
        title: {
          text: '',
        },
        min: 0,
      },
      tooltip: {
        shared: true,
        intersect: false,
        y: {
          formatter: function (y) {
            if (typeof y !== 'undefined') {
              return y.toFixed(0) + ' '
            }
            return y
          },
        },
      },
    },
  })
  useEffect(() => {
    setState({
      series: [
        {
          name: 'Mid Semester 1',
          type: 'column',
          data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30],
        },
        {
          name: 'End Semester 1',
          type: 'area',
          data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43],
        },
        {
          name: 'Mid Semester 2',
          type: 'line',
          data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
        },
        {
          name: 'End Semester 2 Final',
          type: 'line',
          data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
        },
      ],
      options: {
        chart: {
          height: 350,
          type: 'line',
          stacked: false,
        },
        stroke: {
          width: [0, 2, 5],
          curve: 'smooth',
        },
        plotOptions: {
          bar: {
            columnWidth: '50%',
          },
        },

        fill: {
          opacity: [0.85, 0.25, 1],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: 'vertical',
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100],
          },
        },
        labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],

        markers: {
          size: 0,
        },
        xaxis: {
          type: 'text',
        },
        yaxis: {
          title: {
            text: '',
          },
          min: 0,
        },
        title: {
          text: 'Examination Statistics',
          align: 'left',
          margin: 0,
          offsetX: 10,
          offsetY: 20,
          floating: false,
          style: {
            fontSize: '16px',
            fontWeight: '600',
            color: '#263238',
          },
        },
        tooltip: {
          shared: false,
          intersect: false,
          y: {
            formatter: function (y) {
              if (typeof y !== 'undefined') {
                return y.toFixed(0) + ' '
              }
              return y
            },
          },
        },
      },
    })
  }, [])

  return (
    <div className="Chart_graph ">
      <Chart
        options={state.options}
        series={state.series}
        type="line"
        height={350}
      />
    </div>
  )
}

export default Charts
