import React, { useState } from 'react'
// import AgGrid from "../../Pages/Tables";
// import "./Modal.css";

export default function Modal() {
  const [modal, setModal] = useState(false)

  const toggleModal = () => {
    setModal(!modal)
  }

  if (modal) {
    document.body.classList.add('active-modal')
  } else {
    document.body.classList.remove('active-modal')
  }

  return (
    <>
      <button onClick={toggleModal} className="btn-modal">
        Open
      </button>

      {modal && (
        <div className="popup_model">
          <div onClick={toggleModal} className="overlay"></div>
          <div className="modal-content">
            {/* <AgGrid /> */}
            <button className="close-modal" onClick={toggleModal}>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      )}
    </>
  )
}
