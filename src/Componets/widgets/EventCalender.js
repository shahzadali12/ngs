import React, { useState } from 'react'
import { Calendar, momentLocalizer } from 'react-big-calendar'

import moment from 'moment'
import 'react-big-calendar/lib/css/react-big-calendar.css'

export default function EventCalender() {
  const localizer = momentLocalizer(moment)

  function getDate(dayString) {
    const today = new Date()
    const year = today.getFullYear().toString()
    let month = (today.getMonth() + 1).toString()

    if (month.length === 1) {
      month = '0' + month
    }

    return dayString.replace('year', year).replace('month', month)
  }

  const events = [
    {
      start: moment().toDate(),
      end: moment().add(0, 'days').toDate(),
      title: 'NGS Portal',
    },
    {
      start: getDate('year-month-22'),
      end: moment(getDate('year-month-22')).add(0, 'days').toDate(),
      title: 'Holiday in pakistan',
    },
    {
      start: getDate('year-month-13'),
      end: moment(getDate('year-month-13')).add(0, 'days').toDate(),
      title: 'Holiday',
    },
    {
      start: getDate('year-month-19'),
      end: moment(getDate('year-month-19')).add(0, 'days').toDate(),
      title: 'Holiday',
    },
  ]
  const [selectedDay, setState] = useState({})
  return (
    <Calendar
      selectable
      className="calenderHeight"
      localizer={localizer}
      defaultDate={new Date()}
      defaultView="month"
      events={events}
      date={selectedDay}
      onNavigate={(date) => {
        setState({ selectedDate: date })
      }}
    />
  )
}
