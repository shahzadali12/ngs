/* eslint-disable array-callback-return */
import React, { useMemo, useState, useContext } from 'react'
import mainContext from '../../ContextApi/main'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'
import axios from 'axios'
import moment from 'moment'
const ExpensesTable = () => {
  const {
    setAdmissionData,
    admissionData,
    setValue,
    regkey,
    setKey,
    setReglst,
  } = useContext(mainContext)
  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      minWidth: 100,
      resizable: false,
      filter: false,
      floatingFilter: false,
    }
  }, [])

  const [columnDefs, setColumnDefs] = useState([
    {
      field: 'regDate',
      headerName: 'ID No',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      minWidth: 110,
      suppressSizeToFit: true,
      cellRenderer: (data) => {
        return moment(data.regDate).format('DD/MM/YYYY')
      },
    },
    {
      field: 'id',
      headerName: 'Expense Type',
      suppressMenu: false,
      // editable: true,
      // width: auto,
      minWidth: 100,
      suppressSizeToFit: true,
      cellRenderer: (data) => {
        return moment(data.regDate).format('DD/MM/YYYY')
      },
    },

    {
      field: 'studentFirstName',
      headerName: 'Amount',
      minWidth: 110,
      suppressSizeToFit: true,
      //   pinned: 'left',
      suppressMenu: false,
    },
    {
      field: 'expiryDate',
      headerName: 'Status',
      suppressMenu: false,
      // width: auto,
      minWidth: 80,
      suppressMenu: false,
      filter: false,

      cellRenderer: ({ data }) => {
        // // console.log(data, 'show me the data form Api')
        return (
          <>
            {/* {data && data.expiryDate < moment(new Date()).format() ? (
              <span className="status_btn red">Due</span>
            ) : (
              <span className="status_btn green">Paid</span>
            )} */}
            {data && data.gender === '1' ? (
              //   <span className="status_btn">Expiry</span>
              <span className="status_btn green">Paid</span>
            ) : (
              <span className="status_btn red">Due</span>
            )}
            {/* {data && data.gender === '1' ? (
                  <li className="sick-days-Green">
                    <span></span>
                  </li>
                ) : (
                  ''
                )} */}
          </>
        )
      },
    },
    {
      field: 'gender',
      headerName: 'E-mail',
      suppressMenu: false,
      // width: auto,
      suppressSizeToFit: true,
    },
    {
      field: 'expiryDate',
      headerName: 'Date',
      suppressMenu: false,
      // width: auto,
      minWidth: 120,
      suppressSizeToFit: true,
      // cellRenderer: (data) => {
      //   return moment(data.expiryDate).format('DD/MM/YYYY')
      // }
    },
  ])

  //   const isRowSelectable = useMemo(() => {
  //     return (params) => {
  //       return !!params.data && params.data.expiryDate < moment().format()
  //     }
  //   }, [])

  const onGridReady = (params) => {
    fetch(
      `${process.env.REACT_APP_NGSBASEURL}Registration/RegistrationList?LoginId=1`
    )
      // fetch('https://mocki.io/v1/3992eb8c-ef46-45c4-bce2-39a63472eadf')
      .then((resp) => resp.json())
      .then((resp) => {
        params.api.applyTransaction({ add: resp.registration })
        // params.api.applyTransaction({ add: resp })
      })
  }

  const rowSelectionType = 'signle'
  const onSelectionChanged = (event) => {
    const selectedData = event.api.getSelectedRows()
    if (selectedData) {
      setReglst(selectedData[0].id)
    }
  }

  return (
    <>
      <AgGridReact
        className="ag-theme-alpine h100"
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        alwaysShowHorizontalScroll={true}
        alwaysShowVerticalScroll={true}
        onGridReady={onGridReady}
        pagination={false}
        // onSelectionChanged={onSelectionChanged}
        // rowSelection={rowSelectionType}
        // isRowSelectable={isRowSelectable}
      />
    </>
  )
}
export default ExpensesTable
