import React from 'react';

function Students(props) {
    return (
        <>

            <div className='student_list'>
                {/* <img src={process.env.PUBLIC_URL + '/logoB.png'} alt='img here'/> */}
                <img src={props.img} alt={props.alt} />
                <div className='custom_deta'>
                    <h6>{props.id} - {props.name}</h6>
                    <span>{props.phone}</span>
                    <span>{props.email}</span>
                    <p>{props.address}</p>
                </div>
            </div>
                

        </>
    );
}

export default Students;