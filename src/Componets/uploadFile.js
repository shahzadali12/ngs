export default function FileUpload({ onFileChange, fileName, ctrlName }) {
    return (
        <>
            <div className="btn-normal">
                <input accept="image/*" id={fileName} onClick={(e) => { e.target.value = null; // console.log(e); }} onChange={onFileChange} name={ctrlName} type="file"  />
                <label htmlFor={fileName}>{fileName}</label>
            </div>
        </>
    );
}