import { useEffect, useRef } from 'react'

export const useOnOutsideClick = (ref, callback) => {
  const callbackRef = useRef(callback)

  useEffect(() => {
    callbackRef.current = callback
  }, [callback])

  useEffect(() => {
    const handler = (event) => {
      const element = ref.current
      if (element) {
        if (!element.contains(event.target)) {
          callbackRef.current(event)
        }
      }
    }

    document.addEventListener('mousedown', handler)
    document.addEventListener('touchstart', handler)

    // document.addEventListener('click', handler);

    return () => {
      document.removeEventListener('mousedown', handler)
      document.removeEventListener('touchstart', handler)

      // document.removeEventListener('click', handler);
    }
  }, [])
}
