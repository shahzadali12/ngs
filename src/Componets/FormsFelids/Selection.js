import React from 'react'

const Select = ({ name, value, onChange, choices, error, icon }) => {
  return (
    <div className={'custom-field selections_box'}>
      <div
        className={
          error ? 'select is-fullwidth is-danger' : 'select is-fullwidth'
        }
      >
        <label>
          <i className={`fa ${icon}`}></i>
          <h6 className="margin0">
            <span></span>
            {name}
          </h6>
        </label>
        <select name={name} value={value} onChange={onChange}>
          {choices.map((choice, index) => (
            <option key={index} value={choice.value}>
              {choice.label}
            </option>
          ))}
        </select>
      </div>
      {error && <div className="has-text-danger-dark">{error}</div>}
    </div>
  )
}

export default Select
