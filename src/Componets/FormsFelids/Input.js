import React from 'react'

const Input = ({
  type,
  placeholder,
  name,
  value,
  onChange,
  error,
  icon,
  cls,
}) => {
  return (
    <div className={'custom-field' && cls ? cls : 'custom-field'}>
      <input
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
        autoComplete="off"
      />
      <label htmlFor={value}>
        <i className={`fa` && `fa ${icon}` ? `fa ${icon}` : `fa ${icon}`}></i>
        <h6 className="margin0">
          <span></span>
          {error ? `${name} - ${error}` : name}
        </h6>
      </label>
    </div>
  )
}

export default Input
