const [lstCity, setlstCity] = useState([])
const old_objVal = lstCitys
const changeKeyObjects = (old_objVal) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objVal.length; i++) {
    let obj = {
      value: old_objVal[i].id,
      label: old_objVal[i].name,
    }
    res.push(obj)
  }
  setlstCity(res)
}
useEffect(() => {
  if (old_objVal) {
    changeKeyObjects(old_objVal)
  }
}, [old_objVal])

////////////////////////////////////////////////////

const [lstArea, setlstArea] = useState([])
const old_objValArea = lstAreas
const changeKeyObjArea = (old_objValArea) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objValArea.length; i++) {
    let obj = {
      value: old_objValArea[i].id,
      label: old_objValArea[i].name,
    }
    res.push(obj)
  }
  setlstArea(res)
}
useEffect(() => {
  if (old_objValArea) {
    changeKeyObjArea(old_objValArea)
  }
}, [old_objValArea])

////////////////////////////////////////////////////

const [lstTestType, setlstTestType] = useState(null)
const old_objValTest = lstTestTypes
const changeKeyObjTest = (old_objValTest) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objValTest.length; i++) {
    let obj = {
      value: old_objValTest[i].id,
      label: old_objValTest[i].name,
    }
    res.push(obj)
  }
  setlstTestType(res)
}
useEffect(() => {
  if (old_objValTest) {
    changeKeyObjTest(old_objValTest)
  }
}, [old_objValTest])
////////////////////////////////////////////////////

const [lstClassplan, setlstClassplan] = useState(null)
const old_objValClassplan = Classplan
const changeKeyObjClassplan = (old_objValClassplan) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objValClassplan.length; i++) {
    let obj = {
      value: old_objValClassplan[i].id,
      label: old_objValClassplan[i].name,
    }
    res.push(obj)
  }
  setlstClassplan(res)
}
useEffect(() => {
  if (old_objValClassplan) {
    changeKeyObjClassplan(old_objValClassplan)
  }
}, [old_objValClassplan])
////////////////////////////////////////////////////

const [lstSession, setlstSession] = useState(null)
const old_objValSession = lstSessions
const changeKeyObjSession = (old_objValSession) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objValSession.length; i++) {
    let obj = {
      value: old_objValSession[i].id,
      label: old_objValSession[i].name,
    }
    res.push(obj)
  }
  setlstSession(res)
}
useEffect(() => {
  if (old_objValSession) {
    changeKeyObjSession(old_objValSession)
  }
}, [old_objValSession])

////////////////////////////////////////////////////

const [lstOccupation, setlstOccupation] = useState(null)
const old_objValOccupation = lstOccupations
const changeKeyObjOccupation = (old_objValOccupation) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objValOccupation.length; i++) {
    let obj = {
      value: old_objValOccupation[i].id,
      label: old_objValOccupation[i].name,
    }
    res.push(obj)
  }
  setlstOccupation(res)
}
useEffect(() => {
  if (old_objValOccupation) {
    changeKeyObjOccupation(old_objValOccupation)
  }
}, [old_objValOccupation])

////////////////////////////////////////////////////

const [lstRelationship, setlstRelationship] = useState(null)
const old_objValRalationship = lstRelationships
const changeKeyObjRelation = (old_objValRalationship) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objValRalationship.length; i++) {
    let obj = {
      value: old_objValRalationship[i].id,
      label: old_objValRalationship[i].name,
    }
    res.push(obj)
  }
  setlstRelationship(res)
}
useEffect(() => {
  if (old_objValRalationship) {
    changeKeyObjRelation(old_objValRalationship)
  }
}, [old_objValRalationship])

////////////////////////////////////////////////////

const [chooseMotherLanguages, setlstMotherLanguages] = useState(null)
const old_objVallanguage = lstMotherLanguages
const changeKeyObjLanguage = (old_objVallanguage) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objVallanguage.length; i++) {
    let obj = {
      value: old_objVallanguage[i].id,
      label: old_objVallanguage[i].name,
    }
    res.push(obj)
  }
  setlstMotherLanguages(res)
}
useEffect(() => {
  if (old_objVallanguage) {
    changeKeyObjLanguage(old_objVallanguage)
  }
}, [old_objVallanguage])

////////////////////////////////////////////////////

const [chooselstReligions, setlstReligions] = useState(null)
const old_objValReligion = lstReligions
const changeKeyObjReligion = (old_objValReligion) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objValReligion.length; i++) {
    let obj = {
      value: old_objValReligion[i].id,
      label: old_objValReligion[i].name,
    }
    res.push(obj)
  }
  setlstReligions(res)
}
useEffect(() => {
  if (old_objValReligion) {
    changeKeyObjReligion(old_objValReligion)
  }
}, [old_objValReligion])

////////////////////////////////////////////////////

const [chooseGender, setChooseGender] = useState(null)
const old_objGender = Gender
const changeKeyObjGender = (old_objGender) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objGender.length; i++) {
    let obj = {
      value: old_objGender[i].id,
      label: old_objGender[i].name,
    }
    res.push(obj)
  }
  setChooseGender(res)
}
useEffect(() => {
  if (old_objGender) {
    changeKeyObjGender(old_objGender)
  }
}, [old_objGender])

////////////////////////////////////////////////////

const [lstFeeTemplates, setlstFeeTemplates] = useState(null)
const old_objfeetemplate = lstfeeTemplates
const changeKeyObjfeetemplate = (old_objfeetemplate) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objfeetemplate.length; i++) {
    let obj = {
      value: old_objfeetemplate[i].id,
      label: old_objfeetemplate[i].name,
    }
    res.push(obj)
  }
  setlstFeeTemplates(res)
}
useEffect(() => {
  if (old_objfeetemplate) {
    changeKeyObjfeetemplate(old_objfeetemplate)
  }
}, [old_objfeetemplate])

////////////////////////////////////////////////////

const [lstFeeHeads, setlstFeeHeads] = useState(null)
const old_objfeehead = lstfeeHeads
const changeKeyObjfeehead = (old_objfeehead) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objfeehead.length; i++) {
    let obj = {
      value: old_objfeehead[i].id,
      label: old_objfeehead[i].name,
    }
    res.push(obj)
  }
  setlstFeeHeads(res)
}
useEffect(() => {
  if (old_objfeehead) {
    changeKeyObjfeehead(old_objfeehead)
  }
}, [old_objfeehead])
