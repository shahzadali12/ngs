import React, { useEffect, useMemo } from 'react'
import Select from 'react-select'

const initialState = {
  screen: { value: 'InitialScreening', label: 'Initial Screening' },
  level: '',
  user: '',
}

const userData = [
  {
    username: 'Aaron_Stepp',
    maker: true,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Daniel_Chancellor',
    maker: false,
    checker1: false,
    checker2: true,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'dev_sams',
    maker: true,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'dev_sams3',
    maker: false,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: true,
  },
  {
    username: 'dev_sams5',
    maker: false,
    checker1: false,
    checker2: false,
    checker3: true,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Donald_Smith',
    maker: false,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: true,
    checker5: false,
  },
  {
    username: 'Gerald_Middlebrooks',
    maker: false,
    checker1: true,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'kff22450@eoopy.com',
    maker: false,
    checker1: true,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Laurel_Guerrero',
    maker: false,
    checker1: false,
    checker2: false,
    checker3: true,
    checker4: false,
    checker5: false,
  },
  {
    username: 'maker2',
    maker: true,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'maker3',
    maker: false,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: true,
  },
  {
    username: 'Marlin_Gayle',
    maker: true,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Mary_Aiello',
    maker: false,
    checker1: true,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Mary_Hillin',
    maker: false,
    checker1: true,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Megan_Collins',
    maker: false,
    checker1: false,
    checker2: true,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Michael_Cruz',
    maker: false,
    checker1: false,
    checker2: true,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Michael_Principe',
    maker: false,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: true,
    checker5: false,
  },
  {
    username: 'Oliver_Rokus',
    maker: true,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Roland_Lenz',
    maker: false,
    checker1: false,
    checker2: false,
    checker3: true,
    checker4: false,
    checker5: false,
  },
  {
    username: 'Ruth_Voves',
    maker: false,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: true,
    checker5: false,
  },
  {
    username: 'Tyler_Martindale',
    maker: false,
    checker1: true,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
  {
    username: 'yejlhnqzcdeahwzcpi@upived.online',
    maker: true,
    checker1: false,
    checker2: false,
    checker3: false,
    checker4: false,
    checker5: false,
  },
]

export default function App() {
  const [data, setData] = React.useState(userData)
  const [DashboardPageStatus, setDashboardPageStatus] = React.useState('')
  const [index, setIndex] = React.useState(null)
  const [userlevel, setUserlevel] = React.useState({})
  const [formData, setFormData] = React.useState(initialState)
  const { screen, level } = formData
  const [value, setValue] = React.useState('')

  const handleSelectChangeL = (object, action, selected) => {
    setIndex(data[object.id])
    setUserlevel(null)
    setValue(selected)
  }
  const options = useMemo(() => {
    if (index) {
      let levelList = []
      for (let [key, value] of Object.entries(index)) {
        if (value === true) {
          levelList.push({ value: key, label: key })
        }
      }
      return levelList
    }
  }, [index])

  useEffect(() => {
    if (index) {
      setUserlevel(options[0])
    }
  }, [options, index])

  const handlechange = (object, action) => {
    let name = action.name
    let value = object.value
    setUserlevel({ value: value, label: name })
  }

  return (
    <div className="App">
      <Select
        className="drop-down"
        options={data?.map((data, index) => ({
          value: data.username,
          label: data.username,
          id: index,
        }))}
        name="user"
        value={value}
        onChange={handleSelectChangeL}
        placeholder="Analyst Name"
        theme={(theme) => ({
          ...theme,
          colors: {
            ...theme.colors,
            text: 'black',
            primary25: '#d6fdf7',
            primary: '#0bb7a7',
            primary50: '#d6fdf7',
          },
        })}
      ></Select>

      {index !== null && (
        <Select
          className="drop-down"
          options={options}
          name="level"
          value={userlevel}
          // defaultValue={{ label: userlevel, value: userlevel }}
          onChange={handlechange}
          placeholder="Role"
          theme={(theme) => ({
            ...theme,
            colors: {
              ...theme.colors,
              text: 'white',
              primary25: '#d6fdf7',
              primary: '#0bb7a7',
              primary50: '#d6fdf7',
            },
          })}
        ></Select>
      )}
    </div>
  )
}
