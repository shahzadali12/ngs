const old_objVal = lstCity
const changeKeyObjects = (old_objVal) => {
  // debugger
  let res = []
  for (let i = 0; i < old_objVal.length; i++) {
    let obj = {
      value: old_objVal[i].id,
      label: old_objVal[i].name,
    }
    res.push(obj)
  }
  setArea(res)
}
useEffect(() => {
  if (old_objVal) {
    changeKeyObjects(old_objVal)
  }
}, [old_objVal])
