// import React,{useState} from 'react';

// function Brightness() {

//     const [brightness, setBrightness] = useState();
//     const BrightnessChange = (e) => {
//         setBrightness(e.target.value);
//     }

//     return (
//         <div className="Filer">
//             <label htmlFor="brightness">Brightness</label>
//             <input type="range" id="brightness" min={0} max={50} step="0.01" onChange={BrightnessChange} value={brightness}/>
//         </div>
//     );
// }
// function Contrast() {

//     const [contrast, setContrast] = useState();
//     const ContrastChange = (e) => {
//         setContrast(e.target.value);
//     }

//     return (
//         <div className="Filer">
//             <label htmlFor="contrast">Contrast</label>
//             <input type="range" id="contrast" min={ 0 } max={200} step="0.01" onChange={ContrastChange} value={contrast}/>
//         </div>
//     );
// }

// const ColorImage = (props) => <img src={props.img} style={{filter: `${props.type}(${props.state}${props.unit ? props.unit : ''})`}} alt="img here"/>

// export {Brightness, Contrast, ColorImage};

import React from 'react'
function ImageEffeact({
  selectedOptionIndex,
  options,
  setOptions,
  selectedOption,
  handleSliderChange,
  getImageStyle,
  setSelectedOptionIndex,
}) {
  return (
    <div className="buttons_design">
      {/* <img style={getImageStyle()} src={`https://images.wallpaperscraft.com/image/single/camera_lens_strap_145518_2048x1152.jpg`} alt="" /> */}

      {options.map((option, index) => {
        return (
          <div
            key={index}
            className={`colorsBtns ${
              index === selectedOptionIndex ? 'active' : ''
            }`}
            onClick={() => setSelectedOptionIndex(index)}
          >
            {option.name}
          </div>
        )
      })}

      <input
        type="range"
        className="slider"
        min={selectedOption.range.min}
        max={selectedOption.range.max}
        value={selectedOption.value}
        onChange={handleSliderChange}
      />
    </div>
  )
}
export default ImageEffeact
