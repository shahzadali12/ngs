import { useState, useEffect } from 'react'
import Chart from 'react-apexcharts'
import '../../styles/style.css'

const Cards = (props) => {
  return (
    <>
      {/* <div className={props.classCard}>
        <h4 className="sm-title">{props.title}</h4>
        <div className="flex">
          <h2>{props.count}</h2>
          <i className={props.iconName}></i>
        </div>
        <p>
          <span>
            <i className={props.trendingClass}></i>
            {props.percentage}
          </span>
          Up From Yesterday
        </p>
      </div> */}

      <div className={props.classCard}>
        <div className="flex">
          <h2>
            {props.count}
            <p>{props.title}</p>
          </h2>
          {props.iconName === '' ? (
            <span className="bigicon">{props.bigicon}</span>
          ) : (
            <i className={`${props.iconName} designicon`}></i>
          )}
        </div>

        {props.series && (
          <Chart
            options={props.stateSettings}
            series={props.series}
            type="bar"
            height={200}
          />
        )}
      </div>
    </>
  )
}
export default Cards
