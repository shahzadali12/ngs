import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Upload from 'rc-upload'
import { toast, ToastContainer } from 'react-toastify'

import '../../../node_modules/react-toastify/dist/ReactToastify.css'
const Attachment = ({ CaptureImage, setFormData, formData, className }) => {
  // const [destroyed, setDestroyed] = useState(false)

  const [files, setFile] = useState([])
  const [fileResult, setResult] = useState([])
  const [onSuccess, setSuccess] = useState([])
  const [show, setShow] = useState([])

  const Attachment = {
    action: `http://172.17.0.15:8001/api/FileUpload/SaveFile`,
    accept: '.Tiff, .png, .jpeg, .pdf, .jpg',
    multiple: false,
    beforeUpload(file) {
      if (file.size <= 2e6) {
        return new Promise((resolve) => {
          var reader = new FileReader()
          reader.onload = () => {
            setShow({ file: file, document: reader.result })
          }
          reader.readAsDataURL(file)
          resolve(file)
        })
      } else {
        toast.error('Please upload a file smaller than 2 MB')
      }
    },
    //
    onProgress(step, file) {
      if (file.size <= 2e6) {
        setTimeout(() => setSuccess(Math.round(step.percent)), 1000)
      }
    },
    onSuccess(step, file, result) {
      if (file.size <= 2e6) {
        toast.success('Upload Success')
        setResult(result)
        setFile(step.percent)

        return file
      }
      return false
    },
    onError(err) {
      // // console.log('onError', err)
      toast.error('Server not found!')
    },
    customRequest({
      action,
      data,
      file,
      filename,
      headers,
      onError,
      onProgress,
      onSuccess,
      withCredentials,
    }) {
      const FormFile1 = new FormData()
      if (data) {
        Object.keys(data).forEach((key) => {
          FormFile1.append(key, data[key])
        })
      }
      FormFile1.append(filename, file)

      axios
        .post(action, FormFile1, {
          withCredentials,
          headers,
          onUploadProgress: ({ total, loaded }) => {
            onProgress(
              { percent: Math.round((loaded / total) * 100).toFixed(2) },
              file
            )
          },
        })
        .then(({ data: response }) => {
          onSuccess(response, file)

          // setFormData({
          //   ...formData,
          //   regStd: {
          //     ...formData.regStd,
          //     studentNicBform_file: response.filePath,
          //   },
          // })
        })
        .catch(onError)

      return {
        abort() {
          // console.log('upload progress is aborted.')
        },
      }
    },
  }

  // // console.log(show)

  return (
    <div className={className}>
      {/* <ToastContainer /> */}

      <Upload
        className={`btn-normal ${files ? files : 'successfully'}`}
        {...Attachment}
      >
        Attachment File
      </Upload>
      {show != '' && (
        <>
          <img src={show.document} alt="images" />
        </>
      )}
    </div>
  )
}

export default Attachment
