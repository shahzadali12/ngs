import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Upload from 'rc-upload'
import { toast, ToastContainer } from 'react-toastify'

import '../../../node_modules/react-toastify/dist/ReactToastify.css'
const LeaveFile = (props) => {
  // const [destroyed, setDestroyed] = useState(false)

  const [files, setFile] = useState([])
  const [fileResult, setResult] = useState([])
  const [onSuccess, setSuccess] = useState([])

  const uploaderProps = {
    action: `${process.env.REACT_APP_NGSBASEURL}FileUpload/SaveFile`,
    // data: { CaptureImage },
    accept: '.png, .pdf, .txt',
    // capture: 'CaptureImage',
    multiple: false,

    beforeUpload(file) {
      // console.log(file)
    },
    onProgress(step, file) {
      setTimeout(() => setSuccess(Math.round(step.percent)), 1000)
    },
    onSuccess(step, file, result) {
      setFile(step.percent)
      toast.success('Upload Success')
      setResult(result)
    },
    onError(err) {
      // console.log('onError', err)
    },
  }

  let results = fileResult.response
  // useEffect(() => {
  //   // fileResult

  //   setFormData({
  //     ...formData,
  //     regStd: { ...formData.regStd, studentPicture: results },
  //   })
  // }, [results])

  // // console.log(results)

  return (
    <>
      <ToastContainer />

      <Upload
        className={`btn-normal ${files ? files : 'successfully'}`}
        {...uploaderProps}
      >
        {props.fileName}
      </Upload>
    </>
  )
}

export default LeaveFile
