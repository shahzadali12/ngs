import React, { useContext, useCallback } from 'react'
import mainContext from '../../ContextApi/main'
import Upload from 'rc-upload'
import axios from 'axios'
import { toast, ToastContainer } from 'react-toastify'
import ProgressBar from 'react-bootstrap/ProgressBar'
import '../../../node_modules/react-toastify/dist/ReactToastify.css'
const UploadedFiles = () => {
  // const [destroyed, setDestroyed] = useState(false)
  const {
    setFormData,
    formData,
    files,
    setFile,
    show,
    setShow,
    fileResult,
    setResult,
    onSucces,
    setSuccess,
  } = useContext(mainContext)
  const style = `
  .rc-upload-disabled {
     opacity:0.5;
  `

  const uploadFile1 = {
    action: `http://172.17.0.15:8001/api/FileUpload/SaveFile`,
    accept: '.Tiff, .png, .jpeg, .pdf, .jpg',
    multiple: false,
    beforeUpload(file) {
      if (file.size <= 2e6) {
        return new Promise((resolve) => {
          var reader = new FileReader()
          reader.onload = () => {
            setShow({
              ...show,
              Onefile: {
                file: file,
                document: reader.result,
              },
            })
          }
          reader.readAsDataURL(file)
          resolve(file)
        })
      } else {
        toast.error('Please upload a file smaller than 2 MB')
      }
    },
    //
    onProgress(step, file) {
      if (file.size <= 2e6) {
        setTimeout(
          () => setSuccess(Math.round({ onSucces, onSuccess1: step.percent })),
          1000
        )
      }
    },
    onSuccess(step, file, result) {
      if (file.size <= 2e6) {
        toast.success('Upload Success')
        setResult({ ...fileResult, resultone: result })
        setFile({ ...files, First: step.percent })

        return file
      }
      return false
    },
    onError(err) {
      // console.log('onError', err)
      toast.error('Server not found!')
    },
    customRequest({
      action,
      data,
      file,
      filename,
      headers,
      onError,
      onProgress,
      onSuccess,
      withCredentials,
    }) {
      const FormFile1 = new FormData()
      if (data) {
        Object.keys(data).forEach((key) => {
          FormFile1.append(key, data[key])
        })
      }
      FormFile1.append(filename, file)

      axios
        .post(action, FormFile1, {
          withCredentials,
          headers,
          onUploadProgress: ({ total, loaded }) => {
            onProgress(
              { percent: Math.round((loaded / total) * 100).toFixed(2) },
              file
            )
          },
        })
        .then(({ data: response }) => {
          onSuccess(response, file)

          setFormData({
            ...formData,
            regStd: {
              ...formData.regStd,
              studentNicBform_file: response.filePath,
            },
          })
        })
        .catch(onError)

      return {
        abort() {
          // console.log('upload progress is aborted.')
        },
      }
    },
  }

  // // console.log(fileResult.resultone.response)

  /////////////////////////////////////////////////////
  ////////////////////////////////////////////////////

  const uploadFile2 = {
    action: `${process.env.REACT_APP_NGSBASEURL}FileUpload/SaveFile`,
    accept: '.png, .pdf, .txt',
    multiple: false,
    beforeUpload(file) {
      if (file.size <= 2e6) {
        return new Promise((resolve) => {
          var reader = new FileReader()
          reader.onload = () => {
            setShow({
              ...show,
              Twofile: {
                file: file,
                document: reader.result,
              },
            })
          }
          reader.readAsDataURL(file)
          resolve(file)
        })
      } else {
        toast.error('Please upload a file smaller than 2 MB')
      }
    },
    onProgress(step, file) {
      if (file.size <= 2e6) {
        setTimeout(
          () => setSuccess(Math.round({ onSucces, onSuccess2: step.percent })),
          1000
        )
        toast.success(step.percent)
      }
    },
    onSuccess(step, file, result) {
      if (file.size <= 2e6) {
        setFile({ ...files, two: step.percent })
        toast.success('Upload Success')
        setResult({ ...fileResult, resulttwo: result })
        return file
      }
      return false
    },
    onError(err) {
      // // console.log('onError', err)
      toast.error('Server not found!')
    },
    customRequest({
      action,
      data,
      file,
      filename,
      headers,
      onError,
      onProgress,
      onSuccess,
      withCredentials,
    }) {
      const FormFile2 = new FormData()
      if (data) {
        Object.keys(data).forEach((key) => {
          FormFile2.append(key, data[key])
        })
      }
      FormFile2.append(filename, file)

      axios
        .post(action, FormFile2, {
          withCredentials,
          headers,
          onUploadProgress: ({ total, loaded }) => {
            onProgress(
              { percent: Math.round((loaded / total) * 100).toFixed(2) },
              file
            )
          },
        })
        .then(({ data: response }) => {
          onSuccess(response, file)

          setFormData({
            ...formData,
            regStd: { ...formData.regStd, CNICFront_file: response.filePath },
          })
        })
        .catch(onError)

      return {
        abort() {
          // console.log('upload progress is aborted.')
        },
      }
    },
  }

  /////////////////////////////////////////////////////
  ////////////////////////////////////////////////////
  const uploadFile3 = {
    action: `${process.env.REACT_APP_NGSBASEURL}FileUpload/SaveFile`,
    accept: '.png, .pdf, .txt',
    multiple: false,
    beforeUpload(file) {
      if (file.size <= 2e6) {
        return new Promise((resolve) => {
          var reader = new FileReader()
          reader.onload = () => {
            setShow({
              ...show,
              Threefile: {
                file: file,
                document: reader.result,
              },
            })
          }
          reader.readAsDataURL(file)
          resolve(file)
        })
      } else {
        toast.error('Please upload a file smaller than 2 MB')
      }
    },
    onProgress(step, file) {
      if (file.size <= 2e6) {
        setTimeout(
          () => setSuccess(Math.round({ onSucces, onSuccess3: step.percent })),
          1000
        )
      }
    },
    onSuccess(step, file, result) {
      if (file.size <= 2e6) {
        setFile({ ...files, three: step.percent })
        toast.success('Upload Success')
        setResult({ ...fileResult, resultthree: result })
        return file
      }
      return false
    },
    onError(err) {
      // // console.log('onError', err)
      toast.error('Server not found!')
    },
    customRequest({
      action,
      data,
      file,
      filename,
      headers,
      onError,
      onProgress,
      onSuccess,
      withCredentials,
    }) {
      const FormFile3 = new FormData()
      if (data) {
        Object.keys(data).forEach((key) => {
          FormFile3.append(key, data[key])
        })
      }
      FormFile3.append(filename, file)

      axios
        .post(action, FormFile3, {
          withCredentials,
          headers,
          onUploadProgress: ({ total, loaded }) => {
            onProgress(
              { percent: Math.round((loaded / total) * 100).toFixed(2) },
              file
            )
          },
        })
        .then(({ data: response }) => {
          onSuccess(response, file)

          setFormData({
            ...formData,
            regStd: { ...formData.regStd, CNICBack_file: response.filePath },
          })
        })

        .catch(onError)

      return {
        abort() {
          // console.log('upload progress is aborted.')
        },
      }
    },
  }
  /////////////////////////////////////////////////////
  ////////////////////////////////////////////////////
  const uploadFile4 = {
    action: `${process.env.REACT_APP_NGSBASEURL}FileUpload/SaveFile`,
    accept: '.png, .pdf, .txt',
    multiple: false,
    beforeUpload(file) {
      if (file.size <= 2e6) {
        return new Promise((resolve) => {
          var reader = new FileReader()
          reader.onload = () => {
            setShow({
              ...show,
              Forfile: {
                file: file,
                document: reader.result,
              },
            })
          }

          reader.readAsDataURL(file)
          resolve(file)
        })
      } else {
        toast.error('Please upload a file smaller than 2 MB')
      }
    },
    onProgress(step, file) {
      if (file.size <= 2e6) {
        setTimeout(
          () => setSuccess(Math.round({ onSucces, onSuccess4: step.percent })),
          1000
        )
      }
    },
    onSuccess(step, file, result) {
      if (file.size <= 2e6) {
        setFile({ ...files, four: step.percent })
        toast.success('Upload Success')
        setResult({ ...fileResult, resultfor: result })
        return file
      }
      return false
    },
    onError(err) {
      // // console.log('onError', err)
      toast.error('Server not found!')
    },
    customRequest({
      action,
      data,
      file,
      filename,
      headers,
      onError,
      onProgress,
      onSuccess,
      withCredentials,
    }) {
      const FormFile4 = new FormData()
      if (data) {
        Object.keys(data).forEach((key) => {
          FormFile4.append(key, data[key])
        })
      }
      FormFile4.append(filename, file)

      axios
        .post(action, FormFile4, {
          withCredentials,
          headers,
          onUploadProgress: ({ total, loaded }) => {
            onProgress(
              { percent: Math.round((loaded / total) * 100).toFixed(2) },
              file
            )
          },
        })
        .then(({ data: response }) => {
          onSuccess(response, file)
          setFormData({
            ...formData,
            regStd: {
              ...formData.regStd,
              BirthCertificate_file: response.filePath,
            },
          })
        })
        .catch(onError)

      return {
        abort() {
          // console.log('upload progress is aborted.')
        },
      }
    },
  }

  /////////////////////////////////////////////////////
  ////////////////////////////////////////////////////
  const uploadFile5 = {
    action: `${process.env.REACT_APP_NGSBASEURL}FileUpload/SaveFile`,
    accept: '.png, .pdf, .txt',
    multiple: false,
    beforeUpload(file) {
      if (file.size <= 2e6) {
        return new Promise((resolve) => {
          var reader = new FileReader()
          reader.onload = () => {
            setShow({
              ...show,
              Fivefile: {
                file: file,
                document: reader.result,
              },
            })
          }
          reader.readAsDataURL(file)
          resolve(file)
        })
      } else {
        toast.error('Please upload a file smaller than 2 MB')
      }
    },
    onProgress(step, file) {
      setTimeout(
        () => setSuccess(Math.round({ onSucces, onSuccess5: step.percent })),
        1000
      )
    },
    onSuccess(step, file, result) {
      if (file.size <= 2e6) {
        setFile({ ...files, five: step.percent })
        toast.success('Upload Success')
        setResult({ ...fileResult, resultfive: result })
        return file
      }
      return false
    },
    onError(err) {
      // // console.log('onError', err)
      toast.error('Server not found!')
    },
    customRequest({
      action,
      data,
      file,
      filename,
      headers,
      onError,
      onProgress,
      onSuccess,
      withCredentials,
    }) {
      const FormFile5 = new FormData()
      if (data) {
        Object.keys(data).forEach((key) => {
          FormFile5.append(key, data[key])
        })
      }
      FormFile5.append(filename, file)

      axios
        .post(action, FormFile5, {
          withCredentials,
          headers,
          onUploadProgress: ({ total, loaded }) => {
            onProgress(
              { percent: Math.round((loaded / total) * 100).toFixed(2) },
              file
            )
          },
        })
        .then(({ data: response }) => {
          onSuccess(response, file)
          setFormData({
            ...formData,
            regStd: {
              ...formData.regStd,
              Leaving_certificate_file: response.filePath,
            },
          })
        })
        .catch(onError)

      return {
        abort() {
          // console.log('upload progress is aborted.')
        },
      }
    },
  }

  // let results1 = fileResult.resultone.response
  // let results2 = fileResult.resulttwo.response
  // let results3 = fileResult.resultthree.response
  // let results4 = fileResult.resultfor.response
  // let results5 = fileResult.resultfive.response
  return (
    <>
      <ToastContainer />
      <div className="image_box">
        <ProgressBar
          now={onSucces.onSuccess1}
          label={`${onSucces.onSuccess1}%`}
        />
        <Upload
          className={`btn-normal ${files.First ? files.First : 'successfully'}`}
          {...uploadFile1}
        >
          Student B-Form
        </Upload>
        {show.Onefile.document && (
          <>
            <img src={show.Onefile.document} alt="images" />
          </>
        )}
      </div>

      <ToastContainer />
      <div className="image_box">
        <ProgressBar
          now={onSucces.onSuccess2}
          label={`${onSucces.onSuccess2}%`}
        />
        <Upload
          className={`btn-normal ${files.two ? files.two : 'successfully'}`}
          {...uploadFile2}
        >
          CNIC Front
        </Upload>
        {show.Twofile.document && (
          <>
            <img src={show.Twofile.document} alt="images" />
          </>
        )}
      </div>

      <ToastContainer />
      <div className="image_box">
        <ProgressBar
          now={onSucces.onSuccess3}
          label={`${onSucces.onSuccess3}%`}
        />
        <Upload
          className={`btn-normal ${files.three ? files.three : 'successfully'}`}
          {...uploadFile3}
        >
          CNIC Back
        </Upload>
        {show.Threefile.document && (
          <>
            <img src={show.Threefile.document} alt="images" />
          </>
        )}
      </div>

      <ToastContainer />
      <div className="image_box">
        <ProgressBar
          now={onSucces.onSuccess4}
          label={`${onSucces.onSuccess4}%`}
        />
        <Upload
          className={`btn-normal ${files.four ? files.four : 'successfully'}`}
          {...uploadFile4}
        >
          Birth Certificate
        </Upload>
        {show.Forfile.document && (
          <>
            <img src={show.Forfile.document} alt="images" />
          </>
        )}
      </div>

      <ToastContainer />
      <div className="image_box">
        <ProgressBar
          now={onSucces.onSuccess5}
          label={`${onSucces.onSuccess5}%`}
        />
        <Upload
          className={`btn-normal ${files.five ? files.five : 'successfully'}`}
          {...uploadFile5}
        >
          Leaving Certificate
        </Upload>
        {show.Fivefile.document && (
          <>
            <img src={show.Fivefile.document} alt="images" />
          </>
        )}
      </div>
    </>
  )
}

export default UploadedFiles
