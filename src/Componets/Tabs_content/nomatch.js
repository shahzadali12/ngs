import { Link } from 'react-router-dom'

export default function NoMatch() {
  return (
    <div className="flexCnter">
      <img src={process.env.PUBLIC_URL + '/images/404.png'} alt="student" />

      <Link to="/" className="btn-normal">
        Back to Home
      </Link>
    </div>
  )
}
