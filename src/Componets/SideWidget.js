/* eslint-disable no-unreachable */

import { useState, useContext } from 'react'
import { Link } from 'react-router-dom'
import mainContext from '../ContextApi/main'
import useAuth from '../ContextApi/useAuth'
import Footer from '../layouts/Footer'

const Sidemenu = () => {
  const { auth } = useAuth()
  const { toggleBtn, setToggle } = useContext(mainContext)

  const Menu = [
    {
      id: 1,
      title: 'Deshboard',
      link: '/Home',
      icon: (
        <i className="icon-performance">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 1001,
      title: 'Profile',
      link: '/profile',
      icon: (
        <i className="fa">
          <span className="icon-registration"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 2,
      title: 'Student Management',
      link: '/Registrationlist',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Registrations',
          link: '/Registrationlist',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Admissions',
          link: '/AdmissionList',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 3,
      title: 'Fee Management',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Fee Generation ',
          link: '/FeeGeneration',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Fee Collections',
          link: '/',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },

        {
          id: 3,
          title: 'Fee Voucher Printing',
          link: '/',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Student ID Cards',
          link: '/',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 4,
      title: 'Examinations & Testing',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Exam Entry (Junior to Senior)',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Assessment Entry (Preschool)',
          link: '/',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },

        {
          id: 3,
          title: 'Exam Question Paper Builder',
          link: '/',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Test / Quiz / Assignment',
          link: '/',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 5,
      title: 'Attendance',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Student Attendance',
          link: '/Attendance/students',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Staff Attendance',
          link: '/Attendance/staff',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 6,
      title: 'eLearning',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 7,
      title: 'Citrex',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 8,
      title: 'Payroll Management',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Staff Management',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Leave Application',
          link: '/Payroll-Management/leaves',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 3,
          title: 'Staff Salary',
          link: '/',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Staff ID Cards',
          link: '/',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },
    {
      id: 9,
      title: 'Accounts / Financials',
      link: '/',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
    },
    {
      id: 10,
      title: 'Resource Material',
      link: '/resource',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
    },
    {
      id: 11,
      title: 'Settings',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Definitions',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
          children: [
            {
              id: 1,
              title: 'Department',
              link: '/Settings/Definitions/department',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 2,
              title: 'Designation',
              link: '/Settings/Definitions/designation',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 3,
              title: 'Term',
              link: '/Settings/Definitions/Term',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 4,
              title: 'Class',
              link: '/Settings/Definitions/Class',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 5,
              title: 'Subjects',
              link: '/Settings/Definitions/Subjects',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 6,
              title: 'Session',
              link: '/Settings/Definitions/Section',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 7,
              title: 'Shift',
              link: '/Settings/Definitions/Shift',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 8,
              title: 'Expence',
              link: '/Settings/Definitions/Expence',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 9,
              title: 'Degree',
              link: '/Settings/Definitions/Degree',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 10,
              title: 'Conduct',
              link: '/Settings/Definitions/Conduct',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 11,
              title: 'AssessmentTerm',
              link: '/Settings/Definitions/AssessmentTerm',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 12,
              title: 'Country',
              link: '/Settings/Definitions/Country',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 13,
              title: 'Cities',
              link: '/Settings/Definitions/City',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 14,
              title: 'Area',
              link: '/Settings/Definitions/Area',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 15,
              title: 'Custodain',
              link: '/Settings/Definitions/Custodain',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 16,
              title: 'Documents',
              link: '/Settings/Definitions/Documents',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 17,
              title: 'EmployeeType',
              link: '/Settings/Definitions/EmployeeType',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 18,
              title: 'Language',
              link: '/Settings/Definitions/Language',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 19,
              title: 'Relationship',
              link: '/Settings/Definitions/Relationship',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 20,
              title: 'Religion',
              link: '/Settings/Definitions/Religion',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 21,
              title: 'Transport',
              link: '/Settings/Definitions/Transport',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 22,
              title: 'Grade',
              link: '/Settings/Definitions/Grade',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 23,
              title: 'Fee',
              link: '/Settings/Fee/Fee',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 24,
              title: 'Exam',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 25,
              title: 'Payroll',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 26,
              title: 'SMS Services',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 27,
              title: 'Academics',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
          ],
        },
        {
          id: 28,
          title: 'Academics',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
          children: [
            {
              id: 1,
              title: 'Class Plan',
              link: '/Settings/Definitions/Classplan',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
          ],
        },
      ],
    },
    {
      id: 12,
      title: 'NGS School Diary – Admin Material',
      link: '/',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
    },
    {
      id: 13,
      title: 'Notifications Services',
      link: '/',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
      children: [
        {
          id: 1,
          title: 'Regular SMS',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Scheduled SMS',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 3,
          title: 'Wish, SMS & Reminders',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Fee SMS',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },
    {
      id: 14,
      title: 'Utilities',
      link: '/',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
      children: [
        {
          id: 1,
          title: 'Time Table',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Parent Information',
          link: '/ParentInfo',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 3,
          title: 'Academic Calendar',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Birthday',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 5,
          title: 'Task Dairy Management',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 6,
          title: 'System Monitoring',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 7,
          title: 'Gate Pass',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 8,
          title: 'Log Management',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 15,
      title: 'Reports',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Students',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
          children: [
            {
              id: 1,
              title: 'Students Class List',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 2,
              title: 'Student Personal Data Detailed Report',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 3,
              title: 'Student Individual Profile',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 4,
              title: 'Class Strength Report',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 5,
              title: 'Student Subject List',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 6,
              title: 'Student Subject Allocations',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
          ],
        },
        {
          id: 2,
          title: 'Attendance',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
          children: [
            {
              id: 1,
              title: 'Students',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Daily Class Wise Attendance Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Student Individual Attendance Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Class Wise Monthly Attendance Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Subject Wise Monthly Attendance Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 2,
              title: 'Employees',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Daily Attendance Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Individual Employee Attendance Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Department Wise Monthly Attendance Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Subject Wise Monthly Attendance Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 3,
              title: 'Fee',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Fee Day Book Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Student Fee Ledger Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Student Defaulters Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Fee Revenue (Day Book) Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 5,
                  title: 'Discounts and IFL Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 6,
                  title: 'Student Fee Family Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 4,
              title: 'Examinations',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Exam Master Sheet',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Exam Individual Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Exam Assessment Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Class Exam Average Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 5,
                  title: 'Scholarship Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 6,
                  title: 'Co-curricular Activities Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 7,
                  title: 'GPA Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 8,
                  title: 'Awards List',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 9,
                  title: 'Exam List (Manual)',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 5,
              title: 'Payroll',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Employee Management Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Salary Generations Calculations',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Loans and Deductions Report',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Payroll Sheet',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 5,
                  title: 'Salary Generation',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 6,
                  title: 'Bank Letter',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 7,
                  title: 'Salary Scroll Letter',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 6,
              title: 'Accounts',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 7,
              title: 'SMS Services',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'SMS Campaigns',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Branch/ Class Wise SMS Campaigns',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'SMS Templates',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 8,
              title: 'CMS',
              link: '/',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Syllabus Designing',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'CCPs',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Lesson Planning',
                  link: '/',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
          ],
        },
      ],
    },
    {
      id: 16,
      title: 'Head office Operations',
      link: '/',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
      children: [
        {
          id: 1,
          title: 'School Setup and Management',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Branch Setup and Management',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 3,
          title: 'Royalty Calculations and Generations',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Royalty Reports',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 5,
          title: 'ID Card Generation and Reports',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 6,
          title: 'Inventory Management System',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 7,
          title: 'Purchase Requisitions',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 8,
          title: 'Items Issue Requisitions',
          link: '/',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },
  ]

  const navigation = [
    {
      id: 1,
      title: 'Deshboard',
      link: '/',
      icon: (
        <i className="icon-performance">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 2,
      title: 'Students',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Registrations',
          link: '/Registrationlist',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Admissions',
          link: '/AdmissionList',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },
    {
      id: 50,
      title: 'Resource Material',
      link: '/resource',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
    },
    {
      id: 3,
      title: 'Leaves Applications',
      link: '/Payroll-Management/leaves',
      icon: (
        <i className="icon-leave">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 4,
      title: 'system monitoring',
      link: '/system-monitoring',
      icon: (
        <i className="icon-monitor">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 5,
      title: 'Fee voucher printing',
      link: '/',
      icon: (
        <i className="icon-printer">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 6,
      title: 'Print Get Pass',
      link: '/',
      icon: (
        <i className="icon-printer">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 7,
      title: 'School Website',
      link: '/',
      icon: (
        <i className="icon-internet">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 8,
      title: 'HomeWork Diary',
      link: '/',
      icon: (
        <i className="icon-journal">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 9,
      title: 'Parent Information',
      link: '/ParentInfo',
      icon: (
        <i className="icon-mother-and-son">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 10,
      title: 'Academic calendar',
      link: '/',
      icon: <i className="icon-academic"></i>,
      badge: '',
    },
    {
      id: 11,
      title: 'Manage HelpDesk Staff',
      link: '/',
      icon: (
        <i className="icon-help">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 12,
      title: 'Daily Diary Management',
      link: '/',
      icon: (
        <i className="icon-daily-tasks">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
          <span className="path11"></span>
          <span className="path12"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 13,
      title: 'Staff Management',
      link: '/',
      icon: (
        <i className="icon-management">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 14,
      title: 'Staff Salary Management',
      link: '/',
      icon: (
        <i className="icon-salary-1">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 15,
      title: 'Birthday Wishing by SMS',
      link: '/',
      icon: (
        <i className="icon-party">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 16,
      title: 'Accounts Management',
      link: '/',
      icon: (
        <i className="icon-accountant">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 17,
      title: 'Fee Management',
      link: '/',
      icon: (
        <i className="icon-bookkeeping">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 18,
      title: 'Student Attendance',
      link: '/students/attendace',
      icon: (
        <i className="icon-course">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
          <span className="path11"></span>
          <span className="path12"></span>
          <span className="path13"></span>
          <span className="path14"></span>
          <span className="path15"></span>
          <span className="path16"></span>
          <span className="path17"></span>
          <span className="path18"></span>
          <span className="path19"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 19,
      title: 'Staff Attendance',
      link: '/staff/attendace',
      icon: (
        <i className="icon-planning">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 20,
      title: 'Exams Management',
      link: '/',
      icon: (
        <i className="icon-management-1">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 21,
      title: 'Students Performance',
      link: '/',
      icon: (
        <i className="icon-work">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
          <span className="path11"></span>
          <span className="path12"></span>
          <span className="path13"></span>
          <span className="path14"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 22,
      title: 'Exam Question papers',
      link: '/',
      icon: (
        <i className="icon-question-1">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
          <span className="path11"></span>
          <span className="path12"></span>
          <span className="path13"></span>
          <span className="path14"></span>
          <span className="path15"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 23,
      title: 'Class Management',
      link: '/',
      icon: (
        <i className="icon-teacher">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 24,
      title: 'Notification Services',
      link: '/',
      icon: (
        <i className="icon-alarm">
          <span className="path1"></span>
          <span className="path2"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 25,
      title: 'Data Monitoring Reports',
      link: '/',
      icon: (
        <i className="icon-monitor">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 26,
      title: 'Security Features',
      link: '/',
      icon: (
        <i className="icon-padlock">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 27,
      title: 'Log Management',
      link: '/',
      icon: (
        <i className="icon-log">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
          <span className="path11"></span>
          <span className="path12"></span>
          <span className="path13"></span>
          <span className="path14"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 28,
      title: 'WebSite Management',
      link: '/',
      icon: (
        <i className="icon-search-1">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 29,
      title: 'CMS Management',
      link: '/',
      icon: (
        <i className="icon-management-1">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 30,
      title: 'Syllabus Management',
      link: '/',
      icon: (
        <i className="icon-mathematics">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 31,
      title: 'lesson Planning',
      link: '/',
      icon: (
        <i className="icon-planning">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 32,
      title: 'Official Documents',
      link: '/',
      icon: (
        <i className="icon-verification">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 33,
      title: 'Profile Errors Reports',
      link: '/',
      icon: (
        <i className="icon-user-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 34,
      title: 'Clients',
      link: '/',
      icon: (
        <i className="icon-client">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 35,
      title: "Print Id Card's",
      link: '/',
      icon: (
        <i className="icon-printer">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
        </i>
      ),
      badge: '',
    },
    {
      id: 36,
      title: 'Visitors Information',
      link: '/',
      icon: (
        <i className="icon-traveler">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
        </i>
      ),
      badge: '',
    },

    // {
    //     id: 4,
    //     title: "Registrated Students",
    //     link: "/RegistratedStudentslist",
    //     icon: "ti-bar-chart",
    //     badge: "",
    // },
    // {
    //     id: 5,
    //     title: "New Admission",
    //     link: "/newAdmission",
    //     icon: "ti-bar-chart",
    //     badge: "new",
    // },
    // {
    //     id: 6,
    //     title: "Admissions list",
    //     link: "/Admissionslist",
    //     icon: "ti-bar-chart",
    //     badge: "new",
    // },
    // {
    //     id: 7,
    //     title: "All Students",
    //     link: "/student",
    //     icon: "ti-dashboard",
    //     badge: "",
    // },
    // {
    //     id: 8,
    //     title: "File Menager",
    //     link: "/filemaneger",
    //     icon: "ti-bar-chart",
    //     badge: "",
    // },
  ]
  const Teacher = [
    {
      id: 1,
      title: 'Deshboard',
      link: '/',
      icon: (
        <i className="icon-performance">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 2,
      title: 'Students',
      link: '/',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Registrations',
          link: '/Registrationlist',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Admissions',
          link: '/AdmissionList',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },
    {
      id: 50,
      title: 'Resource Material',
      link: '/resource',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
    },
    {
      id: 3,
      title: 'Leaves Applications',
      link: '/Payroll-Management/leaves',
      icon: (
        <i className="icon-leave">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
          <span className="path7"></span>
          <span className="path8"></span>
          <span className="path9"></span>
          <span className="path10"></span>
        </i>
      ),
      badge: '',
    },
  ]
  const Director = [
    {
      id: 1,
      title: 'Deshboard',
      link: '/Home',
      icon: (
        <i className="icon-performance">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
          <span className="path5"></span>
          <span className="path6"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 1001,
      title: 'Profile',
      link: '/profile',
      icon: (
        <i className="fa">
          <span className="icon-registration"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 2,
      title: 'Student Management',
      link: '/Registrationlist',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Registrations',
          link: '/Registrationlist',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Admissions',
          link: '/AdmissionList',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 3,
      title: 'Fee Management',
      link: '/Home',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Fee Generation ',
          link: '/FeeGeneration',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Fee Collections',
          link: '/FeeCollection',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },

        {
          id: 3,
          title: 'Fee Voucher Printing',
          link: '/FeeVoucher',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Student ID Cards',
          link: '/StudentIDGen',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 4,
      title: 'Examinations & Testing',
      link: '/testing',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Exam Entry (Junior to Senior)',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Assessment Entry (Preschool)',
          link: '/Home',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },

        {
          id: 3,
          title: 'Exam Question Paper Builder',
          link: '/Home',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Test / Quiz / Assignment',
          link: '/Home',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 5,
      title: 'Attendance',
      link: '/Home',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Student Attendance',
          link: '/Attendance/students',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Staff Attendance',
          link: '/Attendance/staff',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 6,
      title: 'eLearning',
      link: '/birthday',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 7,
      title: 'Citrex',
      link: '/Home',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
    },
    {
      id: 8,
      title: 'Payroll Management',
      link: '/Home',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Staff Management',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Leave Application',
          link: '/Payroll-Management/leaves',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 3,
          title: 'Staff Salary',
          link: '/Home',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Staff ID Cards',
          link: '/Home',
          icon: (
            <i className="icon-approve">
              <span className="path1"></span>
              <span className="path2"></span>
              <span className="path3"></span>
              <span className="path4"></span>
              <span className="path5"></span>
              <span className="path6"></span>
              <span className="path7"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },
    {
      id: 9,
      title: 'Accounts / Financials',
      link: '/Home',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
    },
    {
      id: 10,
      title: 'Resource Material',
      link: '/resource',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
    },
    {
      id: 11,
      title: 'Settings',
      link: '/Home',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Definitions',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
          children: [
            {
              id: 1,
              title: 'Department',
              link: '/Settings/Definitions/department',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 2,
              title: 'Designation',
              link: '/Settings/Definitions/designation',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 3,
              title: 'Term',
              link: '/Settings/Definitions/Term',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 4,
              title: 'Class',
              link: '/Settings/Definitions/Class',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 5,
              title: 'Subjects',
              link: '/Settings/Definitions/Subjects',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 6,
              title: 'Session',
              link: '/Settings/Definitions/Section',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 7,
              title: 'Shift',
              link: '/Settings/Definitions/Shift',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 8,
              title: 'Expence',
              link: '/Settings/Definitions/Expence',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 9,
              title: 'Degree',
              link: '/Settings/Definitions/Degree',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 10,
              title: 'Conduct',
              link: '/Settings/Definitions/Conduct',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 11,
              title: 'AssessmentTerm',
              link: '/Settings/Definitions/AssessmentTerm',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 12,
              title: 'Country',
              link: '/Settings/Definitions/Country',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 13,
              title: 'Cities',
              link: '/Settings/Definitions/City',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 14,
              title: 'Area',
              link: '/Settings/Definitions/Area',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 15,
              title: 'Custodain',
              link: '/Settings/Definitions/Custodain',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 16,
              title: 'Documents',
              link: '/Settings/Definitions/Documents',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 17,
              title: 'EmployeeType',
              link: '/Settings/Definitions/EmployeeType',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 18,
              title: 'Language',
              link: '/Settings/Definitions/Language',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 19,
              title: 'Relationship',
              link: '/Settings/Definitions/Relationship',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 20,
              title: 'Religion',
              link: '/Settings/Definitions/Religion',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 21,
              title: 'Transport',
              link: '/Settings/Definitions/Transport',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 22,
              title: 'Grade',
              link: '/Settings/Definitions/Grade',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 23,
              title: 'Fee',
              link: '/Settings/Fee/Fee',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 24,
              title: 'Exam',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 25,
              title: 'Payroll',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 26,
              title: 'SMS Services',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 27,
              title: 'Academics',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
          ],
        },
        {
          id: 28,
          title: 'Academics',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
          children: [
            {
              id: 1,
              title: 'Class Plan',
              link: '/Settings/Definitions/Classplan',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
          ],
        },
      ],
    },
    {
      id: 12,
      title: 'NGS School Diary – Admin Material',
      link: '/Home',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
    },
    {
      id: 13,
      title: 'Notifications Services',
      link: '/Home',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
      children: [
        {
          id: 1,
          title: 'Regular SMS',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Scheduled SMS',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 3,
          title: 'Wish, SMS & Reminders',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Fee SMS',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },
    {
      id: 14,
      title: 'Utilities',
      link: '/Home',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
      children: [
        {
          id: 1,
          title: 'Time Table',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Parent Information',
          link: '/ParentInfo',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 3,
          title: 'Academic Calendar',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Birthday',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 5,
          title: 'Task Dairy Management',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 6,
          title: 'System Monitoring',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 7,
          title: 'Gate Pass',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 8,
          title: 'Log Management',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },

    {
      id: 15,
      title: 'Reports',
      link: '/Home',
      icon: (
        <i className="icon-student-2">
          <span className="path1"></span>
          <span className="path2"></span>
          <span className="path3"></span>
          <span className="path4"></span>
        </i>
      ),
      badge: 'new',
      children: [
        {
          id: 1,
          title: 'Students',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
          children: [
            {
              id: 1,
              title: 'Students Class List',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 2,
              title: 'Student Personal Data Detailed Report',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 3,
              title: 'Student Individual Profile',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 4,
              title: 'Class Strength Report',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 5,
              title: 'Student Subject List',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 6,
              title: 'Student Subject Allocations',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
          ],
        },
        {
          id: 2,
          title: 'Attendance',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
          children: [
            {
              id: 1,
              title: 'Students',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Daily Class Wise Attendance Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Student Individual Attendance Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Class Wise Monthly Attendance Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Subject Wise Monthly Attendance Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 2,
              title: 'Employees',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Daily Attendance Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Individual Employee Attendance Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Department Wise Monthly Attendance Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Subject Wise Monthly Attendance Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 3,
              title: 'Fee',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Fee Day Book Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Student Fee Ledger Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Student Defaulters Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Fee Revenue (Day Book) Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 5,
                  title: 'Discounts and IFL Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 6,
                  title: 'Student Fee Family Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 4,
              title: 'Examinations',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Exam Master Sheet',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Exam Individual Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Exam Assessment Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Class Exam Average Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 5,
                  title: 'Scholarship Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 6,
                  title: 'Co-curricular Activities Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 7,
                  title: 'GPA Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 8,
                  title: 'Awards List',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 9,
                  title: 'Exam List (Manual)',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 5,
              title: 'Payroll',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Employee Management Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Salary Generations Calculations',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Loans and Deductions Report',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 4,
                  title: 'Payroll Sheet',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 5,
                  title: 'Salary Generation',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 6,
                  title: 'Bank Letter',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 7,
                  title: 'Salary Scroll Letter',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 6,
              title: 'Accounts',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
            },
            {
              id: 7,
              title: 'SMS Services',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'SMS Campaigns',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'Branch/ Class Wise SMS Campaigns',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'SMS Templates',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
            {
              id: 8,
              title: 'CMS',
              link: '/Home',
              icon: (
                <i className="icon-approve">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                  <span className="path5"></span>
                  <span className="path6"></span>
                  <span className="path7"></span>
                </i>
              ),
              badge: 'new',
              children: [
                {
                  id: 1,
                  title: 'Syllabus Designing',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 2,
                  title: 'CCPs',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
                {
                  id: 3,
                  title: 'Lesson Planning',
                  link: '/Home',
                  icon: (
                    <i className="icon-approve">
                      <span className="path1"></span>
                      <span className="path2"></span>
                      <span className="path3"></span>
                      <span className="path4"></span>
                      <span className="path5"></span>
                      <span className="path6"></span>
                      <span className="path7"></span>
                    </i>
                  ),
                  badge: 'new',
                },
              ],
            },
          ],
        },
      ],
    },
    {
      id: 16,
      title: 'Head office Operations',
      link: '/Home',
      icon: <i className="icon-hierarchy"></i>,
      badge: '',
      children: [
        {
          id: 1,
          title: 'School Setup and Management',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 2,
          title: 'Branch Setup and Management',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 3,
          title: 'Royalty Calculations and Generations',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 4,
          title: 'Royalty Reports',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 5,
          title: 'ID Card Generation and Reports',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 6,
          title: 'Inventory Management System',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 7,
          title: 'Purchase Requisitions',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
        {
          id: 8,
          title: 'Items Issue Requisitions',
          link: '/Home',
          icon: (
            <i className="fa">
              <span className="icon-registration"></span>
            </i>
          ),
          badge: 'new',
        },
      ],
    },
  ]

  const [activeId, setActiveId] = useState(false)
  const [childactive, setChildActive] = useState(false)
  const [Subchildactive, setSubchildactive] = useState(false)

  return (
    <div className={`sidewidget ${toggleBtn === true ? 'expend' : ''}`}>
      <div className={`side_wraper  ${toggleBtn === true ? 'move' : ''}`}>
        {/* <p>Will auto Log off if no activity in 5000 secounds.</p> */}
        <div className="sidebar">
          <nav>
            <ul className="left-sidenav">
              {auth?.roles &&
                Menu.map((val, i) => {
                  return (
                    <li
                      key={val.id}
                      onClick={() => setActiveId(val.id)}
                      className={`${activeId === val.id ? 'active' : ''}`}
                    >
                      <Link to={val.link}>
                        {val.icon}
                        {val.title}
                        {val.badge && (
                          <span className="badge">{val.badge}</span>
                        )}
                      </Link>
                      {val.children && (
                        <ul
                          className={`${
                            activeId === val.id ? 'active children' : 'children'
                          }`}
                        >
                          {val.children.map((child) => {
                            return (
                              <li
                                key={child.id}
                                onClick={() => setChildActive(child.id)}
                                className={`${
                                  childactive === child.id ? 'active' : ''
                                }`}
                              >
                                <Link to={child.link}>
                                  {child.icon}
                                  {child.title}
                                </Link>
                                {child.children && (
                                  <ul
                                    className={`${
                                      childactive === child.id
                                        ? 'children subchildren active'
                                        : 'children subchildren'
                                    }`}
                                  >
                                    {child.children.map((velue) => {
                                      return (
                                        <li
                                          key={velue.id}
                                          onClick={() =>
                                            setSubchildactive(velue.id)
                                          }
                                          className={`${
                                            Subchildactive === velue.id
                                              ? 'active'
                                              : ''
                                          }`}
                                        >
                                          <Link to={velue.link}>
                                            {velue.icon}
                                            {velue.title}
                                          </Link>
                                        </li>
                                      )
                                    })}
                                  </ul>
                                )}
                              </li>
                            )
                          })}
                        </ul>
                      )}
                    </li>
                  )
                })}
              {auth?.teacher &&
                Teacher.map((val, i) => {
                  return (
                    <li
                      key={val.id}
                      onClick={() => setActiveId(val.id)}
                      className={`${activeId === val.id ? 'active' : ''}`}
                    >
                      <Link to={val.link}>
                        {val.icon}
                        {val.title}
                        {val.badge && (
                          <span className="badge">{val.badge}</span>
                        )}
                      </Link>
                      {val.children && (
                        <ul
                          className={`${
                            activeId === val.id ? 'active children' : 'children'
                          }`}
                        >
                          {val.children.map((child) => {
                            return (
                              <li
                                key={child.id}
                                onClick={() => setChildActive(child.id)}
                                className={`${
                                  childactive === child.id ? 'active' : ''
                                }`}
                              >
                                <Link to={child.link}>
                                  {child.icon}
                                  {child.title}
                                </Link>
                                {child.children && (
                                  <ul
                                    className={`${
                                      childactive === child.id
                                        ? 'children subchildren active'
                                        : 'children subchildren'
                                    }`}
                                  >
                                    {child.children.map((velue) => {
                                      return (
                                        <li
                                          key={velue.id}
                                          onClick={() =>
                                            setSubchildactive(velue.id)
                                          }
                                          className={`${
                                            Subchildactive === velue.id
                                              ? 'active'
                                              : ''
                                          }`}
                                        >
                                          <Link to={velue.link}>
                                            {velue.icon}
                                            {velue.title}
                                          </Link>
                                        </li>
                                      )
                                    })}
                                  </ul>
                                )}
                              </li>
                            )
                          })}
                        </ul>
                      )}
                    </li>
                  )
                })}
              {auth?.director &&
                Director.map((val, i) => {
                  return (
                    <li
                      key={val.id}
                      onClick={() => setActiveId(val.id)}
                      className={`${activeId === val.id ? 'active' : ''}`}
                    >
                      <Link to={val.link}>
                        {val.icon}
                        {val.title}
                        {val.badge && (
                          <span className="badge">{val.badge}</span>
                        )}
                        {val.children ? (
                          <i className="arrow fa fa-angle-right"></i>
                        ) : (
                          ''
                        )}
                      </Link>
                      {val.children && (
                        <ul
                          className={`${
                            activeId === val.id ? 'active children' : 'children'
                          }`}
                        >
                          {val.children.map((child) => {
                            return (
                              <li
                                key={child.id}
                                onClick={() => setChildActive(child.id)}
                                className={`${
                                  childactive === child.id ? 'active' : ''
                                }`}
                              >
                                <Link to={child.link}>
                                  {child.icon}
                                  {child.title}
                                </Link>
                                {child.children && (
                                  <ul
                                    className={`${
                                      childactive === child.id
                                        ? 'children subchildren active'
                                        : 'children subchildren'
                                    }`}
                                  >
                                    {child.children.map((velue) => {
                                      return (
                                        <li
                                          key={velue.id}
                                          onClick={() =>
                                            setSubchildactive(velue.id)
                                          }
                                          className={`${
                                            Subchildactive === velue.id
                                              ? 'active'
                                              : ''
                                          }`}
                                        >
                                          <Link to={velue.link}>
                                            {velue.icon}
                                            {velue.title}
                                          </Link>
                                        </li>
                                      )
                                    })}
                                  </ul>
                                )}
                              </li>
                            )
                          })}
                        </ul>
                      )}
                    </li>
                  )
                })}
              {auth?.parent &&
                Teacher.map((val, i) => {
                  return (
                    <li
                      key={val.id}
                      onClick={() => setActiveId(val.id)}
                      className={`${activeId === val.id ? 'active' : ''}`}
                    >
                      <Link to={val.link}>
                        {val.icon}
                        {val.title}
                        {val.badge && (
                          <span className="badge">{val.badge}</span>
                        )}
                      </Link>
                      {val.children && (
                        <ul
                          className={`${
                            activeId === val.id ? 'active children' : 'children'
                          }`}
                        >
                          {val.children.map((child) => {
                            return (
                              <li
                                key={child.id}
                                onClick={() => setChildActive(child.id)}
                                className={`${
                                  childactive === child.id ? 'active' : ''
                                }`}
                              >
                                <Link to={child.link}>
                                  {child.icon}
                                  {child.title}
                                </Link>
                                {child.children && (
                                  <ul
                                    className={`${
                                      childactive === child.id
                                        ? 'children subchildren active'
                                        : 'children subchildren'
                                    }`}
                                  >
                                    {child.children.map((velue) => {
                                      return (
                                        <li
                                          key={velue.id}
                                          onClick={() =>
                                            setSubchildactive(velue.id)
                                          }
                                          className={`${
                                            Subchildactive === velue.id
                                              ? 'active'
                                              : ''
                                          }`}
                                        >
                                          <Link to={velue.link}>
                                            {velue.icon}
                                            {velue.title}
                                          </Link>
                                        </li>
                                      )
                                    })}
                                  </ul>
                                )}
                              </li>
                            )
                          })}
                        </ul>
                      )}
                    </li>
                  )
                })}
            </ul>
          </nav>
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Sidemenu
