import React, { useContext, useState, useEffect, useRef } from 'react'
import '../styles/style.css'
import { Link, useNavigate } from 'react-router-dom'
import mainContext from '../ContextApi/main'
import AuthContext from '../ContextApi/AuthProvider'
import { useOnOutsideClick } from './connect_element/DropConnect'
const Head = () => {
  const regcontxt = useContext(mainContext)
  const { heading, setToggle, toggleBtn, timer } = regcontxt

  const [widthincrese, setWidthincrease] = useState(false)

  const [actives, setActives] = useState(false)

  const [Nofiybtn, setNofiybtn] = useState(false)

  const Refse = useRef()

  useOnOutsideClick(Refse, () => {
    if (actives || Nofiybtn) {
      setActives(false)
      setNofiybtn(false)
    }
  })

  const Profilebtn = () => {
    setActives(!actives)
  }

  const FunNofiybtn = () => {
    setNofiybtn(!Nofiybtn)
  }

  useEffect(() => {
    setTimeout(() => {
      setWidthincrease(false)
    }, 20000)
  }, [])

  const { setAuth, auth } = useContext(AuthContext)
  const navigate = useNavigate()

  const logout = async () => {
    setAuth({})
    navigate('/login')
  }

  const [logo, Setlogo] = useState(false)

  useEffect(() => {
    setInterval(() => {
      Setlogo(true)
    }, 5000)
    setTimeout(() => {
      Setlogo(false)
    }, 10000)
  }, [logo])

  const campus = [
    {
      label: 'Model Town lahore',
      value: 1,
    },
    {
      label: 'Johar Town lahore',
      value: 2,
    },
  ]

  const [activeTab, setActiveTab] = useState('tab1')
  const activeToggle = (e) => {
    setActiveTab(e)
  }

  return (
    <>
      <header className="header">
        <div className="container-fluid">
          <div className="flex_box">
            <div className="logo mr-15">
              {logo ? (
                <div className="animatlogo">
                  <h1>
                    <Link to="/">
                      <img
                        src={process.env.PUBLIC_URL + '/logoB.png'}
                        alt="logo here"
                      />
                    </Link>
                  </h1>
                </div>
              ) : (
                <div className="animatlogo">
                  <h1>
                    <Link to="/">
                      <img
                        src={process.env.PUBLIC_URL + '/preschoollogo.png'}
                        alt="logo here"
                      />
                    </Link>
                  </h1>
                </div>
              )}
              <span className="Titles">
                NGS Portal {auth?.user}
                <br />
                {auth?.user === 'director' ? (
                  <select className="select_small">
                    {campus.map((val, index) => {
                      return (
                        <option key={index} value={val.value}>
                          {val.label} (2021 - 2022)
                        </option>
                      )
                    })}
                  </select>
                ) : (
                  <p>
                    Mall road lahore (2021 - 2022)
                    {/* مال روڈ لاہور */}
                  </p>
                )}
              </span>
            </div>

            {widthincrese === false ? (
              <button
                className="Search_btn"
                type="button"
                onClick={() => setWidthincrease(!widthincrese)}
              >
                <i className="icon-loupe"></i>
              </button>
            ) : (
              ''
            )}
            {widthincrese === true ? (
              <div
                className={`searchBox ${
                  widthincrese === true ? 'expend_width' : ''
                } `}
              >
                <input id="search" type="text" placeholder="Search..." />
                <label htmlFor="search">
                  <i className="icon-loupe"></i>
                </label>
              </div>
            ) : (
              ''
            )}

            <div
              className={`toggle_button ${toggleBtn === true ? 'active' : ''}`}
            >
              <button type="button" onClick={() => setToggle(!toggleBtn)}>
                <i className="icon-widget">
                  <span className="path1"></span>
                  <span className="path2"></span>
                  <span className="path3"></span>
                  <span className="path4"></span>
                </i>
              </button>
            </div>
            {timer}
            {heading && (
              <div className="heading mb20 align-center">
                <h4>
                  <img
                    src={process.env.PUBLIC_URL + '/images/student.png'}
                    alt="student"
                  />
                  {/* <b>New Registraion</b> */}
                  <b>{heading}</b>
                </h4>
              </div>
            )}
            <div className="rtl_sec">
              <div className="nofiy">
                <button type="button" onClick={FunNofiybtn}>
                  <i className="fa fa-bell-o"></i>
                  <span>99+..</span>
                </button>

                {Nofiybtn && (
                  <div
                    ref={Refse}
                    className={`notify_content ${
                      Nofiybtn === true ? 'active' : ''
                    }`}
                  >
                    <div className="heading_notify">
                      <h4>9 New</h4>
                      <span>Notifications</span>
                    </div>
                    <div className="tabs_elements">
                      <ul>
                        <li className={activeTab === 'tab1' ? 'active' : ''}>
                          <button
                            type="button"
                            onClick={() => activeToggle('tab1')}
                          >
                            Alerts
                          </button>
                        </li>
                        <li className={activeTab === 'tab2' ? 'active' : ''}>
                          <button
                            type="button"
                            onClick={() => activeToggle('tab2')}
                          >
                            Events
                          </button>
                        </li>
                        <li className={activeTab === 'tab3' ? 'active' : ''}>
                          <button
                            type="button"
                            onClick={() => activeToggle('tab3')}
                          >
                            Activities
                          </button>
                        </li>
                      </ul>
                      <div className="content_area">
                        {activeTab === 'tab1' && (
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the
                            industry's standard dummy text ever since the 1500s,
                            when an
                          </p>
                        )}
                        {activeTab === 'tab2' && (
                          <p>
                            galley of type and scrambled it to make a type
                            specimen book. It has survived not only five
                            centuries, but also the leap into electronic
                            typesetting, remaining essentially unchanged.
                          </p>
                        )}
                        {activeTab === 'tab3' && (
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the
                            industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and
                            scrambled it to make a type specimen book. It has
                            survived not only five centuries, but also the leap
                            into electronic typesetting, remaining essentially
                            unchanged.
                          </p>
                        )}
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div className="profile_setting">
                <button
                  className="profile_btn dropdown-toggle"
                  type="button"
                  onClick={Profilebtn}
                  // onClick={toggleMobileNav}
                >
                  <img
                    src="https://thumbs.dreamstime.com/b/cute-little-indian-indian-asian-school-boy-wearing-uniform-cute-little-indian-indian-asian-school-boy-wearing-uniform-150099692.jpg"
                    alt="user img"
                  />
                  <span>Shahzad Ali ({auth?.user})</span>
                </button>

                {actives && (
                  <div
                    ref={Refse}
                    className={`dropdown-menu ${
                      actives === true ? 'active' : ''
                    }`}
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link className="dropdown-item" to="/profile">
                      <i className="dripicons-user text-muted mr-2"></i> Profile
                    </Link>
                    <Link className="dropdown-item" to="/lockScreen">
                      <i className="dripicons-user text-muted mr-2"></i> lock
                      Screen
                    </Link>
                    {/* <Link className="dropdown-item" to="/">
                    <i className="dripicons-wallet text-muted mr-2"></i> My
                    Wallet
                  </Link> */}
                    {/* <Link className="dropdown-item" to="/">
                    <i className="dripicons-gear text-muted mr-2"></i> Settings
                  </Link> */}
                    {/* <Link className="dropdown-item" to="/">
                    <i className="dripicons-lock text-muted mr-2"></i> Lock
                    screen
                  </Link> */}
                    <button
                      type="button"
                      onClick={logout}
                      className="dropdown-item"
                    >
                      <i className="dripicons-exit text-muted mr-2"></i> Logout
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  )
}
export default Head
