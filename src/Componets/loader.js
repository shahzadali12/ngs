function Loader() {
    return (
        <div className='preloader'>
            <img src={process.env.PUBLIC_URL + '/logoB.png'} alt='logo' />
            <div className="pointers">
                <span className="a" ></span>
                <span className="b"></span>
                <span className="c"></span>
                <span className="d"></span>
                <span className="e"></span>
            </div>
        </div>
    );
}

export default Loader;