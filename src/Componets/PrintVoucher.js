import React from 'react';

function PrintVoucher({ formDeta, setFormDeta, photoSrc }) {
    return (
        <>
            <div className='voucher'>
                <div className='flex mb30'>
                    <div className='logo'>
                        <img src={process.env.PUBLIC_URL + '/logo.png'} alt="logo here" />
                    </div>
                    <div className='heading_titile'>
                        <h4>National Grammar School</h4>
                        <h5>Registraion Slip</h5>
                    </div>
                    <div className='reg_picture'>
                        {photoSrc && <img src={photoSrc} alt="logo here" />}
                    </div>
                </div>
                <div className='flex align-starts '>
                    <div className='column'>
                        <h6>Student information</h6>
                        <ul>
                            {formDeta.StudentFirstName && <li><b>Name:</b> {formDeta.StudentFirstName} {formDeta.StudentLastName}</li>}
                            {formDeta.StudentGender && <li><b>Gender:</b> {formDeta.StudentGender}</li>}
                            {formDeta.StudentBForm && <li><b>B-Form:</b> {formDeta.StudentBForm}</li>}
                        </ul>
                        <h6>Parent/Guardian information</h6>
                        <ul>
                            {formDeta.GuardianFirstName && <li><b>Name:</b> {formDeta.GuardianFirstName}</li>}
                            {formDeta.GuardianCNIC && <li><b>CNIC:</b> {formDeta.GuardianCNIC}</li>}
                            {formDeta.GuardianPhone && <li><b>Contact:</b> {formDeta.GuardianPhone}</li>}
                            {formDeta.GuardianEmail && <li><b>E-mail:</b> {formDeta.GuardianEmail}</li>}
                            {formDeta.GuardianAddress && <li><b>Address:</b> {formDeta.GuardianAddress}</li>}
                        </ul>
                    </div>

                    <div className='column rtl'>
                        <h6>School information</h6>
                        <ul>
                            {formDeta.regno && <li><b>Reg:</b> {formDeta.regno}</li>}
                            {formDeta.regdate && <li><b>Reg.Date:</b> {formDeta.regdate}</li>}
                            {formDeta.exDate && <li><b>Validated:</b> {formDeta.exDate}</li>}
                            {formDeta.StudentSession && <li><b>Session:</b> {formDeta.StudentSession}</li>}
                            {formDeta.StudentClassPlan && <li><b>Class:</b> {formDeta.StudentClassPlan}</li>}
                        </ul>

                    </div>
                </div>
            </div>
            <div className='Spratorline'></div>
            <div className='voucher'>
                <div className='flex mb30'>
                    <div className='logo'>
                        <img src={process.env.PUBLIC_URL + '/logo.png'} alt="logo here" />
                    </div>
                    <div className='heading_titile'>
                        <h4>National Grammar School</h4>
                        <h5>Registraion Slip</h5>
                    </div>
                    <div className='reg_picture'>
                        {photoSrc && <img src={photoSrc} alt="logo here" />}
                    </div>
                </div>
                <div className='flex align-starts'>
                    <div className='column'>
                        <h6>Student information</h6>
                        <ul>
                            {formDeta.StudentFirstName && <li><b>Name:</b> {formDeta.StudentFirstName} {formDeta.StudentLastName}</li>}
                            {formDeta.StudentGender && <li><b>Gender:</b> {formDeta.StudentGender}</li>}
                            {formDeta.StudentBForm && <li><b>B-Form:</b> {formDeta.StudentBForm}</li>}
                        </ul>

                    </div>
                    <div className='column'>
                        <h6>Parent/Guardian information</h6>
                        <ul>
                            {formDeta.GuardianFirstName && <li><b>Name:</b> {formDeta.GuardianFirstName}</li>}
                            {formDeta.GuardianCNIC && <li><b>CNIC:</b> {formDeta.GuardianCNIC}</li>}
                            {formDeta.GuardianPhone && <li><b>Contact:</b> {formDeta.GuardianPhone}</li>}
                            {formDeta.GuardianEmail && <li><b>E-mail:</b> {formDeta.GuardianEmail}</li>}
                            {formDeta.GuardianAddress && <li><b>Address:</b> {formDeta.GuardianAddress}</li>}
                        </ul>
                    </div>
                    <div className='column'>
                        <h6>School information</h6>
                        <ul>
                            {formDeta.regno && <li><b>Reg:</b> {formDeta.regno}</li>}
                            {formDeta.regdate && <li><b>Reg.Date:</b> {formDeta.regdate}</li>}
                            {formDeta.exDate && <li><b>Validated:</b> {formDeta.exDate}</li>}
                            {formDeta.StudentSession && <li><b>Session:</b> {formDeta.StudentSession}</li>}
                            {formDeta.StudentClassPlan && <li><b>Class:</b> {formDeta.StudentClassPlan}</li>}
                        </ul>
                    </div>
                </div>

                {/* <button onClick={printDiv()} className="btn-normal">Print page</button> */}

            </div>
        </>
    );
}

export default PrintVoucher;