import { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'
// import { Provider } from 'react-redux'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify'
// import createSagaMiddleware from 'redux-saga'
// import { configureStore } from '@reduxjs/toolkit'
// import ngsreducers from './redux/Slice'
// import mySaga from './redux/sagas'
import App from './App'
import './styles/typography.css'
import StateContext from './ContextApi/StateContext'
import { AuthProvider } from './ContextApi/AuthProvider';

// const saga = createSagaMiddleware()
// const store = configureStore({
//   reducer: {
//     ngs: ngsreducers,
//   },
//   middleware: [saga],
// })
// saga.run(mySaga)

const rootElement = document.getElementById('root')
const root = createRoot(rootElement)

root.render(
  <StrictMode>
    <BrowserRouter>
    <AuthProvider>
    <StateContext>
      <ToastContainer />
        <Routes>
          <Route path="/*" element={<App />} />
        </Routes>
        </StateContext>
        </AuthProvider>
    </BrowserRouter>
  </StrictMode>
)
